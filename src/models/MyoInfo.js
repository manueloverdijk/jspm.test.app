/**
 * Created by manuel on 01-05-15.
 */
import PrioritisedArray     from 'arva-ds/core/Model/prioritisedArray';
import Model          from 'arva-ds/core/Model';


export class Info extends Model {

    get name() { }
    get fromUser(){ }
}

export class ChatMessages extends PrioritisedArray
{
    constructor(datasource = null, datasnapshot = null) {
        super(ChatMessage, datasource, datasnapshot);
    }
}