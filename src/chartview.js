/**
 * Created by manuel on 29-04-15.
 */


//import Myo from 'myojs'

export default class ChartView {
    constructor(){

        this.dpsX = [];
        this.dpsY = [];
        this.dpsZ = [];

        this.dpsXTime = 0;
        this.dpsYTime = 0;
        this.dpsZTime = 0;

        this.createGraph();

        var hub = new Myo.Hub();

        hub.on('ready', function() {
            console.log("ready");
        });
        hub.on('connect', function() {
            console.log("connected");

        });

        let time = 0;
        let total = 0;
        hub.on('frame', function(frame){
            console.log(frame);

            if(frame.gyro.valid) {
                time++;

                this.updateGraphX(time,frame.gyro.x);
                this.updateGraphY(time,frame.gyro.y);
                this.updateGraphZ(time,frame.gyro.z);

            }
        }.bind(this));

    }

    updateGraphX(time,X){
        this.dpsXTime++;
        if (this.dpsX.length > 100)
        {
            this.dpsX.shift();
        }

        this.dpsX.push({x: time,y: X});

        if(this.dpsXTime > 10){
            this.chartX.render();
            this.dpsXTime = 0;
        }

    }

    updateGraphZ(time,Z){
        this.dpsZTime++;
        if (this.dpsZ.length > 100)
        {
            this.dpsZ.shift();
        }

        this.dpsZ.push({x: time,y: Z});

        if(this.dpsZTime > 10){
            this.chartZ.render();
            this.dpsZTime = 0;
        }
    }

    updateGraphY(time,Y){
        this.dpsYTime++;
        if (this.dpsY.length > 100)
        {
            this.dpsY.shift();
        }

        this.dpsY.push({x: time,y: Y});

        if(this.dpsYTime > 10){
            this.chartY.render();
            this.dpsYTime = 0;
        }
    }

    createGraph(){
        this.chartX = new CanvasJS.Chart("chartContainerX", {
            title:{
                text: "GyroScope X"
            },
            axisX: {
                title: "Time"
            },
            axisY: {
                title: "Units",
                minimum: -1000,
                maximum: 1000
            },
            zoomEnabled: false,
            data: [{
                type: "line",
                dataPoints : this.dpsX
            }]
        });

        this.chartY = new CanvasJS.Chart("chartContainerY", {
            title:{
                text: "GyroScope Y"
            },
            axisX: {
                title: "Time"
            },
            axisY: {
                title: "Units",
                minimum: -1000,
                maximum: 1000
            },
            data: [{
                type: "line",
                dataPoints : this.dpsY
            }]
        });

        this.chartZ = new CanvasJS.Chart("chartContainerZ", {
            title:{
                text: "GyroScope Z"
            },
            axisX: {
                title: "Time"
            },
            axisY: {
                title: "Units",
                minimum: -1000,
                maximum: 1000
            },
            data: [{
                type: "line",
                dataPoints : this.dpsZ
            }]
        });
    }
}
