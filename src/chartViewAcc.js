/**
 * Created by manuel on 29-04-15.
 */

/**
 * Created by manuel on 29-04-15.
 */


//import Myo from 'myojs'

export default class ChartViewAcc {
    constructor(){

        this.dpsX = [];
        this.dpsY = [];
        this.dpsZ = [];

        this.dpsXTime = 0;
        this.dpsYTime = 0;
        this.dpsZTime = 0;

        this.createGraph();

        var hub = new Myo.Hub();

        hub.on('ready', function() {
            console.log("ready");
        });
        hub.on('connect', function() {
            console.log("connected");

        });

        let time = 0;
        let total = 0;
        var last_x = 0, last_y = 0, last_z = 0;
        var lastUpdate = 0;
        hub.on('frame', function(frame){
            if(frame.accel.valid) {

                let x = frame.accel.x;
                let y = frame.accel.y;
                let z = frame.accel.z;


                let date = new Date();
                let currentTime  = date.getTime();

                if ((currentTime - lastUpdate) > 500) {

                    let diffTime = (currentTime - lastUpdate);
                    lastUpdate = currentTime;

                    // update happen way to fast, lets take a subsample and detect a shake gesture
                    let speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

                    let speedX = (x - last_x) / diffTime *10000;
                    console.log('speedX = ', speedX)
                    //console.log('speed = ',speed);
                    if (speed > 200) {
                        console.log('shake happend!');
                        //alert('shake happend!!');
                    }

                    last_x = x;
                    last_y = y;
                    last_z = z;
                }

            }
            //console.log(frame);
            //
            if(frame.accel.valid) {
                time++;

                this.updateGraphX(time,frame.accel.x);
                this.updateGraphY(time,frame.accel.y);
                this.updateGraphZ(time,frame.accel.z);

            }
        }.bind(this));

    }

    updateGraphX(time,X){
        this.dpsXTime++;
        if (this.dpsX.length > 100)
        {
            this.dpsX.shift();
        }

        this.dpsX.push({x: time,y: X});

        if(this.dpsXTime > 10){
            this.chartX.render();
            this.dpsXTime = 0;
        }

    }

    updateGraphZ(time,Z){
        this.dpsZTime++;
        if (this.dpsZ.length > 100)
        {
            this.dpsZ.shift();
        }

        this.dpsZ.push({x: time,y: Z});

        if(this.dpsZTime > 10){
            this.chartZ.render();
            this.dpsZTime = 0;
        }
    }

    updateGraphY(time,Y){
        this.dpsYTime++;
        if (this.dpsY.length > 100)
        {
            this.dpsY.shift();
        }

        this.dpsY.push({x: time,y: Y});

        if(this.dpsYTime > 10){
            this.chartY.render();
            this.dpsYTime = 0;
        }
    }

    createGraph(){
        this.chartX = new CanvasJS.Chart("chartContainerX", {
            title:{
                text: "Accelerometer X"
            },
            axisX: {
                title: "Time"
            },
            axisY: {
                title: "Units",
                minimum: -10,
                maximum: 10
            },
            zoomEnabled: false,
            data: [{
                type: "line",
                dataPoints : this.dpsX
            }]
        });

        this.chartY = new CanvasJS.Chart("chartContainerY", {
            title:{
                text: "Accelerometer Y"
            },
            axisX: {
                title: "Time"
            },
            axisY: {
                title: "Units",
                minimum: -10,
                maximum: 10
            },
            data: [{
                type: "line",
                dataPoints : this.dpsY
            }]
        });

        this.chartZ = new CanvasJS.Chart("chartContainerZ", {
            title:{
                text: "Accelerometer Z"
            },
            axisX: {
                title: "Time"
            },
            axisY: {
                title: "Units",
                minimum: -10,
                maximum: 10
            },
            data: [{
                type: "line",
                dataPoints : this.dpsZ
            }]
        });
    }
}
