/**
 * Created by mysim1 on 23/03/15.
 */

import Engine           from 'famous/core/Engine';
import RenderController from 'famous/views/RenderController';

/**
 * Container for the Famous Context class. Since Angular di.js has trouble with class initializers from
 * methods. The constructor will return a correctly loaded Context object.
 */
export default class FamousContext {
    constructor() {

        //return Engine.createContext();
        var context = Engine.createContext();
        return context;
        //var renderController = new RenderController();
        //context.add(renderController);
        //return renderController;
    }
}