/**
 * Created by tom on 23/03/15.
 */

import _ from 'lodash'
import Surface from 'famous/core/Surface'
import Context from 'famous/core/Context'
import Modifier from 'famous/core/Modifier'


export default
class AutoSurface extends Surface {

    /** Override this in subclasses. Represents the default options passed to the famo.us Surface */
    get defaultOptions() {
        return {};
    }

    get context() {
        return this._context;
    }

    get modifierChain() {
        return this._modifierChain;
    }

    get topLevelModifier() {
        return this._topLevelModifier;
    }

    /**
     * Constructs an AutoSurface, which is a wrapper around a Surface, a ModifierChain, a render context,
     * and a modifier between the context and the surface (topLevelModifier).
     * @param {Context} context The render context to render to
     * @param {Object} options Optional: An object of options, to be sent to the Surface constructor
     * @param {Object} modifierOptions Optional: If set, creates a modifier with these options between the
     *                                           render context and the surface.
     */
    constructor(context, options = null, modifierOptions = null) {
        /* Extend default famo.us Surface options with the given options */
        //options = _.merge(this.defaultOptions, options);
        super(options);


        /* Add current AutoSurface to the given rendering context, if any are present */
        if (context) {
            let contextBranch = context;

            /* If we have been given top level modifier options in the constructor,
             * use them to add a new modifier to the context before our Surface.
             */
            if (modifierOptions) {
                this._topLevelModifier = new Modifier(modifierOptions);
                contextBranch = context.add(this._topLevelModifier);
            }

            /* If we have any modifiers in our modifierChain, add them to the context before our Surface */
            if (this.modifierChain) {
                contextBranch = contextBranch.add(this.modifierChain);
            }

            /* Finally, add our Surface to the context */
            contextBranch.add(this);
            this._context = contextBranch;
        }
    }

}