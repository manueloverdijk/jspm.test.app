/**
 * Created by manuel on 01-05-15.
 */


import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController';
import {InfoView}                   from './InfoView';
import CubeView                     from './CubeView';
import MapView                      from './MapView';

export class ContentView extends View {
    constructor() {

        super({
            classes: ['view', 'profile'],
            imageSize: [200, 200],
            imageScale: [1, 1, 1],
            nameHeight: 60,
            profileText: 'Scarlett Johansson was born in New York City. Her mother, Melanie Sloan, is from an Ashkenazi Jewish family, and her father, Karsten Johansson, is Danish. Scarlett showed a passion for acting at a young age and starred in many plays.<br><br>She has a sister named Vanessa Johansson, a brother named Adrian, and a twin brother named Hunter Johansson born three minutes after her. She began her acting career starring as Laura Nelson in the comedy film North (1994).<br><br>The acclaimed drama film The Horse Whisperer (1998) brought Johansson critical praise and worldwide recognition. Following the film\'s success, she starred in many other films including the critically acclaimed cult film Ghost World (2001) and then the hit Lost in Translation (2003) with Bill Murray in which she again stunned critics. Later on, she appeared in the drama film Girl with a Pearl Earring (2003).'
        });

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        this._createRenderables();
        this._createLayout();
    }

    _createRenderables() {
        this._renderables = {
            infoView: new InfoView('black'),
            cubeView: new CubeView('#00518B'),
            mapView: new MapView('#00518B')
        };
    }

    _createLayout() {
        this.layout = new LayoutController({
            autoPipeEvents: true,
            layout: function(context, options) {
                context.set('infoView', {
                    size: [context.size[0]/2 - 20, context.size[1]-80],
                    translate: [10,40,1]
                });
                context.set('cubeView', {
                    size: [context.size[0]/2 - 20, context.size[1]/2 - 60],
                    translate: [context.size[0]/2+10, 40, 1]
                });
                context.set('mapView', {
                    size: [context.size[0]/2 - 20, context.size[1]/2 - 40],
                    translate: [context.size[0]/2+10, context.size[1]/2, 1]
                });
            }.bind(this),
            dataSource: this._renderables
        });

        this.add(this.layout);
        this.layout.pipe(this._eventOutput);
    }
}