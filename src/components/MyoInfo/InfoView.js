/**
 * Created by manuel on 01-05-15.
 */


import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController'

export class InfoView extends View {
        constructor(color){
            super()

            this._createRenderables(color);
            this._createLayout();
        }

    _createRenderables(color) {
        this._renderables = {
            background: new Surface({
                properties: {
                    'background-color': 'black'
                }
            }),
            name: new Surface({
                content: "Name:",
                properties: {
                    "color":"white"
                }
            }),
            ready: new Surface({
                content: "Ready: false",
                properties: {
                    "color":"white"
                }
            }),
            connected: new Surface({
                content: "Connected: false",
                properties: {
                    "color":"white"
                }
            }),
            pose: new Surface({
                content: "Pose:",
                properties: {
                    "color":"white"
                }
            }),
            orientation: new Surface({
                content: "Orientation:",
                properties: {
                    "color":"white"
                }
            }),

        };
    }

    _createLayout() {
        this.layout = new LayoutController({
            autoPipeEvents: true,
            layout: function(context, options) {
                context.set('background', {
                    size: [context.size[0],context.size[1]]
                }),
                context.set('name', {
                    size: [undefined, true],
                    translate: [20,20,1]
                });

                context.set('connected', {
                    size: [undefined, true],
                    translate: [20,60,1]
                });
                context.set('ready', {
                    size: [undefined, true],
                    translate: [20,100,1]
                });
                context.set('pose', {
                    size: [undefined, true],
                    translate: [20,140,1]
                });
                context.set('orientation', {
                    size: [undefined, true],
                    translate: [20,180,1]
                });
            }.bind(this),
            dataSource: this._renderables
        });

        this.add(this.layout);
        this.layout.pipe(this._eventOutput);
    }
}