/**
 * Created by manuel on 01-05-15.
 */

import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController'
import NavBarLayout                 from 'famous-flex/src/layouts/NavBarLayout'

export class HeaderView extends View {
    constructor() {

        super({
            classes: ['view', 'profile'],
            imageSize: [200, 200],
            imageScale: [1, 1, 1],
            nameHeight: 60
        });

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');
        //
        //this._createRenderables();
        this._createLayout();

    }

    _createLayout(){
        this.layout = new LayoutController({
            layout: NavBarLayout,
            layoutOptions: {
                margins: [20, 20, 0, 20], // margins to utilize
                itemSpacer: 20,    // space in between items
            },
            dataSource: {
                background: new Surface({properties: {backgroundColor: '#00518B'}}),
                title: new Surface({content: 'My title', size: [true, true]}),
                leftItems:[
                    new Surface({
                        content: 'left1',
                        size: [100, undefined], // use fixed width,
                        properties: {
                            'color':'white'
                        }
                    })
                ],
                rightItems: [
                    new Surface({
                        content: 'right1',
                        size: [true, undefined],
                        properties: {
                            'color':'white'
                        }// use actual width of DOM-node
                    }),
                    new Surface({
                        content: 'right2',
                        size: [true, undefined],
                        properties: {
                            'color':'white'
                        }// use actual width of DOM-node
                    })
                ]
            }
        });

        this.add(this.layout);
        this.layout.pipe(this._eventOutput);

    }
}