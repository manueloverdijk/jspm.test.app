/**
 * Created by manuel on 01-05-15.
 */



import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController'
import CubeSurface                  from '../demo/cubeSurface'

export default class CubeView extends View {
    constructor(color) {
        super()

        this.add(new Surface({}))

        this.cubeSurface = new CubeSurface(this);

    }
    setQuaternion(w, x, y, z){
        this.cubeSurface.setQuaternion(w,x,y,z);
    }
}