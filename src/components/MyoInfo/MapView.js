/**
 * Created by manuel on 13-05-15.
 */
/**
 * Created by manuel on 01-05-15.
 */



import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController'
import MapView                      from 'famous-map/MapView';

export default class Mapview extends View {
    constructor(color) {
        super()

        this.pitchOffset = false;
        this.yawOffset = false;
        this.active = true;
        this.mode = true;


        mapboxgl.accessToken = 'pk.eyJ1IjoiaWp6ZXJlbmhlaW4iLCJhIjoiVFh2bFZRQSJ9.hfo54hMcdhbsChqF7Qtq_g';

        this.mapView = new MapView({
            type: MapView.MapType.MAPBOXGL,
            mapOptions: {
                zoom: 5,
                center: {lat: 51.4484855, lng: 5.451478},
                style: 'https://www.mapbox.com/mapbox-gl-styles/styles/outdoors-v7.json' //stylesheet location
            }
        });


        this.add(this.mapView);

        // Wait for the map to load and initialize
        this.mapView.on('load', function () {

             //Move across the globe and zoom-in when done
            this.mapView.setPosition(
                {lat: 52.38502983, lng: 4.90282059},
                { duration: 5000 },
                function () {
                    this.mapView.getMap().setZoom(15);
                }.bind(this)
            );
        }.bind(this));

    }

    controlZoom(data){
        if(this.active && this.mode === false){
            this.eulerAngles = data;

            if(this.pitchOffset){

                let ySpeed = (this.eulerAngles.pitch - this.pitchOffset);

                if(Math.abs(ySpeed) > 0.25) {
                    let zoomLevel = this.mapView.getZoom();

                    this.mapView.getMap().setZoom(zoomLevel + (ySpeed/5));
                }
            } else {
                this.pitchOffset = this.eulerAngles.pitch;
                this.yawOffset = this.eulerAngles.yaw;
            }
        }

    }

    controlDraggable(data){
        if(this.active && this.mode === true){
            this.eulerAngles = data;

            if(this.pitchOffset && this.yawOffset){

                let xSpeed = (this.eulerAngles.yaw - this.yawOffset);
                let ySpeed = (this.eulerAngles.pitch - this.pitchOffset);

                if(Math.abs(xSpeed) > 0.25 || Math.abs(ySpeed) > 0.25) {
                    let position = this.mapView.getPosition();
                    let zoomLevel = this.mapView.getZoom();

                    let newLat = position.lat - (ySpeed / (zoomLevel * 150));
                    let newLng = position.lng - (xSpeed / (zoomLevel * 150));

                    this.mapView.setPosition({
                        lat: newLat,
                        lng: newLng,
                    });
                }
            } else {
                this.pitchOffset = this.eulerAngles.pitch;
                this.yawOffset = this.eulerAngles.yaw;
            }
        }

    }

    setActive(){
        if(this.active === true){
            this.active = false;
            return;
        }
        this.active = true;
    }

    setMode(){
        console.log('MapView set mode');
        if(this.mode === true){
            this.mode = false;
            return;
        }
        this.mode = true;
    }

}
