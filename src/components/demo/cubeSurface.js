import _ from 'lodash'
import AutoSurface from '../autoSurface'
import Modifier from 'famous/core/Modifier'
import Transform from 'famous/core/Transform'
import CubePlaneSurface from './cubePlaneSurface'
import ModifierChain from 'famous/modifiers/ModifierChain'
import TransitionableTransform from 'famous/transitions/TransitionableTransform'
import ObjectHelper from '../../utils/ObjectHelper'
import Quaternion from 'famous/math/Quaternion'

export default
class CubeSurface extends AutoSurface {

    get defaultOptions() {
        return {
            size: [100, 100],
            properties: {
                backgroundColor: 'black',
                opacity: 0.20
            }
        };
    }

    constructor(context, options, modifierOptions) {
        let ourModifierOptions = {origin: [0.5, 0.5], align: [0.5, 0.5]};
        super(context, options, _.extend(ourModifierOptions, modifierOptions));
        let mod = new Modifier();
        //this._transitionable = new TransitionableTransform();
        //mod.transformFrom(this._transitionable.get.bind(this._transitionable));
        //
        this._modifierChain = new ModifierChain();
        this._modifierChain.addModifier(mod);


        //let ourModifierOptions = {};



        let size = [100,100];

        let planes = [
            /* Top */
            new CubePlaneSurface(this.context, {size: size}, null, 'top'),
            new CubePlaneSurface(this.context, {size: size}, null, 'bottom'),
            new CubePlaneSurface(this.context, {size: size}, null, 'right'),
            new CubePlaneSurface(this.context, {size: size}, null, 'left'),
            new CubePlaneSurface(this.context, {size: size}, null, 'front'),
            new CubePlaneSurface(this.context, {size: size}, null, 'back')
        ];

        ObjectHelper.bindAllMethods(this, this);

        this.quaternion = new Quaternion(1,1,1,1);

        mod.transformFrom(function() {
            return this.quaternion.getTransform();
        }.bind(this));
    }

    setQuaternion(w, x, y, z){
        this.quaternion = new Quaternion(w, x, y, z);
    }

    animate2(quaternion){
        this._transitionable.setTransform(quaternion.getTransform());
    }

    animate(roll, pitch, yaw){
        //if(!this._cycle){
        //    this._cycle = 1;
        //}

        //this._transitionable.setRotate([roll, pitch, yaw],{duration: 50} this.animate);
        //let rotation = this._cycle++ * 2 * Math.PI;
        this._transitionable.setRotate([roll, pitch, yaw]);
    }

}