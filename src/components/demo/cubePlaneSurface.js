/**
 * Created by tom on 23/03/15.
 */

import _ from 'lodash'
import AutoSurface from '../autoSurface'
import Modifier from 'famous/core/Modifier'
import Transform from 'famous/core/Transform'
import ModifierChain from 'famous/modifiers/ModifierChain'

export default
class CubePlaneSurface extends AutoSurface {

    get defaultOptions() {
        return {
            size: [100, 100],
            properties: {
                'backface-visibility': 'visible'
            }
        };
    }

    constructor(context, options, modifierOptions, orientation = 'top') {

        let rotation = [];
        let translation = [];
        let colour;

        let size = _.extend({
            size: [100, 100],
            properties: {
                'backface-visibility': 'visible'
            }
        }, options).size;
        let halfSizeX = size[0] / 2;
        let halfSizeY = size[1] / 2;

        switch (orientation) {
            case 'top':
                rotation = [90 * (Math.PI / 180), 0, 0];
                translation = [0, -halfSizeY, 0];
                colour = 'red';
                break;
            case 'bottom':
                rotation = [90 * (Math.PI / 180), 0, 0];
                translation = [0, halfSizeY, 0];
                colour = 'green';
                break;
            case 'right':
                rotation = [0, 90 * (Math.PI / 180), 0];
                translation = [halfSizeX, 0, 0];
                colour = 'blue';
                break;
            case 'left':
                rotation = [0, 90 * (Math.PI / 180), 0];
                translation = [-halfSizeX, 0, 0];
                colour = 'yellow';
                break;
            case 'front':
                rotation = [0, 0, 0];
                translation = [0, 0, halfSizeX];
                colour = 'orange';
                break;
            case 'back':
                rotation = [0, 0, 0];
                translation = [0, 0, -halfSizeX];
                colour = 'cyan';
                break;
        }

        let ourSurfaceOptions = {properties: {backgroundColor: colour}};
        let ourModifierOptions = {};//{origin: [0, 0], align: [0.5, 0.5]};
        super(context, _.merge(ourSurfaceOptions, options), _.extend(ourModifierOptions, modifierOptions));

        this._modifierChain = new ModifierChain();
        this._modifierChain.addModifier(new Modifier({
            transform: Transform.multiply4x4(
                Transform.translate(...translation),
                Transform.rotate(...rotation))
        }));


    }

}