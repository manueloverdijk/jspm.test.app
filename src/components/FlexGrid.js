/**
 * Created by mysim1 on 31/03/15.
 */

import Engine from 'famous/core/Engine';
import Surface from 'famous/core/Surface';
import View from 'famous/core/View';
import Entity from 'famous/core/Entity';
import Modifier from 'famous/core/Modifier';
import Transform from 'famous/core/Transform';
import Transitionable from 'famous/transitions/Transitionable';
import TransitionableTransform from 'famous/transitions/TransitionableTransform';
import Easing from 'famous/transitions/Easing';
import Scrollview from 'famous/views/Scrollview';



export class FlexGrid extends View {

    get DEFAULT_OPTIONS() {
        return {
            marginTop: undefined,
            marginSide: undefined,
            gutterCol: undefined,
            gutterRow: undefined,
            itemSize: undefined,
            transition: {curve: Easing.outBack, duration: 500}
        };
    }


    constructor(options) {
        super(options);

        this._modifiers = [];
        this._states = [];
        this._height = 0;

        this.id = Entity.register(this);
    }

    sequenceFrom(items) {
        this._items = items;
    }

    render() {
        return this.id;
    }

    getSize() {
        if (!this._height) return;
        return [this._cachedWidth, this._height];
    }

    commit(context) {
        var width = context.size[0];

        var specs = [];

        if (this._cachedWidth !== width) {
            var spacing = this._calcSpacing.call(this, width);
            var size = this.options.itemSize;
            if (spacing.numCols < 2) {
                spacing.numCols = 1;
                spacing.marginSide = 0;
                size = [width, size[1]];
            }
            var positions = this._calcPositions.call(this, spacing);

            for (var i = 0; i < this._items.length; i++) {
                if (this._modifiers[i] === undefined) {
                    this._createModifier.call(this, i, positions[i], size);
                }
                else {
                    this._animateModifier.call(this, i, positions[i], size);
                }
            }

            this._cachedWidth = width;
        }

        for (var i = 0; i < this._modifiers.length; i++) {
            var spec = this._modifiers[i].modify({
                target: this._items[i].render()
            });

            specs.push(spec);
        }

        return specs;
    }


    _calcSpacing(width) {
        var itemWidth = this.options.itemSize[0];
        var gutterCol = this.options.gutterCol;
        var ySpacing = itemWidth + gutterCol;
        var margin = this.options.marginSide;
        var numCols = Math.floor((width - 2 * margin + gutterCol) / ySpacing);
        margin = (width - numCols * ySpacing + gutterCol) / 2;
        return {
            numCols: numCols,
            marginSide: margin,
            ySpacing: ySpacing
        }
    }

    _calcPositions(spacing) {
        var positions = [];

        var col = 0;
        var row = 0;
        var xPos;
        var yPos;

        for (var i = 0; i < this._items.length; i++) {
            xPos = spacing.marginSide + col * spacing.ySpacing;
            yPos = this.options.marginTop + row * (this.options.itemSize[1] + this.options.gutterRow);
            positions.push([xPos, yPos, 0]);

            col++
            if (col === spacing.numCols) {
                row++;
                col = 0;
            }
        }

        this._height = yPos + this.options.itemSize[1] + this.options.marginTop;

        return positions;
    }

    _createModifier(index, position, size) {
        var transitionItem = {
            transform: new TransitionableTransform(Transform.translate.apply(null, position)),
            size: new Transitionable((size || this.options.itemSize))
        }

        var modifier = new Modifier(transitionItem);

        this._states[index] = transitionItem;
        this._modifiers[index] = modifier;
    }

    _animateModifier(index, position, size) {
        var transformTransitionable = this._states[index].transform;
        var sizeTransitionable = this._states[index].size;
        transformTransitionable.halt();
        sizeTransitionable.halt();
        transformTransitionable.setTranslate(position, this.options.transition);
        sizeTransitionable.set(size, this.options.transition);
    }
}