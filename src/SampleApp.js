/**
 * Created by manuel on 30-04-15.
 */

import {Inject}                 from 'di.js';
import {App}                    from 'arva-mvc/core/App';
import Router                   from 'arva-mvc/core/Router';
import Context                  from 'famous/core/Context';
import HomeController           from './controllers/HomeController'
import InfoController           from './controllers/InfoController'
import DemoController           from './controllers/DemoController'

@Inject(Router, Context,DemoController)
export class SampleApp extends App {

    constructor(router, context) {
        router.setDefault(DemoController, 'Index');
        super(router);
    }
}




