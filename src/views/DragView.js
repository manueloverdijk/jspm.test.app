/**
 * Created by manuel on 15-05-15.
 */

/**
 * Created by manuel on 01-05-15.
 */


import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController';
import HeaderFooterLayout           from 'famous-flex/src/layouts/HeaderFooterLayout';
import Modifier                     from 'famous/core/Modifier';
import Transform                    from 'famous/core/Transform';
import Draggable                    from 'famous/modifiers/Draggable';
import StateModifier                from 'famous/modifiers/StateModifier';
import MouseSync                    from 'famous/inputs/MouseSync';
import Transitionable               from 'famous/transitions/Transitionable';
//import {HeaderView}                   from '../components/MyoInfo/HeaderView';
//import {ContentView}                  from '../components/MyoInfo/ContentView';
//import {FooterView}                  from '../components/MyoInfo/FooterView';

export class DragView extends View{
    constructor(){

        super({
            classes: ['drag', 'view']
        });

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        this._createLayout();

    }

    _createLayout(){
        var position = new Transitionable([0, 0]);

        var mouseSync = new MouseSync();
        var backgroundSurface = new Surface({
            size: [undefined, undefined],
            classes: ['grey-bg'],
            properties: {
                'background-color':'white'
            }
        });


        var surfaceArray = [];
        this.draggableArray = [];

        for(let x = 0; x < 5; x++){

            surfaceArray.push(new Surface({
                size: [200, 200],
                properties: {
                    background: 'rgb('+(x*50)+','+(x*50)+','+(x*50)+')',
                    textAlign: 'center',
                }
            }));
            let draggable = new Draggable();
            draggable.subscribe(surfaceArray[x]);
            this.draggableArray.push(draggable);

            // drag on end, go to certain position
            //draggable.on('end', function(e) {
            //    draggable.setPosition([0,0,0]);
            //});
        }

        var centerModifier = new Modifier({origin : [0.5, 0.5], align:[0,0]});

        //Dynamic positioning modifiers
        var panModifier = new StateModifier({
            transform: function(){
                var currentPosition = position.get();
                return Transform.translate(currentPosition[0], currentPosition[1]);
            }
        })

        //Panning listener
        this.pipe(mouseSync);

        mouseSync.on("update", function(data){
            var currentPosition = position.get();
            position.set([
                currentPosition[0] + data.delta[0],
                currentPosition[1] + data.delta[1]
            ]);
        });

        this.add(new Modifier({
                    transform: Transform.translate(100, 100, 1)
                })).add(this.draggableArray[0]).add(surfaceArray[0]);

        //this.add(new Modifier({
        //    transform: Transform.translate(200, 200, 1)
        //})).add(draggableArray[1]).add(surfaceArray[1]);
        //
        //this.add(new Modifier({
        //    transform: Transform.translate(300, 300, 1)
        //})).add(draggableArray[2]).add(surfaceArray[2]);
        //
        //this.add(new Modifier({
        //    transform: Transform.translate(400, 400, 1)
        //})).add(draggableArray[3]).add(surfaceArray[3]);
        //
        //this.add(new Modifier({
        //    transform: Transform.translate(500, 500, 1)
        //})).add(draggableArray[4]).add(surfaceArray[4]);
        //



        //console.log('hello!');
        //
        //setTimeout(function(){
        //    debugger;
        //    var currentPosition = position.get();
        //    position.set([
        //        currentPosition[0] + 20,
        //        currentPosition[1] + 20
        //    ]);
        //}.bind(this), 2000);

        this.add(backgroundSurface);

    }

    setEulerAngles(data){

        //console.log(data);
        this.eulerAngles = data;

        if(this.pitchOffset && this.yawOffset){

            let xSpeed = (this.eulerAngles.yaw - this.yawOffset);
            let ySpeed = (this.eulerAngles.pitch - this.pitchOffset);

            if(Math.abs(xSpeed) > 0.25 || Math.abs(ySpeed) > 0.25) {
                this.draggableArray[0].setRelativePosition([xSpeed*-7.5,ySpeed*-7.5]);
            }
        } else {
            this.pitchOffset = this.eulerAngles.pitch;
            this.yawOffset = this.eulerAngles.yaw;
        }

        //this.draggableArray[0].setRelativePosition([this.eulerAngles.pitch,this.eulerAngles.yaw]);

    }
}