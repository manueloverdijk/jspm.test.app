/**
 * Created by manuel on 15-05-15.
 */

import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import ImageSurface                 from 'famous/surfaces/ImageSurface';
import LayoutController             from 'famous-flex/src/LayoutController';
import ScrollController             from 'famous-flex/src/ScrollController';
import FlexScrollView               from 'famous-flex/src/FlexScrollView';
import WheelLayout                  from 'famous-flex/src/layouts/WheelLayout';
import HeaderFooterLayout           from 'famous-flex/src/layouts/HeaderFooterLayout';
import DatePicker                   from 'famous-flex/src/widgets/DatePicker';
import Modifier                     from 'famous/core/Modifier';
import Transform                    from 'famous/core/Transform';
import Draggable                    from 'famous/modifiers/Draggable';
import StateModifier                from 'famous/modifiers/StateModifier';
import MouseSync                    from 'famous/inputs/MouseSync';
import Transitionable               from 'famous/transitions/Transitionable';

export class TransferListView extends View {
    constructor() {
        super({
            classes: ['transfer', 'view']
        });

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        this._createRenderables();
        this._createLayout();

    }

    _createLayout() {

        var renderables = this._renderables;

        this.layout = new FlexScrollView({
            layoutOptions: {
                itemSize: 200,
                diameter: 600,
                radialOpacity: -0.2
            },
            layout: WheelLayout,
            dataSource: renderables,
            autoPipeEvents: true,
            paginated: true,
            direction: 1,
            mouseMove: true,
            layoutAll: true,
            paginationMode: FlexScrollView.PaginationMode.SCROLL,
            flow: true,             // enable flow-mode (can only be enabled from the constructor)
            flowOptions: {
                spring: {               // spring-options used when transitioning between states
                    dampingRatio: 0.8,  // spring damping ratio
                    period: 1000        // duration of the animation
                },
                insertSpec: {           // render-spec used when inserting renderables
                    opacity: 0,         // start opacity is 0, causing a fade-in effect,
                    size: [0, 0]     // uncommented to create a grow-effect
                    //transform: Transform.translate(-300, 0, 0) // uncomment for slide-in effect
                }
            }
        });

        this.add(new Modifier({
            origin:[0.5,0.5],
            align:[0.5,0.5],
            transform: Transform.translate(40,0,1)
    })).add(this.layout);

    }

    controlListView(angles){

        if(angles.pitch){
            let pitch = angles.pitch;
            if(pitch > 0.5 || pitch < -0.5){
                this.layout.scroll(-12.5*pitch);
            }

        }

    }

    _createSurface(url){
        return new Surface({
            size: [256, 200],
            properties: {
                'background': `url('${url}')`,
                'background-size': '256px 200px',
                'color': 'white',
                'padding': '8px',
            }
        });
    }

    _createRenderables(){

        this.photos = [
            'https://9to5mac.files.wordpress.com/2014/08/yosemite.jpg?w=256&h=200&crop=1',
            'http://hosting.edo-soft.com/stickynotes/stickynotes.png',
            'http://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Hoefler_Text_sample.svg/800px-Hoefler_Text_sample.svg.png',
            'http://xlsxwriter.readthedocs.org/en/latest/_images/chart_simple.png',
            'http://upload.wikimedia.org/wikipedia/commons/5/55/Composition_of_38th_Parliament.png'
        ];

        this._renderables = []
        for(let x = 0; x < 5; x++){
            this._renderables.push(this._createSurface(this.photos[x % 5]));
        }
    }
}


