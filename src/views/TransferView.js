
import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController';
import ScrollController             from 'famous-flex/src/ScrollController';
import FlexScrollView               from 'famous-flex/src/FlexScrollView';
import HeaderFooterLayout           from 'famous-flex/src/layouts/HeaderFooterLayout';
import Modifier                     from 'famous/core/Modifier';
import Transform                    from 'famous/core/Transform';
import Draggable                    from 'famous/modifiers/Draggable';
import StateModifier                from 'famous/modifiers/StateModifier';
import MouseSync                    from 'famous/inputs/MouseSync';
import Transitionable               from 'famous/transitions/Transitionable';
import _                            from 'lodash';

import {TransferListView}            from './TransferListView';
import {TransferDragView}            from './TransferDragView';

export class TransferView extends View {
    constructor() {
        super({
            classes: ['transfer', 'view']
        });

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        Engine.getContexts()[0].setPerspective(5000);
        this.eulerAngles = null;
        this.fistActive = false;
        this.spreadActive = false;
        this._createLayout();

    }

    _createLayout() {
        this.layout = new LayoutController({
            layout: function(context, options) {
            context.set('listview', {
                size: [context.size[0]/3,context.size[1]],
                origin: [0,0],
                align: [0,0]
            }),

            context.set('dragview', {
                size: [context.size[0]/3*2, context.size[1]],
                origin:[0,0],
                align: [0.33,0]
            });
        }.bind(this),
            dataSource: {
                listview: new TransferListView(),
                dragview: new TransferDragView()
            }
        });
        this.add(this.layout);
        this.layout.pipe(this._eventOutput);

        this._createListeners();
    }

    _createListeners(){
        this.layout.get('listview').layout.on('click', function(){
            console.log('click');
            let index =  this.layout.get('listview').layout.getCurrentIndex();
            let surface = this.layout.get('listview').layout.getDataSource()[index];

            let _properties = surface.properties;
            let _content = surface.content;
            let _size = surface.size;
            this.layout.get('dragview').insertSurface(new Surface({
                content: _content,
                properties: _properties,
                size: _size
            }));
        }.bind(this))
    }

    angles(data){
        this.eulerAngles = data;
        if(this.fistActive && this.spreadActive){
            this.layout.get('dragview').controlDraggable(this.eulerAngles);
        } else {
            this.layout.get('listview').controlListView(this.eulerAngles);
        }

    }

    setFist(){
        if(this.fistActive){
            this.fistActive = false;
        } else {
            this.fistActive = true;
        }

        let index =  this.layout.get('listview').layout.getCurrentIndex();
        let surface = this.layout.get('listview').layout.getDataSource()[index];

        let _properties = surface.properties;
        let _content = surface.content;
        let _size = surface.size;
        this.layout.get('dragview').insertSurface(new Surface({
            content: _content,
            properties: _properties,
            size: _size
        }));
    }

    setSpread(){
        if(this.spreadActive){
            this.spreadActive = false;
        } else {
            this.spreadActive = true;
        }
    }
}


