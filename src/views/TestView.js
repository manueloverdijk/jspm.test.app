/**
 * Created by manuel on 01-05-15.
 */


import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import FlexScrollView               from 'famous-flex/src/FlexScrollView';

export class TestView extends FlexScrollView{
    constructor(){

        super({
            autoPipeEvents: true
        })
        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        this._createRenderables();
    }

    _createRenderables() {
        this._renderables = [
            new Surface({
                size:[undefined,200],
                properties: {
                    "background-color":"black"
                }
                //classes: this.options.classes.concat(['background'])
            }),
            new Surface({
                size:[undefined,400],
                //classes: this.options.classes.concat(['name']),
                content: '<div>Scarlett Johansson</div>',
                properties: {
                    "background-color":"yellow"
                }
            }),
           new Surface({
                size: [undefined,400],
                //classes: this.options.classes.concat(['text']),
                content: 'Hello there',
               properties: {
                   "background-color":"cyan"
               }
            })
        ];
        this.sequenceFrom(this._renderables);
    }
}