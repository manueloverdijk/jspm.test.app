/**
 * Created by manuel on 01-05-15.
 */


import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController';
import HeaderFooterLayout           from 'famous-flex/src/layouts/HeaderFooterLayout';
import {HeaderView}                   from '../components/MyoInfo/HeaderView';
import {ContentView}                  from '../components/MyoInfo/ContentView';
import {FooterView}                  from '../components/MyoInfo/FooterView';

export class MyoView extends View{
    constructor(){

        super({
            classes: ['myo', 'info'],
            nameHeight: 60,
            headerText: 'Myo Info'
        });

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        this._createLayout();

    }
    _createLayout() {
        this.layout = new LayoutController({
            layout: HeaderFooterLayout,
            layoutOptions: {
                headerSize: 50,    // header has height of 60 pixels
                footerSize: 50     // footer has height of 20 pixels
            },
            dataSource: {
                header: new HeaderView(),
                content: new ContentView(),
                footer: new FooterView()
            }
        });

        this.add(this.layout);
        this.layout.pipe(this._eventOutput);
    }

    setActive(){
        //this.layout.get('content').layout.get('mapView').setActive();
    }

    setMode(){
        console.log('Set mode!');
        this.layout.get('content').layout.get('mapView').setMode();
    }

    setEulerAngles(data){
        this.layout.get('content').layout.get('mapView').controlDraggable(data);
        this.layout.get('content').layout.get('mapView').controlZoom(data);
    }

}