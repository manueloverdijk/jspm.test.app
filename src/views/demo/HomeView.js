/**
 * Created by manuel on 22-05-15.
 */



import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import ImageSurface                 from 'famous/surfaces/ImageSurface';
import LayoutController             from 'famous-flex/src/LayoutController';
import ScrollController             from 'famous-flex/src/ScrollController';
import FlexScrollView               from 'famous-flex/src/FlexScrollView';
import WheelLayout                  from 'famous-flex/src/layouts/WheelLayout';
import HeaderFooterLayout           from 'famous-flex/src/layouts/HeaderFooterLayout';
import DatePicker                   from 'famous-flex/src/widgets/DatePicker';
import Modifier                     from 'famous/core/Modifier';
import Transform                    from 'famous/core/Transform';
import Draggable                    from 'famous/modifiers/Draggable';
import StateModifier                from 'famous/modifiers/StateModifier';
import MouseSync                    from 'famous/inputs/MouseSync';
import Transitionable               from 'famous/transitions/Transitionable';


export default class HomeView extends View {
    constructor() {
        super({
            classes: ['home', 'view'],
            nameHeight: 60,
            headerText: 'HomeView'
        });
        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        this._createRenderables();
        this._createLayout();
    }

    _createLayout() {
        var renderables = this._renderables;

        this.layout = new FlexScrollView({
            useContainer: true,
            container: { // options passed to the ContainerSurface
                properties: {
                    'overflow':'hidden',
                    'border':'1px solid #ccc',
                    'border-radius':'25px',
                }
            },
            layoutOptions: {
                itemSize: 512,
                diameter: 600,
                radialOpacity: -0.1
            },
            layout: WheelLayout,
            dataSource: renderables,
            autoPipeEvents: true,
            paginated: true,
            direction: 1,
            mouseMove: true,
            layoutAll: true,
            paginationMode: FlexScrollView.PaginationMode.SCROLL,
            flow: true,             // enable flow-mode (can only be enabled from the constructor)
            flowOptions: {
                spring: {               // spring-options used when transitioning between states
                    dampingRatio: 0.8,  // spring damping ratio
                    period: 1000        // duration of the animation
                },
                insertSpec: {           // render-spec used when inserting renderables
                    opacity: 0,         // start opacity is 0, causing a fade-in effect,
                    size: [0, 0]     // uncommented to create a grow-effect
                    //transform: Transform.translate(-300, 0, 0) // uncomment for slide-in effect
                }
            }
        });

        this.add(new Modifier({
            size: [window.innerWidth/2, window.innerHeight-200],
            origin: [0.5, 0.5],
            align: [0.5, 0.5]
        })).add(this.layout);

    }

    angles(angles){

        if(angles.pitch){
            let pitch = -angles.pitch;
            if(pitch > 0.5 || pitch < -0.5){
                this.layout.scroll(-12.5*pitch);
            }

        }
    }

    _createRenderables(){
        this._renderables = [
            new Surface({
                content: '<img width="256" height="265" src="img/close.png"/><h2>Sluit App</h2>',
                size:[512, 512],
                classes: this.options.classes.concat(['background']),
                properties: {
                    'background-color':'transparent',
                    'color':'black',
                    'text-align':'center',
                    'padding':'50px'
                }
            }),
            new Surface({
                content: '<img width="256" height="265" src="img/map.png"/><h1>Map Demo</h1>',
                size:[512, 512],
                classes: this.options.classes.concat(['background']),
                properties: {
                    'background-color':'transparent',
                    'color':'black',
                    'text-align':'center',
                    'padding':'50px'
                }
            }),
            new Surface({
                content: '<img width="256" height="265" src="img/brainstorm.png"/><h1>Drag and Drop</h1>',
                size:[512, 512],
                classes: this.options.classes.concat(['background']),
                properties: {
                    'background-color':'transparent',
                    'color':'black',
                    'text-align':'center',
                    'padding':'50px'
                }
            }),
            new Surface({
                content: '<img width="256" height="265" src="img/brainstorm.png"/><h1>Presentation Demo</h1>',
                size:[512, 512],
                classes: this.options.classes.concat(['background']),
                properties: {
                    'background-color':'transparent',
                    'color':'black',
                    'text-align':'center',
                    'padding':'50px'
                }
            })
        ];

    }
}