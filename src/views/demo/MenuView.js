/**
 * Created by manuel on 22-05-15.
 */


import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import ImageSurface                 from 'famous/surfaces/ImageSurface';
import LayoutController             from 'famous-flex/src/LayoutController';
import ScrollController             from 'famous-flex/src/ScrollController';
import FlexScrollView               from 'famous-flex/src/FlexScrollView';
import WheelLayout                  from 'famous-flex/src/layouts/WheelLayout';
import HeaderFooterLayout           from 'famous-flex/src/layouts/HeaderFooterLayout';
import DatePicker                   from 'famous-flex/src/widgets/DatePicker';
import Modifier                     from 'famous/core/Modifier';
import Transform                    from 'famous/core/Transform';
import Draggable                    from 'famous/modifiers/Draggable';
import StateModifier                from 'famous/modifiers/StateModifier';
import MouseSync                    from 'famous/inputs/MouseSync';
import Transitionable               from 'famous/transitions/Transitionable';


export default class MenuView extends View {
    constructor() {
        super({
            classes: ['home', 'view'],
            nameHeight: 60,
            headerText: 'HomeView'
        });
        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        this._createRenderables();
        this._createLayout();
    }

    /**
     *
     * @private
     */
    _createRenderables() {
        this._renderables = {
            background: new Surface({
                properties: {
                    'border-bottom':'1px solid #ccc',
                    'border-right':'1px solid #ccc',
                    'border-left':'1px solid #ccc',
                    'background-color':'white'
                }
            }),
            title: new Surface({
                content: '<h2>Myo Interaction Research</h2>',
                properties: {
                    'text-align':'center',
                    'transition': 'opacity 0.5s'
                }
            }),
            status: new Surface({
                content: '<h3> Not connected </h3>',
                properties: {
                    'text-align':'center',
                    'transition': 'opacity 1s'
                }
            }),
            pose: new Surface({
                properties: {
                    'text-align':'center',
                    'transition': 'opacity 1s'
                }
            })
        };
    }

    /**
     *
     * @private
     */
    _createLayout() {
        this.layout = new LayoutController({
            autoPipeEvents: true,
            layout: function (context) {

                context.set('background', {
                    size: [context.size[0], 64],
                    translate: [0, 0, 99],
                    origin:[0.5,0],
                    align:[0.5,0]
                });
                context.set('title', {
                    size: [512, 64],
                    translate: [0, 32, 100],
                    origin:[0.5,0.5],
                    align:[0.5,0]
                });
                context.set('status', {
                    size: [200, 64],
                    translate: [0, 32, 100],
                    origin:[0,0.5],
                    align:[0,0]
                });
                context.set('pose', {
                    size: [200, 64],
                    translate: [0, 32, 100],
                    origin:[1,0.5],
                    align:[1,0]
                });


            }.bind(this),
            dataSource: this._renderables
        });
        this.add(this.layout);
        this.layout.pipe(this._eventOutput);
    }

    setPose(type){
        this.layout.get('pose').setContent(`<h3>${type}</h3>`);
    }

    setReady(boolean){
       if(boolean){
           this.layout.get('status').setContent('<h3>Ready</h3>');
       }
    }

    setConnected(boolean){
        if(boolean){
            this.layout.get('status').setContent('<h3>Connected</h3>');
        } else {
            this.layout.get('status').setContent('<h3>Not connected</h3>');
        }
    }
}

