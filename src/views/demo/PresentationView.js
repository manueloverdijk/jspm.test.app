/**
 * Created by manuel on 22-05-15.
 */

import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController';
import ScrollController             from 'famous-flex/src/ScrollController';



export default class PresentationView extends View{

    constructor(id){
        super();

        console.log('constructor id::', id);

        this.colors = [
            'black',
            'orange',
            'green',
            'cyan',
            'red',
            '#ccc'
        ];
        this.currentId = id;

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');


        this._createRenderables();
        this._createLayout();
    }

    _createRenderables(){
        this._renderables = {
            background: new Surface({
                properties: {
                    'background-color': this.colors[this.currentId % 6]
                }
            })
        }
    }

    _createLayout(){
        this.layout = new LayoutController({
            autoPipeEvents: true,
            layout: function (context) {
                context.set('background', {
                    size: [context.size[0], context.size[1]],
                    translate: [0, 0, 2],
                    origin:[0.5,0.5],
                    align:[0.5,0.5]
                });
            }.bind(this),
            dataSource: this._renderables
        });
        this.add(this.layout);
        this.layout.pipe(this._eventOutput);
    }
}