/**
 * Created by manuel on 15-05-15.
 */
import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController';
import HeaderFooterLayout           from 'famous-flex/src/layouts/HeaderFooterLayout';
import Modifier                     from 'famous/core/Modifier';
import Transform                    from 'famous/core/Transform';
import Draggable                    from 'famous/modifiers/Draggable';
import StateModifier                from 'famous/modifiers/StateModifier';
import MouseSync                    from 'famous/inputs/MouseSync';
import Transitionable               from 'famous/transitions/Transitionable';
//import {HeaderView}                   from '../components/MyoInfo/HeaderView';
//import {ContentView}                  from '../components/MyoInfo/ContentView';
//import {FooterView}                  from '../components/MyoInfo/FooterView';

import ContainerSurface             from 'famous/surfaces/ContainerSurface';
import EventHandler                 from 'famous/core/EventHandler';
import PhysicsEngine                from 'famous/physics/PhysicsEngine';
import Body                         from 'famous/physics/bodies/Body';
import Circle                       from 'famous/physics/bodies/Circle';
import Wall                         from 'famous/physics/constraints/Wall';
import Vector                       from 'famous/math/Vector';
import Collision                    from 'famous/physics/constraints/Collision';

export class CollisionView extends View{
    constructor(){

        super({
            classes: ['drag', 'view']
        });

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');

        this._createLayout();

    }

    _createLayout(){
        var context = Engine.getContexts()[0];

        var contextSize;

        var mainSurface = new ContainerSurface ({
            size: [undefined,undefined],
            properties: {
                backgroundColor: '#dddddd'
            }
        });

        mainSurface.on("click",function(){
            addNewBall(1);
            setWalls();
        });


        var handler = new EventHandler();

        var physicsEngine = new PhysicsEngine();

        var balls = [];
        var collision = new Collision();
        function addNewBall(number){
            if(number == undefined) var number = 1;
            for(var i=0; i<number; i++){
                var index = balls.length;
                balls[index] = new Surface ({
                    size: [50,50],
                    properties: {
                        backgroundColor: 'red',
                        borderRadius: '50px'
                    }
                })

                balls[index].state = new StateModifier({origin:[0.5,0.5], align:[0.5,0.5]});

                balls[index].particle = new Circle({radius:25});

                //Attempting to add collision
                physicsEngine.attach( collision,  [balls[index]]);
                //for (var i = 0; i < balls.length; i++) {
                //    physicsEngine.attach(collision, balls, balls[i]);
                //}
                //for (var i = 0; i < balls.length; i++) {
                //    for (var j = 0; j < balls.length; j++) {
                //        if (i !== j) physicsEngine.attach(collision, balls[j], balls[i]);
                //    }
                //}

                balls[index].particle.applyForce(new Vector(Math.random(), 1, 0));

                physicsEngine.addBody(balls[index].particle);


                mainSurface.add(balls[index].state).add(balls[index]);
            }
        }

        this.add(mainSurface);
        addNewBall(1);

        //Function to loop through all balls and set the walls
        function setWalls() {
            contextSize = context.getSize();
            var leftWall    = new Wall({normal : [1,0,0],  distance : contextSize[0]/2.0, restitution : .5});
            var rightWall   = new Wall({normal : [-1,0,0], distance : contextSize[0]/2.0, restitution : .5});
            var topWall     = new Wall({normal : [0,1,0],  distance : contextSize[1]/2.0, restitution : .5});
            var bottomWall  = new Wall({normal : [0,-1,0], distance : contextSize[1]/2.0, restitution : .5});

            for(var i=0; i < balls.length; i++){
                physicsEngine.attach( leftWall,  [balls[i].particle]);
                physicsEngine.attach( rightWall, [balls[i].particle]);
                physicsEngine.attach( topWall,   [balls[i].particle]);
                physicsEngine.attach( bottomWall,[balls[i].particle]);
            }
        }


        Engine.nextTick(setWalls);
        Engine.on('resize',setWalls);

        Engine.on('prerender', function(){
            for(var i=0; i < balls.length; i++){
                balls[i].state.setTransform(balls[i].particle.getTransform())
            }
        });

    }
}