/**
 * Created by manuel on 15-05-15.
 */

import ObjectHelper                 from 'arva-mvc/utils/objectHelper';
import Engine                       from 'famous/core/Engine';
import Surface                      from 'famous/core/Surface';
import View                         from 'famous/core/View';
import LayoutController             from 'famous-flex/src/LayoutController';
import ScrollController             from 'famous-flex/src/ScrollController';
import FlexScrollView               from 'famous-flex/src/FlexScrollView';
import HeaderFooterLayout           from 'famous-flex/src/layouts/HeaderFooterLayout';
import Modifier                     from 'famous/core/Modifier';
import Transform                    from 'famous/core/Transform';
import Draggable                    from 'famous/modifiers/Draggable';
import StateModifier                from 'famous/modifiers/StateModifier';
import MouseSync                    from 'famous/inputs/MouseSync';
import Transitionable               from 'famous/transitions/Transitionable';
//import {HeaderView}                   from '../components/MyoInfo/HeaderView';
//import {ContentView}                  from '../components/MyoInfo/ContentView';
//import {FooterView}                  from '../components/MyoInfo/FooterView';

export class TransferDragView extends View {
    constructor() {
        super({
            classes: ['transfer', 'view']
        });

        /* Bind all local methods to the current object instance, so we can refer to "this"
         * in the methods as expected, even when they're called from event handlers.        */
        ObjectHelper.bindAllMethods(this, this);

        /* Hide all private properties (starting with '_') and methods from enumeration,
         * so when you do for( in ), only actual data properties show up. */
        ObjectHelper.hideMethodsAndPrivatePropertiesFromObject(this);

        /* Hide the priority field from enumeration, so we don't save it to the dataSource. */
        ObjectHelper.hidePropertyFromObject(Object.getPrototypeOf(this), 'length');


        this._createLayout();

        var position = new Transitionable([0, 0]);

        var mouseSync = new MouseSync();
        this.pitchOffset = false;
        this.yawOffset = false;

        //Dynamic positioning modifiers
        this.panModifier = new StateModifier({
            transform: function(){
                var currentPosition = position.get();
                return Transform.translate(currentPosition[0], currentPosition[1]);
            }
        })

        //Panning listener
        this.pipe(mouseSync);

        mouseSync.on("update", function(data){
            var currentPosition = position.get();
            position.set([
                currentPosition[0] + data.delta[0],
                currentPosition[1] + data.delta[1]
            ]);
        });

    }

    _createLayout(){

        this.add(new Surface({
            size:[window.innerWidth - 256, undefined],
            properties: {
                'border-left':'1px dashed black',
                'padding':'8px',
            }
        }));
    }

    controlDraggable(angles){
        this.eulerAngles = angles;

        if(this.pitchOffset == false || this.yawOffset == false){
            this.pitchOffset = this.eulerAngles.pitch;
            this.yawOffset = this.eulerAngles.yaw;
        }

        if(this.latestDraggable && this.pitchOffset && this.yawOffset){
            let xSpeed = (this.eulerAngles.yaw - this.yawOffset);
            let ySpeed = (this.eulerAngles.pitch - this.pitchOffset);

            if(Math.abs(xSpeed) > 0.25 || Math.abs(ySpeed) > 0.25) {
                this.latestDraggable.setRelativePosition([xSpeed*-7.5,ySpeed*-7.5]);
            }
    }
    }

    insertSurface(surface, modifier){
        //if(surface instanceof Surface) {
        //    surface.setSize([50, 50]);
            let draggable = new Draggable();
            draggable.subscribe(surface);
            this.add(new Modifier({
                origin: [0.5, 0.5],
                align: [0.5, 0.5],
            })).add(draggable).add(surface);

        this.latestDraggable = draggable;
            //if(!surface || !modifier) return;
            //this.add(modifier).add(surface);
        //}
    }
}