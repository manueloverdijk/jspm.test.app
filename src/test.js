/**
 * Created by manuel on 22-04-15.
 */

import Engine from "famous/core/Engine"
import Surface from "famous/core/Surface"
import Modifier from "famous/core/Modifier"
import ContainerSurface from  "famous/surfaces/ContainerSurface"
import Scrollview from "famous/views/Scrollview"
import CubeSurface from 'src/components/demo/CubeSurface'
import $ from 'jquery'
//import Myo from 'myojs'


import FlexScrollView from 'famous-flex/src/FlexScrollView'
import ScrollController from 'famous-flex/src/ScrollController'
import ListLayout from 'famous-flex/src/layouts/ListLayout'
import CollectionLayout from 'famous-flex/src/layouts/CollectionLayout'
import CoverLayout from 'famous-flex/src/layouts/CoverLayout'
import WheelLayout from 'famous-flex/src/layouts/WheelLayout'

export default class Test {
    constructor(){
        this.context = Engine.createContext();

        this.scrollViewDemo();
    }

    scrollViewDemo(){

        console.log('scrollViewDemo');

        var items = [];
        var scrollView = new FlexScrollView({
            autoPipeEvents: true
        });
        this.context.add(scrollView);

            items.push(new Surface({
                size:[400,400],
                properties: {
                    'background-color':'black'
                }
            }));
        items.push(new Surface({
            size:[400,400],
            properties: {
                'background-color':'orange'
            }
        }));
        items.push(new Surface({
            size:[400,400],
            properties: {
                'background-color':'yellow'
            }
        }));
        items.push(new Surface({
            size:[400,400],
            properties: {
                'background-color':'cyan'
            }
        }));
        items.push(new Surface({
            size:[400,400],
            properties: {
                'background-color':'green'
            }
        }));

        scrollView.sequenceFrom(items);

        scrollView.on('scroll',function(e){
            console.log('scroll', e);
        })


        scrollView._scrollSync.on('update', function(e){
            console.log('update', e);
        });

        var hub = new Myo.Hub();

        hub.on('ready', function() {
            console.log("ready");
        });
        hub.on('connect', function() {
            console.log("connected");
        });

        let time = 0;
        let total = 0;

        hub.on('frame', function(frame){
            if(frame.pose.valid){
                console.log('frame', frame.pose);
                if(frame.pose.type == 2){
                    scrollView.goToNextPage();
                } else if (frame.pose.type == 3){
                    scrollView.goToPreviousPage();
                }
            }
        });

        //hub.on('frame', function(frame) {
        //
        //    if(frame.gyro.valid){
        //        time++;
        //        if(frame.gyro.x > 100 || frame.gyro.x < -100) {
        //            total = total + frame.gyro.x;
        //            if (time == 10) {
        //                time = 0;
        //                let offset = total / 10;
        //                total = 0;
        //                scrollView.applyScrollForce(0);
        //                scrollView.scroll(offset);
        //                scrollView.releaseScrollForce(0, 0 < 0 ? 1 : -1);
        //            }
        //        }
        //
        //            //console.log(frame.gyro.x);
        //            //scrollView.scroll(-frame.gyro.x);
        //        //let offset = 2;
        //        //scrollView.applyScrollForce(offset);
        //        //scrollView.releaseScrollForce(offset, offset < 0 ? 1 : -1);
        //    }
        //
        //});

        //setTimeout(function(){
        //    let offset = 0;
        //    scrollView.applyScrollForce(offset);
        //    scrollView.scroll(200);
        //        scrollView.releaseScrollForce(offset, offset < 0 ? 1 : -1);
        //},1000);


        //
        //setTimeout(function(){
        //    scrollView.scroll(-100);
        //     //scrollView._scrollSync._eventInput.trigger('update', {delta:[0,200]});
        //    //window.scrollTo(500, 0);
        //    // Create a new jQuery.Event object with specified event properties.
        //    //var e = $.Event( "DOMMouseScroll",{delta: -650} );
        //    //
        //    //// trigger an artificial DOMMouseScroll event with delta -650
        //    //$( ".famous-container" ).trigger( e );
        //},2000)
        //scrollView._eventInput.emit('touch')

        //Engine.on('scroll', function(e){
        //    console.log('scroll', e)
        //})
        //
        //Engine.on('click', function(e){
        //    console.log('click' ,e)
        //})

    }

    cubeSurfaceDemo(){

        let cube = new CubeSurface(this.context, {size: [150, 150]});
    }
}