/**
 * Created by manuel on 29-04-15.
 */


export default class DeviceOrientation {
    constructor(){

        var event = new CustomEvent('orientationquaternion', { 'detail': {
            x: '1',
            y: '2',
            z: '3',
            w: '4'
        }});

// Listen for the event.
        window.addEventListener('click', function(e){
            console.log('click event', e);
        });

        window.addEventListener('orientationquaternion', function (e) {
            console.log('got device orientation', e);
        }, false);

// Dispatch the event.
        window.dispatchEvent(event);


    }
}