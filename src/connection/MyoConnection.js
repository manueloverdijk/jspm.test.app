/**
 * Created by manuel on 01-05-15.
 */

var MyoBluetooth  = require('MyoNodeBluetooth');
//import Quaternion from 'famous/math/Quaternion'

export class MyoConnection {
    constructor(){
        console.log('MyoConnection constructor');

        var MyoAgent = new MyoBluetooth();

        MyoAgent.on('discovered', function(armband){
            console.log('discovered armband: ', armband);
            this.armband = armband;
            this.armband.on('connect', function(connected){

                console.log('connect: ', connected);
                if(connected == true){
                    console.log('connected');

                    // start notifying Emg, IMU and Classifier characteristics
                    this.armband.initStart();
                    startListening.call(this);
                } else {
                    console.log('disconnected!');
                }

            }.bind(this));
        }.bind(this));

        MyoAgent.on('stateChange', function(state){
            console.log('stateChange: ', state);
        }.bind(this));

        function startListening() {

            this.armband.on('ready', function (data) {
                this.armband.on('info', function (data) {
                    console.log('Info:', data);
                });

                this.armband.on('version', function (data) {
                    console.log('Version: ', data);
                });

                this.armband.on('command', function (data) {
                    console.log('Command:', data);
                });

                this.armband.on('batteryInfo', function (data) {
                    console.log('BatteryInfo: ', data);
                });

                this.armband.on('pose', function(data){
                    console.log('Pose: ', data);
                });

                this.armband.on('orientation', function(data){
                    console.log('Orientation:', data);
                });

                this.armband.on('emg', function(data){
                   console.log('Emg: ', data);
                });

                this.armband.readInfo();
                this.armband.readVersion();
                this.armband.vibrate(2);
                this.armband.setUnlockMode(0);
                this.armband.readBatteryInfo();

            }.bind(this));

        }


            //var Hub = new myobluetooth();
        //
        //hub.on('connect', function() {
        //    console.log("connected");
        //}.bind(this));
        //
        //hub.on('disconnect', function(){
        //    console.log("disconnect");
        //
        //    this.eventDispatcher('myoDisconnected',{});
        //}.bind(this));
        //
        //hub.on('frame', function(frame){
        //
        //    this.eventDispatcher('myoQuaternion', {
        //        quaternion: new Quaternion(frame.rotation.w, frame.rotation.y, frame.rotation.z, -frame.rotation.x)
        //    });
        //
        //    this.eventDispatcher('myoEuler',{
        //        pitch: frame.euler.pitch,
        //        roll: frame.euler.roll,
        //        yaw: frame.euler.yaw
        //    });
        //
        //
        //    if(frame.gyro.valid) {
        //
        //        this.eventDispatcher('myoGyro',{
        //            x: frame.gyro.x,
        //            y: frame.gyro.y,
        //            z: frame.gyro.z
        //        });
        //        // emit myoGyro event
        //    }
        //
        //    if(frame.accel.valid){
        //
        //        this.eventDispatcher('myoAccel',{
        //            x: frame.accel.x,
        //            y: frame.accel.y,
        //            z: frame.accel.z
        //        });
        //        // emit myoAccel event
        //
        //        if(true){
        //            this.eventDispatcher('myoShake',{});
        //            // emit myoShake event
        //        }
        //
        //    }
        //
        //    if(frame.pose.valid){
        //
        //        switch (frame.pose.type) {
        //            case 0:
        //                this.eventDispatcher('myoPose',{
        //                    type: 'rest'
        //                });
        //                break;
        //            case 1:
        //                this.eventDispatcher('myoPose',{
        //                    type: 'fist'
        //                });
        //                // emit event
        //                break;
        //            case 2:
        //                this.eventDispatcher('myoPose',{
        //                    type: 'waveIn'
        //                });
        //                // emit event
        //                break;
        //            case 3:
        //                this.eventDispatcher('myoPose',{
        //                    type: 'waveOut'
        //                });
        //                //emit event
        //                break;
        //            case 4:
        //                this.eventDispatcher('myoPose',{
        //                    type: 'spread'
        //                });
        //                //emit event
        //                break;
        //            case 5:
        //                this.eventDispatcher('myoPose',{
        //                    type: 'reserved'
        //                });
        //                //emit event
        //                break;
        //            case 6:
        //                this.eventDispatcher('myoPose',{
        //                    type: 'tap'
        //                });
        //                //emit event
        //                break;
        //        }
        //    }
        //
        //}.bind(this));
    }

    //eventDispatcher(type,detail){
    //    let event = new CustomEvent(type, { 'detail': detail});
    //    window.dispatchEvent(event);
    //}
}