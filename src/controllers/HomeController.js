/**
 * Created by manuel on 30-04-15.
 */

import Engine               from 'famous/core/Engine';
import Surface              from 'famous/core/Surface';
//import {Controller}         from 'arva-mvc/core/Controller';
import {MyoController}        from './MyoController';
import {SampleView}         from '../views/Sampleview';
import {TestView}           from '../views/TestView';
import {MyoView}            from '../views/MyoView';

// Import MyoBluetooth in the Node Context with Node's Require();
//var MyoBluetooth  = require('MyoNodeBluetooth');

export default class HomeController extends MyoController {

    constructor(router, context) {
        super(router, context)

        this.sampleView =   new SampleView();
        this.testView =     new TestView();
        this.myoView =      new MyoView();

    }

    ReRouteExample() {
        this.router.go(this, "Index", ['a','b'])
    }

    Index(){
        return this.sampleView;
    }

    Test(){
        return this.testView;
    }

    Info(){
        return this.myoView;
    }
}