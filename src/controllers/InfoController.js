/**
 * Created by manuel on 12-05-15.
 */

import Engine               from 'famous/core/Engine';
import Surface              from 'famous/core/Surface';
import {MyoController}      from './MyoController';
import {SampleView}         from '../views/Sampleview';
import {TestView}           from '../views/TestView';
import {DragView}           from '../views/DragView';
import {CollisionView}      from '../views/CollisionView';
import {TransferView}       from '../views/TransferView';
import MapView              from '../components/MyoInfo/MapView';
import {MyoView}            from '../views/MyoView';
import {ProfileView}        from 'arva-mvc/views/ProfileView';
import {FullImageView}      from 'arva-mvc/views/FullImageView';
import {NavBarView}         from 'arva-mvc/views/NavBarView';
import ObjectHelper         from 'arva-mvc/utils/objectHelper'
import Easing               from 'famous/transitions/Easing';
import AnimationController  from 'famous-flex/src/AnimationController';

export default class InfoController extends MyoController {
    constructor(router, context) {
        super(router, context, {
            transfer: {
                transition: {duration: 500, curve: Easing.inOutExpo},
                zIndex: 1000,
                items: {
                    'image': ['image', 'navBarImage'],
                    'navBarImage': ['image', 'navBarImage']
                }
            }
        });

        ObjectHelper.bindAllMethods(this,this);
        this.fullImageView = new FullImageView({
            text: 'arva-mvc with famous-flex is magic!'
        });
        this.profileView = new ProfileView();
        this.navBarView =  new NavBarView();
        this.myoView = new MyoView();
        this.dragView = new DragView();
        this.collisionView = new CollisionView();
        this.transferView = new TransferView();
        this.mapView = new MapView();

        this.on('renderend', (arg)=>{
            console.log(arg);
        });


        this.myoAgent.on('discovered', function(armband){
            console.log('discovered armband: ', armband);
            this.armband = armband;
            this.setName(this.myoView,this.armband._peripheral.advertisement.localName);
            this.armband.on('connect', this.onConnect);
            this.armband.on('ready', this.onReady);
            this.armband.connect();

        }.bind(this));
    }

    ReRouteExample() {
        this.router.go(this, "Index", ['a','b']);
    }

    Index() {
        return this.fullImageView;
    }

    Profile() {
        return this.profileView;
    }

    NavBar() {
        return this.navBarView;
    }

    Info() {
        return this.myoView;
    }

    Drag(){
        return this.dragView;
    }

    Collision(){
        return this.collisionView;
    }

    Transfer(){
        return this.transferView;
    }

    Map(){
        return this.mapView;
    }

    onConnect(connected){
        console.log('connect: ', connected);
        if(connected == true){
            console.log('connected');

            // start notifying Emg, IMU and Classifier characteristics
            this.armband.initStart();
            this.setConnected(this.myoView, true);

        } else {
            console.log('disconnected!');
            this.setConnected(this.myoView, false);
        }
    }

    onReady(ready){

        console.log('ready!');

        this.armband.on('pose', function(data){
            console.log('received pose:', data);
            this.setPose(this.myoView, data.type);


            if(data.type == 'waveIn'){
                history.back();
            } else if(data.type == 'waveOut'){
                history.forward();
            } else if(data.type == 'fist'){
                //this.transferView.setFist();
                this.myoView.setMode();
            } else if (data.type == 'spread'){
                this.myoView.setActive();
                this.transferView.setSpread();
            }
        }.bind(this));

        this.armband.on('orientation',function(data){
            this.setOrientation(this.myoView, data);
        }.bind(this));

        setInterval(function(){
            this.armband.emit('pose', {type:'fist'});
        }.bind(this), 5000);

        setInterval(function(){
            this.armband.emit('pose', {type:'spread'});
        }.bind(this), 10000);


        this.armband.on('eulerangle', function(data){
            //console.log('eulerAngle: ', data);
            this.setEulerAngles(this.transferView, data);
            this.setEulerAngles(this.myoView, data);
            //this.setEulerAngles(this.dragView, data);
        }.bind(this));

        this.time = 0;
        this.total = 0;
        this.last_x = 0;
        this.last_y = 0;
        this.last_z = 0;
        this.lastUpdate = 0;

        //this.armband.on('accelerometer', this.accelerometer);

        this.setReady(this.myoView, true);

    }

    setName(view, name){
        view.layout.get('content').layout.get('infoView').layout.get('name').setContent('Name: ' + name);
    }

    setConnected(view, connected){
        view.layout.get('content').layout.get('infoView').layout.get('connected').setContent('Connected: ' + connected);
    }

    setReady(view, ready){
        view.layout.get('content').layout.get('infoView').layout.get('ready').setContent('Ready: ' + ready);
    }

    setPose(view, pose){
        view.layout.get('content').layout.get('infoView').layout.get('pose').setContent('Pose: ' + pose);
    }

    setOrientation(view, orientation){
        view.layout.get('content').layout.get('infoView').layout.get('orientation').setContent('Orientation: </br> x'+ orientation.x + '</br> y' + orientation.y + '</br> z ' + orientation.z + '</br> w' + orientation.w);
        view.layout.get('content').layout.get('cubeView').setQuaternion(orientation.w,orientation.x,orientation.y,orientation.z);
    }

    setEulerAngles(view, angles) {
        view.setEulerAngles(angles);
    }

    setFist(view){
        view.setFist();
    }


    accelerometer(data){

        let x = data[0];
        let y = data[1];
        let z = data[2];


        let date = new Date();
        let currentTime  = date.getTime();

        if ((currentTime - this.lastUpdate) > 500) {
            let diffTime = (currentTime - this.lastUpdate);
            this.lastUpdate = currentTime;

            // update happen way to fast, lets take a subsample and detect a shake gesture
            let speed = Math.abs(x + y + z - this.last_x - this.last_y - this.last_z) / diffTime * 10000;

            let speedX = (x - this.last_x) / diffTime *10000;
            let speedY = (y - this.last_y) / diffTime *10000;
            let speedZ = (z - this.last_z) / diffTime *10000;
            //console.log('speedX = ', speedX)
            //console.log('speedY = ', speedY)
            //console.log('speedZ = ', speedZ)
            //console.log('speed = ',speed);
            if (Math.abs(speed > 50)) {
                console.log('shake happend!');
                //alert('shake happend!!');
            }

            if(speedZ > 20){
                console.log('speedZ movement backward');
                history.back();
            }

            if(speedZ < -20){
                console.log('speedZ movement forward');
                history.forward();
            }

            this.last_x = x;
            this.last_y = y;
            this.last_z = z;
        }
    }
}

