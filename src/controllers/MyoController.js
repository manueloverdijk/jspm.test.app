/**
 * Created by manuel on 12-05-15.
 */

import _                    from 'lodash'
import {Inject, annotate}   from 'di.js'
import {Controller}         from 'arva-mvc/core/Controller';
var MyoBluetooth  = require('MyoNodeBluetooth');

export class MyoController extends Controller {

    constructor(router, context, spec){
        super(router, context, spec);

        this.myoAgent = new MyoBluetooth();
    }

}
