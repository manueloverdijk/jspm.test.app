/**
 * Created by manuel on 30-04-15.
 */

import Engine               from 'famous/core/Engine';
import Surface              from 'famous/core/Surface';
import {MyoController}      from './MyoController';
import HomeView             from '../views/demo/HomeView';
import MenuView             from '../views/demo/MenuView';
import MapView              from '../views/demo/MapView';
import {SampleView}         from '../views/Sampleview';
import {TestView}           from '../views/TestView';
import {DragView}           from '../views/DragView';
import {CollisionView}      from '../views/CollisionView';
import {TransferView}       from '../views/TransferView';
import {MyoView}            from '../views/MyoView';
import {ProfileView}        from 'arva-mvc/views/ProfileView';
import {FullImageView}      from 'arva-mvc/views/FullImageView';
import {NavBarView}         from 'arva-mvc/views/NavBarView';
import ObjectHelper         from 'arva-mvc/utils/objectHelper'
import Easing               from 'famous/transitions/Easing';
import AnimationController  from 'famous-flex/src/AnimationController';
import PresentationView     from '../views/demo/PresentationView';

export default class DemoController extends MyoController {

    constructor(router, context) {
        super(router, context)

        this.homeView =  new HomeView();
        this.mapView =   new MapView();
        this.menuView = new MenuView();
        this.transferView = new TransferView();
        this.presentationView = new PresentationView(0);

        this.currentId = 0;
        this.menuContext = Engine.createContext();
        this.menuContext.add(this.menuView);
        this.createHandlers();

        this.activeView = this.homeView;

        this.myoAgent.on('discovered', function(armband){
            console.log('discovered armband: ', armband);
            this.armband = armband;
            this.armband.on('connect', this.onConnect);
            this.armband.on('ready', this.onReady);
            this.armband.connect();

        }.bind(this));
    }

    Index(){

        this.activeView = this.homeView;
        return this.homeView;

    }

    Map(){
        this.activeView = this.mapView;
        return this.mapView;
    }

    Transfer(){
        this.activeView = this.transferView;
        return this.transferView;
    }

    Presentation(id){
        console.log('id::', id);
        this.presentationView = new PresentationView(parseInt(id)+1);

        this.presentationView.layout.on('click', function(){
            console.log('click');
            this.router.go(this, 'Presentation', {id: parseInt(this.currentId) + 1});
        }.bind(this));

        this.currentId = id;

        return this.presentationView;
    }


    onConnect(connected){
        console.log('connect: ', connected);
        if(connected == true){
            console.log('connected');

            // start notifying Emg, IMU and Classifier characteristics
            this.armband.initStart();
            this.setConnected(true);

        } else {
            console.log('disconnected!');
            this.setConnected(false);
        }
    }

    onReady(ready){
        this.armband.on('pose', function(data){
            this.setPose(data.type);
            if(data.type == 'waveIn'){
                history.back();
            } else if(data.type == 'waveOut'){
                history.forward();
            } else if(data.type == 'fist'){
                if(this.activeView == this.homeView){
                    this.clickRouting();
                } else {
                    this.activeView.setFist();
                }
            } else if (data.type == 'spread'){
                if(this.activeView == this.homeView){

                } else {
                    this.activeView.setSpread();
                }
            }
        }.bind(this));

        this.armband.on('orientation',function(data){
            //this.setOrientation(this.myoView, data);
        }.bind(this));

        setInterval(function(){
            this.armband.emit('pose', {type:'fist'});
        }.bind(this), 5000);

        setInterval(function(){
            this.armband.emit('pose', {type:'spread'});
        }.bind(this), 10000);


        this.armband.on('eulerangle', function(data){
            this.activeView.angles(data);

        }.bind(this));

        this.time = 0;
        this.total = 0;
        this.last_x = 0;
        this.last_y = 0;
        this.last_z = 0;
        this.lastUpdate = 0;

        this.setReady(true);

    }

    setConnected(boolean){
        this.menuView.setConnected(boolean);
    }

    setReady(boolean){
        this.menuView.setReady(boolean);
    }

    setPose(type){
        this.menuView.setPose(type);
    }

    createHandlers(){
        this.homeView.layout.on('click', function(){
            this.clickRouting();
        }.bind(this))

        this.menuView.layout.on('click', function(){
            this.router.go(this, 'Index');
        }.bind(this));
    }

    clickRouting(){
        let index =  this.homeView.layout.getCurrentIndex();
        console.log(index);

        if(index == 0){
            // close application
        }
        else if(index == 1){
            // show map interaction demo
            this.router.go(this, 'Map');

        } else if(index == 2){
            // show drag and drop interaction demo
            this.router.go(this, 'Transfer');

        } else if(index == 3){
            this.router.go(this, 'Presentation', {id : this.currentId});
        }
    }

}