"format register";
(function(global) {

  var defined = {};

  // indexOf polyfill for IE8
  var indexOf = Array.prototype.indexOf || function(item) {
    for (var i = 0, l = this.length; i < l; i++)
      if (this[i] === item)
        return i;
    return -1;
  }

  function dedupe(deps) {
    var newDeps = [];
    for (var i = 0, l = deps.length; i < l; i++)
      if (indexOf.call(newDeps, deps[i]) == -1)
        newDeps.push(deps[i])
    return newDeps;
  }

  function register(name, deps, declare, execute) {
    if (typeof name != 'string')
      throw "System.register provided no module name";
    
    var entry;

    // dynamic
    if (typeof declare == 'boolean') {
      entry = {
        declarative: false,
        deps: deps,
        execute: execute,
        executingRequire: declare
      };
    }
    else {
      // ES6 declarative
      entry = {
        declarative: true,
        deps: deps,
        declare: declare
      };
    }

    entry.name = name;
    
    // we never overwrite an existing define
    if (!defined[name])
      defined[name] = entry; 

    entry.deps = dedupe(entry.deps);

    // we have to normalize dependencies
    // (assume dependencies are normalized for now)
    // entry.normalizedDeps = entry.deps.map(normalize);
    entry.normalizedDeps = entry.deps;
  }

  function buildGroups(entry, groups) {
    groups[entry.groupIndex] = groups[entry.groupIndex] || [];

    if (indexOf.call(groups[entry.groupIndex], entry) != -1)
      return;

    groups[entry.groupIndex].push(entry);

    for (var i = 0, l = entry.normalizedDeps.length; i < l; i++) {
      var depName = entry.normalizedDeps[i];
      var depEntry = defined[depName];
      
      // not in the registry means already linked / ES6
      if (!depEntry || depEntry.evaluated)
        continue;
      
      // now we know the entry is in our unlinked linkage group
      var depGroupIndex = entry.groupIndex + (depEntry.declarative != entry.declarative);

      // the group index of an entry is always the maximum
      if (depEntry.groupIndex === undefined || depEntry.groupIndex < depGroupIndex) {
        
        // if already in a group, remove from the old group
        if (depEntry.groupIndex !== undefined) {
          groups[depEntry.groupIndex].splice(indexOf.call(groups[depEntry.groupIndex], depEntry), 1);

          // if the old group is empty, then we have a mixed depndency cycle
          if (groups[depEntry.groupIndex].length == 0)
            throw new TypeError("Mixed dependency cycle detected");
        }

        depEntry.groupIndex = depGroupIndex;
      }

      buildGroups(depEntry, groups);
    }
  }

  function link(name) {
    var startEntry = defined[name];

    startEntry.groupIndex = 0;

    var groups = [];

    buildGroups(startEntry, groups);

    var curGroupDeclarative = !!startEntry.declarative == groups.length % 2;
    for (var i = groups.length - 1; i >= 0; i--) {
      var group = groups[i];
      for (var j = 0; j < group.length; j++) {
        var entry = group[j];

        // link each group
        if (curGroupDeclarative)
          linkDeclarativeModule(entry);
        else
          linkDynamicModule(entry);
      }
      curGroupDeclarative = !curGroupDeclarative; 
    }
  }

  // module binding records
  var moduleRecords = {};
  function getOrCreateModuleRecord(name) {
    return moduleRecords[name] || (moduleRecords[name] = {
      name: name,
      dependencies: [],
      exports: {}, // start from an empty module and extend
      importers: []
    })
  }

  function linkDeclarativeModule(entry) {
    // only link if already not already started linking (stops at circular)
    if (entry.module)
      return;

    var module = entry.module = getOrCreateModuleRecord(entry.name);
    var exports = entry.module.exports;

    var declaration = entry.declare.call(global, function(name, value) {
      module.locked = true;
      exports[name] = value;

      for (var i = 0, l = module.importers.length; i < l; i++) {
        var importerModule = module.importers[i];
        if (!importerModule.locked) {
          var importerIndex = indexOf.call(importerModule.dependencies, module);
          importerModule.setters[importerIndex](exports);
        }
      }

      module.locked = false;
      return value;
    });
    
    module.setters = declaration.setters;
    module.execute = declaration.execute;

    if (!module.setters || !module.execute)
      throw new TypeError("Invalid System.register form for " + entry.name);

    // now link all the module dependencies
    for (var i = 0, l = entry.normalizedDeps.length; i < l; i++) {
      var depName = entry.normalizedDeps[i];
      var depEntry = defined[depName];
      var depModule = moduleRecords[depName];

      // work out how to set depExports based on scenarios...
      var depExports;

      if (depModule) {
        depExports = depModule.exports;
      }
      else if (depEntry && !depEntry.declarative) {
        depExports = { 'default': depEntry.module.exports, __useDefault: true };
      }
      // in the module registry
      else if (!depEntry) {
        depExports = load(depName);
      }
      // we have an entry -> link
      else {
        linkDeclarativeModule(depEntry);
        depModule = depEntry.module;
        depExports = depModule.exports;
      }

      // only declarative modules have dynamic bindings
      if (depModule && depModule.importers) {
        depModule.importers.push(module);
        module.dependencies.push(depModule);
      }
      else
        module.dependencies.push(null);

      // run the setter for this dependency
      if (module.setters[i])
        module.setters[i](depExports);
    }
  }

  // An analog to loader.get covering execution of all three layers (real declarative, simulated declarative, simulated dynamic)
  function getModule(name) {
    var exports;
    var entry = defined[name];

    if (!entry) {
      exports = load(name);
      if (!exports)
        throw new Error("Unable to load dependency " + name + ".");
    }

    else {
      if (entry.declarative)
        ensureEvaluated(name, []);
    
      else if (!entry.evaluated)
        linkDynamicModule(entry);

      exports = entry.module.exports;
    }

    if ((!entry || entry.declarative) && exports && exports.__useDefault)
      return exports['default'];

    return exports;
  }

  function linkDynamicModule(entry) {
    if (entry.module)
      return;

    var exports = {};

    var module = entry.module = { exports: exports, id: entry.name };

    // AMD requires execute the tree first
    if (!entry.executingRequire) {
      for (var i = 0, l = entry.normalizedDeps.length; i < l; i++) {
        var depName = entry.normalizedDeps[i];
        var depEntry = defined[depName];
        if (depEntry)
          linkDynamicModule(depEntry);
      }
    }

    // now execute
    entry.evaluated = true;
    var output = entry.execute.call(global, function(name) {
      for (var i = 0, l = entry.deps.length; i < l; i++) {
        if (entry.deps[i] != name)
          continue;
        return getModule(entry.normalizedDeps[i]);
      }
      throw new TypeError('Module ' + name + ' not declared as a dependency.');
    }, exports, module);
    
    if (output)
      module.exports = output;
  }

  /*
   * Given a module, and the list of modules for this current branch,
   *  ensure that each of the dependencies of this module is evaluated
   *  (unless one is a circular dependency already in the list of seen
   *  modules, in which case we execute it)
   *
   * Then we evaluate the module itself depth-first left to right 
   * execution to match ES6 modules
   */
  function ensureEvaluated(moduleName, seen) {
    var entry = defined[moduleName];

    // if already seen, that means it's an already-evaluated non circular dependency
    if (entry.evaluated || !entry.declarative)
      return;

    // this only applies to declarative modules which late-execute

    seen.push(moduleName);

    for (var i = 0, l = entry.normalizedDeps.length; i < l; i++) {
      var depName = entry.normalizedDeps[i];
      if (indexOf.call(seen, depName) == -1) {
        if (!defined[depName])
          load(depName);
        else
          ensureEvaluated(depName, seen);
      }
    }

    if (entry.evaluated)
      return;

    entry.evaluated = true;
    entry.module.execute.call(global);
  }

  // magical execution function
  var modules = {};
  function load(name) {
    if (modules[name])
      return modules[name];

    var entry = defined[name];

    // first we check if this module has already been defined in the registry
    if (!entry)
      throw "Module " + name + " not present.";

    // recursively ensure that the module and all its 
    // dependencies are linked (with dependency group handling)
    link(name);

    // now handle dependency execution in correct order
    ensureEvaluated(name, []);

    // remove from the registry
    defined[name] = undefined;

    var module = entry.declarative ? entry.module.exports : { 'default': entry.module.exports, '__useDefault': true };

    // return the defined module object
    return modules[name] = module;
  };

  return function(main, declare) {

    var System;

    // if there's a system loader, define onto it
    if (typeof System != 'undefined' && System.register) {
      declare(System);
      System['import'](main);
    }
    // otherwise, self execute
    else {
      declare(System = {
        register: register, 
        get: load, 
        set: function(name, module) {
          modules[name] = module; 
        },
        newModule: function(module) {
          return module;
        },
        global: global 
      });
      System.set('@empty', System.newModule({}));
      load(main);
    }
  };

})(typeof window != 'undefined' ? window : global)
/* ('mainModule', function(System) {
  System.register(...);
}); */

('src/main', function(System) {

System.register("npm:process@0.10.1/browser", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var process = module.exports = {};
  var queue = [];
  var draining = false;
  function drainQueue() {
    if (draining) {
      return ;
    }
    draining = true;
    var currentQueue;
    var len = queue.length;
    while (len) {
      currentQueue = queue;
      queue = [];
      var i = -1;
      while (++i < len) {
        currentQueue[i]();
      }
      len = queue.length;
    }
    draining = false;
  }
  process.nextTick = function(fun) {
    queue.push(fun);
    if (!draining) {
      setTimeout(drainQueue, 0);
    }
  };
  process.title = 'browser';
  process.browser = true;
  process.env = {};
  process.argv = [];
  process.version = '';
  process.versions = {};
  function noop() {}
  process.on = noop;
  process.addListener = noop;
  process.once = noop;
  process.off = noop;
  process.removeListener = noop;
  process.removeAllListeners = noop;
  process.emit = noop;
  process.binding = function(name) {
    throw new Error('process.binding is not supported');
  };
  process.cwd = function() {
    return '/';
  };
  process.chdir = function(dir) {
    throw new Error('process.chdir is not supported');
  };
  process.umask = function() {
    return 0;
  };
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/Pose", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Pose = module.exports = function(data) {
    this.valid = data.hasOwnProperty("invalid") ? false : true;
    this.type = data.type;
    this.POSE_REST = 0;
    this.POSE_FIST = 1;
    this.POSE_WAVE_IN = 2;
    this.POSE_WAVE_OUT = 3;
    this.POSE_FINGERS_SPREAD = 4;
    this.POSE_RESERVED_1 = 5;
    this.POSE_THUMB_TO_PINKY = 6;
  };
  Pose.prototype.isEqualTo = function(other) {
    return this.type == other.type;
  };
  Pose.invalid = function() {
    return new Pose({
      invalid: true,
      type: 65536
    });
  };
  Pose.prototype.toString = function() {
    if (!this.valid) {
      return "[Pose invalid]";
    }
    switch (this.type) {
      case this.POSE_REST:
        return "[Pose type:" + this.type.toString() + " POSE_NONE]";
        break;
      case this.POSE_FIST:
        return "[Pose type:" + this.type.toString() + " POSE_FIST]";
        break;
      case this.POSE_WAVE_IN:
        return "[Pose type:" + this.type.toString() + " POSE_WAVE_IN]";
        break;
      case this.POSE_WAVE_OUT:
        return "[Pose type:" + this.type.toString() + " POSE_WAVE_OUT]";
        break;
      case this.POSE_FINGERS_SPREAD:
        return "[Pose type:" + this.type.toString() + " POSE_FINGERS_SPREAD]";
        break;
      case this.POSE_RESERVED_1:
        return "[Pose type:" + this.type.toString() + " POSE_RESERVED_1]";
        break;
      case this.POSE_THUMB_TO_PINKY:
        return "[Pose type:" + this.type.toString() + " POSE_THUMB_TO_PINKY]";
        break;
      default:
        break;
    }
    return "[Pose type:" + this.type.toString() + "]";
  };
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/Quaternion", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Quaternion = module.exports = function(data) {
    this.valid = data.hasOwnProperty("invalid") ? false : true;
    if (this.valid) {
      this.x = data[0];
      this.y = data[1];
      this.z = data[2];
      this.w = data[3];
    } else {
      this.x = NaN;
      this.y = NaN;
      this.z = NaN;
      this.w = NaN;
    }
  };
  Quaternion.prototype.normalized = function() {
    var magnitude = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
    return new Quaternion({
      x: this.x / magnitude,
      y: this.y / magnitude,
      z: this.z / magnitude,
      w: this.w / magnitude
    });
  };
  Quaternion.prototype.conjugate = function() {
    return new Quaternion({
      x: -this.x,
      y: -this.y,
      z: -this.z,
      w: this.w
    });
  };
  Quaternion.prototype.roll = function(rotation) {
    return Math.atan2(2 * rotation.y * rotation.w - 2 * rotation.x * rotation.z, 1 - 2 * rotation.y * rotation.y - 2 * rotation.z * rotation.z);
  };
  Quaternion.prototype.pitch = function(rotation) {
    return Math.atan2(2 * rotation.x * rotation.w - 2 * rotation.y * rotation.z, 1 - 2 * rotation.x * rotation.x - 2 * rotation.z * rotation.z);
  };
  Quaternion.prototype.yaw = function(rotation) {
    return Math.asin(2 * rotation.x * rotation.y + 2 * rotation.z * rotation.w);
  };
  Quaternion.invalid = function() {
    return new Quaternion({
      invalid: true,
      x: NaN,
      y: NaN,
      z: NaN
    });
  };
  Quaternion.prototype.toString = function() {
    if (!this.valid) {
      return "[Quaternion invalid]";
    }
    return "[Quaternion x:" + this.x + " y:" + this.y + " z:" + this.z + " w:" + this.w + "]";
  };
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/Vector3", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Vector3 = module.exports = function(data) {
    this.valid = data.hasOwnProperty("invalid") ? false : true;
    if (this.valid) {
      this.x = data[0];
      this.y = data[1];
      this.z = data[2];
    } else {
      this.x = NaN;
      this.y = NaN;
      this.z = NaN;
    }
  };
  Vector3.prototype.opposite = function() {
    return new Vector3({
      x: -this.x,
      y: -this.y,
      z: -this.z
    });
  };
  Vector3.prototype.plus = function(other) {
    return new Vector3({
      x: this.x + other.x,
      y: this.y + other.y,
      z: this.z + other.z
    });
  };
  Vector3.prototype.plusAssign = function(other) {
    this.x += other.x;
    this.y += other.y;
    this.z += other.z;
    return this;
  };
  Vector3.prototype.minus = function(other) {
    return new Vector3({
      x: this.x - other.x,
      y: this.y - other.y,
      z: this.z - other.z
    });
  };
  Vector3.prototype.minusAssign = function(other) {
    this.x -= other.x;
    this.y -= other.y;
    this.z -= other.z;
    return this;
  };
  Vector3.prototype.multiply = function(scalar) {
    return new Vector3({
      x: this.x * scalar,
      y: this.y * scalar,
      z: this.z * scalar
    });
  };
  Vector3.prototype.multiplyAssign = function(scalar) {
    this.x *= scalar;
    this.y *= scalar;
    this.z *= scalar;
    return this;
  };
  Vector3.prototype.divide = function(scalar) {
    return new Vector3({
      x: this.x / scalar,
      y: this.y / scalar,
      z: this.z / scalar
    });
  };
  Vector3.prototype.divideAssign = function(scalar) {
    this.x /= scalar;
    this.y /= scalar;
    this.z /= scalar;
    return this;
  };
  Vector3.prototype.isEqualTo = function(other) {
    if (this.x != other.x || this.y != other.y || this.z != other.z)
      return false;
    else
      return true;
  };
  Vector3.prototype.angleTo = function(other) {
    var denom = this.magnitudeSquared() * other.magnitudeSquared();
    if (denom <= 0)
      return 0;
    return Math.acos(this.dot(other) / Math.sqrt(denom));
  };
  Vector3.prototype.cross = function(other) {
    return new Vector3({
      x: (this.y * other.z) - (this.z * other.y),
      y: (this.z * other.x) - (this.x * other.z),
      z: (this.x * other.y) - (this.y * other.x)
    });
  };
  Vector3.prototype.distanceTo = function(other) {
    return Math.sqrt((this.x - other.x) * (this.x - other.x) + (this.y - other.y) * (this.y - other.y) + (this.z - other.z) * (this.z - other.z));
  };
  Vector3.prototype.dot = function(other) {
    return (this.x * other.x) + (this.y * other.y) + (this.z * other.z);
  };
  Vector3.prototype.isValid = function() {
    return (this.x <= Number.MAX_VALUE && this.x >= -Number.MAX_VALUE) && (this.y <= Number.MAX_VALUE && this.y >= -Number.MAX_VALUE) && (this.z <= Number.MAX_VALUE && this.z >= -Number.MAX_VALUE);
  };
  Vector3.prototype.magnitude = function() {
    return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
  };
  Vector3.prototype.magnitudeSquared = function() {
    return this.x * this.x + this.y * this.y + this.z * this.z;
  };
  Vector3.prototype.normalized = function() {
    var denom = this.magnitudeSquared();
    if (denom <= 0)
      return new Vector3({
        x: 0,
        y: 0,
        z: 0
      });
    denom = 1 / Math.sqrt(denom);
    return new Vector3({
      x: this.x * denom,
      y: this.y * denom,
      z: this.z * denom
    });
  };
  Vector3.prototype.pitch = function() {
    return Math.atan2(this.y, -this.z);
  };
  Vector3.prototype.yaw = function() {
    return Math.atan2(this.x, -this.z);
  };
  Vector3.prototype.roll = function() {
    return Math.atan2(this.x, -this.y);
  };
  Vector3.prototype.zero = function() {
    return new Vector3({
      x: 0,
      y: 0,
      z: 0
    });
  };
  Vector3.prototype.xAxis = function() {
    return new Vector3({
      x: 1,
      y: 0,
      z: 0
    });
  };
  Vector3.prototype.yAxis = function() {
    return new Vector3({
      x: 0,
      y: 1,
      z: 0
    });
  };
  Vector3.prototype.zAxis = function() {
    return new Vector3({
      x: 0,
      y: 0,
      z: 1
    });
  };
  Vector3.prototype.left = function() {
    return new Vector3({
      x: -1,
      y: 0,
      z: 0
    });
  };
  Vector3.prototype.right = function() {
    return this.xAxis();
  };
  Vector3.prototype.down = function() {
    return new Vector3({
      x: 0,
      y: -1,
      z: 0
    });
  };
  Vector3.prototype.up = function() {
    return this.yAxis();
  };
  Vector3.prototype.forward = function() {
    return new Vector3({
      x: 0,
      y: 0,
      z: -1
    });
  };
  Vector3.prototype.backward = function() {
    return this.zAxis();
  };
  Vector3.invalid = function() {
    return new Vector3({invalid: true});
  };
  Vector3.prototype.toString = function() {
    if (!this.valid) {
      return "[Vector3 invalid]";
    }
    return "[Vector3 x:" + this.x + " y:" + this.y + " z:" + this.z + "]";
  };
  global.define = __define;
  return module.exports;
});



System.register("npm:underscore@1.8.3/underscore", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  "format cjs";
  (function() {
    var root = this;
    var previousUnderscore = root._;
    var ArrayProto = Array.prototype,
        ObjProto = Object.prototype,
        FuncProto = Function.prototype;
    var push = ArrayProto.push,
        slice = ArrayProto.slice,
        toString = ObjProto.toString,
        hasOwnProperty = ObjProto.hasOwnProperty;
    var nativeIsArray = Array.isArray,
        nativeKeys = Object.keys,
        nativeBind = FuncProto.bind,
        nativeCreate = Object.create;
    var Ctor = function() {};
    var _ = function(obj) {
      if (obj instanceof _)
        return obj;
      if (!(this instanceof _))
        return new _(obj);
      this._wrapped = obj;
    };
    if (typeof exports !== 'undefined') {
      if (typeof module !== 'undefined' && module.exports) {
        exports = module.exports = _;
      }
      exports._ = _;
    } else {
      root._ = _;
    }
    _.VERSION = '1.8.3';
    var optimizeCb = function(func, context, argCount) {
      if (context === void 0)
        return func;
      switch (argCount == null ? 3 : argCount) {
        case 1:
          return function(value) {
            return func.call(context, value);
          };
        case 2:
          return function(value, other) {
            return func.call(context, value, other);
          };
        case 3:
          return function(value, index, collection) {
            return func.call(context, value, index, collection);
          };
        case 4:
          return function(accumulator, value, index, collection) {
            return func.call(context, accumulator, value, index, collection);
          };
      }
      return function() {
        return func.apply(context, arguments);
      };
    };
    var cb = function(value, context, argCount) {
      if (value == null)
        return _.identity;
      if (_.isFunction(value))
        return optimizeCb(value, context, argCount);
      if (_.isObject(value))
        return _.matcher(value);
      return _.property(value);
    };
    _.iteratee = function(value, context) {
      return cb(value, context, Infinity);
    };
    var createAssigner = function(keysFunc, undefinedOnly) {
      return function(obj) {
        var length = arguments.length;
        if (length < 2 || obj == null)
          return obj;
        for (var index = 1; index < length; index++) {
          var source = arguments[index],
              keys = keysFunc(source),
              l = keys.length;
          for (var i = 0; i < l; i++) {
            var key = keys[i];
            if (!undefinedOnly || obj[key] === void 0)
              obj[key] = source[key];
          }
        }
        return obj;
      };
    };
    var baseCreate = function(prototype) {
      if (!_.isObject(prototype))
        return {};
      if (nativeCreate)
        return nativeCreate(prototype);
      Ctor.prototype = prototype;
      var result = new Ctor;
      Ctor.prototype = null;
      return result;
    };
    var property = function(key) {
      return function(obj) {
        return obj == null ? void 0 : obj[key];
      };
    };
    var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
    var getLength = property('length');
    var isArrayLike = function(collection) {
      var length = getLength(collection);
      return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
    };
    _.each = _.forEach = function(obj, iteratee, context) {
      iteratee = optimizeCb(iteratee, context);
      var i,
          length;
      if (isArrayLike(obj)) {
        for (i = 0, length = obj.length; i < length; i++) {
          iteratee(obj[i], i, obj);
        }
      } else {
        var keys = _.keys(obj);
        for (i = 0, length = keys.length; i < length; i++) {
          iteratee(obj[keys[i]], keys[i], obj);
        }
      }
      return obj;
    };
    _.map = _.collect = function(obj, iteratee, context) {
      iteratee = cb(iteratee, context);
      var keys = !isArrayLike(obj) && _.keys(obj),
          length = (keys || obj).length,
          results = Array(length);
      for (var index = 0; index < length; index++) {
        var currentKey = keys ? keys[index] : index;
        results[index] = iteratee(obj[currentKey], currentKey, obj);
      }
      return results;
    };
    function createReduce(dir) {
      function iterator(obj, iteratee, memo, keys, index, length) {
        for (; index >= 0 && index < length; index += dir) {
          var currentKey = keys ? keys[index] : index;
          memo = iteratee(memo, obj[currentKey], currentKey, obj);
        }
        return memo;
      }
      return function(obj, iteratee, memo, context) {
        iteratee = optimizeCb(iteratee, context, 4);
        var keys = !isArrayLike(obj) && _.keys(obj),
            length = (keys || obj).length,
            index = dir > 0 ? 0 : length - 1;
        if (arguments.length < 3) {
          memo = obj[keys ? keys[index] : index];
          index += dir;
        }
        return iterator(obj, iteratee, memo, keys, index, length);
      };
    }
    _.reduce = _.foldl = _.inject = createReduce(1);
    _.reduceRight = _.foldr = createReduce(-1);
    _.find = _.detect = function(obj, predicate, context) {
      var key;
      if (isArrayLike(obj)) {
        key = _.findIndex(obj, predicate, context);
      } else {
        key = _.findKey(obj, predicate, context);
      }
      if (key !== void 0 && key !== -1)
        return obj[key];
    };
    _.filter = _.select = function(obj, predicate, context) {
      var results = [];
      predicate = cb(predicate, context);
      _.each(obj, function(value, index, list) {
        if (predicate(value, index, list))
          results.push(value);
      });
      return results;
    };
    _.reject = function(obj, predicate, context) {
      return _.filter(obj, _.negate(cb(predicate)), context);
    };
    _.every = _.all = function(obj, predicate, context) {
      predicate = cb(predicate, context);
      var keys = !isArrayLike(obj) && _.keys(obj),
          length = (keys || obj).length;
      for (var index = 0; index < length; index++) {
        var currentKey = keys ? keys[index] : index;
        if (!predicate(obj[currentKey], currentKey, obj))
          return false;
      }
      return true;
    };
    _.some = _.any = function(obj, predicate, context) {
      predicate = cb(predicate, context);
      var keys = !isArrayLike(obj) && _.keys(obj),
          length = (keys || obj).length;
      for (var index = 0; index < length; index++) {
        var currentKey = keys ? keys[index] : index;
        if (predicate(obj[currentKey], currentKey, obj))
          return true;
      }
      return false;
    };
    _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
      if (!isArrayLike(obj))
        obj = _.values(obj);
      if (typeof fromIndex != 'number' || guard)
        fromIndex = 0;
      return _.indexOf(obj, item, fromIndex) >= 0;
    };
    _.invoke = function(obj, method) {
      var args = slice.call(arguments, 2);
      var isFunc = _.isFunction(method);
      return _.map(obj, function(value) {
        var func = isFunc ? method : value[method];
        return func == null ? func : func.apply(value, args);
      });
    };
    _.pluck = function(obj, key) {
      return _.map(obj, _.property(key));
    };
    _.where = function(obj, attrs) {
      return _.filter(obj, _.matcher(attrs));
    };
    _.findWhere = function(obj, attrs) {
      return _.find(obj, _.matcher(attrs));
    };
    _.max = function(obj, iteratee, context) {
      var result = -Infinity,
          lastComputed = -Infinity,
          value,
          computed;
      if (iteratee == null && obj != null) {
        obj = isArrayLike(obj) ? obj : _.values(obj);
        for (var i = 0,
            length = obj.length; i < length; i++) {
          value = obj[i];
          if (value > result) {
            result = value;
          }
        }
      } else {
        iteratee = cb(iteratee, context);
        _.each(obj, function(value, index, list) {
          computed = iteratee(value, index, list);
          if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
            result = value;
            lastComputed = computed;
          }
        });
      }
      return result;
    };
    _.min = function(obj, iteratee, context) {
      var result = Infinity,
          lastComputed = Infinity,
          value,
          computed;
      if (iteratee == null && obj != null) {
        obj = isArrayLike(obj) ? obj : _.values(obj);
        for (var i = 0,
            length = obj.length; i < length; i++) {
          value = obj[i];
          if (value < result) {
            result = value;
          }
        }
      } else {
        iteratee = cb(iteratee, context);
        _.each(obj, function(value, index, list) {
          computed = iteratee(value, index, list);
          if (computed < lastComputed || computed === Infinity && result === Infinity) {
            result = value;
            lastComputed = computed;
          }
        });
      }
      return result;
    };
    _.shuffle = function(obj) {
      var set = isArrayLike(obj) ? obj : _.values(obj);
      var length = set.length;
      var shuffled = Array(length);
      for (var index = 0,
          rand; index < length; index++) {
        rand = _.random(0, index);
        if (rand !== index)
          shuffled[index] = shuffled[rand];
        shuffled[rand] = set[index];
      }
      return shuffled;
    };
    _.sample = function(obj, n, guard) {
      if (n == null || guard) {
        if (!isArrayLike(obj))
          obj = _.values(obj);
        return obj[_.random(obj.length - 1)];
      }
      return _.shuffle(obj).slice(0, Math.max(0, n));
    };
    _.sortBy = function(obj, iteratee, context) {
      iteratee = cb(iteratee, context);
      return _.pluck(_.map(obj, function(value, index, list) {
        return {
          value: value,
          index: index,
          criteria: iteratee(value, index, list)
        };
      }).sort(function(left, right) {
        var a = left.criteria;
        var b = right.criteria;
        if (a !== b) {
          if (a > b || a === void 0)
            return 1;
          if (a < b || b === void 0)
            return -1;
        }
        return left.index - right.index;
      }), 'value');
    };
    var group = function(behavior) {
      return function(obj, iteratee, context) {
        var result = {};
        iteratee = cb(iteratee, context);
        _.each(obj, function(value, index) {
          var key = iteratee(value, index, obj);
          behavior(result, value, key);
        });
        return result;
      };
    };
    _.groupBy = group(function(result, value, key) {
      if (_.has(result, key))
        result[key].push(value);
      else
        result[key] = [value];
    });
    _.indexBy = group(function(result, value, key) {
      result[key] = value;
    });
    _.countBy = group(function(result, value, key) {
      if (_.has(result, key))
        result[key]++;
      else
        result[key] = 1;
    });
    _.toArray = function(obj) {
      if (!obj)
        return [];
      if (_.isArray(obj))
        return slice.call(obj);
      if (isArrayLike(obj))
        return _.map(obj, _.identity);
      return _.values(obj);
    };
    _.size = function(obj) {
      if (obj == null)
        return 0;
      return isArrayLike(obj) ? obj.length : _.keys(obj).length;
    };
    _.partition = function(obj, predicate, context) {
      predicate = cb(predicate, context);
      var pass = [],
          fail = [];
      _.each(obj, function(value, key, obj) {
        (predicate(value, key, obj) ? pass : fail).push(value);
      });
      return [pass, fail];
    };
    _.first = _.head = _.take = function(array, n, guard) {
      if (array == null)
        return void 0;
      if (n == null || guard)
        return array[0];
      return _.initial(array, array.length - n);
    };
    _.initial = function(array, n, guard) {
      return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
    };
    _.last = function(array, n, guard) {
      if (array == null)
        return void 0;
      if (n == null || guard)
        return array[array.length - 1];
      return _.rest(array, Math.max(0, array.length - n));
    };
    _.rest = _.tail = _.drop = function(array, n, guard) {
      return slice.call(array, n == null || guard ? 1 : n);
    };
    _.compact = function(array) {
      return _.filter(array, _.identity);
    };
    var flatten = function(input, shallow, strict, startIndex) {
      var output = [],
          idx = 0;
      for (var i = startIndex || 0,
          length = getLength(input); i < length; i++) {
        var value = input[i];
        if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
          if (!shallow)
            value = flatten(value, shallow, strict);
          var j = 0,
              len = value.length;
          output.length += len;
          while (j < len) {
            output[idx++] = value[j++];
          }
        } else if (!strict) {
          output[idx++] = value;
        }
      }
      return output;
    };
    _.flatten = function(array, shallow) {
      return flatten(array, shallow, false);
    };
    _.without = function(array) {
      return _.difference(array, slice.call(arguments, 1));
    };
    _.uniq = _.unique = function(array, isSorted, iteratee, context) {
      if (!_.isBoolean(isSorted)) {
        context = iteratee;
        iteratee = isSorted;
        isSorted = false;
      }
      if (iteratee != null)
        iteratee = cb(iteratee, context);
      var result = [];
      var seen = [];
      for (var i = 0,
          length = getLength(array); i < length; i++) {
        var value = array[i],
            computed = iteratee ? iteratee(value, i, array) : value;
        if (isSorted) {
          if (!i || seen !== computed)
            result.push(value);
          seen = computed;
        } else if (iteratee) {
          if (!_.contains(seen, computed)) {
            seen.push(computed);
            result.push(value);
          }
        } else if (!_.contains(result, value)) {
          result.push(value);
        }
      }
      return result;
    };
    _.union = function() {
      return _.uniq(flatten(arguments, true, true));
    };
    _.intersection = function(array) {
      var result = [];
      var argsLength = arguments.length;
      for (var i = 0,
          length = getLength(array); i < length; i++) {
        var item = array[i];
        if (_.contains(result, item))
          continue;
        for (var j = 1; j < argsLength; j++) {
          if (!_.contains(arguments[j], item))
            break;
        }
        if (j === argsLength)
          result.push(item);
      }
      return result;
    };
    _.difference = function(array) {
      var rest = flatten(arguments, true, true, 1);
      return _.filter(array, function(value) {
        return !_.contains(rest, value);
      });
    };
    _.zip = function() {
      return _.unzip(arguments);
    };
    _.unzip = function(array) {
      var length = array && _.max(array, getLength).length || 0;
      var result = Array(length);
      for (var index = 0; index < length; index++) {
        result[index] = _.pluck(array, index);
      }
      return result;
    };
    _.object = function(list, values) {
      var result = {};
      for (var i = 0,
          length = getLength(list); i < length; i++) {
        if (values) {
          result[list[i]] = values[i];
        } else {
          result[list[i][0]] = list[i][1];
        }
      }
      return result;
    };
    function createPredicateIndexFinder(dir) {
      return function(array, predicate, context) {
        predicate = cb(predicate, context);
        var length = getLength(array);
        var index = dir > 0 ? 0 : length - 1;
        for (; index >= 0 && index < length; index += dir) {
          if (predicate(array[index], index, array))
            return index;
        }
        return -1;
      };
    }
    _.findIndex = createPredicateIndexFinder(1);
    _.findLastIndex = createPredicateIndexFinder(-1);
    _.sortedIndex = function(array, obj, iteratee, context) {
      iteratee = cb(iteratee, context, 1);
      var value = iteratee(obj);
      var low = 0,
          high = getLength(array);
      while (low < high) {
        var mid = Math.floor((low + high) / 2);
        if (iteratee(array[mid]) < value)
          low = mid + 1;
        else
          high = mid;
      }
      return low;
    };
    function createIndexFinder(dir, predicateFind, sortedIndex) {
      return function(array, item, idx) {
        var i = 0,
            length = getLength(array);
        if (typeof idx == 'number') {
          if (dir > 0) {
            i = idx >= 0 ? idx : Math.max(idx + length, i);
          } else {
            length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
          }
        } else if (sortedIndex && idx && length) {
          idx = sortedIndex(array, item);
          return array[idx] === item ? idx : -1;
        }
        if (item !== item) {
          idx = predicateFind(slice.call(array, i, length), _.isNaN);
          return idx >= 0 ? idx + i : -1;
        }
        for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
          if (array[idx] === item)
            return idx;
        }
        return -1;
      };
    }
    _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
    _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);
    _.range = function(start, stop, step) {
      if (stop == null) {
        stop = start || 0;
        start = 0;
      }
      step = step || 1;
      var length = Math.max(Math.ceil((stop - start) / step), 0);
      var range = Array(length);
      for (var idx = 0; idx < length; idx++, start += step) {
        range[idx] = start;
      }
      return range;
    };
    var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
      if (!(callingContext instanceof boundFunc))
        return sourceFunc.apply(context, args);
      var self = baseCreate(sourceFunc.prototype);
      var result = sourceFunc.apply(self, args);
      if (_.isObject(result))
        return result;
      return self;
    };
    _.bind = function(func, context) {
      if (nativeBind && func.bind === nativeBind)
        return nativeBind.apply(func, slice.call(arguments, 1));
      if (!_.isFunction(func))
        throw new TypeError('Bind must be called on a function');
      var args = slice.call(arguments, 2);
      var bound = function() {
        return executeBound(func, bound, context, this, args.concat(slice.call(arguments)));
      };
      return bound;
    };
    _.partial = function(func) {
      var boundArgs = slice.call(arguments, 1);
      var bound = function() {
        var position = 0,
            length = boundArgs.length;
        var args = Array(length);
        for (var i = 0; i < length; i++) {
          args[i] = boundArgs[i] === _ ? arguments[position++] : boundArgs[i];
        }
        while (position < arguments.length)
          args.push(arguments[position++]);
        return executeBound(func, bound, this, this, args);
      };
      return bound;
    };
    _.bindAll = function(obj) {
      var i,
          length = arguments.length,
          key;
      if (length <= 1)
        throw new Error('bindAll must be passed function names');
      for (i = 1; i < length; i++) {
        key = arguments[i];
        obj[key] = _.bind(obj[key], obj);
      }
      return obj;
    };
    _.memoize = function(func, hasher) {
      var memoize = function(key) {
        var cache = memoize.cache;
        var address = '' + (hasher ? hasher.apply(this, arguments) : key);
        if (!_.has(cache, address))
          cache[address] = func.apply(this, arguments);
        return cache[address];
      };
      memoize.cache = {};
      return memoize;
    };
    _.delay = function(func, wait) {
      var args = slice.call(arguments, 2);
      return setTimeout(function() {
        return func.apply(null, args);
      }, wait);
    };
    _.defer = _.partial(_.delay, _, 1);
    _.throttle = function(func, wait, options) {
      var context,
          args,
          result;
      var timeout = null;
      var previous = 0;
      if (!options)
        options = {};
      var later = function() {
        previous = options.leading === false ? 0 : _.now();
        timeout = null;
        result = func.apply(context, args);
        if (!timeout)
          context = args = null;
      };
      return function() {
        var now = _.now();
        if (!previous && options.leading === false)
          previous = now;
        var remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
          if (timeout) {
            clearTimeout(timeout);
            timeout = null;
          }
          previous = now;
          result = func.apply(context, args);
          if (!timeout)
            context = args = null;
        } else if (!timeout && options.trailing !== false) {
          timeout = setTimeout(later, remaining);
        }
        return result;
      };
    };
    _.debounce = function(func, wait, immediate) {
      var timeout,
          args,
          context,
          timestamp,
          result;
      var later = function() {
        var last = _.now() - timestamp;
        if (last < wait && last >= 0) {
          timeout = setTimeout(later, wait - last);
        } else {
          timeout = null;
          if (!immediate) {
            result = func.apply(context, args);
            if (!timeout)
              context = args = null;
          }
        }
      };
      return function() {
        context = this;
        args = arguments;
        timestamp = _.now();
        var callNow = immediate && !timeout;
        if (!timeout)
          timeout = setTimeout(later, wait);
        if (callNow) {
          result = func.apply(context, args);
          context = args = null;
        }
        return result;
      };
    };
    _.wrap = function(func, wrapper) {
      return _.partial(wrapper, func);
    };
    _.negate = function(predicate) {
      return function() {
        return !predicate.apply(this, arguments);
      };
    };
    _.compose = function() {
      var args = arguments;
      var start = args.length - 1;
      return function() {
        var i = start;
        var result = args[start].apply(this, arguments);
        while (i--)
          result = args[i].call(this, result);
        return result;
      };
    };
    _.after = function(times, func) {
      return function() {
        if (--times < 1) {
          return func.apply(this, arguments);
        }
      };
    };
    _.before = function(times, func) {
      var memo;
      return function() {
        if (--times > 0) {
          memo = func.apply(this, arguments);
        }
        if (times <= 1)
          func = null;
        return memo;
      };
    };
    _.once = _.partial(_.before, 2);
    var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
    var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString', 'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];
    function collectNonEnumProps(obj, keys) {
      var nonEnumIdx = nonEnumerableProps.length;
      var constructor = obj.constructor;
      var proto = (_.isFunction(constructor) && constructor.prototype) || ObjProto;
      var prop = 'constructor';
      if (_.has(obj, prop) && !_.contains(keys, prop))
        keys.push(prop);
      while (nonEnumIdx--) {
        prop = nonEnumerableProps[nonEnumIdx];
        if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
          keys.push(prop);
        }
      }
    }
    _.keys = function(obj) {
      if (!_.isObject(obj))
        return [];
      if (nativeKeys)
        return nativeKeys(obj);
      var keys = [];
      for (var key in obj)
        if (_.has(obj, key))
          keys.push(key);
      if (hasEnumBug)
        collectNonEnumProps(obj, keys);
      return keys;
    };
    _.allKeys = function(obj) {
      if (!_.isObject(obj))
        return [];
      var keys = [];
      for (var key in obj)
        keys.push(key);
      if (hasEnumBug)
        collectNonEnumProps(obj, keys);
      return keys;
    };
    _.values = function(obj) {
      var keys = _.keys(obj);
      var length = keys.length;
      var values = Array(length);
      for (var i = 0; i < length; i++) {
        values[i] = obj[keys[i]];
      }
      return values;
    };
    _.mapObject = function(obj, iteratee, context) {
      iteratee = cb(iteratee, context);
      var keys = _.keys(obj),
          length = keys.length,
          results = {},
          currentKey;
      for (var index = 0; index < length; index++) {
        currentKey = keys[index];
        results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
      }
      return results;
    };
    _.pairs = function(obj) {
      var keys = _.keys(obj);
      var length = keys.length;
      var pairs = Array(length);
      for (var i = 0; i < length; i++) {
        pairs[i] = [keys[i], obj[keys[i]]];
      }
      return pairs;
    };
    _.invert = function(obj) {
      var result = {};
      var keys = _.keys(obj);
      for (var i = 0,
          length = keys.length; i < length; i++) {
        result[obj[keys[i]]] = keys[i];
      }
      return result;
    };
    _.functions = _.methods = function(obj) {
      var names = [];
      for (var key in obj) {
        if (_.isFunction(obj[key]))
          names.push(key);
      }
      return names.sort();
    };
    _.extend = createAssigner(_.allKeys);
    _.extendOwn = _.assign = createAssigner(_.keys);
    _.findKey = function(obj, predicate, context) {
      predicate = cb(predicate, context);
      var keys = _.keys(obj),
          key;
      for (var i = 0,
          length = keys.length; i < length; i++) {
        key = keys[i];
        if (predicate(obj[key], key, obj))
          return key;
      }
    };
    _.pick = function(object, oiteratee, context) {
      var result = {},
          obj = object,
          iteratee,
          keys;
      if (obj == null)
        return result;
      if (_.isFunction(oiteratee)) {
        keys = _.allKeys(obj);
        iteratee = optimizeCb(oiteratee, context);
      } else {
        keys = flatten(arguments, false, false, 1);
        iteratee = function(value, key, obj) {
          return key in obj;
        };
        obj = Object(obj);
      }
      for (var i = 0,
          length = keys.length; i < length; i++) {
        var key = keys[i];
        var value = obj[key];
        if (iteratee(value, key, obj))
          result[key] = value;
      }
      return result;
    };
    _.omit = function(obj, iteratee, context) {
      if (_.isFunction(iteratee)) {
        iteratee = _.negate(iteratee);
      } else {
        var keys = _.map(flatten(arguments, false, false, 1), String);
        iteratee = function(value, key) {
          return !_.contains(keys, key);
        };
      }
      return _.pick(obj, iteratee, context);
    };
    _.defaults = createAssigner(_.allKeys, true);
    _.create = function(prototype, props) {
      var result = baseCreate(prototype);
      if (props)
        _.extendOwn(result, props);
      return result;
    };
    _.clone = function(obj) {
      if (!_.isObject(obj))
        return obj;
      return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
    };
    _.tap = function(obj, interceptor) {
      interceptor(obj);
      return obj;
    };
    _.isMatch = function(object, attrs) {
      var keys = _.keys(attrs),
          length = keys.length;
      if (object == null)
        return !length;
      var obj = Object(object);
      for (var i = 0; i < length; i++) {
        var key = keys[i];
        if (attrs[key] !== obj[key] || !(key in obj))
          return false;
      }
      return true;
    };
    var eq = function(a, b, aStack, bStack) {
      if (a === b)
        return a !== 0 || 1 / a === 1 / b;
      if (a == null || b == null)
        return a === b;
      if (a instanceof _)
        a = a._wrapped;
      if (b instanceof _)
        b = b._wrapped;
      var className = toString.call(a);
      if (className !== toString.call(b))
        return false;
      switch (className) {
        case '[object RegExp]':
        case '[object String]':
          return '' + a === '' + b;
        case '[object Number]':
          if (+a !== +a)
            return +b !== +b;
          return +a === 0 ? 1 / +a === 1 / b : +a === +b;
        case '[object Date]':
        case '[object Boolean]':
          return +a === +b;
      }
      var areArrays = className === '[object Array]';
      if (!areArrays) {
        if (typeof a != 'object' || typeof b != 'object')
          return false;
        var aCtor = a.constructor,
            bCtor = b.constructor;
        if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor && _.isFunction(bCtor) && bCtor instanceof bCtor) && ('constructor' in a && 'constructor' in b)) {
          return false;
        }
      }
      aStack = aStack || [];
      bStack = bStack || [];
      var length = aStack.length;
      while (length--) {
        if (aStack[length] === a)
          return bStack[length] === b;
      }
      aStack.push(a);
      bStack.push(b);
      if (areArrays) {
        length = a.length;
        if (length !== b.length)
          return false;
        while (length--) {
          if (!eq(a[length], b[length], aStack, bStack))
            return false;
        }
      } else {
        var keys = _.keys(a),
            key;
        length = keys.length;
        if (_.keys(b).length !== length)
          return false;
        while (length--) {
          key = keys[length];
          if (!(_.has(b, key) && eq(a[key], b[key], aStack, bStack)))
            return false;
        }
      }
      aStack.pop();
      bStack.pop();
      return true;
    };
    _.isEqual = function(a, b) {
      return eq(a, b);
    };
    _.isEmpty = function(obj) {
      if (obj == null)
        return true;
      if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj)))
        return obj.length === 0;
      return _.keys(obj).length === 0;
    };
    _.isElement = function(obj) {
      return !!(obj && obj.nodeType === 1);
    };
    _.isArray = nativeIsArray || function(obj) {
      return toString.call(obj) === '[object Array]';
    };
    _.isObject = function(obj) {
      var type = typeof obj;
      return type === 'function' || type === 'object' && !!obj;
    };
    _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error'], function(name) {
      _['is' + name] = function(obj) {
        return toString.call(obj) === '[object ' + name + ']';
      };
    });
    if (!_.isArguments(arguments)) {
      _.isArguments = function(obj) {
        return _.has(obj, 'callee');
      };
    }
    if (typeof/./ != 'function' && typeof Int8Array != 'object') {
      _.isFunction = function(obj) {
        return typeof obj == 'function' || false;
      };
    }
    _.isFinite = function(obj) {
      return isFinite(obj) && !isNaN(parseFloat(obj));
    };
    _.isNaN = function(obj) {
      return _.isNumber(obj) && obj !== +obj;
    };
    _.isBoolean = function(obj) {
      return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
    };
    _.isNull = function(obj) {
      return obj === null;
    };
    _.isUndefined = function(obj) {
      return obj === void 0;
    };
    _.has = function(obj, key) {
      return obj != null && hasOwnProperty.call(obj, key);
    };
    _.noConflict = function() {
      root._ = previousUnderscore;
      return this;
    };
    _.identity = function(value) {
      return value;
    };
    _.constant = function(value) {
      return function() {
        return value;
      };
    };
    _.noop = function() {};
    _.property = property;
    _.propertyOf = function(obj) {
      return obj == null ? function() {} : function(key) {
        return obj[key];
      };
    };
    _.matcher = _.matches = function(attrs) {
      attrs = _.extendOwn({}, attrs);
      return function(obj) {
        return _.isMatch(obj, attrs);
      };
    };
    _.times = function(n, iteratee, context) {
      var accum = Array(Math.max(0, n));
      iteratee = optimizeCb(iteratee, context, 1);
      for (var i = 0; i < n; i++)
        accum[i] = iteratee(i);
      return accum;
    };
    _.random = function(min, max) {
      if (max == null) {
        max = min;
        min = 0;
      }
      return min + Math.floor(Math.random() * (max - min + 1));
    };
    _.now = Date.now || function() {
      return new Date().getTime();
    };
    var escapeMap = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;',
      '`': '&#x60;'
    };
    var unescapeMap = _.invert(escapeMap);
    var createEscaper = function(map) {
      var escaper = function(match) {
        return map[match];
      };
      var source = '(?:' + _.keys(map).join('|') + ')';
      var testRegexp = RegExp(source);
      var replaceRegexp = RegExp(source, 'g');
      return function(string) {
        string = string == null ? '' : '' + string;
        return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
      };
    };
    _.escape = createEscaper(escapeMap);
    _.unescape = createEscaper(unescapeMap);
    _.result = function(object, property, fallback) {
      var value = object == null ? void 0 : object[property];
      if (value === void 0) {
        value = fallback;
      }
      return _.isFunction(value) ? value.call(object) : value;
    };
    var idCounter = 0;
    _.uniqueId = function(prefix) {
      var id = ++idCounter + '';
      return prefix ? prefix + id : id;
    };
    _.templateSettings = {
      evaluate: /<%([\s\S]+?)%>/g,
      interpolate: /<%=([\s\S]+?)%>/g,
      escape: /<%-([\s\S]+?)%>/g
    };
    var noMatch = /(.)^/;
    var escapes = {
      "'": "'",
      '\\': '\\',
      '\r': 'r',
      '\n': 'n',
      '\u2028': 'u2028',
      '\u2029': 'u2029'
    };
    var escaper = /\\|'|\r|\n|\u2028|\u2029/g;
    var escapeChar = function(match) {
      return '\\' + escapes[match];
    };
    _.template = function(text, settings, oldSettings) {
      if (!settings && oldSettings)
        settings = oldSettings;
      settings = _.defaults({}, settings, _.templateSettings);
      var matcher = RegExp([(settings.escape || noMatch).source, (settings.interpolate || noMatch).source, (settings.evaluate || noMatch).source].join('|') + '|$', 'g');
      var index = 0;
      var source = "__p+='";
      text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
        source += text.slice(index, offset).replace(escaper, escapeChar);
        index = offset + match.length;
        if (escape) {
          source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
        } else if (interpolate) {
          source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
        } else if (evaluate) {
          source += "';\n" + evaluate + "\n__p+='";
        }
        return match;
      });
      source += "';\n";
      if (!settings.variable)
        source = 'with(obj||{}){\n' + source + '}\n';
      source = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + source + 'return __p;\n';
      try {
        var render = new Function(settings.variable || 'obj', '_', source);
      } catch (e) {
        e.source = source;
        throw e;
      }
      var template = function(data) {
        return render.call(this, data, _);
      };
      var argument = settings.variable || 'obj';
      template.source = 'function(' + argument + '){\n' + source + '}';
      return template;
    };
    _.chain = function(obj) {
      var instance = _(obj);
      instance._chain = true;
      return instance;
    };
    var result = function(instance, obj) {
      return instance._chain ? _(obj).chain() : obj;
    };
    _.mixin = function(obj) {
      _.each(_.functions(obj), function(name) {
        var func = _[name] = obj[name];
        _.prototype[name] = function() {
          var args = [this._wrapped];
          push.apply(args, arguments);
          return result(this, func.apply(_, args));
        };
      });
    };
    _.mixin(_);
    _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
      var method = ArrayProto[name];
      _.prototype[name] = function() {
        var obj = this._wrapped;
        method.apply(obj, arguments);
        if ((name === 'shift' || name === 'splice') && obj.length === 0)
          delete obj[0];
        return result(this, obj);
      };
    });
    _.each(['concat', 'join', 'slice'], function(name) {
      var method = ArrayProto[name];
      _.prototype[name] = function() {
        return result(this, method.apply(this._wrapped, arguments));
      };
    });
    _.prototype.value = function() {
      return this._wrapped;
    };
    _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;
    _.prototype.toString = function() {
      return '' + this._wrapped;
    };
    if (typeof define === 'function' && define.amd) {
      define('underscore', [], function() {
        return _;
      });
    }
  }.call(this));
  global.define = __define;
  return module.exports;
});



System.register("npm:ws@0.7.1/lib/browser", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var global = (function() {
    return this;
  })();
  var WebSocket = global.WebSocket || global.MozWebSocket;
  module.exports = WebSocket ? ws : null;
  function ws(uri, protocols, opts) {
    var instance;
    if (protocols) {
      instance = new WebSocket(uri, protocols);
    } else {
      instance = new WebSocket(uri);
    }
    return instance;
  }
  if (WebSocket)
    ws.prototype = WebSocket.prototype;
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/CircularBuffer", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var CircularBuffer = module.exports = function(size) {
    this.pos = 0;
    this._buf = [];
    this.size = size;
  };
  CircularBuffer.prototype.get = function(i) {
    if (i == undefined)
      i = 0;
    if (i >= this.size)
      return undefined;
    if (i >= this._buf.length)
      return undefined;
    return this._buf[(this.pos - i - 1) % this.size];
  };
  CircularBuffer.prototype.push = function(o) {
    this._buf[this.pos % this.size] = o;
    return this.pos++;
  };
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/Version", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = {
    full: "0.8.17",
    major: 0,
    minor: 8,
    dot: 1
  };
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/Entity", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var entities = [];
  function get(id) {
    return entities[id];
  }
  function set(id, entity) {
    entities[id] = entity;
  }
  function register(entity) {
    var id = entities.length;
    set(id, entity);
    return id;
  }
  function unregister(id) {
    set(id, null);
  }
  module.exports = {
    register: register,
    unregister: unregister,
    get: get,
    set: set
  };
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/Transform", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Transform = {};
  Transform.precision = 0.000001;
  Transform.identity = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
  Transform.multiply4x4 = function multiply4x4(a, b) {
    return [a[0] * b[0] + a[4] * b[1] + a[8] * b[2] + a[12] * b[3], a[1] * b[0] + a[5] * b[1] + a[9] * b[2] + a[13] * b[3], a[2] * b[0] + a[6] * b[1] + a[10] * b[2] + a[14] * b[3], a[3] * b[0] + a[7] * b[1] + a[11] * b[2] + a[15] * b[3], a[0] * b[4] + a[4] * b[5] + a[8] * b[6] + a[12] * b[7], a[1] * b[4] + a[5] * b[5] + a[9] * b[6] + a[13] * b[7], a[2] * b[4] + a[6] * b[5] + a[10] * b[6] + a[14] * b[7], a[3] * b[4] + a[7] * b[5] + a[11] * b[6] + a[15] * b[7], a[0] * b[8] + a[4] * b[9] + a[8] * b[10] + a[12] * b[11], a[1] * b[8] + a[5] * b[9] + a[9] * b[10] + a[13] * b[11], a[2] * b[8] + a[6] * b[9] + a[10] * b[10] + a[14] * b[11], a[3] * b[8] + a[7] * b[9] + a[11] * b[10] + a[15] * b[11], a[0] * b[12] + a[4] * b[13] + a[8] * b[14] + a[12] * b[15], a[1] * b[12] + a[5] * b[13] + a[9] * b[14] + a[13] * b[15], a[2] * b[12] + a[6] * b[13] + a[10] * b[14] + a[14] * b[15], a[3] * b[12] + a[7] * b[13] + a[11] * b[14] + a[15] * b[15]];
  };
  Transform.multiply = function multiply(a, b) {
    return [a[0] * b[0] + a[4] * b[1] + a[8] * b[2], a[1] * b[0] + a[5] * b[1] + a[9] * b[2], a[2] * b[0] + a[6] * b[1] + a[10] * b[2], 0, a[0] * b[4] + a[4] * b[5] + a[8] * b[6], a[1] * b[4] + a[5] * b[5] + a[9] * b[6], a[2] * b[4] + a[6] * b[5] + a[10] * b[6], 0, a[0] * b[8] + a[4] * b[9] + a[8] * b[10], a[1] * b[8] + a[5] * b[9] + a[9] * b[10], a[2] * b[8] + a[6] * b[9] + a[10] * b[10], 0, a[0] * b[12] + a[4] * b[13] + a[8] * b[14] + a[12], a[1] * b[12] + a[5] * b[13] + a[9] * b[14] + a[13], a[2] * b[12] + a[6] * b[13] + a[10] * b[14] + a[14], 1];
  };
  Transform.thenMove = function thenMove(m, t) {
    if (!t[2])
      t[2] = 0;
    return [m[0], m[1], m[2], 0, m[4], m[5], m[6], 0, m[8], m[9], m[10], 0, m[12] + t[0], m[13] + t[1], m[14] + t[2], 1];
  };
  Transform.moveThen = function moveThen(v, m) {
    if (!v[2])
      v[2] = 0;
    var t0 = v[0] * m[0] + v[1] * m[4] + v[2] * m[8];
    var t1 = v[0] * m[1] + v[1] * m[5] + v[2] * m[9];
    var t2 = v[0] * m[2] + v[1] * m[6] + v[2] * m[10];
    return Transform.thenMove(m, [t0, t1, t2]);
  };
  Transform.translate = function translate(x, y, z) {
    if (z === undefined)
      z = 0;
    return [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, x, y, z, 1];
  };
  Transform.thenScale = function thenScale(m, s) {
    return [s[0] * m[0], s[1] * m[1], s[2] * m[2], 0, s[0] * m[4], s[1] * m[5], s[2] * m[6], 0, s[0] * m[8], s[1] * m[9], s[2] * m[10], 0, s[0] * m[12], s[1] * m[13], s[2] * m[14], 1];
  };
  Transform.scale = function scale(x, y, z) {
    if (z === undefined)
      z = 1;
    if (y === undefined)
      y = x;
    return [x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1];
  };
  Transform.rotateX = function rotateX(theta) {
    var cosTheta = Math.cos(theta);
    var sinTheta = Math.sin(theta);
    return [1, 0, 0, 0, 0, cosTheta, sinTheta, 0, 0, -sinTheta, cosTheta, 0, 0, 0, 0, 1];
  };
  Transform.rotateY = function rotateY(theta) {
    var cosTheta = Math.cos(theta);
    var sinTheta = Math.sin(theta);
    return [cosTheta, 0, -sinTheta, 0, 0, 1, 0, 0, sinTheta, 0, cosTheta, 0, 0, 0, 0, 1];
  };
  Transform.rotateZ = function rotateZ(theta) {
    var cosTheta = Math.cos(theta);
    var sinTheta = Math.sin(theta);
    return [cosTheta, sinTheta, 0, 0, -sinTheta, cosTheta, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
  };
  Transform.rotate = function rotate(phi, theta, psi) {
    var cosPhi = Math.cos(phi);
    var sinPhi = Math.sin(phi);
    var cosTheta = Math.cos(theta);
    var sinTheta = Math.sin(theta);
    var cosPsi = Math.cos(psi);
    var sinPsi = Math.sin(psi);
    var result = [cosTheta * cosPsi, cosPhi * sinPsi + sinPhi * sinTheta * cosPsi, sinPhi * sinPsi - cosPhi * sinTheta * cosPsi, 0, -cosTheta * sinPsi, cosPhi * cosPsi - sinPhi * sinTheta * sinPsi, sinPhi * cosPsi + cosPhi * sinTheta * sinPsi, 0, sinTheta, -sinPhi * cosTheta, cosPhi * cosTheta, 0, 0, 0, 0, 1];
    return result;
  };
  Transform.rotateAxis = function rotateAxis(v, theta) {
    var sinTheta = Math.sin(theta);
    var cosTheta = Math.cos(theta);
    var verTheta = 1 - cosTheta;
    var xxV = v[0] * v[0] * verTheta;
    var xyV = v[0] * v[1] * verTheta;
    var xzV = v[0] * v[2] * verTheta;
    var yyV = v[1] * v[1] * verTheta;
    var yzV = v[1] * v[2] * verTheta;
    var zzV = v[2] * v[2] * verTheta;
    var xs = v[0] * sinTheta;
    var ys = v[1] * sinTheta;
    var zs = v[2] * sinTheta;
    var result = [xxV + cosTheta, xyV + zs, xzV - ys, 0, xyV - zs, yyV + cosTheta, yzV + xs, 0, xzV + ys, yzV - xs, zzV + cosTheta, 0, 0, 0, 0, 1];
    return result;
  };
  Transform.aboutOrigin = function aboutOrigin(v, m) {
    var t0 = v[0] - (v[0] * m[0] + v[1] * m[4] + v[2] * m[8]);
    var t1 = v[1] - (v[0] * m[1] + v[1] * m[5] + v[2] * m[9]);
    var t2 = v[2] - (v[0] * m[2] + v[1] * m[6] + v[2] * m[10]);
    return Transform.thenMove(m, [t0, t1, t2]);
  };
  Transform.skew = function skew(phi, theta, psi) {
    return [1, Math.tan(theta), 0, 0, Math.tan(psi), 1, 0, 0, 0, Math.tan(phi), 1, 0, 0, 0, 0, 1];
  };
  Transform.skewX = function skewX(angle) {
    return [1, 0, 0, 0, Math.tan(angle), 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
  };
  Transform.skewY = function skewY(angle) {
    return [1, Math.tan(angle), 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
  };
  Transform.perspective = function perspective(focusZ) {
    return [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -1 / focusZ, 0, 0, 0, 1];
  };
  Transform.getTranslate = function getTranslate(m) {
    return [m[12], m[13], m[14]];
  };
  Transform.inverse = function inverse(m) {
    var c0 = m[5] * m[10] - m[6] * m[9];
    var c1 = m[4] * m[10] - m[6] * m[8];
    var c2 = m[4] * m[9] - m[5] * m[8];
    var c4 = m[1] * m[10] - m[2] * m[9];
    var c5 = m[0] * m[10] - m[2] * m[8];
    var c6 = m[0] * m[9] - m[1] * m[8];
    var c8 = m[1] * m[6] - m[2] * m[5];
    var c9 = m[0] * m[6] - m[2] * m[4];
    var c10 = m[0] * m[5] - m[1] * m[4];
    var detM = m[0] * c0 - m[1] * c1 + m[2] * c2;
    var invD = 1 / detM;
    var result = [invD * c0, -invD * c4, invD * c8, 0, -invD * c1, invD * c5, -invD * c9, 0, invD * c2, -invD * c6, invD * c10, 0, 0, 0, 0, 1];
    result[12] = -m[12] * result[0] - m[13] * result[4] - m[14] * result[8];
    result[13] = -m[12] * result[1] - m[13] * result[5] - m[14] * result[9];
    result[14] = -m[12] * result[2] - m[13] * result[6] - m[14] * result[10];
    return result;
  };
  Transform.transpose = function transpose(m) {
    return [m[0], m[4], m[8], m[12], m[1], m[5], m[9], m[13], m[2], m[6], m[10], m[14], m[3], m[7], m[11], m[15]];
  };
  function _normSquared(v) {
    return v.length === 2 ? v[0] * v[0] + v[1] * v[1] : v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
  }
  function _norm(v) {
    return Math.sqrt(_normSquared(v));
  }
  function _sign(n) {
    return n < 0 ? -1 : 1;
  }
  Transform.interpret = function interpret(M) {
    var x = [M[0], M[1], M[2]];
    var sgn = _sign(x[0]);
    var xNorm = _norm(x);
    var v = [x[0] + sgn * xNorm, x[1], x[2]];
    var mult = 2 / _normSquared(v);
    if (mult >= Infinity) {
      return {
        translate: Transform.getTranslate(M),
        rotate: [0, 0, 0],
        scale: [0, 0, 0],
        skew: [0, 0, 0]
      };
    }
    var Q1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
    Q1[0] = 1 - mult * v[0] * v[0];
    Q1[5] = 1 - mult * v[1] * v[1];
    Q1[10] = 1 - mult * v[2] * v[2];
    Q1[1] = -mult * v[0] * v[1];
    Q1[2] = -mult * v[0] * v[2];
    Q1[6] = -mult * v[1] * v[2];
    Q1[4] = Q1[1];
    Q1[8] = Q1[2];
    Q1[9] = Q1[6];
    var MQ1 = Transform.multiply(Q1, M);
    var x2 = [MQ1[5], MQ1[6]];
    var sgn2 = _sign(x2[0]);
    var x2Norm = _norm(x2);
    var v2 = [x2[0] + sgn2 * x2Norm, x2[1]];
    var mult2 = 2 / _normSquared(v2);
    var Q2 = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
    Q2[5] = 1 - mult2 * v2[0] * v2[0];
    Q2[10] = 1 - mult2 * v2[1] * v2[1];
    Q2[6] = -mult2 * v2[0] * v2[1];
    Q2[9] = Q2[6];
    var Q = Transform.multiply(Q2, Q1);
    var R = Transform.multiply(Q, M);
    var remover = Transform.scale(R[0] < 0 ? -1 : 1, R[5] < 0 ? -1 : 1, R[10] < 0 ? -1 : 1);
    R = Transform.multiply(R, remover);
    Q = Transform.multiply(remover, Q);
    var result = {};
    result.translate = Transform.getTranslate(M);
    result.rotate = [Math.atan2(-Q[6], Q[10]), Math.asin(Q[2]), Math.atan2(-Q[1], Q[0])];
    if (!result.rotate[0]) {
      result.rotate[0] = 0;
      result.rotate[2] = Math.atan2(Q[4], Q[5]);
    }
    result.scale = [R[0], R[5], R[10]];
    result.skew = [Math.atan2(R[9], result.scale[2]), Math.atan2(R[8], result.scale[2]), Math.atan2(R[4], result.scale[0])];
    if (Math.abs(result.rotate[0]) + Math.abs(result.rotate[2]) > 1.5 * Math.PI) {
      result.rotate[1] = Math.PI - result.rotate[1];
      if (result.rotate[1] > Math.PI)
        result.rotate[1] -= 2 * Math.PI;
      if (result.rotate[1] < -Math.PI)
        result.rotate[1] += 2 * Math.PI;
      if (result.rotate[0] < 0)
        result.rotate[0] += Math.PI;
      else
        result.rotate[0] -= Math.PI;
      if (result.rotate[2] < 0)
        result.rotate[2] += Math.PI;
      else
        result.rotate[2] -= Math.PI;
    }
    return result;
  };
  Transform.average = function average(M1, M2, t) {
    t = t === undefined ? 0.5 : t;
    var specM1 = Transform.interpret(M1);
    var specM2 = Transform.interpret(M2);
    var specAvg = {
      translate: [0, 0, 0],
      rotate: [0, 0, 0],
      scale: [0, 0, 0],
      skew: [0, 0, 0]
    };
    for (var i = 0; i < 3; i++) {
      specAvg.translate[i] = (1 - t) * specM1.translate[i] + t * specM2.translate[i];
      specAvg.rotate[i] = (1 - t) * specM1.rotate[i] + t * specM2.rotate[i];
      specAvg.scale[i] = (1 - t) * specM1.scale[i] + t * specM2.scale[i];
      specAvg.skew[i] = (1 - t) * specM1.skew[i] + t * specM2.skew[i];
    }
    return Transform.build(specAvg);
  };
  Transform.build = function build(spec) {
    var scaleMatrix = Transform.scale(spec.scale[0], spec.scale[1], spec.scale[2]);
    var skewMatrix = Transform.skew(spec.skew[0], spec.skew[1], spec.skew[2]);
    var rotateMatrix = Transform.rotate(spec.rotate[0], spec.rotate[1], spec.rotate[2]);
    return Transform.thenMove(Transform.multiply(Transform.multiply(rotateMatrix, skewMatrix), scaleMatrix), spec.translate);
  };
  Transform.equals = function equals(a, b) {
    return !Transform.notEquals(a, b);
  };
  Transform.notEquals = function notEquals(a, b) {
    if (a === b)
      return false;
    return !(a && b) || a[12] !== b[12] || a[13] !== b[13] || a[14] !== b[14] || a[0] !== b[0] || a[1] !== b[1] || a[2] !== b[2] || a[4] !== b[4] || a[5] !== b[5] || a[6] !== b[6] || a[8] !== b[8] || a[9] !== b[9] || a[10] !== b[10];
  };
  Transform.normalizeRotation = function normalizeRotation(rotation) {
    var result = rotation.slice(0);
    if (result[0] === Math.PI * 0.5 || result[0] === -Math.PI * 0.5) {
      result[0] = -result[0];
      result[1] = Math.PI - result[1];
      result[2] -= Math.PI;
    }
    if (result[0] > Math.PI * 0.5) {
      result[0] = result[0] - Math.PI;
      result[1] = Math.PI - result[1];
      result[2] -= Math.PI;
    }
    if (result[0] < -Math.PI * 0.5) {
      result[0] = result[0] + Math.PI;
      result[1] = -Math.PI - result[1];
      result[2] -= Math.PI;
    }
    while (result[1] < -Math.PI)
      result[1] += 2 * Math.PI;
    while (result[1] >= Math.PI)
      result[1] -= 2 * Math.PI;
    while (result[2] < -Math.PI)
      result[2] += 2 * Math.PI;
    while (result[2] >= Math.PI)
      result[2] -= 2 * Math.PI;
    return result;
  };
  Transform.inFront = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0.001, 1];
  Transform.behind = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, -0.001, 1];
  module.exports = Transform;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/EventEmitter", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  function EventEmitter() {
    this.listeners = {};
    this._owner = this;
  }
  EventEmitter.prototype.emit = function emit(type, event) {
    var handlers = this.listeners[type];
    if (handlers) {
      for (var i = 0; i < handlers.length; i++) {
        handlers[i].call(this._owner, event);
      }
    }
    return this;
  };
  EventEmitter.prototype.on = function on(type, handler) {
    if (!(type in this.listeners))
      this.listeners[type] = [];
    var index = this.listeners[type].indexOf(handler);
    if (index < 0)
      this.listeners[type].push(handler);
    return this;
  };
  EventEmitter.prototype.addListener = EventEmitter.prototype.on;
  EventEmitter.prototype.removeListener = function removeListener(type, handler) {
    var listener = this.listeners[type];
    if (listener !== undefined) {
      var index = listener.indexOf(handler);
      if (index >= 0)
        listener.splice(index, 1);
    }
    return this;
  };
  EventEmitter.prototype.bindThis = function bindThis(owner) {
    this._owner = owner;
  };
  module.exports = EventEmitter;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/ElementAllocator", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  function ElementAllocator(container) {
    if (!container)
      container = document.createDocumentFragment();
    this.container = container;
    this.detachedNodes = {};
    this.nodeCount = 0;
  }
  ElementAllocator.prototype.migrate = function migrate(container) {
    var oldContainer = this.container;
    if (container === oldContainer)
      return ;
    if (oldContainer instanceof DocumentFragment) {
      container.appendChild(oldContainer);
    } else {
      while (oldContainer.hasChildNodes()) {
        container.appendChild(oldContainer.firstChild);
      }
    }
    this.container = container;
  };
  ElementAllocator.prototype.allocate = function allocate(type) {
    type = type.toLowerCase();
    if (!(type in this.detachedNodes))
      this.detachedNodes[type] = [];
    var nodeStore = this.detachedNodes[type];
    var result;
    if (nodeStore.length > 0) {
      result = nodeStore.pop();
    } else {
      result = document.createElement(type);
      this.container.appendChild(result);
    }
    this.nodeCount++;
    return result;
  };
  ElementAllocator.prototype.deallocate = function deallocate(element) {
    var nodeType = element.nodeName.toLowerCase();
    var nodeStore = this.detachedNodes[nodeType];
    nodeStore.push(element);
    this.nodeCount--;
  };
  ElementAllocator.prototype.getNodeCount = function getNodeCount() {
    return this.nodeCount;
  };
  module.exports = ElementAllocator;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/utilities/Utility", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Utility = {};
  Utility.Direction = {
    X: 0,
    Y: 1,
    Z: 2
  };
  Utility.after = function after(count, callback) {
    var counter = count;
    return function() {
      counter--;
      if (counter === 0)
        callback.apply(this, arguments);
    };
  };
  Utility.loadURL = function loadURL(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function onreadystatechange() {
      if (this.readyState === 4) {
        if (callback)
          callback(this.responseText);
      }
    };
    xhr.open('GET', url);
    xhr.send();
  };
  Utility.createDocumentFragmentFromHTML = function createDocumentFragmentFromHTML(html) {
    var element = document.createElement('div');
    element.innerHTML = html;
    var result = document.createDocumentFragment();
    while (element.hasChildNodes())
      result.appendChild(element.firstChild);
    return result;
  };
  Utility.clone = function clone(b) {
    var a;
    if (typeof b === 'object') {
      a = b instanceof Array ? [] : {};
      for (var key in b) {
        if (typeof b[key] === 'object' && b[key] !== null) {
          if (b[key] instanceof Array) {
            a[key] = new Array(b[key].length);
            for (var i = 0; i < b[key].length; i++) {
              a[key][i] = Utility.clone(b[key][i]);
            }
          } else {
            a[key] = Utility.clone(b[key]);
          }
        } else {
          a[key] = b[key];
        }
      }
    } else {
      a = b;
    }
    return a;
  };
  module.exports = Utility;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/transitions/TweenTransition", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  function TweenTransition(options) {
    this.options = Object.create(TweenTransition.DEFAULT_OPTIONS);
    if (options)
      this.setOptions(options);
    this._startTime = 0;
    this._startValue = 0;
    this._updateTime = 0;
    this._endValue = 0;
    this._curve = undefined;
    this._duration = 0;
    this._active = false;
    this._callback = undefined;
    this.state = 0;
    this.velocity = undefined;
  }
  TweenTransition.Curves = {
    linear: function(t) {
      return t;
    },
    easeIn: function(t) {
      return t * t;
    },
    easeOut: function(t) {
      return t * (2 - t);
    },
    easeInOut: function(t) {
      if (t <= 0.5)
        return 2 * t * t;
      else
        return -2 * t * t + 4 * t - 1;
    },
    easeOutBounce: function(t) {
      return t * (3 - 2 * t);
    },
    spring: function(t) {
      return (1 - t) * Math.sin(6 * Math.PI * t) + t;
    }
  };
  TweenTransition.SUPPORTS_MULTIPLE = true;
  TweenTransition.DEFAULT_OPTIONS = {
    curve: TweenTransition.Curves.linear,
    duration: 500,
    speed: 0
  };
  var registeredCurves = {};
  TweenTransition.registerCurve = function registerCurve(curveName, curve) {
    if (!registeredCurves[curveName]) {
      registeredCurves[curveName] = curve;
      return true;
    } else {
      return false;
    }
  };
  TweenTransition.unregisterCurve = function unregisterCurve(curveName) {
    if (registeredCurves[curveName]) {
      delete registeredCurves[curveName];
      return true;
    } else {
      return false;
    }
  };
  TweenTransition.getCurve = function getCurve(curveName) {
    var curve = registeredCurves[curveName];
    if (curve !== undefined)
      return curve;
    else
      throw new Error('curve not registered');
  };
  TweenTransition.getCurves = function getCurves() {
    return registeredCurves;
  };
  function _interpolate(a, b, t) {
    return (1 - t) * a + t * b;
  }
  function _clone(obj) {
    if (obj instanceof Object) {
      if (obj instanceof Array)
        return obj.slice(0);
      else
        return Object.create(obj);
    } else
      return obj;
  }
  function _normalize(transition, defaultTransition) {
    var result = {curve: defaultTransition.curve};
    if (defaultTransition.duration)
      result.duration = defaultTransition.duration;
    if (defaultTransition.speed)
      result.speed = defaultTransition.speed;
    if (transition instanceof Object) {
      if (transition.duration !== undefined)
        result.duration = transition.duration;
      if (transition.curve)
        result.curve = transition.curve;
      if (transition.speed)
        result.speed = transition.speed;
    }
    if (typeof result.curve === 'string')
      result.curve = TweenTransition.getCurve(result.curve);
    return result;
  }
  TweenTransition.prototype.setOptions = function setOptions(options) {
    if (options.curve !== undefined)
      this.options.curve = options.curve;
    if (options.duration !== undefined)
      this.options.duration = options.duration;
    if (options.speed !== undefined)
      this.options.speed = options.speed;
  };
  TweenTransition.prototype.set = function set(endValue, transition, callback) {
    if (!transition) {
      this.reset(endValue);
      if (callback)
        callback();
      return ;
    }
    this._startValue = _clone(this.get());
    transition = _normalize(transition, this.options);
    if (transition.speed) {
      var startValue = this._startValue;
      if (startValue instanceof Object) {
        var variance = 0;
        for (var i in startValue)
          variance += (endValue[i] - startValue[i]) * (endValue[i] - startValue[i]);
        transition.duration = Math.sqrt(variance) / transition.speed;
      } else {
        transition.duration = Math.abs(endValue - startValue) / transition.speed;
      }
    }
    this._startTime = Date.now();
    this._endValue = _clone(endValue);
    this._startVelocity = _clone(transition.velocity);
    this._duration = transition.duration;
    this._curve = transition.curve;
    this._active = true;
    this._callback = callback;
  };
  TweenTransition.prototype.reset = function reset(startValue, startVelocity) {
    if (this._callback) {
      var callback = this._callback;
      this._callback = undefined;
      callback();
    }
    this.state = _clone(startValue);
    this.velocity = _clone(startVelocity);
    this._startTime = 0;
    this._duration = 0;
    this._updateTime = 0;
    this._startValue = this.state;
    this._startVelocity = this.velocity;
    this._endValue = this.state;
    this._active = false;
  };
  TweenTransition.prototype.getVelocity = function getVelocity() {
    return this.velocity;
  };
  TweenTransition.prototype.get = function get(timestamp) {
    this.update(timestamp);
    return this.state;
  };
  function _calculateVelocity(current, start, curve, duration, t) {
    var velocity;
    var eps = 1e-7;
    var speed = (curve(t) - curve(t - eps)) / eps;
    if (current instanceof Array) {
      velocity = [];
      for (var i = 0; i < current.length; i++) {
        if (typeof current[i] === 'number')
          velocity[i] = speed * (current[i] - start[i]) / duration;
        else
          velocity[i] = 0;
      }
    } else
      velocity = speed * (current - start) / duration;
    return velocity;
  }
  function _calculateState(start, end, t) {
    var state;
    if (start instanceof Array) {
      state = [];
      for (var i = 0; i < start.length; i++) {
        if (typeof start[i] === 'number')
          state[i] = _interpolate(start[i], end[i], t);
        else
          state[i] = start[i];
      }
    } else
      state = _interpolate(start, end, t);
    return state;
  }
  TweenTransition.prototype.update = function update(timestamp) {
    if (!this._active) {
      if (this._callback) {
        var callback = this._callback;
        this._callback = undefined;
        callback();
      }
      return ;
    }
    if (!timestamp)
      timestamp = Date.now();
    if (this._updateTime >= timestamp)
      return ;
    this._updateTime = timestamp;
    var timeSinceStart = timestamp - this._startTime;
    if (timeSinceStart >= this._duration) {
      this.state = this._endValue;
      this.velocity = _calculateVelocity(this.state, this._startValue, this._curve, this._duration, 1);
      this._active = false;
    } else if (timeSinceStart < 0) {
      this.state = this._startValue;
      this.velocity = this._startVelocity;
    } else {
      var t = timeSinceStart / this._duration;
      this.state = _calculateState(this._startValue, this._endValue, this._curve(t));
      this.velocity = _calculateVelocity(this.state, this._startValue, this._curve, this._duration, t);
    }
  };
  TweenTransition.prototype.isActive = function isActive() {
    return this._active;
  };
  TweenTransition.prototype.halt = function halt() {
    this.reset(this.get());
  };
  TweenTransition.registerCurve('linear', TweenTransition.Curves.linear);
  TweenTransition.registerCurve('easeIn', TweenTransition.Curves.easeIn);
  TweenTransition.registerCurve('easeOut', TweenTransition.Curves.easeOut);
  TweenTransition.registerCurve('easeInOut', TweenTransition.Curves.easeInOut);
  TweenTransition.registerCurve('easeOutBounce', TweenTransition.Curves.easeOutBounce);
  TweenTransition.registerCurve('spring', TweenTransition.Curves.spring);
  TweenTransition.customCurve = function customCurve(v1, v2) {
    v1 = v1 || 0;
    v2 = v2 || 0;
    return function(t) {
      return v1 * t + (-2 * v1 - v2 + 3) * t * t + (v1 + v2 - 2) * t * t * t;
    };
  };
  module.exports = TweenTransition;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/OptionsManager", ["npm:famous@0.3.5/core/EventHandler"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  function OptionsManager(value) {
    this._value = value;
    this.eventOutput = null;
  }
  OptionsManager.patch = function patchObject(source, data) {
    var manager = new OptionsManager(source);
    for (var i = 1; i < arguments.length; i++)
      manager.patch(arguments[i]);
    return source;
  };
  function _createEventOutput() {
    this.eventOutput = new EventHandler();
    this.eventOutput.bindThis(this);
    EventHandler.setOutputHandler(this, this.eventOutput);
  }
  OptionsManager.prototype.patch = function patch() {
    var myState = this._value;
    for (var i = 0; i < arguments.length; i++) {
      var data = arguments[i];
      for (var k in data) {
        if (k in myState && (data[k] && data[k].constructor === Object) && (myState[k] && myState[k].constructor === Object)) {
          if (!myState.hasOwnProperty(k))
            myState[k] = Object.create(myState[k]);
          this.key(k).patch(data[k]);
          if (this.eventOutput)
            this.eventOutput.emit('change', {
              id: k,
              value: this.key(k).value()
            });
        } else
          this.set(k, data[k]);
      }
    }
    return this;
  };
  OptionsManager.prototype.setOptions = OptionsManager.prototype.patch;
  OptionsManager.prototype.key = function key(identifier) {
    var result = new OptionsManager(this._value[identifier]);
    if (!(result._value instanceof Object) || result._value instanceof Array)
      result._value = {};
    return result;
  };
  OptionsManager.prototype.get = function get(key) {
    return key ? this._value[key] : this._value;
  };
  OptionsManager.prototype.getOptions = OptionsManager.prototype.get;
  OptionsManager.prototype.set = function set(key, value) {
    var originalValue = this.get(key);
    this._value[key] = value;
    if (this.eventOutput && value !== originalValue)
      this.eventOutput.emit('change', {
        id: key,
        value: value
      });
    return this;
  };
  OptionsManager.prototype.on = function on() {
    _createEventOutput.call(this);
    return this.on.apply(this, arguments);
  };
  OptionsManager.prototype.removeListener = function removeListener() {
    _createEventOutput.call(this);
    return this.removeListener.apply(this, arguments);
  };
  OptionsManager.prototype.pipe = function pipe() {
    _createEventOutput.call(this);
    return this.pipe.apply(this, arguments);
  };
  OptionsManager.prototype.unpipe = function unpipe() {
    _createEventOutput.call(this);
    return this.unpipe.apply(this, arguments);
  };
  module.exports = OptionsManager;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/ElementOutput", ["npm:famous@0.3.5/core/Entity", "npm:famous@0.3.5/core/EventHandler", "npm:famous@0.3.5/core/Transform"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Entity = require("npm:famous@0.3.5/core/Entity");
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  var Transform = require("npm:famous@0.3.5/core/Transform");
  var usePrefix = !('transform' in document.documentElement.style);
  var devicePixelRatio = window.devicePixelRatio || 1;
  function ElementOutput(element) {
    this._matrix = null;
    this._opacity = 1;
    this._origin = null;
    this._size = null;
    this._eventOutput = new EventHandler();
    this._eventOutput.bindThis(this);
    this.eventForwarder = function eventForwarder(event) {
      this._eventOutput.emit(event.type, event);
    }.bind(this);
    this.id = Entity.register(this);
    this._element = null;
    this._sizeDirty = false;
    this._originDirty = false;
    this._transformDirty = false;
    this._invisible = false;
    if (element)
      this.attach(element);
  }
  ElementOutput.prototype.on = function on(type, fn) {
    if (this._element)
      this._element.addEventListener(type, this.eventForwarder);
    this._eventOutput.on(type, fn);
  };
  ElementOutput.prototype.removeListener = function removeListener(type, fn) {
    this._eventOutput.removeListener(type, fn);
  };
  ElementOutput.prototype.emit = function emit(type, event) {
    if (event && !event.origin)
      event.origin = this;
    var handled = this._eventOutput.emit(type, event);
    if (handled && event && event.stopPropagation)
      event.stopPropagation();
    return handled;
  };
  ElementOutput.prototype.pipe = function pipe(target) {
    return this._eventOutput.pipe(target);
  };
  ElementOutput.prototype.unpipe = function unpipe(target) {
    return this._eventOutput.unpipe(target);
  };
  ElementOutput.prototype.render = function render() {
    return this.id;
  };
  function _addEventListeners(target) {
    for (var i in this._eventOutput.listeners) {
      target.addEventListener(i, this.eventForwarder);
    }
  }
  function _removeEventListeners(target) {
    for (var i in this._eventOutput.listeners) {
      target.removeEventListener(i, this.eventForwarder);
    }
  }
  function _formatCSSTransform(m) {
    m[12] = Math.round(m[12] * devicePixelRatio) / devicePixelRatio;
    m[13] = Math.round(m[13] * devicePixelRatio) / devicePixelRatio;
    var result = 'matrix3d(';
    for (var i = 0; i < 15; i++) {
      result += m[i] < 0.000001 && m[i] > -0.000001 ? '0,' : m[i] + ',';
    }
    result += m[15] + ')';
    return result;
  }
  var _setMatrix;
  if (usePrefix) {
    _setMatrix = function(element, matrix) {
      element.style.webkitTransform = _formatCSSTransform(matrix);
    };
  } else {
    _setMatrix = function(element, matrix) {
      element.style.transform = _formatCSSTransform(matrix);
    };
  }
  function _formatCSSOrigin(origin) {
    return 100 * origin[0] + '% ' + 100 * origin[1] + '%';
  }
  var _setOrigin = usePrefix ? function(element, origin) {
    element.style.webkitTransformOrigin = _formatCSSOrigin(origin);
  } : function(element, origin) {
    element.style.transformOrigin = _formatCSSOrigin(origin);
  };
  var _setInvisible = usePrefix ? function(element) {
    element.style.webkitTransform = 'scale3d(0.0001,0.0001,0.0001)';
    element.style.opacity = 0;
  } : function(element) {
    element.style.transform = 'scale3d(0.0001,0.0001,0.0001)';
    element.style.opacity = 0;
  };
  function _xyNotEquals(a, b) {
    return a && b ? a[0] !== b[0] || a[1] !== b[1] : a !== b;
  }
  ElementOutput.prototype.commit = function commit(context) {
    var target = this._element;
    if (!target)
      return ;
    var matrix = context.transform;
    var opacity = context.opacity;
    var origin = context.origin;
    var size = context.size;
    if (!matrix && this._matrix) {
      this._matrix = null;
      this._opacity = 0;
      _setInvisible(target);
      return ;
    }
    if (_xyNotEquals(this._origin, origin))
      this._originDirty = true;
    if (Transform.notEquals(this._matrix, matrix))
      this._transformDirty = true;
    if (this._invisible) {
      this._invisible = false;
      this._element.style.display = '';
    }
    if (this._opacity !== opacity) {
      this._opacity = opacity;
      target.style.opacity = opacity >= 1 ? '0.999999' : opacity;
    }
    if (this._transformDirty || this._originDirty || this._sizeDirty) {
      if (this._sizeDirty)
        this._sizeDirty = false;
      if (this._originDirty) {
        if (origin) {
          if (!this._origin)
            this._origin = [0, 0];
          this._origin[0] = origin[0];
          this._origin[1] = origin[1];
        } else
          this._origin = null;
        _setOrigin(target, this._origin);
        this._originDirty = false;
      }
      if (!matrix)
        matrix = Transform.identity;
      this._matrix = matrix;
      var aaMatrix = this._size ? Transform.thenMove(matrix, [-this._size[0] * origin[0], -this._size[1] * origin[1], 0]) : matrix;
      _setMatrix(target, aaMatrix);
      this._transformDirty = false;
    }
  };
  ElementOutput.prototype.cleanup = function cleanup() {
    if (this._element) {
      this._invisible = true;
      this._element.style.display = 'none';
    }
  };
  ElementOutput.prototype.attach = function attach(target) {
    this._element = target;
    _addEventListeners.call(this, target);
  };
  ElementOutput.prototype.detach = function detach() {
    var target = this._element;
    if (target) {
      _removeEventListeners.call(this, target);
      if (this._invisible) {
        this._invisible = false;
        this._element.style.display = '';
      }
    }
    this._element = null;
    return target;
  };
  module.exports = ElementOutput;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/transitions/TransitionableTransform", ["npm:famous@0.3.5/transitions/Transitionable", "npm:famous@0.3.5/core/Transform", "npm:famous@0.3.5/utilities/Utility"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Transitionable = require("npm:famous@0.3.5/transitions/Transitionable");
  var Transform = require("npm:famous@0.3.5/core/Transform");
  var Utility = require("npm:famous@0.3.5/utilities/Utility");
  function TransitionableTransform(transform) {
    this._final = Transform.identity.slice();
    this._finalTranslate = [0, 0, 0];
    this._finalRotate = [0, 0, 0];
    this._finalSkew = [0, 0, 0];
    this._finalScale = [1, 1, 1];
    this.translate = new Transitionable(this._finalTranslate);
    this.rotate = new Transitionable(this._finalRotate);
    this.skew = new Transitionable(this._finalSkew);
    this.scale = new Transitionable(this._finalScale);
    if (transform)
      this.set(transform);
  }
  function _build() {
    return Transform.build({
      translate: this.translate.get(),
      rotate: this.rotate.get(),
      skew: this.skew.get(),
      scale: this.scale.get()
    });
  }
  function _buildFinal() {
    return Transform.build({
      translate: this._finalTranslate,
      rotate: this._finalRotate,
      skew: this._finalSkew,
      scale: this._finalScale
    });
  }
  TransitionableTransform.prototype.setTranslate = function setTranslate(translate, transition, callback) {
    this._finalTranslate = translate;
    this._final = _buildFinal.call(this);
    this.translate.set(translate, transition, callback);
    return this;
  };
  TransitionableTransform.prototype.setScale = function setScale(scale, transition, callback) {
    this._finalScale = scale;
    this._final = _buildFinal.call(this);
    this.scale.set(scale, transition, callback);
    return this;
  };
  TransitionableTransform.prototype.setRotate = function setRotate(eulerAngles, transition, callback) {
    this._finalRotate = eulerAngles;
    this._final = _buildFinal.call(this);
    this.rotate.set(eulerAngles, transition, callback);
    return this;
  };
  TransitionableTransform.prototype.setSkew = function setSkew(skewAngles, transition, callback) {
    this._finalSkew = skewAngles;
    this._final = _buildFinal.call(this);
    this.skew.set(skewAngles, transition, callback);
    return this;
  };
  TransitionableTransform.prototype.set = function set(transform, transition, callback) {
    var components = Transform.interpret(transform);
    this._finalTranslate = components.translate;
    this._finalRotate = components.rotate;
    this._finalSkew = components.skew;
    this._finalScale = components.scale;
    this._final = transform;
    var _callback = callback ? Utility.after(4, callback) : null;
    this.translate.set(components.translate, transition, _callback);
    this.rotate.set(components.rotate, transition, _callback);
    this.skew.set(components.skew, transition, _callback);
    this.scale.set(components.scale, transition, _callback);
    return this;
  };
  TransitionableTransform.prototype.setDefaultTransition = function setDefaultTransition(transition) {
    this.translate.setDefault(transition);
    this.rotate.setDefault(transition);
    this.skew.setDefault(transition);
    this.scale.setDefault(transition);
  };
  TransitionableTransform.prototype.get = function get() {
    if (this.isActive()) {
      return _build.call(this);
    } else
      return this._final;
  };
  TransitionableTransform.prototype.getFinal = function getFinal() {
    return this._final;
  };
  TransitionableTransform.prototype.isActive = function isActive() {
    return this.translate.isActive() || this.rotate.isActive() || this.scale.isActive() || this.skew.isActive();
  };
  TransitionableTransform.prototype.halt = function halt() {
    this.translate.halt();
    this.rotate.halt();
    this.skew.halt();
    this.scale.halt();
    this._final = this.get();
    this._finalTranslate = this.translate.get();
    this._finalRotate = this.rotate.get();
    this._finalSkew = this.skew.get();
    this._finalScale = this.scale.get();
    return this;
  };
  module.exports = TransitionableTransform;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/surfaces/ContainerSurface", ["npm:famous@0.3.5/core/Surface", "npm:famous@0.3.5/core/Context"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Surface = require("npm:famous@0.3.5/core/Surface");
  var Context = require("npm:famous@0.3.5/core/Context");
  function ContainerSurface(options) {
    Surface.call(this, options);
    this._container = document.createElement('div');
    this._container.classList.add('famous-group');
    this._container.classList.add('famous-container-group');
    this._shouldRecalculateSize = false;
    this.context = new Context(this._container);
    this.setContent(this._container);
  }
  ContainerSurface.prototype = Object.create(Surface.prototype);
  ContainerSurface.prototype.constructor = ContainerSurface;
  ContainerSurface.prototype.elementType = 'div';
  ContainerSurface.prototype.elementClass = 'famous-surface';
  ContainerSurface.prototype.add = function add() {
    return this.context.add.apply(this.context, arguments);
  };
  ContainerSurface.prototype.render = function render() {
    if (this._sizeDirty)
      this._shouldRecalculateSize = true;
    return Surface.prototype.render.apply(this, arguments);
  };
  ContainerSurface.prototype.deploy = function deploy() {
    this._shouldRecalculateSize = true;
    return Surface.prototype.deploy.apply(this, arguments);
  };
  ContainerSurface.prototype.commit = function commit(context, transform, opacity, origin, size) {
    var previousSize = this._size ? [this._size[0], this._size[1]] : null;
    var result = Surface.prototype.commit.apply(this, arguments);
    if (this._shouldRecalculateSize || previousSize && (this._size[0] !== previousSize[0] || this._size[1] !== previousSize[1])) {
      this.context.setSize();
      this._shouldRecalculateSize = false;
    }
    this.context.update();
    return result;
  };
  module.exports = ContainerSurface;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/physics/PhysicsEngine", ["npm:famous@0.3.5/core/EventHandler"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  function PhysicsEngine(options) {
    this.options = Object.create(PhysicsEngine.DEFAULT_OPTIONS);
    if (options)
      this.setOptions(options);
    this._particles = [];
    this._bodies = [];
    this._agentData = {};
    this._forces = [];
    this._constraints = [];
    this._buffer = 0;
    this._prevTime = now();
    this._isSleeping = false;
    this._eventHandler = null;
    this._currAgentId = 0;
    this._hasBodies = false;
    this._eventHandler = null;
  }
  var TIMESTEP = 17;
  var MIN_TIME_STEP = 1000 / 120;
  var MAX_TIME_STEP = 17;
  var now = Date.now;
  var _events = {
    start: 'start',
    update: 'update',
    end: 'end'
  };
  PhysicsEngine.DEFAULT_OPTIONS = {
    constraintSteps: 1,
    sleepTolerance: 1e-7,
    velocityCap: undefined,
    angularVelocityCap: undefined
  };
  PhysicsEngine.prototype.setOptions = function setOptions(opts) {
    for (var key in opts)
      if (this.options[key])
        this.options[key] = opts[key];
  };
  PhysicsEngine.prototype.addBody = function addBody(body) {
    body._engine = this;
    if (body.isBody) {
      this._bodies.push(body);
      this._hasBodies = true;
    } else
      this._particles.push(body);
    body.on('start', this.wake.bind(this));
    return body;
  };
  PhysicsEngine.prototype.removeBody = function removeBody(body) {
    var array = body.isBody ? this._bodies : this._particles;
    var index = array.indexOf(body);
    if (index > -1) {
      for (var agentKey in this._agentData) {
        if (this._agentData.hasOwnProperty(agentKey)) {
          this.detachFrom(this._agentData[agentKey].id, body);
        }
      }
      array.splice(index, 1);
    }
    if (this.getBodies().length === 0)
      this._hasBodies = false;
  };
  function _mapAgentArray(agent) {
    if (agent.applyForce)
      return this._forces;
    if (agent.applyConstraint)
      return this._constraints;
  }
  function _attachOne(agent, targets, source) {
    if (targets === undefined)
      targets = this.getParticlesAndBodies();
    if (!(targets instanceof Array))
      targets = [targets];
    agent.on('change', this.wake.bind(this));
    this._agentData[this._currAgentId] = {
      agent: agent,
      id: this._currAgentId,
      targets: targets,
      source: source
    };
    _mapAgentArray.call(this, agent).push(this._currAgentId);
    return this._currAgentId++;
  }
  PhysicsEngine.prototype.attach = function attach(agents, targets, source) {
    this.wake();
    if (agents instanceof Array) {
      var agentIDs = [];
      for (var i = 0; i < agents.length; i++)
        agentIDs[i] = _attachOne.call(this, agents[i], targets, source);
      return agentIDs;
    } else
      return _attachOne.call(this, agents, targets, source);
  };
  PhysicsEngine.prototype.attachTo = function attachTo(agentID, target) {
    _getAgentData.call(this, agentID).targets.push(target);
  };
  PhysicsEngine.prototype.detach = function detach(id) {
    var agent = this.getAgent(id);
    var agentArray = _mapAgentArray.call(this, agent);
    var index = agentArray.indexOf(id);
    agentArray.splice(index, 1);
    delete this._agentData[id];
  };
  PhysicsEngine.prototype.detachFrom = function detachFrom(id, target) {
    var boundAgent = _getAgentData.call(this, id);
    if (boundAgent.source === target)
      this.detach(id);
    else {
      var targets = boundAgent.targets;
      var index = targets.indexOf(target);
      if (index > -1)
        targets.splice(index, 1);
    }
  };
  PhysicsEngine.prototype.detachAll = function detachAll() {
    this._agentData = {};
    this._forces = [];
    this._constraints = [];
    this._currAgentId = 0;
  };
  function _getAgentData(id) {
    return this._agentData[id];
  }
  PhysicsEngine.prototype.getAgent = function getAgent(id) {
    return _getAgentData.call(this, id).agent;
  };
  PhysicsEngine.prototype.getParticles = function getParticles() {
    return this._particles;
  };
  PhysicsEngine.prototype.getBodies = function getBodies() {
    return this._bodies;
  };
  PhysicsEngine.prototype.getParticlesAndBodies = function getParticlesAndBodies() {
    return this.getParticles().concat(this.getBodies());
  };
  PhysicsEngine.prototype.forEachParticle = function forEachParticle(fn, dt) {
    var particles = this.getParticles();
    for (var index = 0,
        len = particles.length; index < len; index++)
      fn.call(this, particles[index], dt);
  };
  PhysicsEngine.prototype.forEachBody = function forEachBody(fn, dt) {
    if (!this._hasBodies)
      return ;
    var bodies = this.getBodies();
    for (var index = 0,
        len = bodies.length; index < len; index++)
      fn.call(this, bodies[index], dt);
  };
  PhysicsEngine.prototype.forEach = function forEach(fn, dt) {
    this.forEachParticle(fn, dt);
    this.forEachBody(fn, dt);
  };
  function _updateForce(index) {
    var boundAgent = _getAgentData.call(this, this._forces[index]);
    boundAgent.agent.applyForce(boundAgent.targets, boundAgent.source);
  }
  function _updateForces() {
    for (var index = this._forces.length - 1; index > -1; index--)
      _updateForce.call(this, index);
  }
  function _updateConstraint(index, dt) {
    var boundAgent = this._agentData[this._constraints[index]];
    return boundAgent.agent.applyConstraint(boundAgent.targets, boundAgent.source, dt);
  }
  function _updateConstraints(dt) {
    var iteration = 0;
    while (iteration < this.options.constraintSteps) {
      for (var index = this._constraints.length - 1; index > -1; index--)
        _updateConstraint.call(this, index, dt);
      iteration++;
    }
  }
  function _updateVelocities(body, dt) {
    body.integrateVelocity(dt);
    if (this.options.velocityCap)
      body.velocity.cap(this.options.velocityCap).put(body.velocity);
  }
  function _updateAngularVelocities(body, dt) {
    body.integrateAngularMomentum(dt);
    body.updateAngularVelocity();
    if (this.options.angularVelocityCap)
      body.angularVelocity.cap(this.options.angularVelocityCap).put(body.angularVelocity);
  }
  function _updateOrientations(body, dt) {
    body.integrateOrientation(dt);
  }
  function _updatePositions(body, dt) {
    body.integratePosition(dt);
    body.emit(_events.update, body);
  }
  function _integrate(dt) {
    _updateForces.call(this, dt);
    this.forEach(_updateVelocities, dt);
    this.forEachBody(_updateAngularVelocities, dt);
    _updateConstraints.call(this, dt);
    this.forEachBody(_updateOrientations, dt);
    this.forEach(_updatePositions, dt);
  }
  function _getParticlesEnergy() {
    var energy = 0;
    var particleEnergy = 0;
    this.forEach(function(particle) {
      particleEnergy = particle.getEnergy();
      energy += particleEnergy;
    });
    return energy;
  }
  function _getAgentsEnergy() {
    var energy = 0;
    for (var id in this._agentData)
      energy += this.getAgentEnergy(id);
    return energy;
  }
  PhysicsEngine.prototype.getAgentEnergy = function(agentId) {
    var agentData = _getAgentData.call(this, agentId);
    return agentData.agent.getEnergy(agentData.targets, agentData.source);
  };
  PhysicsEngine.prototype.getEnergy = function getEnergy() {
    return _getParticlesEnergy.call(this) + _getAgentsEnergy.call(this);
  };
  PhysicsEngine.prototype.step = function step() {
    if (this.isSleeping())
      return ;
    var currTime = now();
    var dtFrame = currTime - this._prevTime;
    this._prevTime = currTime;
    if (dtFrame < MIN_TIME_STEP)
      return ;
    if (dtFrame > MAX_TIME_STEP)
      dtFrame = MAX_TIME_STEP;
    _integrate.call(this, TIMESTEP);
    this.emit(_events.update, this);
    if (this.getEnergy() < this.options.sleepTolerance)
      this.sleep();
  };
  PhysicsEngine.prototype.isSleeping = function isSleeping() {
    return this._isSleeping;
  };
  PhysicsEngine.prototype.isActive = function isSleeping() {
    return !this._isSleeping;
  };
  PhysicsEngine.prototype.sleep = function sleep() {
    if (this._isSleeping)
      return ;
    this.forEach(function(body) {
      body.sleep();
    });
    this.emit(_events.end, this);
    this._isSleeping = true;
  };
  PhysicsEngine.prototype.wake = function wake() {
    if (!this._isSleeping)
      return ;
    this._prevTime = now();
    this.emit(_events.start, this);
    this._isSleeping = false;
  };
  PhysicsEngine.prototype.emit = function emit(type, data) {
    if (this._eventHandler === null)
      return ;
    this._eventHandler.emit(type, data);
  };
  PhysicsEngine.prototype.on = function on(event, fn) {
    if (this._eventHandler === null)
      this._eventHandler = new EventHandler();
    this._eventHandler.on(event, fn);
  };
  module.exports = PhysicsEngine;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/math/Vector", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  function Vector(x, y, z) {
    if (arguments.length === 1 && x !== undefined)
      this.set(x);
    else {
      this.x = x || 0;
      this.y = y || 0;
      this.z = z || 0;
    }
    return this;
  }
  var _register = new Vector(0, 0, 0);
  Vector.prototype.add = function add(v) {
    return _setXYZ.call(_register, this.x + v.x, this.y + v.y, this.z + v.z);
  };
  Vector.prototype.sub = function sub(v) {
    return _setXYZ.call(_register, this.x - v.x, this.y - v.y, this.z - v.z);
  };
  Vector.prototype.mult = function mult(r) {
    return _setXYZ.call(_register, r * this.x, r * this.y, r * this.z);
  };
  Vector.prototype.div = function div(r) {
    return this.mult(1 / r);
  };
  Vector.prototype.cross = function cross(v) {
    var x = this.x;
    var y = this.y;
    var z = this.z;
    var vx = v.x;
    var vy = v.y;
    var vz = v.z;
    return _setXYZ.call(_register, z * vy - y * vz, x * vz - z * vx, y * vx - x * vy);
  };
  Vector.prototype.equals = function equals(v) {
    return v.x === this.x && v.y === this.y && v.z === this.z;
  };
  Vector.prototype.rotateX = function rotateX(theta) {
    var x = this.x;
    var y = this.y;
    var z = this.z;
    var cosTheta = Math.cos(theta);
    var sinTheta = Math.sin(theta);
    return _setXYZ.call(_register, x, y * cosTheta - z * sinTheta, y * sinTheta + z * cosTheta);
  };
  Vector.prototype.rotateY = function rotateY(theta) {
    var x = this.x;
    var y = this.y;
    var z = this.z;
    var cosTheta = Math.cos(theta);
    var sinTheta = Math.sin(theta);
    return _setXYZ.call(_register, z * sinTheta + x * cosTheta, y, z * cosTheta - x * sinTheta);
  };
  Vector.prototype.rotateZ = function rotateZ(theta) {
    var x = this.x;
    var y = this.y;
    var z = this.z;
    var cosTheta = Math.cos(theta);
    var sinTheta = Math.sin(theta);
    return _setXYZ.call(_register, x * cosTheta - y * sinTheta, x * sinTheta + y * cosTheta, z);
  };
  Vector.prototype.dot = function dot(v) {
    return this.x * v.x + this.y * v.y + this.z * v.z;
  };
  Vector.prototype.normSquared = function normSquared() {
    return this.dot(this);
  };
  Vector.prototype.norm = function norm() {
    return Math.sqrt(this.normSquared());
  };
  Vector.prototype.normalize = function normalize(length) {
    if (arguments.length === 0)
      length = 1;
    var norm = this.norm();
    if (norm > 1e-7)
      return _setFromVector.call(_register, this.mult(length / norm));
    else
      return _setXYZ.call(_register, length, 0, 0);
  };
  Vector.prototype.clone = function clone() {
    return new Vector(this);
  };
  Vector.prototype.isZero = function isZero() {
    return !(this.x || this.y || this.z);
  };
  function _setXYZ(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
    return this;
  }
  function _setFromArray(v) {
    return _setXYZ.call(this, v[0], v[1], v[2] || 0);
  }
  function _setFromVector(v) {
    return _setXYZ.call(this, v.x, v.y, v.z);
  }
  function _setFromNumber(x) {
    return _setXYZ.call(this, x, 0, 0);
  }
  Vector.prototype.set = function set(v) {
    if (v instanceof Array)
      return _setFromArray.call(this, v);
    if (typeof v === 'number')
      return _setFromNumber.call(this, v);
    return _setFromVector.call(this, v);
  };
  Vector.prototype.setXYZ = function(x, y, z) {
    return _setXYZ.apply(this, arguments);
  };
  Vector.prototype.set1D = function(x) {
    return _setFromNumber.call(this, x);
  };
  Vector.prototype.put = function put(v) {
    if (this === _register)
      _setFromVector.call(v, _register);
    else
      _setFromVector.call(v, this);
  };
  Vector.prototype.clear = function clear() {
    return _setXYZ.call(this, 0, 0, 0);
  };
  Vector.prototype.cap = function cap(cap) {
    if (cap === Infinity)
      return _setFromVector.call(_register, this);
    var norm = this.norm();
    if (norm > cap)
      return _setFromVector.call(_register, this.mult(cap / norm));
    else
      return _setFromVector.call(_register, this);
  };
  Vector.prototype.project = function project(n) {
    return n.mult(this.dot(n));
  };
  Vector.prototype.reflectAcross = function reflectAcross(n) {
    n.normalize().put(n);
    return _setFromVector(_register, this.sub(this.project(n).mult(2)));
  };
  Vector.prototype.get = function get() {
    return [this.x, this.y, this.z];
  };
  Vector.prototype.get1D = function() {
    return this.x;
  };
  module.exports = Vector;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/physics/integrators/SymplecticEuler", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var SymplecticEuler = {};
  SymplecticEuler.integrateVelocity = function integrateVelocity(body, dt) {
    var v = body.velocity;
    var w = body.inverseMass;
    var f = body.force;
    if (f.isZero())
      return ;
    v.add(f.mult(dt * w)).put(v);
    f.clear();
  };
  SymplecticEuler.integratePosition = function integratePosition(body, dt) {
    var p = body.position;
    var v = body.velocity;
    p.add(v.mult(dt)).put(p);
  };
  SymplecticEuler.integrateAngularMomentum = function integrateAngularMomentum(body, dt) {
    var L = body.angularMomentum;
    var t = body.torque;
    if (t.isZero())
      return ;
    L.add(t.mult(dt)).put(L);
    t.clear();
  };
  SymplecticEuler.integrateOrientation = function integrateOrientation(body, dt) {
    var q = body.orientation;
    var w = body.angularVelocity;
    if (w.isZero())
      return ;
    q.add(q.multiply(w).scalarMultiply(0.5 * dt)).put(q);
  };
  module.exports = SymplecticEuler;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/physics/forces/Force", ["npm:famous@0.3.5/math/Vector", "npm:famous@0.3.5/core/EventHandler"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Vector = require("npm:famous@0.3.5/math/Vector");
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  function Force(force) {
    this.force = new Vector(force);
    this._eventOutput = new EventHandler();
    EventHandler.setOutputHandler(this, this._eventOutput);
  }
  Force.prototype.setOptions = function setOptions(options) {
    this._eventOutput.emit('change', options);
  };
  Force.prototype.applyForce = function applyForce(targets) {
    var length = targets.length;
    while (length--) {
      targets[length].applyForce(this.force);
    }
  };
  Force.prototype.getEnergy = function getEnergy() {
    return 0;
  };
  module.exports = Force;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/physics/forces/Spring", ["npm:famous@0.3.5/physics/forces/Force", "npm:famous@0.3.5/math/Vector"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Force = require("npm:famous@0.3.5/physics/forces/Force");
  var Vector = require("npm:famous@0.3.5/math/Vector");
  function Spring(options) {
    Force.call(this);
    this.options = Object.create(this.constructor.DEFAULT_OPTIONS);
    if (options)
      this.setOptions(options);
    this.disp = new Vector(0, 0, 0);
    _init.call(this);
  }
  Spring.prototype = Object.create(Force.prototype);
  Spring.prototype.constructor = Spring;
  var pi = Math.PI;
  var MIN_PERIOD = 150;
  Spring.FORCE_FUNCTIONS = {
    FENE: function(dist, rMax) {
      var rMaxSmall = rMax * 0.99;
      var r = Math.max(Math.min(dist, rMaxSmall), -rMaxSmall);
      return r / (1 - r * r / (rMax * rMax));
    },
    HOOK: function(dist) {
      return dist;
    }
  };
  Spring.DEFAULT_OPTIONS = {
    period: 300,
    dampingRatio: 0.1,
    length: 0,
    maxLength: Infinity,
    anchor: undefined,
    forceFunction: Spring.FORCE_FUNCTIONS.HOOK
  };
  function _calcStiffness() {
    var options = this.options;
    options.stiffness = Math.pow(2 * pi / options.period, 2);
  }
  function _calcDamping() {
    var options = this.options;
    options.damping = 4 * pi * options.dampingRatio / options.period;
  }
  function _init() {
    _calcStiffness.call(this);
    _calcDamping.call(this);
  }
  Spring.prototype.setOptions = function setOptions(options) {
    if (options.anchor !== undefined) {
      if (options.anchor.position instanceof Vector)
        this.options.anchor = options.anchor.position;
      if (options.anchor instanceof Vector)
        this.options.anchor = options.anchor;
      if (options.anchor instanceof Array)
        this.options.anchor = new Vector(options.anchor);
    }
    if (options.period !== undefined) {
      if (options.period < MIN_PERIOD) {
        options.period = MIN_PERIOD;
        console.warn('The period of a SpringTransition is capped at ' + MIN_PERIOD + ' ms. Use a SnapTransition for faster transitions');
      }
      this.options.period = options.period;
    }
    if (options.dampingRatio !== undefined)
      this.options.dampingRatio = options.dampingRatio;
    if (options.length !== undefined)
      this.options.length = options.length;
    if (options.forceFunction !== undefined)
      this.options.forceFunction = options.forceFunction;
    if (options.maxLength !== undefined)
      this.options.maxLength = options.maxLength;
    _init.call(this);
    Force.prototype.setOptions.call(this, options);
  };
  Spring.prototype.applyForce = function applyForce(targets, source) {
    var force = this.force;
    var disp = this.disp;
    var options = this.options;
    var stiffness = options.stiffness;
    var damping = options.damping;
    var restLength = options.length;
    var maxLength = options.maxLength;
    var anchor = options.anchor || source.position;
    var forceFunction = options.forceFunction;
    var i;
    var target;
    var p2;
    var v2;
    var dist;
    var m;
    for (i = 0; i < targets.length; i++) {
      target = targets[i];
      p2 = target.position;
      v2 = target.velocity;
      anchor.sub(p2).put(disp);
      dist = disp.norm() - restLength;
      if (dist === 0)
        return ;
      m = target.mass;
      stiffness *= m;
      damping *= m;
      disp.normalize(stiffness * forceFunction(dist, maxLength)).put(force);
      if (damping)
        if (source)
          force.add(v2.sub(source.velocity).mult(-damping)).put(force);
        else
          force.add(v2.mult(-damping)).put(force);
      target.applyForce(force);
      if (source)
        source.applyForce(force.mult(-1));
    }
  };
  Spring.prototype.getEnergy = function getEnergy(targets, source) {
    var options = this.options;
    var restLength = options.length;
    var anchor = source ? source.position : options.anchor;
    var strength = options.stiffness;
    var energy = 0;
    for (var i = 0; i < targets.length; i++) {
      var target = targets[i];
      var dist = anchor.sub(target.position).norm() - restLength;
      energy += 0.5 * strength * dist * dist;
    }
    return energy;
  };
  module.exports = Spring;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/ViewSequence", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  function ViewSequence(options) {
    if (!options)
      options = [];
    if (options instanceof Array)
      options = {array: options};
    this._ = null;
    this.index = options.index || 0;
    if (options.array)
      this._ = new this.constructor.Backing(options.array);
    else if (options._)
      this._ = options._;
    if (this.index === this._.firstIndex)
      this._.firstNode = this;
    if (this.index === this._.firstIndex + this._.array.length - 1)
      this._.lastNode = this;
    if (options.loop !== undefined)
      this._.loop = options.loop;
    if (options.trackSize !== undefined)
      this._.trackSize = options.trackSize;
    this._previousNode = null;
    this._nextNode = null;
  }
  ViewSequence.Backing = function Backing(array) {
    this.array = array;
    this.firstIndex = 0;
    this.loop = false;
    this.firstNode = null;
    this.lastNode = null;
    this.cumulativeSizes = [[0, 0]];
    this.sizeDirty = true;
    this.trackSize = false;
  };
  ViewSequence.Backing.prototype.getValue = function getValue(i) {
    var _i = i - this.firstIndex;
    if (_i < 0 || _i >= this.array.length)
      return null;
    return this.array[_i];
  };
  ViewSequence.Backing.prototype.setValue = function setValue(i, value) {
    this.array[i - this.firstIndex] = value;
  };
  ViewSequence.Backing.prototype.getSize = function getSize(index) {
    return this.cumulativeSizes[index];
  };
  ViewSequence.Backing.prototype.calculateSize = function calculateSize(index) {
    index = index || this.array.length;
    var size = [0, 0];
    for (var i = 0; i < index; i++) {
      var nodeSize = this.array[i].getSize();
      if (!nodeSize)
        return undefined;
      if (size[0] !== undefined) {
        if (nodeSize[0] === undefined)
          size[0] = undefined;
        else
          size[0] += nodeSize[0];
      }
      if (size[1] !== undefined) {
        if (nodeSize[1] === undefined)
          size[1] = undefined;
        else
          size[1] += nodeSize[1];
      }
      this.cumulativeSizes[i + 1] = size.slice();
    }
    this.sizeDirty = false;
    return size;
  };
  ViewSequence.Backing.prototype.reindex = function reindex(start, removeCount, insertCount) {
    if (!this.array[0])
      return ;
    var i = 0;
    var index = this.firstIndex;
    var indexShiftAmount = insertCount - removeCount;
    var node = this.firstNode;
    while (index < start - 1) {
      node = node.getNext();
      index++;
    }
    var spliceStartNode = node;
    for (i = 0; i < removeCount; i++) {
      node = node.getNext();
      if (node)
        node._previousNode = spliceStartNode;
    }
    var spliceResumeNode = node ? node.getNext() : null;
    spliceStartNode._nextNode = null;
    node = spliceStartNode;
    for (i = 0; i < insertCount; i++)
      node = node.getNext();
    index += insertCount;
    if (node !== spliceResumeNode) {
      node._nextNode = spliceResumeNode;
      if (spliceResumeNode)
        spliceResumeNode._previousNode = node;
    }
    if (spliceResumeNode) {
      node = spliceResumeNode;
      index++;
      while (node && index < this.array.length + this.firstIndex) {
        if (node._nextNode)
          node.index += indexShiftAmount;
        else
          node.index = index;
        node = node.getNext();
        index++;
      }
    }
    if (this.trackSize)
      this.sizeDirty = true;
  };
  ViewSequence.prototype.getPrevious = function getPrevious() {
    var len = this._.array.length;
    if (!len) {
      this._previousNode = null;
    } else if (this.index === this._.firstIndex) {
      if (this._.loop) {
        this._previousNode = this._.lastNode || new this.constructor({
          _: this._,
          index: this._.firstIndex + len - 1
        });
        this._previousNode._nextNode = this;
      } else {
        this._previousNode = null;
      }
    } else if (!this._previousNode) {
      this._previousNode = new this.constructor({
        _: this._,
        index: this.index - 1
      });
      this._previousNode._nextNode = this;
    }
    return this._previousNode;
  };
  ViewSequence.prototype.getNext = function getNext() {
    var len = this._.array.length;
    if (!len) {
      this._nextNode = null;
    } else if (this.index === this._.firstIndex + len - 1) {
      if (this._.loop) {
        this._nextNode = this._.firstNode || new this.constructor({
          _: this._,
          index: this._.firstIndex
        });
        this._nextNode._previousNode = this;
      } else {
        this._nextNode = null;
      }
    } else if (!this._nextNode) {
      this._nextNode = new this.constructor({
        _: this._,
        index: this.index + 1
      });
      this._nextNode._previousNode = this;
    }
    return this._nextNode;
  };
  ViewSequence.prototype.indexOf = function indexOf(item) {
    return this._.array.indexOf(item);
  };
  ViewSequence.prototype.getIndex = function getIndex() {
    return this.index;
  };
  ViewSequence.prototype.toString = function toString() {
    return '' + this.index;
  };
  ViewSequence.prototype.unshift = function unshift(value) {
    this._.array.unshift.apply(this._.array, arguments);
    this._.firstIndex -= arguments.length;
    if (this._.trackSize)
      this._.sizeDirty = true;
  };
  ViewSequence.prototype.push = function push(value) {
    this._.array.push.apply(this._.array, arguments);
    if (this._.trackSize)
      this._.sizeDirty = true;
  };
  ViewSequence.prototype.splice = function splice(index, howMany) {
    var values = Array.prototype.slice.call(arguments, 2);
    this._.array.splice.apply(this._.array, [index - this._.firstIndex, howMany].concat(values));
    this._.reindex(index, howMany, values.length);
  };
  ViewSequence.prototype.swap = function swap(other) {
    var otherValue = other.get();
    var myValue = this.get();
    this._.setValue(this.index, otherValue);
    this._.setValue(other.index, myValue);
    var myPrevious = this._previousNode;
    var myNext = this._nextNode;
    var myIndex = this.index;
    var otherPrevious = other._previousNode;
    var otherNext = other._nextNode;
    var otherIndex = other.index;
    this.index = otherIndex;
    this._previousNode = otherPrevious === this ? other : otherPrevious;
    if (this._previousNode)
      this._previousNode._nextNode = this;
    this._nextNode = otherNext === this ? other : otherNext;
    if (this._nextNode)
      this._nextNode._previousNode = this;
    other.index = myIndex;
    other._previousNode = myPrevious === other ? this : myPrevious;
    if (other._previousNode)
      other._previousNode._nextNode = other;
    other._nextNode = myNext === other ? this : myNext;
    if (other._nextNode)
      other._nextNode._previousNode = other;
    if (this.index === this._.firstIndex)
      this._.firstNode = this;
    else if (this.index === this._.firstIndex + this._.array.length - 1)
      this._.lastNode = this;
    if (other.index === this._.firstIndex)
      this._.firstNode = other;
    else if (other.index === this._.firstIndex + this._.array.length - 1)
      this._.lastNode = other;
    if (this._.trackSize)
      this._.sizeDirty = true;
  };
  ViewSequence.prototype.get = function get() {
    return this._.getValue(this.index);
  };
  ViewSequence.prototype.getSize = function getSize() {
    var target = this.get();
    return target ? target.getSize() : null;
  };
  ViewSequence.prototype.render = function render() {
    if (this._.trackSize && this._.sizeDirty)
      this._.calculateSize();
    var target = this.get();
    return target ? target.render.apply(target, arguments) : null;
  };
  module.exports = ViewSequence;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/Group", ["npm:famous@0.3.5/core/Context", "npm:famous@0.3.5/core/Transform", "npm:famous@0.3.5/core/Surface"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Context = require("npm:famous@0.3.5/core/Context");
  var Transform = require("npm:famous@0.3.5/core/Transform");
  var Surface = require("npm:famous@0.3.5/core/Surface");
  function Group(options) {
    Surface.call(this, options);
    this._shouldRecalculateSize = false;
    this._container = document.createDocumentFragment();
    this.context = new Context(this._container);
    this.setContent(this._container);
    this._groupSize = [undefined, undefined];
  }
  Group.SIZE_ZERO = [0, 0];
  Group.prototype = Object.create(Surface.prototype);
  Group.prototype.elementType = 'div';
  Group.prototype.elementClass = 'famous-group';
  Group.prototype.add = function add() {
    return this.context.add.apply(this.context, arguments);
  };
  Group.prototype.render = function render() {
    return Surface.prototype.render.call(this);
  };
  Group.prototype.deploy = function deploy(target) {
    this.context.migrate(target);
  };
  Group.prototype.recall = function recall(target) {
    this._container = document.createDocumentFragment();
    this.context.migrate(this._container);
  };
  Group.prototype.commit = function commit(context) {
    var transform = context.transform;
    var origin = context.origin;
    var opacity = context.opacity;
    var size = context.size;
    var result = Surface.prototype.commit.call(this, {
      allocator: context.allocator,
      transform: Transform.thenMove(transform, [-origin[0] * size[0], -origin[1] * size[1], 0]),
      opacity: opacity,
      origin: origin,
      size: Group.SIZE_ZERO
    });
    if (size[0] !== this._groupSize[0] || size[1] !== this._groupSize[1]) {
      this._groupSize[0] = size[0];
      this._groupSize[1] = size[1];
      this.context.setSize(size);
    }
    this.context.update({
      transform: Transform.translate(-origin[0] * size[0], -origin[1] * size[1], 0),
      origin: origin,
      size: size
    });
    return result;
  };
  module.exports = Group;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/inputs/GenericSync", ["npm:famous@0.3.5/core/EventHandler"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  function GenericSync(syncs, options) {
    this._eventInput = new EventHandler();
    this._eventOutput = new EventHandler();
    EventHandler.setInputHandler(this, this._eventInput);
    EventHandler.setOutputHandler(this, this._eventOutput);
    this._syncs = {};
    if (syncs)
      this.addSync(syncs);
    if (options)
      this.setOptions(options);
  }
  GenericSync.DIRECTION_X = 0;
  GenericSync.DIRECTION_Y = 1;
  GenericSync.DIRECTION_Z = 2;
  var registry = {};
  GenericSync.register = function register(syncObject) {
    for (var key in syncObject) {
      if (registry[key]) {
        if (registry[key] !== syncObject[key])
          throw new Error('Conflicting sync classes for key: ' + key);
      } else
        registry[key] = syncObject[key];
    }
  };
  GenericSync.prototype.setOptions = function(options) {
    for (var key in this._syncs) {
      this._syncs[key].setOptions(options);
    }
  };
  GenericSync.prototype.pipeSync = function pipeToSync(key) {
    var sync = this._syncs[key];
    this._eventInput.pipe(sync);
    sync.pipe(this._eventOutput);
  };
  GenericSync.prototype.unpipeSync = function unpipeFromSync(key) {
    var sync = this._syncs[key];
    this._eventInput.unpipe(sync);
    sync.unpipe(this._eventOutput);
  };
  function _addSingleSync(key, options) {
    if (!registry[key])
      return ;
    this._syncs[key] = new registry[key](options);
    this.pipeSync(key);
  }
  GenericSync.prototype.addSync = function addSync(syncs) {
    if (syncs instanceof Array)
      for (var i = 0; i < syncs.length; i++)
        _addSingleSync.call(this, syncs[i]);
    else if (syncs instanceof Object)
      for (var key in syncs)
        _addSingleSync.call(this, key, syncs[key]);
  };
  module.exports = GenericSync;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/inputs/ScrollSync", ["npm:famous@0.3.5/core/EventHandler", "npm:famous@0.3.5/core/Engine", "npm:famous@0.3.5/core/OptionsManager"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  var Engine = require("npm:famous@0.3.5/core/Engine");
  var OptionsManager = require("npm:famous@0.3.5/core/OptionsManager");
  function ScrollSync(options) {
    this.options = Object.create(ScrollSync.DEFAULT_OPTIONS);
    this._optionsManager = new OptionsManager(this.options);
    if (options)
      this.setOptions(options);
    this._payload = {
      delta: null,
      position: null,
      velocity: null,
      slip: true
    };
    this._eventInput = new EventHandler();
    this._eventOutput = new EventHandler();
    EventHandler.setInputHandler(this, this._eventInput);
    EventHandler.setOutputHandler(this, this._eventOutput);
    this._position = this.options.direction === undefined ? [0, 0] : 0;
    this._prevTime = undefined;
    this._prevVel = undefined;
    this._eventInput.on('mousewheel', _handleMove.bind(this));
    this._eventInput.on('wheel', _handleMove.bind(this));
    this._inProgress = false;
    this._loopBound = false;
  }
  ScrollSync.DEFAULT_OPTIONS = {
    direction: undefined,
    minimumEndSpeed: Infinity,
    rails: false,
    scale: 1,
    stallTime: 50,
    lineHeight: 40,
    preventDefault: true
  };
  ScrollSync.DIRECTION_X = 0;
  ScrollSync.DIRECTION_Y = 1;
  var MINIMUM_TICK_TIME = 8;
  var _now = Date.now;
  function _newFrame() {
    if (this._inProgress && _now() - this._prevTime > this.options.stallTime) {
      this._inProgress = false;
      var finalVel = Math.abs(this._prevVel) >= this.options.minimumEndSpeed ? this._prevVel : 0;
      var payload = this._payload;
      payload.position = this._position;
      payload.velocity = finalVel;
      payload.slip = true;
      this._eventOutput.emit('end', payload);
    }
  }
  function _handleMove(event) {
    if (this.options.preventDefault)
      event.preventDefault();
    if (!this._inProgress) {
      this._inProgress = true;
      this._position = this.options.direction === undefined ? [0, 0] : 0;
      payload = this._payload;
      payload.slip = true;
      payload.position = this._position;
      payload.clientX = event.clientX;
      payload.clientY = event.clientY;
      payload.offsetX = event.offsetX;
      payload.offsetY = event.offsetY;
      this._eventOutput.emit('start', payload);
      if (!this._loopBound) {
        Engine.on('prerender', _newFrame.bind(this));
        this._loopBound = true;
      }
    }
    var currTime = _now();
    var prevTime = this._prevTime || currTime;
    var diffX = event.wheelDeltaX !== undefined ? event.wheelDeltaX : -event.deltaX;
    var diffY = event.wheelDeltaY !== undefined ? event.wheelDeltaY : -event.deltaY;
    if (event.deltaMode === 1) {
      diffX *= this.options.lineHeight;
      diffY *= this.options.lineHeight;
    }
    if (this.options.rails) {
      if (Math.abs(diffX) > Math.abs(diffY))
        diffY = 0;
      else
        diffX = 0;
    }
    var diffTime = Math.max(currTime - prevTime, MINIMUM_TICK_TIME);
    var velX = diffX / diffTime;
    var velY = diffY / diffTime;
    var scale = this.options.scale;
    var nextVel;
    var nextDelta;
    if (this.options.direction === ScrollSync.DIRECTION_X) {
      nextDelta = scale * diffX;
      nextVel = scale * velX;
      this._position += nextDelta;
    } else if (this.options.direction === ScrollSync.DIRECTION_Y) {
      nextDelta = scale * diffY;
      nextVel = scale * velY;
      this._position += nextDelta;
    } else {
      nextDelta = [scale * diffX, scale * diffY];
      nextVel = [scale * velX, scale * velY];
      this._position[0] += nextDelta[0];
      this._position[1] += nextDelta[1];
    }
    var payload = this._payload;
    payload.delta = nextDelta;
    payload.velocity = nextVel;
    payload.position = this._position;
    payload.slip = true;
    this._eventOutput.emit('update', payload);
    this._prevTime = currTime;
    this._prevVel = nextVel;
  }
  ScrollSync.prototype.getOptions = function getOptions() {
    return this.options;
  };
  ScrollSync.prototype.setOptions = function setOptions(options) {
    return this._optionsManager.setOptions(options);
  };
  module.exports = ScrollSync;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/inputs/TouchTracker", ["npm:famous@0.3.5/core/EventHandler"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  var _now = Date.now;
  function _timestampTouch(touch, event, history) {
    return {
      x: touch.clientX,
      y: touch.clientY,
      identifier: touch.identifier,
      origin: event.origin,
      timestamp: _now(),
      count: event.touches.length,
      history: history
    };
  }
  function _handleStart(event) {
    if (event.touches.length > this.touchLimit)
      return ;
    this.isTouched = true;
    for (var i = 0; i < event.changedTouches.length; i++) {
      var touch = event.changedTouches[i];
      var data = _timestampTouch(touch, event, null);
      this.eventOutput.emit('trackstart', data);
      if (!this.selective && !this.touchHistory[touch.identifier])
        this.track(data);
    }
  }
  function _handleMove(event) {
    if (event.touches.length > this.touchLimit)
      return ;
    for (var i = 0; i < event.changedTouches.length; i++) {
      var touch = event.changedTouches[i];
      var history = this.touchHistory[touch.identifier];
      if (history) {
        var data = _timestampTouch(touch, event, history);
        this.touchHistory[touch.identifier].push(data);
        this.eventOutput.emit('trackmove', data);
      }
    }
  }
  function _handleEnd(event) {
    if (!this.isTouched)
      return ;
    for (var i = 0; i < event.changedTouches.length; i++) {
      var touch = event.changedTouches[i];
      var history = this.touchHistory[touch.identifier];
      if (history) {
        var data = _timestampTouch(touch, event, history);
        this.eventOutput.emit('trackend', data);
        delete this.touchHistory[touch.identifier];
      }
    }
    this.isTouched = false;
  }
  function _handleUnpipe() {
    for (var i in this.touchHistory) {
      var history = this.touchHistory[i];
      this.eventOutput.emit('trackend', {
        touch: history[history.length - 1].touch,
        timestamp: Date.now(),
        count: 0,
        history: history
      });
      delete this.touchHistory[i];
    }
  }
  function TouchTracker(options) {
    this.selective = options.selective;
    this.touchLimit = options.touchLimit || 1;
    this.touchHistory = {};
    this.eventInput = new EventHandler();
    this.eventOutput = new EventHandler();
    EventHandler.setInputHandler(this, this.eventInput);
    EventHandler.setOutputHandler(this, this.eventOutput);
    this.eventInput.on('touchstart', _handleStart.bind(this));
    this.eventInput.on('touchmove', _handleMove.bind(this));
    this.eventInput.on('touchend', _handleEnd.bind(this));
    this.eventInput.on('touchcancel', _handleEnd.bind(this));
    this.eventInput.on('unpipe', _handleUnpipe.bind(this));
    this.isTouched = false;
  }
  TouchTracker.prototype.track = function track(data) {
    this.touchHistory[data.identifier] = [data];
  };
  module.exports = TouchTracker;
  global.define = __define;
  return module.exports;
});



System.register("npm:lodash@3.7.0/index", ["github:jspm/nodelibs-process@0.1.1"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  "format cjs";
  (function(process) {
    ;
    (function() {
      var undefined;
      var VERSION = '3.7.0';
      var BIND_FLAG = 1,
          BIND_KEY_FLAG = 2,
          CURRY_BOUND_FLAG = 4,
          CURRY_FLAG = 8,
          CURRY_RIGHT_FLAG = 16,
          PARTIAL_FLAG = 32,
          PARTIAL_RIGHT_FLAG = 64,
          ARY_FLAG = 128,
          REARG_FLAG = 256;
      var DEFAULT_TRUNC_LENGTH = 30,
          DEFAULT_TRUNC_OMISSION = '...';
      var HOT_COUNT = 150,
          HOT_SPAN = 16;
      var LAZY_DROP_WHILE_FLAG = 0,
          LAZY_FILTER_FLAG = 1,
          LAZY_MAP_FLAG = 2;
      var FUNC_ERROR_TEXT = 'Expected a function';
      var PLACEHOLDER = '__lodash_placeholder__';
      var argsTag = '[object Arguments]',
          arrayTag = '[object Array]',
          boolTag = '[object Boolean]',
          dateTag = '[object Date]',
          errorTag = '[object Error]',
          funcTag = '[object Function]',
          mapTag = '[object Map]',
          numberTag = '[object Number]',
          objectTag = '[object Object]',
          regexpTag = '[object RegExp]',
          setTag = '[object Set]',
          stringTag = '[object String]',
          weakMapTag = '[object WeakMap]';
      var arrayBufferTag = '[object ArrayBuffer]',
          float32Tag = '[object Float32Array]',
          float64Tag = '[object Float64Array]',
          int8Tag = '[object Int8Array]',
          int16Tag = '[object Int16Array]',
          int32Tag = '[object Int32Array]',
          uint8Tag = '[object Uint8Array]',
          uint8ClampedTag = '[object Uint8ClampedArray]',
          uint16Tag = '[object Uint16Array]',
          uint32Tag = '[object Uint32Array]';
      var reEmptyStringLeading = /\b__p \+= '';/g,
          reEmptyStringMiddle = /\b(__p \+=) '' \+/g,
          reEmptyStringTrailing = /(__e\(.*?\)|\b__t\)) \+\n'';/g;
      var reEscapedHtml = /&(?:amp|lt|gt|quot|#39|#96);/g,
          reUnescapedHtml = /[&<>"'`]/g,
          reHasEscapedHtml = RegExp(reEscapedHtml.source),
          reHasUnescapedHtml = RegExp(reUnescapedHtml.source);
      var reEscape = /<%-([\s\S]+?)%>/g,
          reEvaluate = /<%([\s\S]+?)%>/g,
          reInterpolate = /<%=([\s\S]+?)%>/g;
      var reIsDeepProp = /\.|\[(?:[^[\]]+|(["'])(?:(?!\1)[^\n\\]|\\.)*?)\1\]/,
          reIsPlainProp = /^\w*$/,
          rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\n\\]|\\.)*?)\2)\]/g;
      var reRegExpChars = /[.*+?^${}()|[\]\/\\]/g,
          reHasRegExpChars = RegExp(reRegExpChars.source);
      var reComboMark = /[\u0300-\u036f\ufe20-\ufe23]/g;
      var reEscapeChar = /\\(\\)?/g;
      var reEsTemplate = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g;
      var reFlags = /\w*$/;
      var reHasHexPrefix = /^0[xX]/;
      var reIsHostCtor = /^\[object .+?Constructor\]$/;
      var reLatin1 = /[\xc0-\xd6\xd8-\xde\xdf-\xf6\xf8-\xff]/g;
      var reNoMatch = /($^)/;
      var reUnescapedString = /['\n\r\u2028\u2029\\]/g;
      var reWords = (function() {
        var upper = '[A-Z\\xc0-\\xd6\\xd8-\\xde]',
            lower = '[a-z\\xdf-\\xf6\\xf8-\\xff]+';
        return RegExp(upper + '+(?=' + upper + lower + ')|' + upper + '?' + lower + '|' + upper + '+|[0-9]+', 'g');
      }());
      var whitespace = (' \t\x0b\f\xa0\ufeff' + '\n\r\u2028\u2029' + '\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000');
      var contextProps = ['Array', 'ArrayBuffer', 'Date', 'Error', 'Float32Array', 'Float64Array', 'Function', 'Int8Array', 'Int16Array', 'Int32Array', 'Math', 'Number', 'Object', 'RegExp', 'Set', 'String', '_', 'clearTimeout', 'document', 'isFinite', 'parseInt', 'setTimeout', 'TypeError', 'Uint8Array', 'Uint8ClampedArray', 'Uint16Array', 'Uint32Array', 'WeakMap', 'window'];
      var templateCounter = -1;
      var typedArrayTags = {};
      typedArrayTags[float32Tag] = typedArrayTags[float64Tag] = typedArrayTags[int8Tag] = typedArrayTags[int16Tag] = typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] = typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] = typedArrayTags[uint32Tag] = true;
      typedArrayTags[argsTag] = typedArrayTags[arrayTag] = typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] = typedArrayTags[dateTag] = typedArrayTags[errorTag] = typedArrayTags[funcTag] = typedArrayTags[mapTag] = typedArrayTags[numberTag] = typedArrayTags[objectTag] = typedArrayTags[regexpTag] = typedArrayTags[setTag] = typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = false;
      var cloneableTags = {};
      cloneableTags[argsTag] = cloneableTags[arrayTag] = cloneableTags[arrayBufferTag] = cloneableTags[boolTag] = cloneableTags[dateTag] = cloneableTags[float32Tag] = cloneableTags[float64Tag] = cloneableTags[int8Tag] = cloneableTags[int16Tag] = cloneableTags[int32Tag] = cloneableTags[numberTag] = cloneableTags[objectTag] = cloneableTags[regexpTag] = cloneableTags[stringTag] = cloneableTags[uint8Tag] = cloneableTags[uint8ClampedTag] = cloneableTags[uint16Tag] = cloneableTags[uint32Tag] = true;
      cloneableTags[errorTag] = cloneableTags[funcTag] = cloneableTags[mapTag] = cloneableTags[setTag] = cloneableTags[weakMapTag] = false;
      var debounceOptions = {
        'leading': false,
        'maxWait': 0,
        'trailing': false
      };
      var deburredLetters = {
        '\xc0': 'A',
        '\xc1': 'A',
        '\xc2': 'A',
        '\xc3': 'A',
        '\xc4': 'A',
        '\xc5': 'A',
        '\xe0': 'a',
        '\xe1': 'a',
        '\xe2': 'a',
        '\xe3': 'a',
        '\xe4': 'a',
        '\xe5': 'a',
        '\xc7': 'C',
        '\xe7': 'c',
        '\xd0': 'D',
        '\xf0': 'd',
        '\xc8': 'E',
        '\xc9': 'E',
        '\xca': 'E',
        '\xcb': 'E',
        '\xe8': 'e',
        '\xe9': 'e',
        '\xea': 'e',
        '\xeb': 'e',
        '\xcC': 'I',
        '\xcd': 'I',
        '\xce': 'I',
        '\xcf': 'I',
        '\xeC': 'i',
        '\xed': 'i',
        '\xee': 'i',
        '\xef': 'i',
        '\xd1': 'N',
        '\xf1': 'n',
        '\xd2': 'O',
        '\xd3': 'O',
        '\xd4': 'O',
        '\xd5': 'O',
        '\xd6': 'O',
        '\xd8': 'O',
        '\xf2': 'o',
        '\xf3': 'o',
        '\xf4': 'o',
        '\xf5': 'o',
        '\xf6': 'o',
        '\xf8': 'o',
        '\xd9': 'U',
        '\xda': 'U',
        '\xdb': 'U',
        '\xdc': 'U',
        '\xf9': 'u',
        '\xfa': 'u',
        '\xfb': 'u',
        '\xfc': 'u',
        '\xdd': 'Y',
        '\xfd': 'y',
        '\xff': 'y',
        '\xc6': 'Ae',
        '\xe6': 'ae',
        '\xde': 'Th',
        '\xfe': 'th',
        '\xdf': 'ss'
      };
      var htmlEscapes = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '`': '&#96;'
      };
      var htmlUnescapes = {
        '&amp;': '&',
        '&lt;': '<',
        '&gt;': '>',
        '&quot;': '"',
        '&#39;': "'",
        '&#96;': '`'
      };
      var objectTypes = {
        'function': true,
        'object': true
      };
      var stringEscapes = {
        '\\': '\\',
        "'": "'",
        '\n': 'n',
        '\r': 'r',
        '\u2028': 'u2028',
        '\u2029': 'u2029'
      };
      var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;
      var freeModule = objectTypes[typeof module] && module && !module.nodeType && module;
      var freeGlobal = freeExports && freeModule && typeof global == 'object' && global && global.Object && global;
      var freeSelf = objectTypes[typeof self] && self && self.Object && self;
      var freeWindow = objectTypes[typeof window] && window && window.Object && window;
      var moduleExports = freeModule && freeModule.exports === freeExports && freeExports;
      var root = freeGlobal || ((freeWindow !== (this && this.window)) && freeWindow) || freeSelf || this;
      function baseCompareAscending(value, other) {
        if (value !== other) {
          var valIsReflexive = value === value,
              othIsReflexive = other === other;
          if (value > other || !valIsReflexive || (value === undefined && othIsReflexive)) {
            return 1;
          }
          if (value < other || !othIsReflexive || (other === undefined && valIsReflexive)) {
            return -1;
          }
        }
        return 0;
      }
      function baseFindIndex(array, predicate, fromRight) {
        var length = array.length,
            index = fromRight ? length : -1;
        while ((fromRight ? index-- : ++index < length)) {
          if (predicate(array[index], index, array)) {
            return index;
          }
        }
        return -1;
      }
      function baseIndexOf(array, value, fromIndex) {
        if (value !== value) {
          return indexOfNaN(array, fromIndex);
        }
        var index = fromIndex - 1,
            length = array.length;
        while (++index < length) {
          if (array[index] === value) {
            return index;
          }
        }
        return -1;
      }
      function baseIsFunction(value) {
        return typeof value == 'function' || false;
      }
      function baseToString(value) {
        if (typeof value == 'string') {
          return value;
        }
        return value == null ? '' : (value + '');
      }
      function charAtCallback(string) {
        return string.charCodeAt(0);
      }
      function charsLeftIndex(string, chars) {
        var index = -1,
            length = string.length;
        while (++index < length && chars.indexOf(string.charAt(index)) > -1) {}
        return index;
      }
      function charsRightIndex(string, chars) {
        var index = string.length;
        while (index-- && chars.indexOf(string.charAt(index)) > -1) {}
        return index;
      }
      function compareAscending(object, other) {
        return baseCompareAscending(object.criteria, other.criteria) || (object.index - other.index);
      }
      function compareMultiple(object, other, orders) {
        var index = -1,
            objCriteria = object.criteria,
            othCriteria = other.criteria,
            length = objCriteria.length,
            ordersLength = orders.length;
        while (++index < length) {
          var result = baseCompareAscending(objCriteria[index], othCriteria[index]);
          if (result) {
            if (index >= ordersLength) {
              return result;
            }
            return result * (orders[index] ? 1 : -1);
          }
        }
        return object.index - other.index;
      }
      function deburrLetter(letter) {
        return deburredLetters[letter];
      }
      function escapeHtmlChar(chr) {
        return htmlEscapes[chr];
      }
      function escapeStringChar(chr) {
        return '\\' + stringEscapes[chr];
      }
      function indexOfNaN(array, fromIndex, fromRight) {
        var length = array.length,
            index = fromIndex + (fromRight ? 0 : -1);
        while ((fromRight ? index-- : ++index < length)) {
          var other = array[index];
          if (other !== other) {
            return index;
          }
        }
        return -1;
      }
      function isObjectLike(value) {
        return !!value && typeof value == 'object';
      }
      function isSpace(charCode) {
        return ((charCode <= 160 && (charCode >= 9 && charCode <= 13) || charCode == 32 || charCode == 160) || charCode == 5760 || charCode == 6158 || (charCode >= 8192 && (charCode <= 8202 || charCode == 8232 || charCode == 8233 || charCode == 8239 || charCode == 8287 || charCode == 12288 || charCode == 65279)));
      }
      function replaceHolders(array, placeholder) {
        var index = -1,
            length = array.length,
            resIndex = -1,
            result = [];
        while (++index < length) {
          if (array[index] === placeholder) {
            array[index] = PLACEHOLDER;
            result[++resIndex] = index;
          }
        }
        return result;
      }
      function sortedUniq(array, iteratee) {
        var seen,
            index = -1,
            length = array.length,
            resIndex = -1,
            result = [];
        while (++index < length) {
          var value = array[index],
              computed = iteratee ? iteratee(value, index, array) : value;
          if (!index || seen !== computed) {
            seen = computed;
            result[++resIndex] = value;
          }
        }
        return result;
      }
      function trimmedLeftIndex(string) {
        var index = -1,
            length = string.length;
        while (++index < length && isSpace(string.charCodeAt(index))) {}
        return index;
      }
      function trimmedRightIndex(string) {
        var index = string.length;
        while (index-- && isSpace(string.charCodeAt(index))) {}
        return index;
      }
      function unescapeHtmlChar(chr) {
        return htmlUnescapes[chr];
      }
      function runInContext(context) {
        context = context ? _.defaults(root.Object(), context, _.pick(root, contextProps)) : root;
        var Array = context.Array,
            Date = context.Date,
            Error = context.Error,
            Function = context.Function,
            Math = context.Math,
            Number = context.Number,
            Object = context.Object,
            RegExp = context.RegExp,
            String = context.String,
            TypeError = context.TypeError;
        var arrayProto = Array.prototype,
            objectProto = Object.prototype,
            stringProto = String.prototype;
        var document = (document = context.window) && document.document;
        var fnToString = Function.prototype.toString;
        var hasOwnProperty = objectProto.hasOwnProperty;
        var idCounter = 0;
        var objToString = objectProto.toString;
        var oldDash = context._;
        var reIsNative = RegExp('^' + escapeRegExp(objToString).replace(/toString|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$');
        var ArrayBuffer = isNative(ArrayBuffer = context.ArrayBuffer) && ArrayBuffer,
            bufferSlice = isNative(bufferSlice = ArrayBuffer && new ArrayBuffer(0).slice) && bufferSlice,
            ceil = Math.ceil,
            clearTimeout = context.clearTimeout,
            floor = Math.floor,
            getOwnPropertySymbols = isNative(getOwnPropertySymbols = Object.getOwnPropertySymbols) && getOwnPropertySymbols,
            getPrototypeOf = isNative(getPrototypeOf = Object.getPrototypeOf) && getPrototypeOf,
            push = arrayProto.push,
            preventExtensions = isNative(Object.preventExtensions = Object.preventExtensions) && preventExtensions,
            propertyIsEnumerable = objectProto.propertyIsEnumerable,
            Set = isNative(Set = context.Set) && Set,
            setTimeout = context.setTimeout,
            splice = arrayProto.splice,
            Uint8Array = isNative(Uint8Array = context.Uint8Array) && Uint8Array,
            WeakMap = isNative(WeakMap = context.WeakMap) && WeakMap;
        var Float64Array = (function() {
          try {
            var func = isNative(func = context.Float64Array) && func,
                result = new func(new ArrayBuffer(10), 0, 1) && func;
          } catch (e) {}
          return result;
        }());
        var nativeAssign = (function() {
          var object = {'1': 0},
              func = preventExtensions && isNative(func = Object.assign) && func;
          try {
            func(preventExtensions(object), 'xo');
          } catch (e) {}
          return !object[1] && func;
        }());
        var nativeIsArray = isNative(nativeIsArray = Array.isArray) && nativeIsArray,
            nativeCreate = isNative(nativeCreate = Object.create) && nativeCreate,
            nativeIsFinite = context.isFinite,
            nativeKeys = isNative(nativeKeys = Object.keys) && nativeKeys,
            nativeMax = Math.max,
            nativeMin = Math.min,
            nativeNow = isNative(nativeNow = Date.now) && nativeNow,
            nativeNumIsFinite = isNative(nativeNumIsFinite = Number.isFinite) && nativeNumIsFinite,
            nativeParseInt = context.parseInt,
            nativeRandom = Math.random;
        var NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY,
            POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
        var MAX_ARRAY_LENGTH = Math.pow(2, 32) - 1,
            MAX_ARRAY_INDEX = MAX_ARRAY_LENGTH - 1,
            HALF_MAX_ARRAY_LENGTH = MAX_ARRAY_LENGTH >>> 1;
        var FLOAT64_BYTES_PER_ELEMENT = Float64Array ? Float64Array.BYTES_PER_ELEMENT : 0;
        var MAX_SAFE_INTEGER = Math.pow(2, 53) - 1;
        var metaMap = WeakMap && new WeakMap;
        var realNames = {};
        function lodash(value) {
          if (isObjectLike(value) && !isArray(value) && !(value instanceof LazyWrapper)) {
            if (value instanceof LodashWrapper) {
              return value;
            }
            if (hasOwnProperty.call(value, '__chain__') && hasOwnProperty.call(value, '__wrapped__')) {
              return wrapperClone(value);
            }
          }
          return new LodashWrapper(value);
        }
        function baseLodash() {}
        function LodashWrapper(value, chainAll, actions) {
          this.__wrapped__ = value;
          this.__actions__ = actions || [];
          this.__chain__ = !!chainAll;
        }
        var support = lodash.support = {};
        (function(x) {
          var Ctor = function() {
            this.x = x;
          },
              object = {
                '0': x,
                'length': x
              },
              props = [];
          Ctor.prototype = {
            'valueOf': x,
            'y': x
          };
          for (var key in new Ctor) {
            props.push(key);
          }
          support.funcDecomp = /\bthis\b/.test(function() {
            return this;
          });
          support.funcNames = typeof Function.name == 'string';
          try {
            support.dom = document.createDocumentFragment().nodeType === 11;
          } catch (e) {
            support.dom = false;
          }
          try {
            support.nonEnumArgs = !propertyIsEnumerable.call(arguments, 1);
          } catch (e) {
            support.nonEnumArgs = true;
          }
        }(1, 0));
        lodash.templateSettings = {
          'escape': reEscape,
          'evaluate': reEvaluate,
          'interpolate': reInterpolate,
          'variable': '',
          'imports': {'_': lodash}
        };
        function LazyWrapper(value) {
          this.__wrapped__ = value;
          this.__actions__ = null;
          this.__dir__ = 1;
          this.__dropCount__ = 0;
          this.__filtered__ = false;
          this.__iteratees__ = null;
          this.__takeCount__ = POSITIVE_INFINITY;
          this.__views__ = null;
        }
        function lazyClone() {
          var actions = this.__actions__,
              iteratees = this.__iteratees__,
              views = this.__views__,
              result = new LazyWrapper(this.__wrapped__);
          result.__actions__ = actions ? arrayCopy(actions) : null;
          result.__dir__ = this.__dir__;
          result.__filtered__ = this.__filtered__;
          result.__iteratees__ = iteratees ? arrayCopy(iteratees) : null;
          result.__takeCount__ = this.__takeCount__;
          result.__views__ = views ? arrayCopy(views) : null;
          return result;
        }
        function lazyReverse() {
          if (this.__filtered__) {
            var result = new LazyWrapper(this);
            result.__dir__ = -1;
            result.__filtered__ = true;
          } else {
            result = this.clone();
            result.__dir__ *= -1;
          }
          return result;
        }
        function lazyValue() {
          var array = this.__wrapped__.value();
          if (!isArray(array)) {
            return baseWrapperValue(array, this.__actions__);
          }
          var dir = this.__dir__,
              isRight = dir < 0,
              view = getView(0, array.length, this.__views__),
              start = view.start,
              end = view.end,
              length = end - start,
              index = isRight ? end : (start - 1),
              takeCount = nativeMin(length, this.__takeCount__),
              iteratees = this.__iteratees__,
              iterLength = iteratees ? iteratees.length : 0,
              resIndex = 0,
              result = [];
          outer: while (length-- && resIndex < takeCount) {
            index += dir;
            var iterIndex = -1,
                value = array[index];
            while (++iterIndex < iterLength) {
              var data = iteratees[iterIndex],
                  iteratee = data.iteratee,
                  type = data.type;
              if (type == LAZY_DROP_WHILE_FLAG) {
                if (data.done && (isRight ? (index > data.index) : (index < data.index))) {
                  data.count = 0;
                  data.done = false;
                }
                data.index = index;
                if (!data.done) {
                  var limit = data.limit;
                  if (!(data.done = limit > -1 ? (data.count++ >= limit) : !iteratee(value))) {
                    continue outer;
                  }
                }
              } else {
                var computed = iteratee(value);
                if (type == LAZY_MAP_FLAG) {
                  value = computed;
                } else if (!computed) {
                  if (type == LAZY_FILTER_FLAG) {
                    continue outer;
                  } else {
                    break outer;
                  }
                }
              }
            }
            result[resIndex++] = value;
          }
          return result;
        }
        function MapCache() {
          this.__data__ = {};
        }
        function mapDelete(key) {
          return this.has(key) && delete this.__data__[key];
        }
        function mapGet(key) {
          return key == '__proto__' ? undefined : this.__data__[key];
        }
        function mapHas(key) {
          return key != '__proto__' && hasOwnProperty.call(this.__data__, key);
        }
        function mapSet(key, value) {
          if (key != '__proto__') {
            this.__data__[key] = value;
          }
          return this;
        }
        function SetCache(values) {
          var length = values ? values.length : 0;
          this.data = {
            'hash': nativeCreate(null),
            'set': new Set
          };
          while (length--) {
            this.push(values[length]);
          }
        }
        function cacheIndexOf(cache, value) {
          var data = cache.data,
              result = (typeof value == 'string' || isObject(value)) ? data.set.has(value) : data.hash[value];
          return result ? 0 : -1;
        }
        function cachePush(value) {
          var data = this.data;
          if (typeof value == 'string' || isObject(value)) {
            data.set.add(value);
          } else {
            data.hash[value] = true;
          }
        }
        function arrayCopy(source, array) {
          var index = -1,
              length = source.length;
          array || (array = Array(length));
          while (++index < length) {
            array[index] = source[index];
          }
          return array;
        }
        function arrayEach(array, iteratee) {
          var index = -1,
              length = array.length;
          while (++index < length) {
            if (iteratee(array[index], index, array) === false) {
              break;
            }
          }
          return array;
        }
        function arrayEachRight(array, iteratee) {
          var length = array.length;
          while (length--) {
            if (iteratee(array[length], length, array) === false) {
              break;
            }
          }
          return array;
        }
        function arrayEvery(array, predicate) {
          var index = -1,
              length = array.length;
          while (++index < length) {
            if (!predicate(array[index], index, array)) {
              return false;
            }
          }
          return true;
        }
        function arrayFilter(array, predicate) {
          var index = -1,
              length = array.length,
              resIndex = -1,
              result = [];
          while (++index < length) {
            var value = array[index];
            if (predicate(value, index, array)) {
              result[++resIndex] = value;
            }
          }
          return result;
        }
        function arrayMap(array, iteratee) {
          var index = -1,
              length = array.length,
              result = Array(length);
          while (++index < length) {
            result[index] = iteratee(array[index], index, array);
          }
          return result;
        }
        function arrayMax(array) {
          var index = -1,
              length = array.length,
              result = NEGATIVE_INFINITY;
          while (++index < length) {
            var value = array[index];
            if (value > result) {
              result = value;
            }
          }
          return result;
        }
        function arrayMin(array) {
          var index = -1,
              length = array.length,
              result = POSITIVE_INFINITY;
          while (++index < length) {
            var value = array[index];
            if (value < result) {
              result = value;
            }
          }
          return result;
        }
        function arrayReduce(array, iteratee, accumulator, initFromArray) {
          var index = -1,
              length = array.length;
          if (initFromArray && length) {
            accumulator = array[++index];
          }
          while (++index < length) {
            accumulator = iteratee(accumulator, array[index], index, array);
          }
          return accumulator;
        }
        function arrayReduceRight(array, iteratee, accumulator, initFromArray) {
          var length = array.length;
          if (initFromArray && length) {
            accumulator = array[--length];
          }
          while (length--) {
            accumulator = iteratee(accumulator, array[length], length, array);
          }
          return accumulator;
        }
        function arraySome(array, predicate) {
          var index = -1,
              length = array.length;
          while (++index < length) {
            if (predicate(array[index], index, array)) {
              return true;
            }
          }
          return false;
        }
        function arraySum(array) {
          var length = array.length,
              result = 0;
          while (length--) {
            result += +array[length] || 0;
          }
          return result;
        }
        function assignDefaults(objectValue, sourceValue) {
          return objectValue === undefined ? sourceValue : objectValue;
        }
        function assignOwnDefaults(objectValue, sourceValue, key, object) {
          return (objectValue === undefined || !hasOwnProperty.call(object, key)) ? sourceValue : objectValue;
        }
        function assignWith(object, source, customizer) {
          var props = keys(source);
          push.apply(props, getSymbols(source));
          var index = -1,
              length = props.length;
          while (++index < length) {
            var key = props[index],
                value = object[key],
                result = customizer(value, source[key], key, object, source);
            if ((result === result ? (result !== value) : (value === value)) || (value === undefined && !(key in object))) {
              object[key] = result;
            }
          }
          return object;
        }
        var baseAssign = nativeAssign || function(object, source) {
          return source == null ? object : baseCopy(source, getSymbols(source), baseCopy(source, keys(source), object));
        };
        function baseAt(collection, props) {
          var index = -1,
              length = collection.length,
              isArr = isLength(length),
              propsLength = props.length,
              result = Array(propsLength);
          while (++index < propsLength) {
            var key = props[index];
            if (isArr) {
              result[index] = isIndex(key, length) ? collection[key] : undefined;
            } else {
              result[index] = collection[key];
            }
          }
          return result;
        }
        function baseCopy(source, props, object) {
          object || (object = {});
          var index = -1,
              length = props.length;
          while (++index < length) {
            var key = props[index];
            object[key] = source[key];
          }
          return object;
        }
        function baseCallback(func, thisArg, argCount) {
          var type = typeof func;
          if (type == 'function') {
            return thisArg === undefined ? func : bindCallback(func, thisArg, argCount);
          }
          if (func == null) {
            return identity;
          }
          if (type == 'object') {
            return baseMatches(func);
          }
          return thisArg === undefined ? property(func) : baseMatchesProperty(func, thisArg);
        }
        function baseClone(value, isDeep, customizer, key, object, stackA, stackB) {
          var result;
          if (customizer) {
            result = object ? customizer(value, key, object) : customizer(value);
          }
          if (result !== undefined) {
            return result;
          }
          if (!isObject(value)) {
            return value;
          }
          var isArr = isArray(value);
          if (isArr) {
            result = initCloneArray(value);
            if (!isDeep) {
              return arrayCopy(value, result);
            }
          } else {
            var tag = objToString.call(value),
                isFunc = tag == funcTag;
            if (tag == objectTag || tag == argsTag || (isFunc && !object)) {
              result = initCloneObject(isFunc ? {} : value);
              if (!isDeep) {
                return baseAssign(result, value);
              }
            } else {
              return cloneableTags[tag] ? initCloneByTag(value, tag, isDeep) : (object ? value : {});
            }
          }
          stackA || (stackA = []);
          stackB || (stackB = []);
          var length = stackA.length;
          while (length--) {
            if (stackA[length] == value) {
              return stackB[length];
            }
          }
          stackA.push(value);
          stackB.push(result);
          (isArr ? arrayEach : baseForOwn)(value, function(subValue, key) {
            result[key] = baseClone(subValue, isDeep, customizer, key, value, stackA, stackB);
          });
          return result;
        }
        var baseCreate = (function() {
          function Object() {}
          return function(prototype) {
            if (isObject(prototype)) {
              Object.prototype = prototype;
              var result = new Object;
              Object.prototype = null;
            }
            return result || context.Object();
          };
        }());
        function baseDelay(func, wait, args) {
          if (typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          return setTimeout(function() {
            func.apply(undefined, args);
          }, wait);
        }
        function baseDifference(array, values) {
          var length = array ? array.length : 0,
              result = [];
          if (!length) {
            return result;
          }
          var index = -1,
              indexOf = getIndexOf(),
              isCommon = indexOf == baseIndexOf,
              cache = (isCommon && values.length >= 200) ? createCache(values) : null,
              valuesLength = values.length;
          if (cache) {
            indexOf = cacheIndexOf;
            isCommon = false;
            values = cache;
          }
          outer: while (++index < length) {
            var value = array[index];
            if (isCommon && value === value) {
              var valuesIndex = valuesLength;
              while (valuesIndex--) {
                if (values[valuesIndex] === value) {
                  continue outer;
                }
              }
              result.push(value);
            } else if (indexOf(values, value, 0) < 0) {
              result.push(value);
            }
          }
          return result;
        }
        var baseEach = createBaseEach(baseForOwn);
        var baseEachRight = createBaseEach(baseForOwnRight, true);
        function baseEvery(collection, predicate) {
          var result = true;
          baseEach(collection, function(value, index, collection) {
            result = !!predicate(value, index, collection);
            return result;
          });
          return result;
        }
        function baseFill(array, value, start, end) {
          var length = array.length;
          start = start == null ? 0 : (+start || 0);
          if (start < 0) {
            start = -start > length ? 0 : (length + start);
          }
          end = (end === undefined || end > length) ? length : (+end || 0);
          if (end < 0) {
            end += length;
          }
          length = start > end ? 0 : (end >>> 0);
          start >>>= 0;
          while (start < length) {
            array[start++] = value;
          }
          return array;
        }
        function baseFilter(collection, predicate) {
          var result = [];
          baseEach(collection, function(value, index, collection) {
            if (predicate(value, index, collection)) {
              result.push(value);
            }
          });
          return result;
        }
        function baseFind(collection, predicate, eachFunc, retKey) {
          var result;
          eachFunc(collection, function(value, key, collection) {
            if (predicate(value, key, collection)) {
              result = retKey ? key : value;
              return false;
            }
          });
          return result;
        }
        function baseFlatten(array, isDeep, isStrict) {
          var index = -1,
              length = array.length,
              resIndex = -1,
              result = [];
          while (++index < length) {
            var value = array[index];
            if (isObjectLike(value) && isLength(value.length) && (isArray(value) || isArguments(value))) {
              if (isDeep) {
                value = baseFlatten(value, isDeep, isStrict);
              }
              var valIndex = -1,
                  valLength = value.length;
              result.length += valLength;
              while (++valIndex < valLength) {
                result[++resIndex] = value[valIndex];
              }
            } else if (!isStrict) {
              result[++resIndex] = value;
            }
          }
          return result;
        }
        var baseFor = createBaseFor();
        var baseForRight = createBaseFor(true);
        function baseForIn(object, iteratee) {
          return baseFor(object, iteratee, keysIn);
        }
        function baseForOwn(object, iteratee) {
          return baseFor(object, iteratee, keys);
        }
        function baseForOwnRight(object, iteratee) {
          return baseForRight(object, iteratee, keys);
        }
        function baseFunctions(object, props) {
          var index = -1,
              length = props.length,
              resIndex = -1,
              result = [];
          while (++index < length) {
            var key = props[index];
            if (isFunction(object[key])) {
              result[++resIndex] = key;
            }
          }
          return result;
        }
        function baseGet(object, path, pathKey) {
          if (object == null) {
            return ;
          }
          if (pathKey !== undefined && pathKey in toObject(object)) {
            path = [pathKey];
          }
          var index = -1,
              length = path.length;
          while (object != null && ++index < length) {
            var result = object = object[path[index]];
          }
          return result;
        }
        function baseIsEqual(value, other, customizer, isLoose, stackA, stackB) {
          if (value === other) {
            return value !== 0 || (1 / value == 1 / other);
          }
          var valType = typeof value,
              othType = typeof other;
          if ((valType != 'function' && valType != 'object' && othType != 'function' && othType != 'object') || value == null || other == null) {
            return value !== value && other !== other;
          }
          return baseIsEqualDeep(value, other, baseIsEqual, customizer, isLoose, stackA, stackB);
        }
        function baseIsEqualDeep(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
          var objIsArr = isArray(object),
              othIsArr = isArray(other),
              objTag = arrayTag,
              othTag = arrayTag;
          if (!objIsArr) {
            objTag = objToString.call(object);
            if (objTag == argsTag) {
              objTag = objectTag;
            } else if (objTag != objectTag) {
              objIsArr = isTypedArray(object);
            }
          }
          if (!othIsArr) {
            othTag = objToString.call(other);
            if (othTag == argsTag) {
              othTag = objectTag;
            } else if (othTag != objectTag) {
              othIsArr = isTypedArray(other);
            }
          }
          var objIsObj = objTag == objectTag,
              othIsObj = othTag == objectTag,
              isSameTag = objTag == othTag;
          if (isSameTag && !(objIsArr || objIsObj)) {
            return equalByTag(object, other, objTag);
          }
          if (!isLoose) {
            var valWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
                othWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');
            if (valWrapped || othWrapped) {
              return equalFunc(valWrapped ? object.value() : object, othWrapped ? other.value() : other, customizer, isLoose, stackA, stackB);
            }
          }
          if (!isSameTag) {
            return false;
          }
          stackA || (stackA = []);
          stackB || (stackB = []);
          var length = stackA.length;
          while (length--) {
            if (stackA[length] == object) {
              return stackB[length] == other;
            }
          }
          stackA.push(object);
          stackB.push(other);
          var result = (objIsArr ? equalArrays : equalObjects)(object, other, equalFunc, customizer, isLoose, stackA, stackB);
          stackA.pop();
          stackB.pop();
          return result;
        }
        function baseIsMatch(object, props, values, strictCompareFlags, customizer) {
          var index = -1,
              length = props.length,
              noCustomizer = !customizer;
          while (++index < length) {
            if ((noCustomizer && strictCompareFlags[index]) ? values[index] !== object[props[index]] : !(props[index] in object)) {
              return false;
            }
          }
          index = -1;
          while (++index < length) {
            var key = props[index],
                objValue = object[key],
                srcValue = values[index];
            if (noCustomizer && strictCompareFlags[index]) {
              var result = objValue !== undefined || (key in object);
            } else {
              result = customizer ? customizer(objValue, srcValue, key) : undefined;
              if (result === undefined) {
                result = baseIsEqual(srcValue, objValue, customizer, true);
              }
            }
            if (!result) {
              return false;
            }
          }
          return true;
        }
        function baseMap(collection, iteratee) {
          var index = -1,
              length = getLength(collection),
              result = isLength(length) ? Array(length) : [];
          baseEach(collection, function(value, key, collection) {
            result[++index] = iteratee(value, key, collection);
          });
          return result;
        }
        function baseMatches(source) {
          var props = keys(source),
              length = props.length;
          if (!length) {
            return constant(true);
          }
          if (length == 1) {
            var key = props[0],
                value = source[key];
            if (isStrictComparable(value)) {
              return function(object) {
                if (object == null) {
                  return false;
                }
                return object[key] === value && (value !== undefined || (key in toObject(object)));
              };
            }
          }
          var values = Array(length),
              strictCompareFlags = Array(length);
          while (length--) {
            value = source[props[length]];
            values[length] = value;
            strictCompareFlags[length] = isStrictComparable(value);
          }
          return function(object) {
            return object != null && baseIsMatch(toObject(object), props, values, strictCompareFlags);
          };
        }
        function baseMatchesProperty(path, value) {
          var isArr = isArray(path),
              isCommon = isKey(path) && isStrictComparable(value),
              pathKey = (path + '');
          path = toPath(path);
          return function(object) {
            if (object == null) {
              return false;
            }
            var key = pathKey;
            object = toObject(object);
            if ((isArr || !isCommon) && !(key in object)) {
              object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
              if (object == null) {
                return false;
              }
              key = last(path);
              object = toObject(object);
            }
            return object[key] === value ? (value !== undefined || (key in object)) : baseIsEqual(value, object[key], null, true);
          };
        }
        function baseMerge(object, source, customizer, stackA, stackB) {
          if (!isObject(object)) {
            return object;
          }
          var isSrcArr = isLength(source.length) && (isArray(source) || isTypedArray(source));
          if (!isSrcArr) {
            var props = keys(source);
            push.apply(props, getSymbols(source));
          }
          arrayEach(props || source, function(srcValue, key) {
            if (props) {
              key = srcValue;
              srcValue = source[key];
            }
            if (isObjectLike(srcValue)) {
              stackA || (stackA = []);
              stackB || (stackB = []);
              baseMergeDeep(object, source, key, baseMerge, customizer, stackA, stackB);
            } else {
              var value = object[key],
                  result = customizer ? customizer(value, srcValue, key, object, source) : undefined,
                  isCommon = result === undefined;
              if (isCommon) {
                result = srcValue;
              }
              if ((isSrcArr || result !== undefined) && (isCommon || (result === result ? (result !== value) : (value === value)))) {
                object[key] = result;
              }
            }
          });
          return object;
        }
        function baseMergeDeep(object, source, key, mergeFunc, customizer, stackA, stackB) {
          var length = stackA.length,
              srcValue = source[key];
          while (length--) {
            if (stackA[length] == srcValue) {
              object[key] = stackB[length];
              return ;
            }
          }
          var value = object[key],
              result = customizer ? customizer(value, srcValue, key, object, source) : undefined,
              isCommon = result === undefined;
          if (isCommon) {
            result = srcValue;
            if (isLength(srcValue.length) && (isArray(srcValue) || isTypedArray(srcValue))) {
              result = isArray(value) ? value : (getLength(value) ? arrayCopy(value) : []);
            } else if (isPlainObject(srcValue) || isArguments(srcValue)) {
              result = isArguments(value) ? toPlainObject(value) : (isPlainObject(value) ? value : {});
            } else {
              isCommon = false;
            }
          }
          stackA.push(srcValue);
          stackB.push(result);
          if (isCommon) {
            object[key] = mergeFunc(result, srcValue, customizer, stackA, stackB);
          } else if (result === result ? (result !== value) : (value === value)) {
            object[key] = result;
          }
        }
        function baseProperty(key) {
          return function(object) {
            return object == null ? undefined : object[key];
          };
        }
        function basePropertyDeep(path) {
          var pathKey = (path + '');
          path = toPath(path);
          return function(object) {
            return baseGet(object, path, pathKey);
          };
        }
        function basePullAt(array, indexes) {
          var length = indexes.length;
          while (length--) {
            var index = parseFloat(indexes[length]);
            if (index != previous && isIndex(index)) {
              var previous = index;
              splice.call(array, index, 1);
            }
          }
          return array;
        }
        function baseRandom(min, max) {
          return min + floor(nativeRandom() * (max - min + 1));
        }
        function baseReduce(collection, iteratee, accumulator, initFromCollection, eachFunc) {
          eachFunc(collection, function(value, index, collection) {
            accumulator = initFromCollection ? (initFromCollection = false, value) : iteratee(accumulator, value, index, collection);
          });
          return accumulator;
        }
        var baseSetData = !metaMap ? identity : function(func, data) {
          metaMap.set(func, data);
          return func;
        };
        function baseSlice(array, start, end) {
          var index = -1,
              length = array.length;
          start = start == null ? 0 : (+start || 0);
          if (start < 0) {
            start = -start > length ? 0 : (length + start);
          }
          end = (end === undefined || end > length) ? length : (+end || 0);
          if (end < 0) {
            end += length;
          }
          length = start > end ? 0 : ((end - start) >>> 0);
          start >>>= 0;
          var result = Array(length);
          while (++index < length) {
            result[index] = array[index + start];
          }
          return result;
        }
        function baseSome(collection, predicate) {
          var result;
          baseEach(collection, function(value, index, collection) {
            result = predicate(value, index, collection);
            return !result;
          });
          return !!result;
        }
        function baseSortBy(array, comparer) {
          var length = array.length;
          array.sort(comparer);
          while (length--) {
            array[length] = array[length].value;
          }
          return array;
        }
        function baseSortByOrder(collection, iteratees, orders) {
          var callback = getCallback(),
              index = -1;
          iteratees = arrayMap(iteratees, function(iteratee) {
            return callback(iteratee);
          });
          var result = baseMap(collection, function(value) {
            var criteria = arrayMap(iteratees, function(iteratee) {
              return iteratee(value);
            });
            return {
              'criteria': criteria,
              'index': ++index,
              'value': value
            };
          });
          return baseSortBy(result, function(object, other) {
            return compareMultiple(object, other, orders);
          });
        }
        function baseSum(collection, iteratee) {
          var result = 0;
          baseEach(collection, function(value, index, collection) {
            result += +iteratee(value, index, collection) || 0;
          });
          return result;
        }
        function baseUniq(array, iteratee) {
          var index = -1,
              indexOf = getIndexOf(),
              length = array.length,
              isCommon = indexOf == baseIndexOf,
              isLarge = isCommon && length >= 200,
              seen = isLarge ? createCache() : null,
              result = [];
          if (seen) {
            indexOf = cacheIndexOf;
            isCommon = false;
          } else {
            isLarge = false;
            seen = iteratee ? [] : result;
          }
          outer: while (++index < length) {
            var value = array[index],
                computed = iteratee ? iteratee(value, index, array) : value;
            if (isCommon && value === value) {
              var seenIndex = seen.length;
              while (seenIndex--) {
                if (seen[seenIndex] === computed) {
                  continue outer;
                }
              }
              if (iteratee) {
                seen.push(computed);
              }
              result.push(value);
            } else if (indexOf(seen, computed, 0) < 0) {
              if (iteratee || isLarge) {
                seen.push(computed);
              }
              result.push(value);
            }
          }
          return result;
        }
        function baseValues(object, props) {
          var index = -1,
              length = props.length,
              result = Array(length);
          while (++index < length) {
            result[index] = object[props[index]];
          }
          return result;
        }
        function baseWhile(array, predicate, isDrop, fromRight) {
          var length = array.length,
              index = fromRight ? length : -1;
          while ((fromRight ? index-- : ++index < length) && predicate(array[index], index, array)) {}
          return isDrop ? baseSlice(array, (fromRight ? 0 : index), (fromRight ? index + 1 : length)) : baseSlice(array, (fromRight ? index + 1 : 0), (fromRight ? length : index));
        }
        function baseWrapperValue(value, actions) {
          var result = value;
          if (result instanceof LazyWrapper) {
            result = result.value();
          }
          var index = -1,
              length = actions.length;
          while (++index < length) {
            var args = [result],
                action = actions[index];
            push.apply(args, action.args);
            result = action.func.apply(action.thisArg, args);
          }
          return result;
        }
        function binaryIndex(array, value, retHighest) {
          var low = 0,
              high = array ? array.length : low;
          if (typeof value == 'number' && value === value && high <= HALF_MAX_ARRAY_LENGTH) {
            while (low < high) {
              var mid = (low + high) >>> 1,
                  computed = array[mid];
              if (retHighest ? (computed <= value) : (computed < value)) {
                low = mid + 1;
              } else {
                high = mid;
              }
            }
            return high;
          }
          return binaryIndexBy(array, value, identity, retHighest);
        }
        function binaryIndexBy(array, value, iteratee, retHighest) {
          value = iteratee(value);
          var low = 0,
              high = array ? array.length : 0,
              valIsNaN = value !== value,
              valIsUndef = value === undefined;
          while (low < high) {
            var mid = floor((low + high) / 2),
                computed = iteratee(array[mid]),
                isReflexive = computed === computed;
            if (valIsNaN) {
              var setLow = isReflexive || retHighest;
            } else if (valIsUndef) {
              setLow = isReflexive && (retHighest || computed !== undefined);
            } else {
              setLow = retHighest ? (computed <= value) : (computed < value);
            }
            if (setLow) {
              low = mid + 1;
            } else {
              high = mid;
            }
          }
          return nativeMin(high, MAX_ARRAY_INDEX);
        }
        function bindCallback(func, thisArg, argCount) {
          if (typeof func != 'function') {
            return identity;
          }
          if (thisArg === undefined) {
            return func;
          }
          switch (argCount) {
            case 1:
              return function(value) {
                return func.call(thisArg, value);
              };
            case 3:
              return function(value, index, collection) {
                return func.call(thisArg, value, index, collection);
              };
            case 4:
              return function(accumulator, value, index, collection) {
                return func.call(thisArg, accumulator, value, index, collection);
              };
            case 5:
              return function(value, other, key, object, source) {
                return func.call(thisArg, value, other, key, object, source);
              };
          }
          return function() {
            return func.apply(thisArg, arguments);
          };
        }
        function bufferClone(buffer) {
          return bufferSlice.call(buffer, 0);
        }
        if (!bufferSlice) {
          bufferClone = !(ArrayBuffer && Uint8Array) ? constant(null) : function(buffer) {
            var byteLength = buffer.byteLength,
                floatLength = Float64Array ? floor(byteLength / FLOAT64_BYTES_PER_ELEMENT) : 0,
                offset = floatLength * FLOAT64_BYTES_PER_ELEMENT,
                result = new ArrayBuffer(byteLength);
            if (floatLength) {
              var view = new Float64Array(result, 0, floatLength);
              view.set(new Float64Array(buffer, 0, floatLength));
            }
            if (byteLength != offset) {
              view = new Uint8Array(result, offset);
              view.set(new Uint8Array(buffer, offset));
            }
            return result;
          };
        }
        function composeArgs(args, partials, holders) {
          var holdersLength = holders.length,
              argsIndex = -1,
              argsLength = nativeMax(args.length - holdersLength, 0),
              leftIndex = -1,
              leftLength = partials.length,
              result = Array(argsLength + leftLength);
          while (++leftIndex < leftLength) {
            result[leftIndex] = partials[leftIndex];
          }
          while (++argsIndex < holdersLength) {
            result[holders[argsIndex]] = args[argsIndex];
          }
          while (argsLength--) {
            result[leftIndex++] = args[argsIndex++];
          }
          return result;
        }
        function composeArgsRight(args, partials, holders) {
          var holdersIndex = -1,
              holdersLength = holders.length,
              argsIndex = -1,
              argsLength = nativeMax(args.length - holdersLength, 0),
              rightIndex = -1,
              rightLength = partials.length,
              result = Array(argsLength + rightLength);
          while (++argsIndex < argsLength) {
            result[argsIndex] = args[argsIndex];
          }
          var pad = argsIndex;
          while (++rightIndex < rightLength) {
            result[pad + rightIndex] = partials[rightIndex];
          }
          while (++holdersIndex < holdersLength) {
            result[pad + holders[holdersIndex]] = args[argsIndex++];
          }
          return result;
        }
        function createAggregator(setter, initializer) {
          return function(collection, iteratee, thisArg) {
            var result = initializer ? initializer() : {};
            iteratee = getCallback(iteratee, thisArg, 3);
            if (isArray(collection)) {
              var index = -1,
                  length = collection.length;
              while (++index < length) {
                var value = collection[index];
                setter(result, value, iteratee(value, index, collection), collection);
              }
            } else {
              baseEach(collection, function(value, key, collection) {
                setter(result, value, iteratee(value, key, collection), collection);
              });
            }
            return result;
          };
        }
        function createAssigner(assigner) {
          return restParam(function(object, sources) {
            var index = -1,
                length = object == null ? 0 : sources.length,
                customizer = length > 2 && sources[length - 2],
                guard = length > 2 && sources[2],
                thisArg = length > 1 && sources[length - 1];
            if (typeof customizer == 'function') {
              customizer = bindCallback(customizer, thisArg, 5);
              length -= 2;
            } else {
              customizer = typeof thisArg == 'function' ? thisArg : null;
              length -= (customizer ? 1 : 0);
            }
            if (guard && isIterateeCall(sources[0], sources[1], guard)) {
              customizer = length < 3 ? null : customizer;
              length = 1;
            }
            while (++index < length) {
              var source = sources[index];
              if (source) {
                assigner(object, source, customizer);
              }
            }
            return object;
          });
        }
        function createBaseEach(eachFunc, fromRight) {
          return function(collection, iteratee) {
            var length = collection ? getLength(collection) : 0;
            if (!isLength(length)) {
              return eachFunc(collection, iteratee);
            }
            var index = fromRight ? length : -1,
                iterable = toObject(collection);
            while ((fromRight ? index-- : ++index < length)) {
              if (iteratee(iterable[index], index, iterable) === false) {
                break;
              }
            }
            return collection;
          };
        }
        function createBaseFor(fromRight) {
          return function(object, iteratee, keysFunc) {
            var iterable = toObject(object),
                props = keysFunc(object),
                length = props.length,
                index = fromRight ? length : -1;
            while ((fromRight ? index-- : ++index < length)) {
              var key = props[index];
              if (iteratee(iterable[key], key, iterable) === false) {
                break;
              }
            }
            return object;
          };
        }
        function createBindWrapper(func, thisArg) {
          var Ctor = createCtorWrapper(func);
          function wrapper() {
            var fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
            return fn.apply(thisArg, arguments);
          }
          return wrapper;
        }
        var createCache = !(nativeCreate && Set) ? constant(null) : function(values) {
          return new SetCache(values);
        };
        function createCompounder(callback) {
          return function(string) {
            var index = -1,
                array = words(deburr(string)),
                length = array.length,
                result = '';
            while (++index < length) {
              result = callback(result, array[index], index);
            }
            return result;
          };
        }
        function createCtorWrapper(Ctor) {
          return function() {
            var thisBinding = baseCreate(Ctor.prototype),
                result = Ctor.apply(thisBinding, arguments);
            return isObject(result) ? result : thisBinding;
          };
        }
        function createCurry(flag) {
          function curryFunc(func, arity, guard) {
            if (guard && isIterateeCall(func, arity, guard)) {
              arity = null;
            }
            var result = createWrapper(func, flag, null, null, null, null, null, arity);
            result.placeholder = curryFunc.placeholder;
            return result;
          }
          return curryFunc;
        }
        function createExtremum(arrayFunc, isMin) {
          return function(collection, iteratee, thisArg) {
            if (thisArg && isIterateeCall(collection, iteratee, thisArg)) {
              iteratee = null;
            }
            var func = getCallback(),
                noIteratee = iteratee == null;
            if (!(func === baseCallback && noIteratee)) {
              noIteratee = false;
              iteratee = func(iteratee, thisArg, 3);
            }
            if (noIteratee) {
              var isArr = isArray(collection);
              if (!isArr && isString(collection)) {
                iteratee = charAtCallback;
              } else {
                return arrayFunc(isArr ? collection : toIterable(collection));
              }
            }
            return extremumBy(collection, iteratee, isMin);
          };
        }
        function createFind(eachFunc, fromRight) {
          return function(collection, predicate, thisArg) {
            predicate = getCallback(predicate, thisArg, 3);
            if (isArray(collection)) {
              var index = baseFindIndex(collection, predicate, fromRight);
              return index > -1 ? collection[index] : undefined;
            }
            return baseFind(collection, predicate, eachFunc);
          };
        }
        function createFindIndex(fromRight) {
          return function(array, predicate, thisArg) {
            if (!(array && array.length)) {
              return -1;
            }
            predicate = getCallback(predicate, thisArg, 3);
            return baseFindIndex(array, predicate, fromRight);
          };
        }
        function createFindKey(objectFunc) {
          return function(object, predicate, thisArg) {
            predicate = getCallback(predicate, thisArg, 3);
            return baseFind(object, predicate, objectFunc, true);
          };
        }
        function createFlow(fromRight) {
          return function() {
            var length = arguments.length;
            if (!length) {
              return function() {
                return arguments[0];
              };
            }
            var wrapper,
                index = fromRight ? length : -1,
                leftIndex = 0,
                funcs = Array(length);
            while ((fromRight ? index-- : ++index < length)) {
              var func = funcs[leftIndex++] = arguments[index];
              if (typeof func != 'function') {
                throw new TypeError(FUNC_ERROR_TEXT);
              }
              var funcName = wrapper ? '' : getFuncName(func);
              wrapper = funcName == 'wrapper' ? new LodashWrapper([]) : wrapper;
            }
            index = wrapper ? -1 : length;
            while (++index < length) {
              func = funcs[index];
              funcName = getFuncName(func);
              var data = funcName == 'wrapper' ? getData(func) : null;
              if (data && isLaziable(data[0])) {
                wrapper = wrapper[getFuncName(data[0])].apply(wrapper, data[3]);
              } else {
                wrapper = (func.length == 1 && isLaziable(func)) ? wrapper[funcName]() : wrapper.thru(func);
              }
            }
            return function() {
              var args = arguments;
              if (wrapper && args.length == 1 && isArray(args[0])) {
                return wrapper.plant(args[0]).value();
              }
              var index = 0,
                  result = funcs[index].apply(this, args);
              while (++index < length) {
                result = funcs[index].call(this, result);
              }
              return result;
            };
          };
        }
        function createForEach(arrayFunc, eachFunc) {
          return function(collection, iteratee, thisArg) {
            return (typeof iteratee == 'function' && thisArg === undefined && isArray(collection)) ? arrayFunc(collection, iteratee) : eachFunc(collection, bindCallback(iteratee, thisArg, 3));
          };
        }
        function createForIn(objectFunc) {
          return function(object, iteratee, thisArg) {
            if (typeof iteratee != 'function' || thisArg !== undefined) {
              iteratee = bindCallback(iteratee, thisArg, 3);
            }
            return objectFunc(object, iteratee, keysIn);
          };
        }
        function createForOwn(objectFunc) {
          return function(object, iteratee, thisArg) {
            if (typeof iteratee != 'function' || thisArg !== undefined) {
              iteratee = bindCallback(iteratee, thisArg, 3);
            }
            return objectFunc(object, iteratee);
          };
        }
        function createPadDir(fromRight) {
          return function(string, length, chars) {
            string = baseToString(string);
            return string && ((fromRight ? string : '') + createPadding(string, length, chars) + (fromRight ? '' : string));
          };
        }
        function createPartial(flag) {
          var partialFunc = restParam(function(func, partials) {
            var holders = replaceHolders(partials, partialFunc.placeholder);
            return createWrapper(func, flag, null, partials, holders);
          });
          return partialFunc;
        }
        function createReduce(arrayFunc, eachFunc) {
          return function(collection, iteratee, accumulator, thisArg) {
            var initFromArray = arguments.length < 3;
            return (typeof iteratee == 'function' && thisArg === undefined && isArray(collection)) ? arrayFunc(collection, iteratee, accumulator, initFromArray) : baseReduce(collection, getCallback(iteratee, thisArg, 4), accumulator, initFromArray, eachFunc);
          };
        }
        function createHybridWrapper(func, bitmask, thisArg, partials, holders, partialsRight, holdersRight, argPos, ary, arity) {
          var isAry = bitmask & ARY_FLAG,
              isBind = bitmask & BIND_FLAG,
              isBindKey = bitmask & BIND_KEY_FLAG,
              isCurry = bitmask & CURRY_FLAG,
              isCurryBound = bitmask & CURRY_BOUND_FLAG,
              isCurryRight = bitmask & CURRY_RIGHT_FLAG;
          var Ctor = !isBindKey && createCtorWrapper(func),
              key = func;
          function wrapper() {
            var length = arguments.length,
                index = length,
                args = Array(length);
            while (index--) {
              args[index] = arguments[index];
            }
            if (partials) {
              args = composeArgs(args, partials, holders);
            }
            if (partialsRight) {
              args = composeArgsRight(args, partialsRight, holdersRight);
            }
            if (isCurry || isCurryRight) {
              var placeholder = wrapper.placeholder,
                  argsHolders = replaceHolders(args, placeholder);
              length -= argsHolders.length;
              if (length < arity) {
                var newArgPos = argPos ? arrayCopy(argPos) : null,
                    newArity = nativeMax(arity - length, 0),
                    newsHolders = isCurry ? argsHolders : null,
                    newHoldersRight = isCurry ? null : argsHolders,
                    newPartials = isCurry ? args : null,
                    newPartialsRight = isCurry ? null : args;
                bitmask |= (isCurry ? PARTIAL_FLAG : PARTIAL_RIGHT_FLAG);
                bitmask &= ~(isCurry ? PARTIAL_RIGHT_FLAG : PARTIAL_FLAG);
                if (!isCurryBound) {
                  bitmask &= ~(BIND_FLAG | BIND_KEY_FLAG);
                }
                var newData = [func, bitmask, thisArg, newPartials, newsHolders, newPartialsRight, newHoldersRight, newArgPos, ary, newArity],
                    result = createHybridWrapper.apply(undefined, newData);
                if (isLaziable(func)) {
                  setData(result, newData);
                }
                result.placeholder = placeholder;
                return result;
              }
            }
            var thisBinding = isBind ? thisArg : this;
            if (isBindKey) {
              func = thisBinding[key];
            }
            if (argPos) {
              args = reorder(args, argPos);
            }
            if (isAry && ary < args.length) {
              args.length = ary;
            }
            var fn = (this && this !== root && this instanceof wrapper) ? (Ctor || createCtorWrapper(func)) : func;
            return fn.apply(thisBinding, args);
          }
          return wrapper;
        }
        function createPadding(string, length, chars) {
          var strLength = string.length;
          length = +length;
          if (strLength >= length || !nativeIsFinite(length)) {
            return '';
          }
          var padLength = length - strLength;
          chars = chars == null ? ' ' : (chars + '');
          return repeat(chars, ceil(padLength / chars.length)).slice(0, padLength);
        }
        function createPartialWrapper(func, bitmask, thisArg, partials) {
          var isBind = bitmask & BIND_FLAG,
              Ctor = createCtorWrapper(func);
          function wrapper() {
            var argsIndex = -1,
                argsLength = arguments.length,
                leftIndex = -1,
                leftLength = partials.length,
                args = Array(argsLength + leftLength);
            while (++leftIndex < leftLength) {
              args[leftIndex] = partials[leftIndex];
            }
            while (argsLength--) {
              args[leftIndex++] = arguments[++argsIndex];
            }
            var fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
            return fn.apply(isBind ? thisArg : this, args);
          }
          return wrapper;
        }
        function createSortedIndex(retHighest) {
          return function(array, value, iteratee, thisArg) {
            var func = getCallback(iteratee);
            return (func === baseCallback && iteratee == null) ? binaryIndex(array, value, retHighest) : binaryIndexBy(array, value, func(iteratee, thisArg, 1), retHighest);
          };
        }
        function createWrapper(func, bitmask, thisArg, partials, holders, argPos, ary, arity) {
          var isBindKey = bitmask & BIND_KEY_FLAG;
          if (!isBindKey && typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          var length = partials ? partials.length : 0;
          if (!length) {
            bitmask &= ~(PARTIAL_FLAG | PARTIAL_RIGHT_FLAG);
            partials = holders = null;
          }
          length -= (holders ? holders.length : 0);
          if (bitmask & PARTIAL_RIGHT_FLAG) {
            var partialsRight = partials,
                holdersRight = holders;
            partials = holders = null;
          }
          var data = isBindKey ? null : getData(func),
              newData = [func, bitmask, thisArg, partials, holders, partialsRight, holdersRight, argPos, ary, arity];
          if (data) {
            mergeData(newData, data);
            bitmask = newData[1];
            arity = newData[9];
          }
          newData[9] = arity == null ? (isBindKey ? 0 : func.length) : (nativeMax(arity - length, 0) || 0);
          if (bitmask == BIND_FLAG) {
            var result = createBindWrapper(newData[0], newData[2]);
          } else if ((bitmask == PARTIAL_FLAG || bitmask == (BIND_FLAG | PARTIAL_FLAG)) && !newData[4].length) {
            result = createPartialWrapper.apply(undefined, newData);
          } else {
            result = createHybridWrapper.apply(undefined, newData);
          }
          var setter = data ? baseSetData : setData;
          return setter(result, newData);
        }
        function equalArrays(array, other, equalFunc, customizer, isLoose, stackA, stackB) {
          var index = -1,
              arrLength = array.length,
              othLength = other.length,
              result = true;
          if (arrLength != othLength && !(isLoose && othLength > arrLength)) {
            return false;
          }
          while (result && ++index < arrLength) {
            var arrValue = array[index],
                othValue = other[index];
            result = undefined;
            if (customizer) {
              result = isLoose ? customizer(othValue, arrValue, index) : customizer(arrValue, othValue, index);
            }
            if (result === undefined) {
              if (isLoose) {
                var othIndex = othLength;
                while (othIndex--) {
                  othValue = other[othIndex];
                  result = (arrValue && arrValue === othValue) || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB);
                  if (result) {
                    break;
                  }
                }
              } else {
                result = (arrValue && arrValue === othValue) || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB);
              }
            }
          }
          return !!result;
        }
        function equalByTag(object, other, tag) {
          switch (tag) {
            case boolTag:
            case dateTag:
              return +object == +other;
            case errorTag:
              return object.name == other.name && object.message == other.message;
            case numberTag:
              return (object != +object) ? other != +other : (object == 0 ? ((1 / object) == (1 / other)) : object == +other);
            case regexpTag:
            case stringTag:
              return object == (other + '');
          }
          return false;
        }
        function equalObjects(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
          var objProps = keys(object),
              objLength = objProps.length,
              othProps = keys(other),
              othLength = othProps.length;
          if (objLength != othLength && !isLoose) {
            return false;
          }
          var skipCtor = isLoose,
              index = -1;
          while (++index < objLength) {
            var key = objProps[index],
                result = isLoose ? key in other : hasOwnProperty.call(other, key);
            if (result) {
              var objValue = object[key],
                  othValue = other[key];
              result = undefined;
              if (customizer) {
                result = isLoose ? customizer(othValue, objValue, key) : customizer(objValue, othValue, key);
              }
              if (result === undefined) {
                result = (objValue && objValue === othValue) || equalFunc(objValue, othValue, customizer, isLoose, stackA, stackB);
              }
            }
            if (!result) {
              return false;
            }
            skipCtor || (skipCtor = key == 'constructor');
          }
          if (!skipCtor) {
            var objCtor = object.constructor,
                othCtor = other.constructor;
            if (objCtor != othCtor && ('constructor' in object && 'constructor' in other) && !(typeof objCtor == 'function' && objCtor instanceof objCtor && typeof othCtor == 'function' && othCtor instanceof othCtor)) {
              return false;
            }
          }
          return true;
        }
        function extremumBy(collection, iteratee, isMin) {
          var exValue = isMin ? POSITIVE_INFINITY : NEGATIVE_INFINITY,
              computed = exValue,
              result = computed;
          baseEach(collection, function(value, index, collection) {
            var current = iteratee(value, index, collection);
            if ((isMin ? (current < computed) : (current > computed)) || (current === exValue && current === result)) {
              computed = current;
              result = value;
            }
          });
          return result;
        }
        function getCallback(func, thisArg, argCount) {
          var result = lodash.callback || callback;
          result = result === callback ? baseCallback : result;
          return argCount ? result(func, thisArg, argCount) : result;
        }
        var getData = !metaMap ? noop : function(func) {
          return metaMap.get(func);
        };
        var getFuncName = (function() {
          if (!support.funcNames) {
            return constant('');
          }
          if (constant.name == 'constant') {
            return baseProperty('name');
          }
          return function(func) {
            var result = func.name,
                array = realNames[result],
                length = array ? array.length : 0;
            while (length--) {
              var data = array[length],
                  otherFunc = data.func;
              if (otherFunc == null || otherFunc == func) {
                return data.name;
              }
            }
            return result;
          };
        }());
        function getIndexOf(collection, target, fromIndex) {
          var result = lodash.indexOf || indexOf;
          result = result === indexOf ? baseIndexOf : result;
          return collection ? result(collection, target, fromIndex) : result;
        }
        var getLength = baseProperty('length');
        var getSymbols = !getOwnPropertySymbols ? constant([]) : function(object) {
          return getOwnPropertySymbols(toObject(object));
        };
        function getView(start, end, transforms) {
          var index = -1,
              length = transforms ? transforms.length : 0;
          while (++index < length) {
            var data = transforms[index],
                size = data.size;
            switch (data.type) {
              case 'drop':
                start += size;
                break;
              case 'dropRight':
                end -= size;
                break;
              case 'take':
                end = nativeMin(end, start + size);
                break;
              case 'takeRight':
                start = nativeMax(start, end - size);
                break;
            }
          }
          return {
            'start': start,
            'end': end
          };
        }
        function initCloneArray(array) {
          var length = array.length,
              result = new array.constructor(length);
          if (length && typeof array[0] == 'string' && hasOwnProperty.call(array, 'index')) {
            result.index = array.index;
            result.input = array.input;
          }
          return result;
        }
        function initCloneObject(object) {
          var Ctor = object.constructor;
          if (!(typeof Ctor == 'function' && Ctor instanceof Ctor)) {
            Ctor = Object;
          }
          return new Ctor;
        }
        function initCloneByTag(object, tag, isDeep) {
          var Ctor = object.constructor;
          switch (tag) {
            case arrayBufferTag:
              return bufferClone(object);
            case boolTag:
            case dateTag:
              return new Ctor(+object);
            case float32Tag:
            case float64Tag:
            case int8Tag:
            case int16Tag:
            case int32Tag:
            case uint8Tag:
            case uint8ClampedTag:
            case uint16Tag:
            case uint32Tag:
              var buffer = object.buffer;
              return new Ctor(isDeep ? bufferClone(buffer) : buffer, object.byteOffset, object.length);
            case numberTag:
            case stringTag:
              return new Ctor(object);
            case regexpTag:
              var result = new Ctor(object.source, reFlags.exec(object));
              result.lastIndex = object.lastIndex;
          }
          return result;
        }
        function invokePath(object, path, args) {
          if (object != null && !isKey(path, object)) {
            path = toPath(path);
            object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
            path = last(path);
          }
          var func = object == null ? object : object[path];
          return func == null ? undefined : func.apply(object, args);
        }
        function isIndex(value, length) {
          value = +value;
          length = length == null ? MAX_SAFE_INTEGER : length;
          return value > -1 && value % 1 == 0 && value < length;
        }
        function isIterateeCall(value, index, object) {
          if (!isObject(object)) {
            return false;
          }
          var type = typeof index;
          if (type == 'number') {
            var length = getLength(object),
                prereq = isLength(length) && isIndex(index, length);
          } else {
            prereq = type == 'string' && index in object;
          }
          if (prereq) {
            var other = object[index];
            return value === value ? (value === other) : (other !== other);
          }
          return false;
        }
        function isKey(value, object) {
          var type = typeof value;
          if ((type == 'string' && reIsPlainProp.test(value)) || type == 'number') {
            return true;
          }
          if (isArray(value)) {
            return false;
          }
          var result = !reIsDeepProp.test(value);
          return result || (object != null && value in toObject(object));
        }
        function isLaziable(func) {
          var funcName = getFuncName(func);
          return !!funcName && func === lodash[funcName] && funcName in LazyWrapper.prototype;
        }
        function isLength(value) {
          return typeof value == 'number' && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
        }
        function isStrictComparable(value) {
          return value === value && (value === 0 ? ((1 / value) > 0) : !isObject(value));
        }
        function mergeData(data, source) {
          var bitmask = data[1],
              srcBitmask = source[1],
              newBitmask = bitmask | srcBitmask,
              isCommon = newBitmask < ARY_FLAG;
          var isCombo = (srcBitmask == ARY_FLAG && bitmask == CURRY_FLAG) || (srcBitmask == ARY_FLAG && bitmask == REARG_FLAG && data[7].length <= source[8]) || (srcBitmask == (ARY_FLAG | REARG_FLAG) && bitmask == CURRY_FLAG);
          if (!(isCommon || isCombo)) {
            return data;
          }
          if (srcBitmask & BIND_FLAG) {
            data[2] = source[2];
            newBitmask |= (bitmask & BIND_FLAG) ? 0 : CURRY_BOUND_FLAG;
          }
          var value = source[3];
          if (value) {
            var partials = data[3];
            data[3] = partials ? composeArgs(partials, value, source[4]) : arrayCopy(value);
            data[4] = partials ? replaceHolders(data[3], PLACEHOLDER) : arrayCopy(source[4]);
          }
          value = source[5];
          if (value) {
            partials = data[5];
            data[5] = partials ? composeArgsRight(partials, value, source[6]) : arrayCopy(value);
            data[6] = partials ? replaceHolders(data[5], PLACEHOLDER) : arrayCopy(source[6]);
          }
          value = source[7];
          if (value) {
            data[7] = arrayCopy(value);
          }
          if (srcBitmask & ARY_FLAG) {
            data[8] = data[8] == null ? source[8] : nativeMin(data[8], source[8]);
          }
          if (data[9] == null) {
            data[9] = source[9];
          }
          data[0] = source[0];
          data[1] = newBitmask;
          return data;
        }
        function pickByArray(object, props) {
          object = toObject(object);
          var index = -1,
              length = props.length,
              result = {};
          while (++index < length) {
            var key = props[index];
            if (key in object) {
              result[key] = object[key];
            }
          }
          return result;
        }
        function pickByCallback(object, predicate) {
          var result = {};
          baseForIn(object, function(value, key, object) {
            if (predicate(value, key, object)) {
              result[key] = value;
            }
          });
          return result;
        }
        function reorder(array, indexes) {
          var arrLength = array.length,
              length = nativeMin(indexes.length, arrLength),
              oldArray = arrayCopy(array);
          while (length--) {
            var index = indexes[length];
            array[length] = isIndex(index, arrLength) ? oldArray[index] : undefined;
          }
          return array;
        }
        var setData = (function() {
          var count = 0,
              lastCalled = 0;
          return function(key, value) {
            var stamp = now(),
                remaining = HOT_SPAN - (stamp - lastCalled);
            lastCalled = stamp;
            if (remaining > 0) {
              if (++count >= HOT_COUNT) {
                return key;
              }
            } else {
              count = 0;
            }
            return baseSetData(key, value);
          };
        }());
        function shimIsPlainObject(value) {
          var Ctor,
              support = lodash.support;
          if (!(isObjectLike(value) && objToString.call(value) == objectTag) || (!hasOwnProperty.call(value, 'constructor') && (Ctor = value.constructor, typeof Ctor == 'function' && !(Ctor instanceof Ctor)))) {
            return false;
          }
          var result;
          baseForIn(value, function(subValue, key) {
            result = key;
          });
          return result === undefined || hasOwnProperty.call(value, result);
        }
        function shimKeys(object) {
          var props = keysIn(object),
              propsLength = props.length,
              length = propsLength && object.length,
              support = lodash.support;
          var allowIndexes = length && isLength(length) && (isArray(object) || (support.nonEnumArgs && isArguments(object)));
          var index = -1,
              result = [];
          while (++index < propsLength) {
            var key = props[index];
            if ((allowIndexes && isIndex(key, length)) || hasOwnProperty.call(object, key)) {
              result.push(key);
            }
          }
          return result;
        }
        function toIterable(value) {
          if (value == null) {
            return [];
          }
          if (!isLength(getLength(value))) {
            return values(value);
          }
          return isObject(value) ? value : Object(value);
        }
        function toObject(value) {
          return isObject(value) ? value : Object(value);
        }
        function toPath(value) {
          if (isArray(value)) {
            return value;
          }
          var result = [];
          baseToString(value).replace(rePropName, function(match, number, quote, string) {
            result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
          });
          return result;
        }
        function wrapperClone(wrapper) {
          return wrapper instanceof LazyWrapper ? wrapper.clone() : new LodashWrapper(wrapper.__wrapped__, wrapper.__chain__, arrayCopy(wrapper.__actions__));
        }
        function chunk(array, size, guard) {
          if (guard ? isIterateeCall(array, size, guard) : size == null) {
            size = 1;
          } else {
            size = nativeMax(+size || 1, 1);
          }
          var index = 0,
              length = array ? array.length : 0,
              resIndex = -1,
              result = Array(ceil(length / size));
          while (index < length) {
            result[++resIndex] = baseSlice(array, index, (index += size));
          }
          return result;
        }
        function compact(array) {
          var index = -1,
              length = array ? array.length : 0,
              resIndex = -1,
              result = [];
          while (++index < length) {
            var value = array[index];
            if (value) {
              result[++resIndex] = value;
            }
          }
          return result;
        }
        var difference = restParam(function(array, values) {
          return (isArray(array) || isArguments(array)) ? baseDifference(array, baseFlatten(values, false, true)) : [];
        });
        function drop(array, n, guard) {
          var length = array ? array.length : 0;
          if (!length) {
            return [];
          }
          if (guard ? isIterateeCall(array, n, guard) : n == null) {
            n = 1;
          }
          return baseSlice(array, n < 0 ? 0 : n);
        }
        function dropRight(array, n, guard) {
          var length = array ? array.length : 0;
          if (!length) {
            return [];
          }
          if (guard ? isIterateeCall(array, n, guard) : n == null) {
            n = 1;
          }
          n = length - (+n || 0);
          return baseSlice(array, 0, n < 0 ? 0 : n);
        }
        function dropRightWhile(array, predicate, thisArg) {
          return (array && array.length) ? baseWhile(array, getCallback(predicate, thisArg, 3), true, true) : [];
        }
        function dropWhile(array, predicate, thisArg) {
          return (array && array.length) ? baseWhile(array, getCallback(predicate, thisArg, 3), true) : [];
        }
        function fill(array, value, start, end) {
          var length = array ? array.length : 0;
          if (!length) {
            return [];
          }
          if (start && typeof start != 'number' && isIterateeCall(array, value, start)) {
            start = 0;
            end = length;
          }
          return baseFill(array, value, start, end);
        }
        var findIndex = createFindIndex();
        var findLastIndex = createFindIndex(true);
        function first(array) {
          return array ? array[0] : undefined;
        }
        function flatten(array, isDeep, guard) {
          var length = array ? array.length : 0;
          if (guard && isIterateeCall(array, isDeep, guard)) {
            isDeep = false;
          }
          return length ? baseFlatten(array, isDeep) : [];
        }
        function flattenDeep(array) {
          var length = array ? array.length : 0;
          return length ? baseFlatten(array, true) : [];
        }
        function indexOf(array, value, fromIndex) {
          var length = array ? array.length : 0;
          if (!length) {
            return -1;
          }
          if (typeof fromIndex == 'number') {
            fromIndex = fromIndex < 0 ? nativeMax(length + fromIndex, 0) : fromIndex;
          } else if (fromIndex) {
            var index = binaryIndex(array, value),
                other = array[index];
            if (value === value ? (value === other) : (other !== other)) {
              return index;
            }
            return -1;
          }
          return baseIndexOf(array, value, fromIndex || 0);
        }
        function initial(array) {
          return dropRight(array, 1);
        }
        function intersection() {
          var args = [],
              argsIndex = -1,
              argsLength = arguments.length,
              caches = [],
              indexOf = getIndexOf(),
              isCommon = indexOf == baseIndexOf,
              result = [];
          while (++argsIndex < argsLength) {
            var value = arguments[argsIndex];
            if (isArray(value) || isArguments(value)) {
              args.push(value);
              caches.push((isCommon && value.length >= 120) ? createCache(argsIndex && value) : null);
            }
          }
          argsLength = args.length;
          if (argsLength < 2) {
            return result;
          }
          var array = args[0],
              index = -1,
              length = array ? array.length : 0,
              seen = caches[0];
          outer: while (++index < length) {
            value = array[index];
            if ((seen ? cacheIndexOf(seen, value) : indexOf(result, value, 0)) < 0) {
              argsIndex = argsLength;
              while (--argsIndex) {
                var cache = caches[argsIndex];
                if ((cache ? cacheIndexOf(cache, value) : indexOf(args[argsIndex], value, 0)) < 0) {
                  continue outer;
                }
              }
              if (seen) {
                seen.push(value);
              }
              result.push(value);
            }
          }
          return result;
        }
        function last(array) {
          var length = array ? array.length : 0;
          return length ? array[length - 1] : undefined;
        }
        function lastIndexOf(array, value, fromIndex) {
          var length = array ? array.length : 0;
          if (!length) {
            return -1;
          }
          var index = length;
          if (typeof fromIndex == 'number') {
            index = (fromIndex < 0 ? nativeMax(length + fromIndex, 0) : nativeMin(fromIndex || 0, length - 1)) + 1;
          } else if (fromIndex) {
            index = binaryIndex(array, value, true) - 1;
            var other = array[index];
            if (value === value ? (value === other) : (other !== other)) {
              return index;
            }
            return -1;
          }
          if (value !== value) {
            return indexOfNaN(array, index, true);
          }
          while (index--) {
            if (array[index] === value) {
              return index;
            }
          }
          return -1;
        }
        function pull() {
          var args = arguments,
              array = args[0];
          if (!(array && array.length)) {
            return array;
          }
          var index = 0,
              indexOf = getIndexOf(),
              length = args.length;
          while (++index < length) {
            var fromIndex = 0,
                value = args[index];
            while ((fromIndex = indexOf(array, value, fromIndex)) > -1) {
              splice.call(array, fromIndex, 1);
            }
          }
          return array;
        }
        var pullAt = restParam(function(array, indexes) {
          array || (array = []);
          indexes = baseFlatten(indexes);
          var result = baseAt(array, indexes);
          basePullAt(array, indexes.sort(baseCompareAscending));
          return result;
        });
        function remove(array, predicate, thisArg) {
          var result = [];
          if (!(array && array.length)) {
            return result;
          }
          var index = -1,
              indexes = [],
              length = array.length;
          predicate = getCallback(predicate, thisArg, 3);
          while (++index < length) {
            var value = array[index];
            if (predicate(value, index, array)) {
              result.push(value);
              indexes.push(index);
            }
          }
          basePullAt(array, indexes);
          return result;
        }
        function rest(array) {
          return drop(array, 1);
        }
        function slice(array, start, end) {
          var length = array ? array.length : 0;
          if (!length) {
            return [];
          }
          if (end && typeof end != 'number' && isIterateeCall(array, start, end)) {
            start = 0;
            end = length;
          }
          return baseSlice(array, start, end);
        }
        var sortedIndex = createSortedIndex();
        var sortedLastIndex = createSortedIndex(true);
        function take(array, n, guard) {
          var length = array ? array.length : 0;
          if (!length) {
            return [];
          }
          if (guard ? isIterateeCall(array, n, guard) : n == null) {
            n = 1;
          }
          return baseSlice(array, 0, n < 0 ? 0 : n);
        }
        function takeRight(array, n, guard) {
          var length = array ? array.length : 0;
          if (!length) {
            return [];
          }
          if (guard ? isIterateeCall(array, n, guard) : n == null) {
            n = 1;
          }
          n = length - (+n || 0);
          return baseSlice(array, n < 0 ? 0 : n);
        }
        function takeRightWhile(array, predicate, thisArg) {
          return (array && array.length) ? baseWhile(array, getCallback(predicate, thisArg, 3), false, true) : [];
        }
        function takeWhile(array, predicate, thisArg) {
          return (array && array.length) ? baseWhile(array, getCallback(predicate, thisArg, 3)) : [];
        }
        var union = restParam(function(arrays) {
          return baseUniq(baseFlatten(arrays, false, true));
        });
        function uniq(array, isSorted, iteratee, thisArg) {
          var length = array ? array.length : 0;
          if (!length) {
            return [];
          }
          if (isSorted != null && typeof isSorted != 'boolean') {
            thisArg = iteratee;
            iteratee = isIterateeCall(array, isSorted, thisArg) ? null : isSorted;
            isSorted = false;
          }
          var func = getCallback();
          if (!(func === baseCallback && iteratee == null)) {
            iteratee = func(iteratee, thisArg, 3);
          }
          return (isSorted && getIndexOf() == baseIndexOf) ? sortedUniq(array, iteratee) : baseUniq(array, iteratee);
        }
        function unzip(array) {
          var index = -1,
              length = (array && array.length && arrayMax(arrayMap(array, getLength))) >>> 0,
              result = Array(length);
          while (++index < length) {
            result[index] = arrayMap(array, baseProperty(index));
          }
          return result;
        }
        var without = restParam(function(array, values) {
          return (isArray(array) || isArguments(array)) ? baseDifference(array, values) : [];
        });
        function xor() {
          var index = -1,
              length = arguments.length;
          while (++index < length) {
            var array = arguments[index];
            if (isArray(array) || isArguments(array)) {
              var result = result ? baseDifference(result, array).concat(baseDifference(array, result)) : array;
            }
          }
          return result ? baseUniq(result) : [];
        }
        var zip = restParam(unzip);
        function zipObject(props, values) {
          var index = -1,
              length = props ? props.length : 0,
              result = {};
          if (length && !values && !isArray(props[0])) {
            values = [];
          }
          while (++index < length) {
            var key = props[index];
            if (values) {
              result[key] = values[index];
            } else if (key) {
              result[key[0]] = key[1];
            }
          }
          return result;
        }
        function chain(value) {
          var result = lodash(value);
          result.__chain__ = true;
          return result;
        }
        function tap(value, interceptor, thisArg) {
          interceptor.call(thisArg, value);
          return value;
        }
        function thru(value, interceptor, thisArg) {
          return interceptor.call(thisArg, value);
        }
        function wrapperChain() {
          return chain(this);
        }
        function wrapperCommit() {
          return new LodashWrapper(this.value(), this.__chain__);
        }
        function wrapperPlant(value) {
          var result,
              parent = this;
          while (parent instanceof baseLodash) {
            var clone = wrapperClone(parent);
            if (result) {
              previous.__wrapped__ = clone;
            } else {
              result = clone;
            }
            var previous = clone;
            parent = parent.__wrapped__;
          }
          previous.__wrapped__ = value;
          return result;
        }
        function wrapperReverse() {
          var value = this.__wrapped__;
          if (value instanceof LazyWrapper) {
            if (this.__actions__.length) {
              value = new LazyWrapper(this);
            }
            return new LodashWrapper(value.reverse(), this.__chain__);
          }
          return this.thru(function(value) {
            return value.reverse();
          });
        }
        function wrapperToString() {
          return (this.value() + '');
        }
        function wrapperValue() {
          return baseWrapperValue(this.__wrapped__, this.__actions__);
        }
        var at = restParam(function(collection, props) {
          var length = collection ? getLength(collection) : 0;
          if (isLength(length)) {
            collection = toIterable(collection);
          }
          return baseAt(collection, baseFlatten(props));
        });
        var countBy = createAggregator(function(result, value, key) {
          hasOwnProperty.call(result, key) ? ++result[key] : (result[key] = 1);
        });
        function every(collection, predicate, thisArg) {
          var func = isArray(collection) ? arrayEvery : baseEvery;
          if (thisArg && isIterateeCall(collection, predicate, thisArg)) {
            predicate = null;
          }
          if (typeof predicate != 'function' || thisArg !== undefined) {
            predicate = getCallback(predicate, thisArg, 3);
          }
          return func(collection, predicate);
        }
        function filter(collection, predicate, thisArg) {
          var func = isArray(collection) ? arrayFilter : baseFilter;
          predicate = getCallback(predicate, thisArg, 3);
          return func(collection, predicate);
        }
        var find = createFind(baseEach);
        var findLast = createFind(baseEachRight, true);
        function findWhere(collection, source) {
          return find(collection, baseMatches(source));
        }
        var forEach = createForEach(arrayEach, baseEach);
        var forEachRight = createForEach(arrayEachRight, baseEachRight);
        var groupBy = createAggregator(function(result, value, key) {
          if (hasOwnProperty.call(result, key)) {
            result[key].push(value);
          } else {
            result[key] = [value];
          }
        });
        function includes(collection, target, fromIndex, guard) {
          var length = collection ? getLength(collection) : 0;
          if (!isLength(length)) {
            collection = values(collection);
            length = collection.length;
          }
          if (!length) {
            return false;
          }
          if (typeof fromIndex != 'number' || (guard && isIterateeCall(target, fromIndex, guard))) {
            fromIndex = 0;
          } else {
            fromIndex = fromIndex < 0 ? nativeMax(length + fromIndex, 0) : (fromIndex || 0);
          }
          return (typeof collection == 'string' || !isArray(collection) && isString(collection)) ? (fromIndex < length && collection.indexOf(target, fromIndex) > -1) : (getIndexOf(collection, target, fromIndex) > -1);
        }
        var indexBy = createAggregator(function(result, value, key) {
          result[key] = value;
        });
        var invoke = restParam(function(collection, path, args) {
          var index = -1,
              isFunc = typeof path == 'function',
              isProp = isKey(path),
              length = getLength(collection),
              result = isLength(length) ? Array(length) : [];
          baseEach(collection, function(value) {
            var func = isFunc ? path : (isProp && value != null && value[path]);
            result[++index] = func ? func.apply(value, args) : invokePath(value, path, args);
          });
          return result;
        });
        function map(collection, iteratee, thisArg) {
          var func = isArray(collection) ? arrayMap : baseMap;
          iteratee = getCallback(iteratee, thisArg, 3);
          return func(collection, iteratee);
        }
        var partition = createAggregator(function(result, value, key) {
          result[key ? 0 : 1].push(value);
        }, function() {
          return [[], []];
        });
        function pluck(collection, path) {
          return map(collection, property(path));
        }
        var reduce = createReduce(arrayReduce, baseEach);
        var reduceRight = createReduce(arrayReduceRight, baseEachRight);
        function reject(collection, predicate, thisArg) {
          var func = isArray(collection) ? arrayFilter : baseFilter;
          predicate = getCallback(predicate, thisArg, 3);
          return func(collection, function(value, index, collection) {
            return !predicate(value, index, collection);
          });
        }
        function sample(collection, n, guard) {
          if (guard ? isIterateeCall(collection, n, guard) : n == null) {
            collection = toIterable(collection);
            var length = collection.length;
            return length > 0 ? collection[baseRandom(0, length - 1)] : undefined;
          }
          var result = shuffle(collection);
          result.length = nativeMin(n < 0 ? 0 : (+n || 0), result.length);
          return result;
        }
        function shuffle(collection) {
          collection = toIterable(collection);
          var index = -1,
              length = collection.length,
              result = Array(length);
          while (++index < length) {
            var rand = baseRandom(0, index);
            if (index != rand) {
              result[index] = result[rand];
            }
            result[rand] = collection[index];
          }
          return result;
        }
        function size(collection) {
          var length = collection ? getLength(collection) : 0;
          return isLength(length) ? length : keys(collection).length;
        }
        function some(collection, predicate, thisArg) {
          var func = isArray(collection) ? arraySome : baseSome;
          if (thisArg && isIterateeCall(collection, predicate, thisArg)) {
            predicate = null;
          }
          if (typeof predicate != 'function' || thisArg !== undefined) {
            predicate = getCallback(predicate, thisArg, 3);
          }
          return func(collection, predicate);
        }
        function sortBy(collection, iteratee, thisArg) {
          if (collection == null) {
            return [];
          }
          if (thisArg && isIterateeCall(collection, iteratee, thisArg)) {
            iteratee = null;
          }
          var index = -1;
          iteratee = getCallback(iteratee, thisArg, 3);
          var result = baseMap(collection, function(value, key, collection) {
            return {
              'criteria': iteratee(value, key, collection),
              'index': ++index,
              'value': value
            };
          });
          return baseSortBy(result, compareAscending);
        }
        var sortByAll = restParam(function(collection, iteratees) {
          if (collection == null) {
            return [];
          }
          var guard = iteratees[2];
          if (guard && isIterateeCall(iteratees[0], iteratees[1], guard)) {
            iteratees.length = 1;
          }
          return baseSortByOrder(collection, baseFlatten(iteratees), []);
        });
        function sortByOrder(collection, iteratees, orders, guard) {
          if (collection == null) {
            return [];
          }
          if (guard && isIterateeCall(iteratees, orders, guard)) {
            orders = null;
          }
          if (!isArray(iteratees)) {
            iteratees = iteratees == null ? [] : [iteratees];
          }
          if (!isArray(orders)) {
            orders = orders == null ? [] : [orders];
          }
          return baseSortByOrder(collection, iteratees, orders);
        }
        function where(collection, source) {
          return filter(collection, baseMatches(source));
        }
        var now = nativeNow || function() {
          return new Date().getTime();
        };
        function after(n, func) {
          if (typeof func != 'function') {
            if (typeof n == 'function') {
              var temp = n;
              n = func;
              func = temp;
            } else {
              throw new TypeError(FUNC_ERROR_TEXT);
            }
          }
          n = nativeIsFinite(n = +n) ? n : 0;
          return function() {
            if (--n < 1) {
              return func.apply(this, arguments);
            }
          };
        }
        function ary(func, n, guard) {
          if (guard && isIterateeCall(func, n, guard)) {
            n = null;
          }
          n = (func && n == null) ? func.length : nativeMax(+n || 0, 0);
          return createWrapper(func, ARY_FLAG, null, null, null, null, n);
        }
        function before(n, func) {
          var result;
          if (typeof func != 'function') {
            if (typeof n == 'function') {
              var temp = n;
              n = func;
              func = temp;
            } else {
              throw new TypeError(FUNC_ERROR_TEXT);
            }
          }
          return function() {
            if (--n > 0) {
              result = func.apply(this, arguments);
            }
            if (n <= 1) {
              func = null;
            }
            return result;
          };
        }
        var bind = restParam(function(func, thisArg, partials) {
          var bitmask = BIND_FLAG;
          if (partials.length) {
            var holders = replaceHolders(partials, bind.placeholder);
            bitmask |= PARTIAL_FLAG;
          }
          return createWrapper(func, bitmask, thisArg, partials, holders);
        });
        var bindAll = restParam(function(object, methodNames) {
          methodNames = methodNames.length ? baseFlatten(methodNames) : functions(object);
          var index = -1,
              length = methodNames.length;
          while (++index < length) {
            var key = methodNames[index];
            object[key] = createWrapper(object[key], BIND_FLAG, object);
          }
          return object;
        });
        var bindKey = restParam(function(object, key, partials) {
          var bitmask = BIND_FLAG | BIND_KEY_FLAG;
          if (partials.length) {
            var holders = replaceHolders(partials, bindKey.placeholder);
            bitmask |= PARTIAL_FLAG;
          }
          return createWrapper(key, bitmask, object, partials, holders);
        });
        var curry = createCurry(CURRY_FLAG);
        var curryRight = createCurry(CURRY_RIGHT_FLAG);
        function debounce(func, wait, options) {
          var args,
              maxTimeoutId,
              result,
              stamp,
              thisArg,
              timeoutId,
              trailingCall,
              lastCalled = 0,
              maxWait = false,
              trailing = true;
          if (typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          wait = wait < 0 ? 0 : (+wait || 0);
          if (options === true) {
            var leading = true;
            trailing = false;
          } else if (isObject(options)) {
            leading = options.leading;
            maxWait = 'maxWait' in options && nativeMax(+options.maxWait || 0, wait);
            trailing = 'trailing' in options ? options.trailing : trailing;
          }
          function cancel() {
            if (timeoutId) {
              clearTimeout(timeoutId);
            }
            if (maxTimeoutId) {
              clearTimeout(maxTimeoutId);
            }
            maxTimeoutId = timeoutId = trailingCall = undefined;
          }
          function delayed() {
            var remaining = wait - (now() - stamp);
            if (remaining <= 0 || remaining > wait) {
              if (maxTimeoutId) {
                clearTimeout(maxTimeoutId);
              }
              var isCalled = trailingCall;
              maxTimeoutId = timeoutId = trailingCall = undefined;
              if (isCalled) {
                lastCalled = now();
                result = func.apply(thisArg, args);
                if (!timeoutId && !maxTimeoutId) {
                  args = thisArg = null;
                }
              }
            } else {
              timeoutId = setTimeout(delayed, remaining);
            }
          }
          function maxDelayed() {
            if (timeoutId) {
              clearTimeout(timeoutId);
            }
            maxTimeoutId = timeoutId = trailingCall = undefined;
            if (trailing || (maxWait !== wait)) {
              lastCalled = now();
              result = func.apply(thisArg, args);
              if (!timeoutId && !maxTimeoutId) {
                args = thisArg = null;
              }
            }
          }
          function debounced() {
            args = arguments;
            stamp = now();
            thisArg = this;
            trailingCall = trailing && (timeoutId || !leading);
            if (maxWait === false) {
              var leadingCall = leading && !timeoutId;
            } else {
              if (!maxTimeoutId && !leading) {
                lastCalled = stamp;
              }
              var remaining = maxWait - (stamp - lastCalled),
                  isCalled = remaining <= 0 || remaining > maxWait;
              if (isCalled) {
                if (maxTimeoutId) {
                  maxTimeoutId = clearTimeout(maxTimeoutId);
                }
                lastCalled = stamp;
                result = func.apply(thisArg, args);
              } else if (!maxTimeoutId) {
                maxTimeoutId = setTimeout(maxDelayed, remaining);
              }
            }
            if (isCalled && timeoutId) {
              timeoutId = clearTimeout(timeoutId);
            } else if (!timeoutId && wait !== maxWait) {
              timeoutId = setTimeout(delayed, wait);
            }
            if (leadingCall) {
              isCalled = true;
              result = func.apply(thisArg, args);
            }
            if (isCalled && !timeoutId && !maxTimeoutId) {
              args = thisArg = null;
            }
            return result;
          }
          debounced.cancel = cancel;
          return debounced;
        }
        var defer = restParam(function(func, args) {
          return baseDelay(func, 1, args);
        });
        var delay = restParam(function(func, wait, args) {
          return baseDelay(func, wait, args);
        });
        var flow = createFlow();
        var flowRight = createFlow(true);
        function memoize(func, resolver) {
          if (typeof func != 'function' || (resolver && typeof resolver != 'function')) {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          var memoized = function() {
            var args = arguments,
                cache = memoized.cache,
                key = resolver ? resolver.apply(this, args) : args[0];
            if (cache.has(key)) {
              return cache.get(key);
            }
            var result = func.apply(this, args);
            cache.set(key, result);
            return result;
          };
          memoized.cache = new memoize.Cache;
          return memoized;
        }
        function negate(predicate) {
          if (typeof predicate != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          return function() {
            return !predicate.apply(this, arguments);
          };
        }
        function once(func) {
          return before(2, func);
        }
        var partial = createPartial(PARTIAL_FLAG);
        var partialRight = createPartial(PARTIAL_RIGHT_FLAG);
        var rearg = restParam(function(func, indexes) {
          return createWrapper(func, REARG_FLAG, null, null, null, baseFlatten(indexes));
        });
        function restParam(func, start) {
          if (typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          start = nativeMax(start === undefined ? (func.length - 1) : (+start || 0), 0);
          return function() {
            var args = arguments,
                index = -1,
                length = nativeMax(args.length - start, 0),
                rest = Array(length);
            while (++index < length) {
              rest[index] = args[start + index];
            }
            switch (start) {
              case 0:
                return func.call(this, rest);
              case 1:
                return func.call(this, args[0], rest);
              case 2:
                return func.call(this, args[0], args[1], rest);
            }
            var otherArgs = Array(start + 1);
            index = -1;
            while (++index < start) {
              otherArgs[index] = args[index];
            }
            otherArgs[start] = rest;
            return func.apply(this, otherArgs);
          };
        }
        function spread(func) {
          if (typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          return function(array) {
            return func.apply(this, array);
          };
        }
        function throttle(func, wait, options) {
          var leading = true,
              trailing = true;
          if (typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          if (options === false) {
            leading = false;
          } else if (isObject(options)) {
            leading = 'leading' in options ? !!options.leading : leading;
            trailing = 'trailing' in options ? !!options.trailing : trailing;
          }
          debounceOptions.leading = leading;
          debounceOptions.maxWait = +wait;
          debounceOptions.trailing = trailing;
          return debounce(func, wait, debounceOptions);
        }
        function wrap(value, wrapper) {
          wrapper = wrapper == null ? identity : wrapper;
          return createWrapper(wrapper, PARTIAL_FLAG, null, [value], []);
        }
        function clone(value, isDeep, customizer, thisArg) {
          if (isDeep && typeof isDeep != 'boolean' && isIterateeCall(value, isDeep, customizer)) {
            isDeep = false;
          } else if (typeof isDeep == 'function') {
            thisArg = customizer;
            customizer = isDeep;
            isDeep = false;
          }
          customizer = typeof customizer == 'function' && bindCallback(customizer, thisArg, 1);
          return baseClone(value, isDeep, customizer);
        }
        function cloneDeep(value, customizer, thisArg) {
          customizer = typeof customizer == 'function' && bindCallback(customizer, thisArg, 1);
          return baseClone(value, true, customizer);
        }
        function isArguments(value) {
          var length = isObjectLike(value) ? value.length : undefined;
          return isLength(length) && objToString.call(value) == argsTag;
        }
        var isArray = nativeIsArray || function(value) {
          return isObjectLike(value) && isLength(value.length) && objToString.call(value) == arrayTag;
        };
        function isBoolean(value) {
          return value === true || value === false || (isObjectLike(value) && objToString.call(value) == boolTag);
        }
        function isDate(value) {
          return isObjectLike(value) && objToString.call(value) == dateTag;
        }
        function isElement(value) {
          return !!value && value.nodeType === 1 && isObjectLike(value) && (objToString.call(value).indexOf('Element') > -1);
        }
        if (!support.dom) {
          isElement = function(value) {
            return !!value && value.nodeType === 1 && isObjectLike(value) && !isPlainObject(value);
          };
        }
        function isEmpty(value) {
          if (value == null) {
            return true;
          }
          var length = getLength(value);
          if (isLength(length) && (isArray(value) || isString(value) || isArguments(value) || (isObjectLike(value) && isFunction(value.splice)))) {
            return !length;
          }
          return !keys(value).length;
        }
        function isEqual(value, other, customizer, thisArg) {
          customizer = typeof customizer == 'function' && bindCallback(customizer, thisArg, 3);
          if (!customizer && isStrictComparable(value) && isStrictComparable(other)) {
            return value === other;
          }
          var result = customizer ? customizer(value, other) : undefined;
          return result === undefined ? baseIsEqual(value, other, customizer) : !!result;
        }
        function isError(value) {
          return isObjectLike(value) && typeof value.message == 'string' && objToString.call(value) == errorTag;
        }
        var isFinite = nativeNumIsFinite || function(value) {
          return typeof value == 'number' && nativeIsFinite(value);
        };
        var isFunction = !(baseIsFunction(/x/) || (Uint8Array && !baseIsFunction(Uint8Array))) ? baseIsFunction : function(value) {
          return objToString.call(value) == funcTag;
        };
        function isObject(value) {
          var type = typeof value;
          return type == 'function' || (!!value && type == 'object');
        }
        function isMatch(object, source, customizer, thisArg) {
          var props = keys(source),
              length = props.length;
          if (!length) {
            return true;
          }
          if (object == null) {
            return false;
          }
          customizer = typeof customizer == 'function' && bindCallback(customizer, thisArg, 3);
          object = toObject(object);
          if (!customizer && length == 1) {
            var key = props[0],
                value = source[key];
            if (isStrictComparable(value)) {
              return value === object[key] && (value !== undefined || (key in object));
            }
          }
          var values = Array(length),
              strictCompareFlags = Array(length);
          while (length--) {
            value = values[length] = source[props[length]];
            strictCompareFlags[length] = isStrictComparable(value);
          }
          return baseIsMatch(object, props, values, strictCompareFlags, customizer);
        }
        function isNaN(value) {
          return isNumber(value) && value != +value;
        }
        function isNative(value) {
          if (value == null) {
            return false;
          }
          if (objToString.call(value) == funcTag) {
            return reIsNative.test(fnToString.call(value));
          }
          return isObjectLike(value) && reIsHostCtor.test(value);
        }
        function isNull(value) {
          return value === null;
        }
        function isNumber(value) {
          return typeof value == 'number' || (isObjectLike(value) && objToString.call(value) == numberTag);
        }
        var isPlainObject = !getPrototypeOf ? shimIsPlainObject : function(value) {
          if (!(value && objToString.call(value) == objectTag)) {
            return false;
          }
          var valueOf = value.valueOf,
              objProto = isNative(valueOf) && (objProto = getPrototypeOf(valueOf)) && getPrototypeOf(objProto);
          return objProto ? (value == objProto || getPrototypeOf(value) == objProto) : shimIsPlainObject(value);
        };
        function isRegExp(value) {
          return (isObjectLike(value) && objToString.call(value) == regexpTag) || false;
        }
        function isString(value) {
          return typeof value == 'string' || (isObjectLike(value) && objToString.call(value) == stringTag);
        }
        function isTypedArray(value) {
          return isObjectLike(value) && isLength(value.length) && !!typedArrayTags[objToString.call(value)];
        }
        function isUndefined(value) {
          return value === undefined;
        }
        function toArray(value) {
          var length = value ? getLength(value) : 0;
          if (!isLength(length)) {
            return values(value);
          }
          if (!length) {
            return [];
          }
          return arrayCopy(value);
        }
        function toPlainObject(value) {
          return baseCopy(value, keysIn(value));
        }
        var assign = createAssigner(function(object, source, customizer) {
          return customizer ? assignWith(object, source, customizer) : baseAssign(object, source);
        });
        function create(prototype, properties, guard) {
          var result = baseCreate(prototype);
          if (guard && isIterateeCall(prototype, properties, guard)) {
            properties = null;
          }
          return properties ? baseAssign(result, properties) : result;
        }
        var defaults = restParam(function(args) {
          var object = args[0];
          if (object == null) {
            return object;
          }
          args.push(assignDefaults);
          return assign.apply(undefined, args);
        });
        var findKey = createFindKey(baseForOwn);
        var findLastKey = createFindKey(baseForOwnRight);
        var forIn = createForIn(baseFor);
        var forInRight = createForIn(baseForRight);
        var forOwn = createForOwn(baseForOwn);
        var forOwnRight = createForOwn(baseForOwnRight);
        function functions(object) {
          return baseFunctions(object, keysIn(object));
        }
        function get(object, path, defaultValue) {
          var result = object == null ? undefined : baseGet(object, toPath(path), path + '');
          return result === undefined ? defaultValue : result;
        }
        function has(object, path) {
          if (object == null) {
            return false;
          }
          var result = hasOwnProperty.call(object, path);
          if (!result && !isKey(path)) {
            path = toPath(path);
            object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
            path = last(path);
            result = object != null && hasOwnProperty.call(object, path);
          }
          return result;
        }
        function invert(object, multiValue, guard) {
          if (guard && isIterateeCall(object, multiValue, guard)) {
            multiValue = null;
          }
          var index = -1,
              props = keys(object),
              length = props.length,
              result = {};
          while (++index < length) {
            var key = props[index],
                value = object[key];
            if (multiValue) {
              if (hasOwnProperty.call(result, value)) {
                result[value].push(key);
              } else {
                result[value] = [key];
              }
            } else {
              result[value] = key;
            }
          }
          return result;
        }
        var keys = !nativeKeys ? shimKeys : function(object) {
          if (object) {
            var Ctor = object.constructor,
                length = object.length;
          }
          if ((typeof Ctor == 'function' && Ctor.prototype === object) || (typeof object != 'function' && isLength(length))) {
            return shimKeys(object);
          }
          return isObject(object) ? nativeKeys(object) : [];
        };
        function keysIn(object) {
          if (object == null) {
            return [];
          }
          if (!isObject(object)) {
            object = Object(object);
          }
          var length = object.length;
          length = (length && isLength(length) && (isArray(object) || (support.nonEnumArgs && isArguments(object))) && length) || 0;
          var Ctor = object.constructor,
              index = -1,
              isProto = typeof Ctor == 'function' && Ctor.prototype === object,
              result = Array(length),
              skipIndexes = length > 0;
          while (++index < length) {
            result[index] = (index + '');
          }
          for (var key in object) {
            if (!(skipIndexes && isIndex(key, length)) && !(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
              result.push(key);
            }
          }
          return result;
        }
        function mapValues(object, iteratee, thisArg) {
          var result = {};
          iteratee = getCallback(iteratee, thisArg, 3);
          baseForOwn(object, function(value, key, object) {
            result[key] = iteratee(value, key, object);
          });
          return result;
        }
        var merge = createAssigner(baseMerge);
        var omit = restParam(function(object, props) {
          if (object == null) {
            return {};
          }
          if (typeof props[0] != 'function') {
            var props = arrayMap(baseFlatten(props), String);
            return pickByArray(object, baseDifference(keysIn(object), props));
          }
          var predicate = bindCallback(props[0], props[1], 3);
          return pickByCallback(object, function(value, key, object) {
            return !predicate(value, key, object);
          });
        });
        function pairs(object) {
          var index = -1,
              props = keys(object),
              length = props.length,
              result = Array(length);
          while (++index < length) {
            var key = props[index];
            result[index] = [key, object[key]];
          }
          return result;
        }
        var pick = restParam(function(object, props) {
          if (object == null) {
            return {};
          }
          return typeof props[0] == 'function' ? pickByCallback(object, bindCallback(props[0], props[1], 3)) : pickByArray(object, baseFlatten(props));
        });
        function result(object, path, defaultValue) {
          var result = object == null ? undefined : object[path];
          if (result === undefined) {
            if (object != null && !isKey(path, object)) {
              path = toPath(path);
              object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
              result = object == null ? undefined : object[last(path)];
            }
            result = result === undefined ? defaultValue : result;
          }
          return isFunction(result) ? result.call(object) : result;
        }
        function set(object, path, value) {
          if (object == null) {
            return object;
          }
          var pathKey = (path + '');
          path = (object[pathKey] != null || isKey(path, object)) ? [pathKey] : toPath(path);
          var index = -1,
              length = path.length,
              endIndex = length - 1,
              nested = object;
          while (nested != null && ++index < length) {
            var key = path[index];
            if (isObject(nested)) {
              if (index == endIndex) {
                nested[key] = value;
              } else if (nested[key] == null) {
                nested[key] = isIndex(path[index + 1]) ? [] : {};
              }
            }
            nested = nested[key];
          }
          return object;
        }
        function transform(object, iteratee, accumulator, thisArg) {
          var isArr = isArray(object) || isTypedArray(object);
          iteratee = getCallback(iteratee, thisArg, 4);
          if (accumulator == null) {
            if (isArr || isObject(object)) {
              var Ctor = object.constructor;
              if (isArr) {
                accumulator = isArray(object) ? new Ctor : [];
              } else {
                accumulator = baseCreate(isFunction(Ctor) && Ctor.prototype);
              }
            } else {
              accumulator = {};
            }
          }
          (isArr ? arrayEach : baseForOwn)(object, function(value, index, object) {
            return iteratee(accumulator, value, index, object);
          });
          return accumulator;
        }
        function values(object) {
          return baseValues(object, keys(object));
        }
        function valuesIn(object) {
          return baseValues(object, keysIn(object));
        }
        function inRange(value, start, end) {
          start = +start || 0;
          if (typeof end === 'undefined') {
            end = start;
            start = 0;
          } else {
            end = +end || 0;
          }
          return value >= nativeMin(start, end) && value < nativeMax(start, end);
        }
        function random(min, max, floating) {
          if (floating && isIterateeCall(min, max, floating)) {
            max = floating = null;
          }
          var noMin = min == null,
              noMax = max == null;
          if (floating == null) {
            if (noMax && typeof min == 'boolean') {
              floating = min;
              min = 1;
            } else if (typeof max == 'boolean') {
              floating = max;
              noMax = true;
            }
          }
          if (noMin && noMax) {
            max = 1;
            noMax = false;
          }
          min = +min || 0;
          if (noMax) {
            max = min;
            min = 0;
          } else {
            max = +max || 0;
          }
          if (floating || min % 1 || max % 1) {
            var rand = nativeRandom();
            return nativeMin(min + (rand * (max - min + parseFloat('1e-' + ((rand + '').length - 1)))), max);
          }
          return baseRandom(min, max);
        }
        var camelCase = createCompounder(function(result, word, index) {
          word = word.toLowerCase();
          return result + (index ? (word.charAt(0).toUpperCase() + word.slice(1)) : word);
        });
        function capitalize(string) {
          string = baseToString(string);
          return string && (string.charAt(0).toUpperCase() + string.slice(1));
        }
        function deburr(string) {
          string = baseToString(string);
          return string && string.replace(reLatin1, deburrLetter).replace(reComboMark, '');
        }
        function endsWith(string, target, position) {
          string = baseToString(string);
          target = (target + '');
          var length = string.length;
          position = position === undefined ? length : nativeMin(position < 0 ? 0 : (+position || 0), length);
          position -= target.length;
          return position >= 0 && string.indexOf(target, position) == position;
        }
        function escape(string) {
          string = baseToString(string);
          return (string && reHasUnescapedHtml.test(string)) ? string.replace(reUnescapedHtml, escapeHtmlChar) : string;
        }
        function escapeRegExp(string) {
          string = baseToString(string);
          return (string && reHasRegExpChars.test(string)) ? string.replace(reRegExpChars, '\\$&') : string;
        }
        var kebabCase = createCompounder(function(result, word, index) {
          return result + (index ? '-' : '') + word.toLowerCase();
        });
        function pad(string, length, chars) {
          string = baseToString(string);
          length = +length;
          var strLength = string.length;
          if (strLength >= length || !nativeIsFinite(length)) {
            return string;
          }
          var mid = (length - strLength) / 2,
              leftLength = floor(mid),
              rightLength = ceil(mid);
          chars = createPadding('', rightLength, chars);
          return chars.slice(0, leftLength) + string + chars;
        }
        var padLeft = createPadDir();
        var padRight = createPadDir(true);
        function parseInt(string, radix, guard) {
          if (guard && isIterateeCall(string, radix, guard)) {
            radix = 0;
          }
          return nativeParseInt(string, radix);
        }
        if (nativeParseInt(whitespace + '08') != 8) {
          parseInt = function(string, radix, guard) {
            if (guard ? isIterateeCall(string, radix, guard) : radix == null) {
              radix = 0;
            } else if (radix) {
              radix = +radix;
            }
            string = trim(string);
            return nativeParseInt(string, radix || (reHasHexPrefix.test(string) ? 16 : 10));
          };
        }
        function repeat(string, n) {
          var result = '';
          string = baseToString(string);
          n = +n;
          if (n < 1 || !string || !nativeIsFinite(n)) {
            return result;
          }
          do {
            if (n % 2) {
              result += string;
            }
            n = floor(n / 2);
            string += string;
          } while (n);
          return result;
        }
        var snakeCase = createCompounder(function(result, word, index) {
          return result + (index ? '_' : '') + word.toLowerCase();
        });
        var startCase = createCompounder(function(result, word, index) {
          return result + (index ? ' ' : '') + (word.charAt(0).toUpperCase() + word.slice(1));
        });
        function startsWith(string, target, position) {
          string = baseToString(string);
          position = position == null ? 0 : nativeMin(position < 0 ? 0 : (+position || 0), string.length);
          return string.lastIndexOf(target, position) == position;
        }
        function template(string, options, otherOptions) {
          var settings = lodash.templateSettings;
          if (otherOptions && isIterateeCall(string, options, otherOptions)) {
            options = otherOptions = null;
          }
          string = baseToString(string);
          options = assignWith(baseAssign({}, otherOptions || options), settings, assignOwnDefaults);
          var imports = assignWith(baseAssign({}, options.imports), settings.imports, assignOwnDefaults),
              importsKeys = keys(imports),
              importsValues = baseValues(imports, importsKeys);
          var isEscaping,
              isEvaluating,
              index = 0,
              interpolate = options.interpolate || reNoMatch,
              source = "__p += '";
          var reDelimiters = RegExp((options.escape || reNoMatch).source + '|' + interpolate.source + '|' + (interpolate === reInterpolate ? reEsTemplate : reNoMatch).source + '|' + (options.evaluate || reNoMatch).source + '|$', 'g');
          var sourceURL = '//# sourceURL=' + ('sourceURL' in options ? options.sourceURL : ('lodash.templateSources[' + (++templateCounter) + ']')) + '\n';
          string.replace(reDelimiters, function(match, escapeValue, interpolateValue, esTemplateValue, evaluateValue, offset) {
            interpolateValue || (interpolateValue = esTemplateValue);
            source += string.slice(index, offset).replace(reUnescapedString, escapeStringChar);
            if (escapeValue) {
              isEscaping = true;
              source += "' +\n__e(" + escapeValue + ") +\n'";
            }
            if (evaluateValue) {
              isEvaluating = true;
              source += "';\n" + evaluateValue + ";\n__p += '";
            }
            if (interpolateValue) {
              source += "' +\n((__t = (" + interpolateValue + ")) == null ? '' : __t) +\n'";
            }
            index = offset + match.length;
            return match;
          });
          source += "';\n";
          var variable = options.variable;
          if (!variable) {
            source = 'with (obj) {\n' + source + '\n}\n';
          }
          source = (isEvaluating ? source.replace(reEmptyStringLeading, '') : source).replace(reEmptyStringMiddle, '$1').replace(reEmptyStringTrailing, '$1;');
          source = 'function(' + (variable || 'obj') + ') {\n' + (variable ? '' : 'obj || (obj = {});\n') + "var __t, __p = ''" + (isEscaping ? ', __e = _.escape' : '') + (isEvaluating ? ', __j = Array.prototype.join;\n' + "function print() { __p += __j.call(arguments, '') }\n" : ';\n') + source + 'return __p\n}';
          var result = attempt(function() {
            return Function(importsKeys, sourceURL + 'return ' + source).apply(undefined, importsValues);
          });
          result.source = source;
          if (isError(result)) {
            throw result;
          }
          return result;
        }
        function trim(string, chars, guard) {
          var value = string;
          string = baseToString(string);
          if (!string) {
            return string;
          }
          if (guard ? isIterateeCall(value, chars, guard) : chars == null) {
            return string.slice(trimmedLeftIndex(string), trimmedRightIndex(string) + 1);
          }
          chars = (chars + '');
          return string.slice(charsLeftIndex(string, chars), charsRightIndex(string, chars) + 1);
        }
        function trimLeft(string, chars, guard) {
          var value = string;
          string = baseToString(string);
          if (!string) {
            return string;
          }
          if (guard ? isIterateeCall(value, chars, guard) : chars == null) {
            return string.slice(trimmedLeftIndex(string));
          }
          return string.slice(charsLeftIndex(string, (chars + '')));
        }
        function trimRight(string, chars, guard) {
          var value = string;
          string = baseToString(string);
          if (!string) {
            return string;
          }
          if (guard ? isIterateeCall(value, chars, guard) : chars == null) {
            return string.slice(0, trimmedRightIndex(string) + 1);
          }
          return string.slice(0, charsRightIndex(string, (chars + '')) + 1);
        }
        function trunc(string, options, guard) {
          if (guard && isIterateeCall(string, options, guard)) {
            options = null;
          }
          var length = DEFAULT_TRUNC_LENGTH,
              omission = DEFAULT_TRUNC_OMISSION;
          if (options != null) {
            if (isObject(options)) {
              var separator = 'separator' in options ? options.separator : separator;
              length = 'length' in options ? (+options.length || 0) : length;
              omission = 'omission' in options ? baseToString(options.omission) : omission;
            } else {
              length = +options || 0;
            }
          }
          string = baseToString(string);
          if (length >= string.length) {
            return string;
          }
          var end = length - omission.length;
          if (end < 1) {
            return omission;
          }
          var result = string.slice(0, end);
          if (separator == null) {
            return result + omission;
          }
          if (isRegExp(separator)) {
            if (string.slice(end).search(separator)) {
              var match,
                  newEnd,
                  substring = string.slice(0, end);
              if (!separator.global) {
                separator = RegExp(separator.source, (reFlags.exec(separator) || '') + 'g');
              }
              separator.lastIndex = 0;
              while ((match = separator.exec(substring))) {
                newEnd = match.index;
              }
              result = result.slice(0, newEnd == null ? end : newEnd);
            }
          } else if (string.indexOf(separator, end) != end) {
            var index = result.lastIndexOf(separator);
            if (index > -1) {
              result = result.slice(0, index);
            }
          }
          return result + omission;
        }
        function unescape(string) {
          string = baseToString(string);
          return (string && reHasEscapedHtml.test(string)) ? string.replace(reEscapedHtml, unescapeHtmlChar) : string;
        }
        function words(string, pattern, guard) {
          if (guard && isIterateeCall(string, pattern, guard)) {
            pattern = null;
          }
          string = baseToString(string);
          return string.match(pattern || reWords) || [];
        }
        var attempt = restParam(function(func, args) {
          try {
            return func.apply(undefined, args);
          } catch (e) {
            return isError(e) ? e : new Error(e);
          }
        });
        function callback(func, thisArg, guard) {
          if (guard && isIterateeCall(func, thisArg, guard)) {
            thisArg = null;
          }
          return baseCallback(func, thisArg);
        }
        function constant(value) {
          return function() {
            return value;
          };
        }
        function identity(value) {
          return value;
        }
        function matches(source) {
          return baseMatches(baseClone(source, true));
        }
        function matchesProperty(path, value) {
          return baseMatchesProperty(path, baseClone(value, true));
        }
        var method = restParam(function(path, args) {
          return function(object) {
            return invokePath(object, path, args);
          };
        });
        var methodOf = restParam(function(object, args) {
          return function(path) {
            return invokePath(object, path, args);
          };
        });
        function mixin(object, source, options) {
          if (options == null) {
            var isObj = isObject(source),
                props = isObj && keys(source),
                methodNames = props && props.length && baseFunctions(source, props);
            if (!(methodNames ? methodNames.length : isObj)) {
              methodNames = false;
              options = source;
              source = object;
              object = this;
            }
          }
          if (!methodNames) {
            methodNames = baseFunctions(source, keys(source));
          }
          var chain = true,
              index = -1,
              isFunc = isFunction(object),
              length = methodNames.length;
          if (options === false) {
            chain = false;
          } else if (isObject(options) && 'chain' in options) {
            chain = options.chain;
          }
          while (++index < length) {
            var methodName = methodNames[index],
                func = source[methodName];
            object[methodName] = func;
            if (isFunc) {
              object.prototype[methodName] = (function(func) {
                return function() {
                  var chainAll = this.__chain__;
                  if (chain || chainAll) {
                    var result = object(this.__wrapped__),
                        actions = result.__actions__ = arrayCopy(this.__actions__);
                    actions.push({
                      'func': func,
                      'args': arguments,
                      'thisArg': object
                    });
                    result.__chain__ = chainAll;
                    return result;
                  }
                  var args = [this.value()];
                  push.apply(args, arguments);
                  return func.apply(object, args);
                };
              }(func));
            }
          }
          return object;
        }
        function noConflict() {
          context._ = oldDash;
          return this;
        }
        function noop() {}
        function property(path) {
          return isKey(path) ? baseProperty(path) : basePropertyDeep(path);
        }
        function propertyOf(object) {
          return function(path) {
            return baseGet(object, toPath(path), path + '');
          };
        }
        function range(start, end, step) {
          if (step && isIterateeCall(start, end, step)) {
            end = step = null;
          }
          start = +start || 0;
          step = step == null ? 1 : (+step || 0);
          if (end == null) {
            end = start;
            start = 0;
          } else {
            end = +end || 0;
          }
          var index = -1,
              length = nativeMax(ceil((end - start) / (step || 1)), 0),
              result = Array(length);
          while (++index < length) {
            result[index] = start;
            start += step;
          }
          return result;
        }
        function times(n, iteratee, thisArg) {
          n = floor(n);
          if (n < 1 || !nativeIsFinite(n)) {
            return [];
          }
          var index = -1,
              result = Array(nativeMin(n, MAX_ARRAY_LENGTH));
          iteratee = bindCallback(iteratee, thisArg, 1);
          while (++index < n) {
            if (index < MAX_ARRAY_LENGTH) {
              result[index] = iteratee(index);
            } else {
              iteratee(index);
            }
          }
          return result;
        }
        function uniqueId(prefix) {
          var id = ++idCounter;
          return baseToString(prefix) + id;
        }
        function add(augend, addend) {
          return (+augend || 0) + (+addend || 0);
        }
        var max = createExtremum(arrayMax);
        var min = createExtremum(arrayMin, true);
        function sum(collection, iteratee, thisArg) {
          if (thisArg && isIterateeCall(collection, iteratee, thisArg)) {
            iteratee = null;
          }
          var func = getCallback(),
              noIteratee = iteratee == null;
          if (!(func === baseCallback && noIteratee)) {
            noIteratee = false;
            iteratee = func(iteratee, thisArg, 3);
          }
          return noIteratee ? arraySum(isArray(collection) ? collection : toIterable(collection)) : baseSum(collection, iteratee);
        }
        lodash.prototype = baseLodash.prototype;
        LodashWrapper.prototype = baseCreate(baseLodash.prototype);
        LodashWrapper.prototype.constructor = LodashWrapper;
        LazyWrapper.prototype = baseCreate(baseLodash.prototype);
        LazyWrapper.prototype.constructor = LazyWrapper;
        MapCache.prototype['delete'] = mapDelete;
        MapCache.prototype.get = mapGet;
        MapCache.prototype.has = mapHas;
        MapCache.prototype.set = mapSet;
        SetCache.prototype.push = cachePush;
        memoize.Cache = MapCache;
        lodash.after = after;
        lodash.ary = ary;
        lodash.assign = assign;
        lodash.at = at;
        lodash.before = before;
        lodash.bind = bind;
        lodash.bindAll = bindAll;
        lodash.bindKey = bindKey;
        lodash.callback = callback;
        lodash.chain = chain;
        lodash.chunk = chunk;
        lodash.compact = compact;
        lodash.constant = constant;
        lodash.countBy = countBy;
        lodash.create = create;
        lodash.curry = curry;
        lodash.curryRight = curryRight;
        lodash.debounce = debounce;
        lodash.defaults = defaults;
        lodash.defer = defer;
        lodash.delay = delay;
        lodash.difference = difference;
        lodash.drop = drop;
        lodash.dropRight = dropRight;
        lodash.dropRightWhile = dropRightWhile;
        lodash.dropWhile = dropWhile;
        lodash.fill = fill;
        lodash.filter = filter;
        lodash.flatten = flatten;
        lodash.flattenDeep = flattenDeep;
        lodash.flow = flow;
        lodash.flowRight = flowRight;
        lodash.forEach = forEach;
        lodash.forEachRight = forEachRight;
        lodash.forIn = forIn;
        lodash.forInRight = forInRight;
        lodash.forOwn = forOwn;
        lodash.forOwnRight = forOwnRight;
        lodash.functions = functions;
        lodash.groupBy = groupBy;
        lodash.indexBy = indexBy;
        lodash.initial = initial;
        lodash.intersection = intersection;
        lodash.invert = invert;
        lodash.invoke = invoke;
        lodash.keys = keys;
        lodash.keysIn = keysIn;
        lodash.map = map;
        lodash.mapValues = mapValues;
        lodash.matches = matches;
        lodash.matchesProperty = matchesProperty;
        lodash.memoize = memoize;
        lodash.merge = merge;
        lodash.method = method;
        lodash.methodOf = methodOf;
        lodash.mixin = mixin;
        lodash.negate = negate;
        lodash.omit = omit;
        lodash.once = once;
        lodash.pairs = pairs;
        lodash.partial = partial;
        lodash.partialRight = partialRight;
        lodash.partition = partition;
        lodash.pick = pick;
        lodash.pluck = pluck;
        lodash.property = property;
        lodash.propertyOf = propertyOf;
        lodash.pull = pull;
        lodash.pullAt = pullAt;
        lodash.range = range;
        lodash.rearg = rearg;
        lodash.reject = reject;
        lodash.remove = remove;
        lodash.rest = rest;
        lodash.restParam = restParam;
        lodash.set = set;
        lodash.shuffle = shuffle;
        lodash.slice = slice;
        lodash.sortBy = sortBy;
        lodash.sortByAll = sortByAll;
        lodash.sortByOrder = sortByOrder;
        lodash.spread = spread;
        lodash.take = take;
        lodash.takeRight = takeRight;
        lodash.takeRightWhile = takeRightWhile;
        lodash.takeWhile = takeWhile;
        lodash.tap = tap;
        lodash.throttle = throttle;
        lodash.thru = thru;
        lodash.times = times;
        lodash.toArray = toArray;
        lodash.toPlainObject = toPlainObject;
        lodash.transform = transform;
        lodash.union = union;
        lodash.uniq = uniq;
        lodash.unzip = unzip;
        lodash.values = values;
        lodash.valuesIn = valuesIn;
        lodash.where = where;
        lodash.without = without;
        lodash.wrap = wrap;
        lodash.xor = xor;
        lodash.zip = zip;
        lodash.zipObject = zipObject;
        lodash.backflow = flowRight;
        lodash.collect = map;
        lodash.compose = flowRight;
        lodash.each = forEach;
        lodash.eachRight = forEachRight;
        lodash.extend = assign;
        lodash.iteratee = callback;
        lodash.methods = functions;
        lodash.object = zipObject;
        lodash.select = filter;
        lodash.tail = rest;
        lodash.unique = uniq;
        mixin(lodash, lodash);
        lodash.add = add;
        lodash.attempt = attempt;
        lodash.camelCase = camelCase;
        lodash.capitalize = capitalize;
        lodash.clone = clone;
        lodash.cloneDeep = cloneDeep;
        lodash.deburr = deburr;
        lodash.endsWith = endsWith;
        lodash.escape = escape;
        lodash.escapeRegExp = escapeRegExp;
        lodash.every = every;
        lodash.find = find;
        lodash.findIndex = findIndex;
        lodash.findKey = findKey;
        lodash.findLast = findLast;
        lodash.findLastIndex = findLastIndex;
        lodash.findLastKey = findLastKey;
        lodash.findWhere = findWhere;
        lodash.first = first;
        lodash.get = get;
        lodash.has = has;
        lodash.identity = identity;
        lodash.includes = includes;
        lodash.indexOf = indexOf;
        lodash.inRange = inRange;
        lodash.isArguments = isArguments;
        lodash.isArray = isArray;
        lodash.isBoolean = isBoolean;
        lodash.isDate = isDate;
        lodash.isElement = isElement;
        lodash.isEmpty = isEmpty;
        lodash.isEqual = isEqual;
        lodash.isError = isError;
        lodash.isFinite = isFinite;
        lodash.isFunction = isFunction;
        lodash.isMatch = isMatch;
        lodash.isNaN = isNaN;
        lodash.isNative = isNative;
        lodash.isNull = isNull;
        lodash.isNumber = isNumber;
        lodash.isObject = isObject;
        lodash.isPlainObject = isPlainObject;
        lodash.isRegExp = isRegExp;
        lodash.isString = isString;
        lodash.isTypedArray = isTypedArray;
        lodash.isUndefined = isUndefined;
        lodash.kebabCase = kebabCase;
        lodash.last = last;
        lodash.lastIndexOf = lastIndexOf;
        lodash.max = max;
        lodash.min = min;
        lodash.noConflict = noConflict;
        lodash.noop = noop;
        lodash.now = now;
        lodash.pad = pad;
        lodash.padLeft = padLeft;
        lodash.padRight = padRight;
        lodash.parseInt = parseInt;
        lodash.random = random;
        lodash.reduce = reduce;
        lodash.reduceRight = reduceRight;
        lodash.repeat = repeat;
        lodash.result = result;
        lodash.runInContext = runInContext;
        lodash.size = size;
        lodash.snakeCase = snakeCase;
        lodash.some = some;
        lodash.sortedIndex = sortedIndex;
        lodash.sortedLastIndex = sortedLastIndex;
        lodash.startCase = startCase;
        lodash.startsWith = startsWith;
        lodash.sum = sum;
        lodash.template = template;
        lodash.trim = trim;
        lodash.trimLeft = trimLeft;
        lodash.trimRight = trimRight;
        lodash.trunc = trunc;
        lodash.unescape = unescape;
        lodash.uniqueId = uniqueId;
        lodash.words = words;
        lodash.all = every;
        lodash.any = some;
        lodash.contains = includes;
        lodash.detect = find;
        lodash.foldl = reduce;
        lodash.foldr = reduceRight;
        lodash.head = first;
        lodash.include = includes;
        lodash.inject = reduce;
        mixin(lodash, (function() {
          var source = {};
          baseForOwn(lodash, function(func, methodName) {
            if (!lodash.prototype[methodName]) {
              source[methodName] = func;
            }
          });
          return source;
        }()), false);
        lodash.sample = sample;
        lodash.prototype.sample = function(n) {
          if (!this.__chain__ && n == null) {
            return sample(this.value());
          }
          return this.thru(function(value) {
            return sample(value, n);
          });
        };
        lodash.VERSION = VERSION;
        arrayEach(['bind', 'bindKey', 'curry', 'curryRight', 'partial', 'partialRight'], function(methodName) {
          lodash[methodName].placeholder = lodash;
        });
        arrayEach(['dropWhile', 'filter', 'map', 'takeWhile'], function(methodName, type) {
          var isFilter = type != LAZY_MAP_FLAG,
              isDropWhile = type == LAZY_DROP_WHILE_FLAG;
          LazyWrapper.prototype[methodName] = function(iteratee, thisArg) {
            var filtered = this.__filtered__,
                result = (filtered && isDropWhile) ? new LazyWrapper(this) : this.clone(),
                iteratees = result.__iteratees__ || (result.__iteratees__ = []);
            iteratees.push({
              'done': false,
              'count': 0,
              'index': 0,
              'iteratee': getCallback(iteratee, thisArg, 1),
              'limit': -1,
              'type': type
            });
            result.__filtered__ = filtered || isFilter;
            return result;
          };
        });
        arrayEach(['drop', 'take'], function(methodName, index) {
          var whileName = methodName + 'While';
          LazyWrapper.prototype[methodName] = function(n) {
            var filtered = this.__filtered__,
                result = (filtered && !index) ? this.dropWhile() : this.clone();
            n = n == null ? 1 : nativeMax(floor(n) || 0, 0);
            if (filtered) {
              if (index) {
                result.__takeCount__ = nativeMin(result.__takeCount__, n);
              } else {
                last(result.__iteratees__).limit = n;
              }
            } else {
              var views = result.__views__ || (result.__views__ = []);
              views.push({
                'size': n,
                'type': methodName + (result.__dir__ < 0 ? 'Right' : '')
              });
            }
            return result;
          };
          LazyWrapper.prototype[methodName + 'Right'] = function(n) {
            return this.reverse()[methodName](n).reverse();
          };
          LazyWrapper.prototype[methodName + 'RightWhile'] = function(predicate, thisArg) {
            return this.reverse()[whileName](predicate, thisArg).reverse();
          };
        });
        arrayEach(['first', 'last'], function(methodName, index) {
          var takeName = 'take' + (index ? 'Right' : '');
          LazyWrapper.prototype[methodName] = function() {
            return this[takeName](1).value()[0];
          };
        });
        arrayEach(['initial', 'rest'], function(methodName, index) {
          var dropName = 'drop' + (index ? '' : 'Right');
          LazyWrapper.prototype[methodName] = function() {
            return this[dropName](1);
          };
        });
        arrayEach(['pluck', 'where'], function(methodName, index) {
          var operationName = index ? 'filter' : 'map',
              createCallback = index ? baseMatches : property;
          LazyWrapper.prototype[methodName] = function(value) {
            return this[operationName](createCallback(value));
          };
        });
        LazyWrapper.prototype.compact = function() {
          return this.filter(identity);
        };
        LazyWrapper.prototype.reject = function(predicate, thisArg) {
          predicate = getCallback(predicate, thisArg, 1);
          return this.filter(function(value) {
            return !predicate(value);
          });
        };
        LazyWrapper.prototype.slice = function(start, end) {
          start = start == null ? 0 : (+start || 0);
          var result = start < 0 ? this.takeRight(-start) : this.drop(start);
          if (end !== undefined) {
            end = (+end || 0);
            result = end < 0 ? result.dropRight(-end) : result.take(end - start);
          }
          return result;
        };
        LazyWrapper.prototype.toArray = function() {
          return this.drop(0);
        };
        baseForOwn(LazyWrapper.prototype, function(func, methodName) {
          var lodashFunc = lodash[methodName];
          if (!lodashFunc) {
            return ;
          }
          var checkIteratee = /^(?:filter|map|reject)|While$/.test(methodName),
              retUnwrapped = /^(?:first|last)$/.test(methodName);
          lodash.prototype[methodName] = function() {
            var args = arguments,
                length = args.length,
                chainAll = this.__chain__,
                value = this.__wrapped__,
                isHybrid = !!this.__actions__.length,
                isLazy = value instanceof LazyWrapper,
                iteratee = args[0],
                useLazy = isLazy || isArray(value);
            if (useLazy && checkIteratee && typeof iteratee == 'function' && iteratee.length != 1) {
              isLazy = useLazy = false;
            }
            var onlyLazy = isLazy && !isHybrid;
            if (retUnwrapped && !chainAll) {
              return onlyLazy ? func.call(value) : lodashFunc.call(lodash, this.value());
            }
            var interceptor = function(value) {
              var otherArgs = [value];
              push.apply(otherArgs, args);
              return lodashFunc.apply(lodash, otherArgs);
            };
            if (useLazy) {
              var wrapper = onlyLazy ? value : new LazyWrapper(this),
                  result = func.apply(wrapper, args);
              if (!retUnwrapped && (isHybrid || result.__actions__)) {
                var actions = result.__actions__ || (result.__actions__ = []);
                actions.push({
                  'func': thru,
                  'args': [interceptor],
                  'thisArg': lodash
                });
              }
              return new LodashWrapper(result, chainAll);
            }
            return this.thru(interceptor);
          };
        });
        arrayEach(['concat', 'join', 'pop', 'push', 'replace', 'shift', 'sort', 'splice', 'split', 'unshift'], function(methodName) {
          var func = (/^(?:replace|split)$/.test(methodName) ? stringProto : arrayProto)[methodName],
              chainName = /^(?:push|sort|unshift)$/.test(methodName) ? 'tap' : 'thru',
              retUnwrapped = /^(?:join|pop|replace|shift)$/.test(methodName);
          lodash.prototype[methodName] = function() {
            var args = arguments;
            if (retUnwrapped && !this.__chain__) {
              return func.apply(this.value(), args);
            }
            return this[chainName](function(value) {
              return func.apply(value, args);
            });
          };
        });
        baseForOwn(LazyWrapper.prototype, function(func, methodName) {
          var lodashFunc = lodash[methodName];
          if (lodashFunc) {
            var key = lodashFunc.name,
                names = realNames[key] || (realNames[key] = []);
            names.push({
              'name': methodName,
              'func': lodashFunc
            });
          }
        });
        realNames[createHybridWrapper(null, BIND_KEY_FLAG).name] = [{
          'name': 'wrapper',
          'func': null
        }];
        LazyWrapper.prototype.clone = lazyClone;
        LazyWrapper.prototype.reverse = lazyReverse;
        LazyWrapper.prototype.value = lazyValue;
        lodash.prototype.chain = wrapperChain;
        lodash.prototype.commit = wrapperCommit;
        lodash.prototype.plant = wrapperPlant;
        lodash.prototype.reverse = wrapperReverse;
        lodash.prototype.toString = wrapperToString;
        lodash.prototype.run = lodash.prototype.toJSON = lodash.prototype.valueOf = lodash.prototype.value = wrapperValue;
        lodash.prototype.collect = lodash.prototype.map;
        lodash.prototype.head = lodash.prototype.first;
        lodash.prototype.select = lodash.prototype.filter;
        lodash.prototype.tail = lodash.prototype.rest;
        return lodash;
      }
      var _ = runInContext();
      if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
        root._ = _;
        define(function() {
          return _;
        });
      } else if (freeExports && freeModule) {
        if (moduleExports) {
          (freeModule.exports = _)._ = _;
        } else {
          freeExports._ = _;
        }
      } else {
        root._ = _;
      }
    }.call(this));
  })(require("github:jspm/nodelibs-process@0.1.1"));
  global.define = __define;
  return module.exports;
});



System.register("src/components/autoSurface", ["npm:lodash@3.7.0", "npm:famous@0.3.5/core/Surface", "npm:famous@0.3.5/core/Context", "npm:famous@0.3.5/core/Modifier"], function($__export) {
  "use strict";
  var __moduleName = "src/components/autoSurface";
  var _,
      Surface,
      Context,
      Modifier;
  return {
    setters: [function($__m) {
      _ = $__m.default;
    }, function($__m) {
      Surface = $__m.default;
    }, function($__m) {
      Context = $__m.default;
    }, function($__m) {
      Modifier = $__m.default;
    }],
    execute: function() {
      $__export('default', (function($__super) {
        var AutoSurface = function AutoSurface(context) {
          var options = arguments[1] !== (void 0) ? arguments[1] : null;
          var modifierOptions = arguments[2] !== (void 0) ? arguments[2] : null;
          options = _.merge(this.defaultOptions, options);
          $traceurRuntime.superConstructor(AutoSurface).call(this, options);
          if (context) {
            var contextBranch = context;
            if (modifierOptions) {
              this._topLevelModifier = new Modifier(modifierOptions);
              contextBranch = context.add(this._topLevelModifier);
            }
            if (this.modifierChain) {
              contextBranch = contextBranch.add(this.modifierChain);
            }
            contextBranch.add(this);
            this._context = contextBranch;
          }
        };
        return ($traceurRuntime.createClass)(AutoSurface, {
          get defaultOptions() {
            return {};
          },
          get context() {
            return this._context;
          },
          get modifierChain() {
            return this._modifierChain;
          },
          get topLevelModifier() {
            return this._topLevelModifier;
          }
        }, {}, $__super);
      }(Surface)));
    }
  };
});



System.register("npm:famous@0.3.5/modifiers/ModifierChain", [], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  function ModifierChain() {
    this._chain = [];
    if (arguments.length)
      this.addModifier.apply(this, arguments);
  }
  ModifierChain.prototype.addModifier = function addModifier(varargs) {
    Array.prototype.push.apply(this._chain, arguments);
  };
  ModifierChain.prototype.removeModifier = function removeModifier(modifier) {
    var index = this._chain.indexOf(modifier);
    if (index < 0)
      return ;
    this._chain.splice(index, 1);
  };
  ModifierChain.prototype.modify = function modify(input) {
    var chain = this._chain;
    var result = input;
    for (var i = 0; i < chain.length; i++) {
      result = chain[i].modify(result);
    }
    return result;
  };
  module.exports = ModifierChain;
  global.define = __define;
  return module.exports;
});



System.register("src/utils/ObjectHelper", ["npm:lodash@3.7.0"], function($__export) {
  "use strict";
  var __moduleName = "src/utils/ObjectHelper";
  var _;
  return {
    setters: [function($__m) {
      _ = $__m.default;
    }],
    execute: function() {
      $__export('default', (function() {
        var ObjectHelper = function ObjectHelper() {};
        return ($traceurRuntime.createClass)(ObjectHelper, {}, {
          hideMethodsAndPrivatePropertiesFromObject: function(object) {
            for (var propName in object) {
              var prototype = Object.getPrototypeOf(object);
              var descriptor = prototype ? Object.getOwnPropertyDescriptor(prototype, propName) : undefined;
              if (descriptor && (descriptor.get || descriptor.set) && !propName.startsWith('_')) {
                continue;
              }
              var property = object[propName];
              if (typeof property === 'function' || propName.startsWith('_')) {
                ObjectHelper.hidePropertyFromObject(object, propName);
              }
            }
          },
          hideMethodsFromObject: function(object) {
            for (var propName in object) {
              var property = object[propName];
              if (typeof property === 'function') {
                ObjectHelper.hidePropertyFromObject(object, propName);
              }
            }
          },
          hidePropertyFromObject: function(object, propName) {
            var prototype = object;
            var descriptor = Object.getOwnPropertyDescriptor(object, propName);
            while (!descriptor) {
              prototype = Object.getPrototypeOf(prototype);
              if (prototype.constructor.name === 'Object' || prototype.constructor.name === 'Array') {
                return ;
              }
              descriptor = Object.getOwnPropertyDescriptor(prototype, propName);
            }
            descriptor.enumerable = false;
            Object.defineProperty(prototype, propName, descriptor);
            Object.defineProperty(object, propName, descriptor);
          },
          hideAllPropertiesFromObject: function(object) {
            for (var propName in object) {
              ObjectHelper.hidePropertyFromObject(object, propName);
            }
          },
          addHiddenPropertyToObject: function(object, propName, prop) {
            var writable = arguments[3] !== (void 0) ? arguments[3] : true;
            var useAccessors = arguments[4] !== (void 0) ? arguments[4] : true;
            return ObjectHelper.addPropertyToObject(object, propName, prop, false, writable, undefined, useAccessors);
          },
          addPropertyToObject: function(object, propName, prop) {
            var enumerable = arguments[3] !== (void 0) ? arguments[3] : true;
            var writable = arguments[4] !== (void 0) ? arguments[4] : true;
            var setCallback = arguments[5] !== (void 0) ? arguments[5] : null;
            var useAccessors = arguments[6] !== (void 0) ? arguments[6] : true;
            if (!writable || !useAccessors) {
              var descriptor = {
                enumerable: enumerable,
                writable: writable,
                value: prop
              };
              Object.defineProperty(object, propName, descriptor);
            } else {
              ObjectHelper.addGetSetPropertyWithShadow(object, propName, prop, enumerable, writable, setCallback);
            }
          },
          addGetSetPropertyWithShadow: function(object, propName, prop) {
            var enumerable = arguments[3] !== (void 0) ? arguments[3] : true;
            var writable = arguments[4] !== (void 0) ? arguments[4] : true;
            var setCallback = arguments[5] !== (void 0) ? arguments[5] : null;
            ObjectHelper.buildPropertyShadow(object, propName, prop);
            ObjectHelper.buildGetSetProperty(object, propName, enumerable, writable, setCallback);
          },
          buildPropertyShadow: function(object, propName, prop) {
            var shadow = {};
            if (!object || !propName) {
              debugger;
            }
            try {
              if ('shadow' in object) {
                shadow = object['shadow'];
              }
            } catch (error) {
              debugger;
            }
            shadow[propName] = prop;
            Object.defineProperty(object, 'shadow', {
              writable: true,
              configurable: true,
              enumerable: false,
              value: shadow
            });
          },
          buildGetSetProperty: function(object, propName) {
            var enumerable = arguments[2] !== (void 0) ? arguments[2] : true;
            var writable = arguments[3] !== (void 0) ? arguments[3] : true;
            var setCallback = arguments[4] !== (void 0) ? arguments[4] : null;
            var descriptor = {
              enumerable: enumerable,
              configurable: true,
              get: function() {
                return object['shadow'][propName];
              },
              set: function(value) {
                if (writable) {
                  object['shadow'][propName] = value;
                  if (setCallback && typeof setCallback === 'function') {
                    setCallback({
                      propertyName: propName,
                      newValue: value
                    });
                  }
                } else {
                  throw new ReferenceError('Attempted to write to non-writable property "' + propName + '".');
                }
              }
            };
            Object.defineProperty(object, propName, descriptor);
          },
          bindAllMethods: function(object, bindTarget) {
            var methodNames = ObjectHelper.getMethodNames(object);
            methodNames.forEach(function(name) {
              object[name] = object[name].bind(bindTarget);
            });
          },
          getMethodNames: function(object) {
            var methodNames = arguments[1] !== (void 0) ? arguments[1] : [];
            var propNames = Object.getOwnPropertyNames(object).filter(function(c) {
              return typeof object[c] === 'function';
            });
            methodNames = methodNames.concat(propNames);
            var prototype = Object.getPrototypeOf(object);
            if (prototype.constructor.name !== 'Object' && prototype.constructor.name !== 'Array') {
              return ObjectHelper.getMethodNames(prototype, methodNames);
            }
            return methodNames;
          },
          getEnumerableProperties: function(object) {
            return ObjectHelper.getPrototypeEnumerableProperties(object, object);
          },
          getPrototypeEnumerableProperties: function(rootObject, prototype) {
            var result = {};
            var propNames = Object.keys(prototype);
            for (var $__1 = propNames.values()[$traceurRuntime.toProperty(Symbol.iterator)](),
                $__2 = void 0; !($__2 = $__1.next()).done; ) {
              var name = $__2.value;
              {
                var value = rootObject[name];
                if (value !== null && value !== undefined && typeof value !== 'function') {
                  if (typeof value == 'object') {
                    result[name] = ObjectHelper.getEnumerableProperties(value);
                  } else {
                    result[name] = value;
                  }
                }
              }
            }
            var descriptorNames = Object.getOwnPropertyNames(prototype);
            descriptorNames = descriptorNames.filter(function(name) {
              return propNames.indexOf(name) < 0;
            });
            for (var $__3 = descriptorNames.values()[$traceurRuntime.toProperty(Symbol.iterator)](),
                $__4 = void 0; !($__4 = $__3.next()).done; ) {
              var name$__5 = $__4.value;
              {
                var descriptor = Object.getOwnPropertyDescriptor(prototype, name$__5);
                if (descriptor && descriptor.enumerable) {
                  var value$__6 = rootObject[name$__5];
                  if (value$__6 !== null && value$__6 !== undefined && typeof value$__6 !== 'function') {
                    if (typeof value$__6 == 'object') {
                      result[name$__5] = ObjectHelper.getEnumerableProperties(value$__6);
                    } else {
                      result[name$__5] = value$__6;
                    }
                  }
                }
              }
            }
            var superPrototype = Object.getPrototypeOf(prototype);
            if (superPrototype.constructor.name !== 'Object' && superPrototype.constructor.name !== 'Array') {
              var prototypeEnumerables = ObjectHelper.getPrototypeEnumerableProperties(rootObject, superPrototype);
              _.merge(result, prototypeEnumerables);
            }
            return result;
          }
        });
      }()));
    }
  };
});



System.register("npm:process@0.10.1", ["npm:process@0.10.1/browser"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = require("npm:process@0.10.1/browser");
  global.define = __define;
  return module.exports;
});



System.register("npm:underscore@1.8.3", ["npm:underscore@1.8.3/underscore"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = require("npm:underscore@1.8.3/underscore");
  global.define = __define;
  return module.exports;
});



System.register("npm:ws@0.7.1", ["npm:ws@0.7.1/lib/browser"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = require("npm:ws@0.7.1/lib/browser");
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/SpecParser", ["npm:famous@0.3.5/core/Transform"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Transform = require("npm:famous@0.3.5/core/Transform");
  function SpecParser() {
    this.result = {};
  }
  SpecParser._instance = new SpecParser();
  SpecParser.parse = function parse(spec, context) {
    return SpecParser._instance.parse(spec, context);
  };
  SpecParser.prototype.parse = function parse(spec, context) {
    this.reset();
    this._parseSpec(spec, context, Transform.identity);
    return this.result;
  };
  SpecParser.prototype.reset = function reset() {
    this.result = {};
  };
  function _vecInContext(v, m) {
    return [v[0] * m[0] + v[1] * m[4] + v[2] * m[8], v[0] * m[1] + v[1] * m[5] + v[2] * m[9], v[0] * m[2] + v[1] * m[6] + v[2] * m[10]];
  }
  var _zeroZero = [0, 0];
  SpecParser.prototype._parseSpec = function _parseSpec(spec, parentContext, sizeContext) {
    var id;
    var target;
    var transform;
    var opacity;
    var origin;
    var align;
    var size;
    if (typeof spec === 'number') {
      id = spec;
      transform = parentContext.transform;
      align = parentContext.align || _zeroZero;
      if (parentContext.size && align && (align[0] || align[1])) {
        var alignAdjust = [align[0] * parentContext.size[0], align[1] * parentContext.size[1], 0];
        transform = Transform.thenMove(transform, _vecInContext(alignAdjust, sizeContext));
      }
      this.result[id] = {
        transform: transform,
        opacity: parentContext.opacity,
        origin: parentContext.origin || _zeroZero,
        align: parentContext.align || _zeroZero,
        size: parentContext.size
      };
    } else if (!spec) {
      return ;
    } else if (spec instanceof Array) {
      for (var i = 0; i < spec.length; i++) {
        this._parseSpec(spec[i], parentContext, sizeContext);
      }
    } else {
      target = spec.target;
      transform = parentContext.transform;
      opacity = parentContext.opacity;
      origin = parentContext.origin;
      align = parentContext.align;
      size = parentContext.size;
      var nextSizeContext = sizeContext;
      if (spec.opacity !== undefined)
        opacity = parentContext.opacity * spec.opacity;
      if (spec.transform)
        transform = Transform.multiply(parentContext.transform, spec.transform);
      if (spec.origin) {
        origin = spec.origin;
        nextSizeContext = parentContext.transform;
      }
      if (spec.align)
        align = spec.align;
      if (spec.size || spec.proportions) {
        var parentSize = size;
        size = [size[0], size[1]];
        if (spec.size) {
          if (spec.size[0] !== undefined)
            size[0] = spec.size[0];
          if (spec.size[1] !== undefined)
            size[1] = spec.size[1];
        }
        if (spec.proportions) {
          if (spec.proportions[0] !== undefined)
            size[0] = size[0] * spec.proportions[0];
          if (spec.proportions[1] !== undefined)
            size[1] = size[1] * spec.proportions[1];
        }
        if (parentSize) {
          if (align && (align[0] || align[1]))
            transform = Transform.thenMove(transform, _vecInContext([align[0] * parentSize[0], align[1] * parentSize[1], 0], sizeContext));
          if (origin && (origin[0] || origin[1]))
            transform = Transform.moveThen([-origin[0] * size[0], -origin[1] * size[1], 0], transform);
        }
        nextSizeContext = parentContext.transform;
        origin = null;
        align = null;
      }
      this._parseSpec(target, {
        transform: transform,
        opacity: opacity,
        origin: origin,
        align: align,
        size: size
      }, nextSizeContext);
    }
  };
  module.exports = SpecParser;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/EventHandler", ["npm:famous@0.3.5/core/EventEmitter"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var EventEmitter = require("npm:famous@0.3.5/core/EventEmitter");
  function EventHandler() {
    EventEmitter.apply(this, arguments);
    this.downstream = [];
    this.downstreamFn = [];
    this.upstream = [];
    this.upstreamListeners = {};
  }
  EventHandler.prototype = Object.create(EventEmitter.prototype);
  EventHandler.prototype.constructor = EventHandler;
  EventHandler.setInputHandler = function setInputHandler(object, handler) {
    object.trigger = handler.trigger.bind(handler);
    if (handler.subscribe && handler.unsubscribe) {
      object.subscribe = handler.subscribe.bind(handler);
      object.unsubscribe = handler.unsubscribe.bind(handler);
    }
  };
  EventHandler.setOutputHandler = function setOutputHandler(object, handler) {
    if (handler instanceof EventHandler)
      handler.bindThis(object);
    object.pipe = handler.pipe.bind(handler);
    object.unpipe = handler.unpipe.bind(handler);
    object.on = handler.on.bind(handler);
    object.addListener = object.on;
    object.removeListener = handler.removeListener.bind(handler);
  };
  EventHandler.prototype.emit = function emit(type, event) {
    EventEmitter.prototype.emit.apply(this, arguments);
    var i = 0;
    for (i = 0; i < this.downstream.length; i++) {
      if (this.downstream[i].trigger)
        this.downstream[i].trigger(type, event);
    }
    for (i = 0; i < this.downstreamFn.length; i++) {
      this.downstreamFn[i](type, event);
    }
    return this;
  };
  EventHandler.prototype.trigger = EventHandler.prototype.emit;
  EventHandler.prototype.pipe = function pipe(target) {
    if (target.subscribe instanceof Function)
      return target.subscribe(this);
    var downstreamCtx = target instanceof Function ? this.downstreamFn : this.downstream;
    var index = downstreamCtx.indexOf(target);
    if (index < 0)
      downstreamCtx.push(target);
    if (target instanceof Function)
      target('pipe', null);
    else if (target.trigger)
      target.trigger('pipe', null);
    return target;
  };
  EventHandler.prototype.unpipe = function unpipe(target) {
    if (target.unsubscribe instanceof Function)
      return target.unsubscribe(this);
    var downstreamCtx = target instanceof Function ? this.downstreamFn : this.downstream;
    var index = downstreamCtx.indexOf(target);
    if (index >= 0) {
      downstreamCtx.splice(index, 1);
      if (target instanceof Function)
        target('unpipe', null);
      else if (target.trigger)
        target.trigger('unpipe', null);
      return target;
    } else
      return false;
  };
  EventHandler.prototype.on = function on(type, handler) {
    EventEmitter.prototype.on.apply(this, arguments);
    if (!(type in this.upstreamListeners)) {
      var upstreamListener = this.trigger.bind(this, type);
      this.upstreamListeners[type] = upstreamListener;
      for (var i = 0; i < this.upstream.length; i++) {
        this.upstream[i].on(type, upstreamListener);
      }
    }
    return this;
  };
  EventHandler.prototype.addListener = EventHandler.prototype.on;
  EventHandler.prototype.subscribe = function subscribe(source) {
    var index = this.upstream.indexOf(source);
    if (index < 0) {
      this.upstream.push(source);
      for (var type in this.upstreamListeners) {
        source.on(type, this.upstreamListeners[type]);
      }
    }
    return this;
  };
  EventHandler.prototype.unsubscribe = function unsubscribe(source) {
    var index = this.upstream.indexOf(source);
    if (index >= 0) {
      this.upstream.splice(index, 1);
      for (var type in this.upstreamListeners) {
        source.removeListener(type, this.upstreamListeners[type]);
      }
    }
    return this;
  };
  module.exports = EventHandler;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/transitions/MultipleTransition", ["npm:famous@0.3.5/utilities/Utility"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Utility = require("npm:famous@0.3.5/utilities/Utility");
  function MultipleTransition(method) {
    this.method = method;
    this._instances = [];
    this.state = [];
  }
  MultipleTransition.SUPPORTS_MULTIPLE = true;
  MultipleTransition.prototype.get = function get() {
    for (var i = 0; i < this._instances.length; i++) {
      this.state[i] = this._instances[i].get();
    }
    return this.state;
  };
  MultipleTransition.prototype.set = function set(endState, transition, callback) {
    var _allCallback = Utility.after(endState.length, callback);
    for (var i = 0; i < endState.length; i++) {
      if (!this._instances[i])
        this._instances[i] = new this.method();
      this._instances[i].set(endState[i], transition, _allCallback);
    }
  };
  MultipleTransition.prototype.reset = function reset(startState) {
    for (var i = 0; i < startState.length; i++) {
      if (!this._instances[i])
        this._instances[i] = new this.method();
      this._instances[i].reset(startState[i]);
    }
  };
  module.exports = MultipleTransition;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/Surface", ["npm:famous@0.3.5/core/ElementOutput"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var ElementOutput = require("npm:famous@0.3.5/core/ElementOutput");
  function Surface(options) {
    ElementOutput.call(this);
    this.options = {};
    this.properties = {};
    this.attributes = {};
    this.content = '';
    this.classList = [];
    this.size = null;
    this._classesDirty = true;
    this._stylesDirty = true;
    this._attributesDirty = true;
    this._sizeDirty = true;
    this._contentDirty = true;
    this._trueSizeCheck = true;
    this._dirtyClasses = [];
    if (options)
      this.setOptions(options);
    this._currentTarget = null;
  }
  Surface.prototype = Object.create(ElementOutput.prototype);
  Surface.prototype.constructor = Surface;
  Surface.prototype.elementType = 'div';
  Surface.prototype.elementClass = 'famous-surface';
  Surface.prototype.setAttributes = function setAttributes(attributes) {
    for (var n in attributes) {
      if (n === 'style')
        throw new Error('Cannot set styles via "setAttributes" as it will break Famo.us.  Use "setProperties" instead.');
      this.attributes[n] = attributes[n];
    }
    this._attributesDirty = true;
  };
  Surface.prototype.getAttributes = function getAttributes() {
    return this.attributes;
  };
  Surface.prototype.setProperties = function setProperties(properties) {
    for (var n in properties) {
      this.properties[n] = properties[n];
    }
    this._stylesDirty = true;
    return this;
  };
  Surface.prototype.getProperties = function getProperties() {
    return this.properties;
  };
  Surface.prototype.addClass = function addClass(className) {
    if (this.classList.indexOf(className) < 0) {
      this.classList.push(className);
      this._classesDirty = true;
    }
    return this;
  };
  Surface.prototype.removeClass = function removeClass(className) {
    var i = this.classList.indexOf(className);
    if (i >= 0) {
      this._dirtyClasses.push(this.classList.splice(i, 1)[0]);
      this._classesDirty = true;
    }
    return this;
  };
  Surface.prototype.toggleClass = function toggleClass(className) {
    var i = this.classList.indexOf(className);
    if (i >= 0) {
      this.removeClass(className);
    } else {
      this.addClass(className);
    }
    return this;
  };
  Surface.prototype.setClasses = function setClasses(classList) {
    var i = 0;
    var removal = [];
    for (i = 0; i < this.classList.length; i++) {
      if (classList.indexOf(this.classList[i]) < 0)
        removal.push(this.classList[i]);
    }
    for (i = 0; i < removal.length; i++)
      this.removeClass(removal[i]);
    for (i = 0; i < classList.length; i++)
      this.addClass(classList[i]);
    return this;
  };
  Surface.prototype.getClassList = function getClassList() {
    return this.classList;
  };
  Surface.prototype.setContent = function setContent(content) {
    if (this.content !== content) {
      this.content = content;
      this._contentDirty = true;
    }
    return this;
  };
  Surface.prototype.getContent = function getContent() {
    return this.content;
  };
  Surface.prototype.setOptions = function setOptions(options) {
    if (options.size)
      this.setSize(options.size);
    if (options.classes)
      this.setClasses(options.classes);
    if (options.properties)
      this.setProperties(options.properties);
    if (options.attributes)
      this.setAttributes(options.attributes);
    if (options.content)
      this.setContent(options.content);
    return this;
  };
  function _cleanupClasses(target) {
    for (var i = 0; i < this._dirtyClasses.length; i++)
      target.classList.remove(this._dirtyClasses[i]);
    this._dirtyClasses = [];
  }
  function _applyStyles(target) {
    for (var n in this.properties) {
      target.style[n] = this.properties[n];
    }
  }
  function _cleanupStyles(target) {
    for (var n in this.properties) {
      target.style[n] = '';
    }
  }
  function _applyAttributes(target) {
    for (var n in this.attributes) {
      target.setAttribute(n, this.attributes[n]);
    }
  }
  function _cleanupAttributes(target) {
    for (var n in this.attributes) {
      target.removeAttribute(n);
    }
  }
  function _xyNotEquals(a, b) {
    return a && b ? a[0] !== b[0] || a[1] !== b[1] : a !== b;
  }
  Surface.prototype.setup = function setup(allocator) {
    var target = allocator.allocate(this.elementType);
    if (this.elementClass) {
      if (this.elementClass instanceof Array) {
        for (var i = 0; i < this.elementClass.length; i++) {
          target.classList.add(this.elementClass[i]);
        }
      } else {
        target.classList.add(this.elementClass);
      }
    }
    target.style.display = '';
    this.attach(target);
    this._opacity = null;
    this._currentTarget = target;
    this._stylesDirty = true;
    this._classesDirty = true;
    this._attributesDirty = true;
    this._sizeDirty = true;
    this._contentDirty = true;
    this._originDirty = true;
    this._transformDirty = true;
  };
  Surface.prototype.commit = function commit(context) {
    if (!this._currentTarget)
      this.setup(context.allocator);
    var target = this._currentTarget;
    var size = context.size;
    if (this._classesDirty) {
      _cleanupClasses.call(this, target);
      var classList = this.getClassList();
      for (var i = 0; i < classList.length; i++)
        target.classList.add(classList[i]);
      this._classesDirty = false;
      this._trueSizeCheck = true;
    }
    if (this._stylesDirty) {
      _applyStyles.call(this, target);
      this._stylesDirty = false;
      this._trueSizeCheck = true;
    }
    if (this._attributesDirty) {
      _applyAttributes.call(this, target);
      this._attributesDirty = false;
      this._trueSizeCheck = true;
    }
    if (this.size) {
      var origSize = context.size;
      size = [this.size[0], this.size[1]];
      if (size[0] === undefined)
        size[0] = origSize[0];
      if (size[1] === undefined)
        size[1] = origSize[1];
      if (size[0] === true || size[1] === true) {
        if (size[0] === true) {
          if (this._trueSizeCheck || this._size[0] === 0) {
            var width = target.offsetWidth;
            if (this._size && this._size[0] !== width) {
              this._size[0] = width;
              this._sizeDirty = true;
            }
            size[0] = width;
          } else {
            if (this._size)
              size[0] = this._size[0];
          }
        }
        if (size[1] === true) {
          if (this._trueSizeCheck || this._size[1] === 0) {
            var height = target.offsetHeight;
            if (this._size && this._size[1] !== height) {
              this._size[1] = height;
              this._sizeDirty = true;
            }
            size[1] = height;
          } else {
            if (this._size)
              size[1] = this._size[1];
          }
        }
        this._trueSizeCheck = false;
      }
    }
    if (_xyNotEquals(this._size, size)) {
      if (!this._size)
        this._size = [0, 0];
      this._size[0] = size[0];
      this._size[1] = size[1];
      this._sizeDirty = true;
    }
    if (this._sizeDirty) {
      if (this._size) {
        target.style.width = this.size && this.size[0] === true ? '' : this._size[0] + 'px';
        target.style.height = this.size && this.size[1] === true ? '' : this._size[1] + 'px';
      }
      this._eventOutput.emit('resize');
    }
    if (this._contentDirty) {
      this.deploy(target);
      this._eventOutput.emit('deploy');
      this._contentDirty = false;
      this._trueSizeCheck = true;
    }
    ElementOutput.prototype.commit.call(this, context);
  };
  Surface.prototype.cleanup = function cleanup(allocator) {
    var i = 0;
    var target = this._currentTarget;
    this._eventOutput.emit('recall');
    this.recall(target);
    target.style.display = 'none';
    target.style.opacity = '';
    target.style.width = '';
    target.style.height = '';
    _cleanupStyles.call(this, target);
    _cleanupAttributes.call(this, target);
    var classList = this.getClassList();
    _cleanupClasses.call(this, target);
    for (i = 0; i < classList.length; i++)
      target.classList.remove(classList[i]);
    if (this.elementClass) {
      if (this.elementClass instanceof Array) {
        for (i = 0; i < this.elementClass.length; i++) {
          target.classList.remove(this.elementClass[i]);
        }
      } else {
        target.classList.remove(this.elementClass);
      }
    }
    this.detach(target);
    this._currentTarget = null;
    allocator.deallocate(target);
  };
  Surface.prototype.deploy = function deploy(target) {
    var content = this.getContent();
    if (content instanceof Node) {
      while (target.hasChildNodes())
        target.removeChild(target.firstChild);
      target.appendChild(content);
    } else
      target.innerHTML = content;
  };
  Surface.prototype.recall = function recall(target) {
    var df = document.createDocumentFragment();
    while (target.hasChildNodes())
      df.appendChild(target.firstChild);
    this.setContent(df);
  };
  Surface.prototype.getSize = function getSize() {
    return this._size ? this._size : this.size;
  };
  Surface.prototype.setSize = function setSize(size) {
    this.size = size ? [size[0], size[1]] : null;
    this._sizeDirty = true;
    return this;
  };
  module.exports = Surface;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/Modifier", ["npm:famous@0.3.5/core/Transform", "npm:famous@0.3.5/transitions/Transitionable", "npm:famous@0.3.5/transitions/TransitionableTransform"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Transform = require("npm:famous@0.3.5/core/Transform");
  var Transitionable = require("npm:famous@0.3.5/transitions/Transitionable");
  var TransitionableTransform = require("npm:famous@0.3.5/transitions/TransitionableTransform");
  function Modifier(options) {
    this._transformGetter = null;
    this._opacityGetter = null;
    this._originGetter = null;
    this._alignGetter = null;
    this._sizeGetter = null;
    this._proportionGetter = null;
    this._legacyStates = {};
    this._output = {
      transform: Transform.identity,
      opacity: 1,
      origin: null,
      align: null,
      size: null,
      proportions: null,
      target: null
    };
    if (options) {
      if (options.transform)
        this.transformFrom(options.transform);
      if (options.opacity !== undefined)
        this.opacityFrom(options.opacity);
      if (options.origin)
        this.originFrom(options.origin);
      if (options.align)
        this.alignFrom(options.align);
      if (options.size)
        this.sizeFrom(options.size);
      if (options.proportions)
        this.proportionsFrom(options.proportions);
    }
  }
  Modifier.prototype.transformFrom = function transformFrom(transform) {
    if (transform instanceof Function)
      this._transformGetter = transform;
    else if (transform instanceof Object && transform.get)
      this._transformGetter = transform.get.bind(transform);
    else {
      this._transformGetter = null;
      this._output.transform = transform;
    }
    return this;
  };
  Modifier.prototype.opacityFrom = function opacityFrom(opacity) {
    if (opacity instanceof Function)
      this._opacityGetter = opacity;
    else if (opacity instanceof Object && opacity.get)
      this._opacityGetter = opacity.get.bind(opacity);
    else {
      this._opacityGetter = null;
      this._output.opacity = opacity;
    }
    return this;
  };
  Modifier.prototype.originFrom = function originFrom(origin) {
    if (origin instanceof Function)
      this._originGetter = origin;
    else if (origin instanceof Object && origin.get)
      this._originGetter = origin.get.bind(origin);
    else {
      this._originGetter = null;
      this._output.origin = origin;
    }
    return this;
  };
  Modifier.prototype.alignFrom = function alignFrom(align) {
    if (align instanceof Function)
      this._alignGetter = align;
    else if (align instanceof Object && align.get)
      this._alignGetter = align.get.bind(align);
    else {
      this._alignGetter = null;
      this._output.align = align;
    }
    return this;
  };
  Modifier.prototype.sizeFrom = function sizeFrom(size) {
    if (size instanceof Function)
      this._sizeGetter = size;
    else if (size instanceof Object && size.get)
      this._sizeGetter = size.get.bind(size);
    else {
      this._sizeGetter = null;
      this._output.size = size;
    }
    return this;
  };
  Modifier.prototype.proportionsFrom = function proportionsFrom(proportions) {
    if (proportions instanceof Function)
      this._proportionGetter = proportions;
    else if (proportions instanceof Object && proportions.get)
      this._proportionGetter = proportions.get.bind(proportions);
    else {
      this._proportionGetter = null;
      this._output.proportions = proportions;
    }
    return this;
  };
  Modifier.prototype.setTransform = function setTransform(transform, transition, callback) {
    if (transition || this._legacyStates.transform) {
      if (!this._legacyStates.transform) {
        this._legacyStates.transform = new TransitionableTransform(this._output.transform);
      }
      if (!this._transformGetter)
        this.transformFrom(this._legacyStates.transform);
      this._legacyStates.transform.set(transform, transition, callback);
      return this;
    } else
      return this.transformFrom(transform);
  };
  Modifier.prototype.setOpacity = function setOpacity(opacity, transition, callback) {
    if (transition || this._legacyStates.opacity) {
      if (!this._legacyStates.opacity) {
        this._legacyStates.opacity = new Transitionable(this._output.opacity);
      }
      if (!this._opacityGetter)
        this.opacityFrom(this._legacyStates.opacity);
      return this._legacyStates.opacity.set(opacity, transition, callback);
    } else
      return this.opacityFrom(opacity);
  };
  Modifier.prototype.setOrigin = function setOrigin(origin, transition, callback) {
    if (transition || this._legacyStates.origin) {
      if (!this._legacyStates.origin) {
        this._legacyStates.origin = new Transitionable(this._output.origin || [0, 0]);
      }
      if (!this._originGetter)
        this.originFrom(this._legacyStates.origin);
      this._legacyStates.origin.set(origin, transition, callback);
      return this;
    } else
      return this.originFrom(origin);
  };
  Modifier.prototype.setAlign = function setAlign(align, transition, callback) {
    if (transition || this._legacyStates.align) {
      if (!this._legacyStates.align) {
        this._legacyStates.align = new Transitionable(this._output.align || [0, 0]);
      }
      if (!this._alignGetter)
        this.alignFrom(this._legacyStates.align);
      this._legacyStates.align.set(align, transition, callback);
      return this;
    } else
      return this.alignFrom(align);
  };
  Modifier.prototype.setSize = function setSize(size, transition, callback) {
    if (size && (transition || this._legacyStates.size)) {
      if (!this._legacyStates.size) {
        this._legacyStates.size = new Transitionable(this._output.size || [0, 0]);
      }
      if (!this._sizeGetter)
        this.sizeFrom(this._legacyStates.size);
      this._legacyStates.size.set(size, transition, callback);
      return this;
    } else
      return this.sizeFrom(size);
  };
  Modifier.prototype.setProportions = function setProportions(proportions, transition, callback) {
    if (proportions && (transition || this._legacyStates.proportions)) {
      if (!this._legacyStates.proportions) {
        this._legacyStates.proportions = new Transitionable(this._output.proportions || [0, 0]);
      }
      if (!this._proportionGetter)
        this.proportionsFrom(this._legacyStates.proportions);
      this._legacyStates.proportions.set(proportions, transition, callback);
      return this;
    } else
      return this.proportionsFrom(proportions);
  };
  Modifier.prototype.halt = function halt() {
    if (this._legacyStates.transform)
      this._legacyStates.transform.halt();
    if (this._legacyStates.opacity)
      this._legacyStates.opacity.halt();
    if (this._legacyStates.origin)
      this._legacyStates.origin.halt();
    if (this._legacyStates.align)
      this._legacyStates.align.halt();
    if (this._legacyStates.size)
      this._legacyStates.size.halt();
    if (this._legacyStates.proportions)
      this._legacyStates.proportions.halt();
    this._transformGetter = null;
    this._opacityGetter = null;
    this._originGetter = null;
    this._alignGetter = null;
    this._sizeGetter = null;
    this._proportionGetter = null;
  };
  Modifier.prototype.getTransform = function getTransform() {
    return this._transformGetter();
  };
  Modifier.prototype.getFinalTransform = function getFinalTransform() {
    return this._legacyStates.transform ? this._legacyStates.transform.getFinal() : this._output.transform;
  };
  Modifier.prototype.getOpacity = function getOpacity() {
    return this._opacityGetter();
  };
  Modifier.prototype.getOrigin = function getOrigin() {
    return this._originGetter();
  };
  Modifier.prototype.getAlign = function getAlign() {
    return this._alignGetter();
  };
  Modifier.prototype.getSize = function getSize() {
    return this._sizeGetter ? this._sizeGetter() : this._output.size;
  };
  Modifier.prototype.getProportions = function getProportions() {
    return this._proportionGetter ? this._proportionGetter() : this._output.proportions;
  };
  function _update() {
    if (this._transformGetter)
      this._output.transform = this._transformGetter();
    if (this._opacityGetter)
      this._output.opacity = this._opacityGetter();
    if (this._originGetter)
      this._output.origin = this._originGetter();
    if (this._alignGetter)
      this._output.align = this._alignGetter();
    if (this._sizeGetter)
      this._output.size = this._sizeGetter();
    if (this._proportionGetter)
      this._output.proportions = this._proportionGetter();
  }
  Modifier.prototype.modify = function modify(target) {
    _update.call(this);
    this._output.target = target;
    return this._output;
  };
  module.exports = Modifier;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/physics/bodies/Particle", ["npm:famous@0.3.5/math/Vector", "npm:famous@0.3.5/core/Transform", "npm:famous@0.3.5/core/EventHandler", "npm:famous@0.3.5/physics/integrators/SymplecticEuler"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Vector = require("npm:famous@0.3.5/math/Vector");
  var Transform = require("npm:famous@0.3.5/core/Transform");
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  var Integrator = require("npm:famous@0.3.5/physics/integrators/SymplecticEuler");
  function Particle(options) {
    options = options || {};
    var defaults = Particle.DEFAULT_OPTIONS;
    this.position = new Vector();
    this.velocity = new Vector();
    this.force = new Vector();
    this._engine = null;
    this._isSleeping = true;
    this._eventOutput = null;
    this.mass = options.mass !== undefined ? options.mass : defaults.mass;
    this.inverseMass = 1 / this.mass;
    this.setPosition(options.position || defaults.position);
    this.setVelocity(options.velocity || defaults.velocity);
    this.force.set(options.force || [0, 0, 0]);
    this.transform = Transform.identity.slice();
    this._spec = {
      size: [true, true],
      target: {
        transform: this.transform,
        origin: [0.5, 0.5],
        target: null
      }
    };
  }
  Particle.DEFAULT_OPTIONS = {
    position: [0, 0, 0],
    velocity: [0, 0, 0],
    mass: 1
  };
  var _events = {
    start: 'start',
    update: 'update',
    end: 'end'
  };
  var now = Date.now;
  Particle.prototype.isBody = false;
  Particle.prototype.isActive = function isActive() {
    return !this._isSleeping;
  };
  Particle.prototype.sleep = function sleep() {
    if (this._isSleeping)
      return ;
    this.emit(_events.end, this);
    this._isSleeping = true;
  };
  Particle.prototype.wake = function wake() {
    if (!this._isSleeping)
      return ;
    this.emit(_events.start, this);
    this._isSleeping = false;
    this._prevTime = now();
    if (this._engine)
      this._engine.wake();
  };
  Particle.prototype.setPosition = function setPosition(position) {
    this.position.set(position);
  };
  Particle.prototype.setPosition1D = function setPosition1D(x) {
    this.position.x = x;
  };
  Particle.prototype.getPosition = function getPosition() {
    this._engine.step();
    return this.position.get();
  };
  Particle.prototype.getPosition1D = function getPosition1D() {
    this._engine.step();
    return this.position.x;
  };
  Particle.prototype.setVelocity = function setVelocity(velocity) {
    this.velocity.set(velocity);
    if (!(velocity[0] === 0 && velocity[1] === 0 && velocity[2] === 0))
      this.wake();
  };
  Particle.prototype.setVelocity1D = function setVelocity1D(x) {
    this.velocity.x = x;
    if (x !== 0)
      this.wake();
  };
  Particle.prototype.getVelocity = function getVelocity() {
    return this.velocity.get();
  };
  Particle.prototype.setForce = function setForce(force) {
    this.force.set(force);
    this.wake();
  };
  Particle.prototype.getVelocity1D = function getVelocity1D() {
    return this.velocity.x;
  };
  Particle.prototype.setMass = function setMass(mass) {
    this.mass = mass;
    this.inverseMass = 1 / mass;
  };
  Particle.prototype.getMass = function getMass() {
    return this.mass;
  };
  Particle.prototype.reset = function reset(position, velocity) {
    this.setPosition(position || [0, 0, 0]);
    this.setVelocity(velocity || [0, 0, 0]);
  };
  Particle.prototype.applyForce = function applyForce(force) {
    if (force.isZero())
      return ;
    this.force.add(force).put(this.force);
    this.wake();
  };
  Particle.prototype.applyImpulse = function applyImpulse(impulse) {
    if (impulse.isZero())
      return ;
    var velocity = this.velocity;
    velocity.add(impulse.mult(this.inverseMass)).put(velocity);
  };
  Particle.prototype.integrateVelocity = function integrateVelocity(dt) {
    Integrator.integrateVelocity(this, dt);
  };
  Particle.prototype.integratePosition = function integratePosition(dt) {
    Integrator.integratePosition(this, dt);
  };
  Particle.prototype._integrate = function _integrate(dt) {
    this.integrateVelocity(dt);
    this.integratePosition(dt);
  };
  Particle.prototype.getEnergy = function getEnergy() {
    return 0.5 * this.mass * this.velocity.normSquared();
  };
  Particle.prototype.getTransform = function getTransform() {
    this._engine.step();
    var position = this.position;
    var transform = this.transform;
    transform[12] = position.x;
    transform[13] = position.y;
    transform[14] = position.z;
    return transform;
  };
  Particle.prototype.modify = function modify(target) {
    var _spec = this._spec.target;
    _spec.transform = this.getTransform();
    _spec.target = target;
    return this._spec;
  };
  function _createEventOutput() {
    this._eventOutput = new EventHandler();
    this._eventOutput.bindThis(this);
    EventHandler.setOutputHandler(this, this._eventOutput);
  }
  Particle.prototype.emit = function emit(type, data) {
    if (!this._eventOutput)
      return ;
    this._eventOutput.emit(type, data);
  };
  Particle.prototype.on = function on() {
    _createEventOutput.call(this);
    return this.on.apply(this, arguments);
  };
  Particle.prototype.removeListener = function removeListener() {
    _createEventOutput.call(this);
    return this.removeListener.apply(this, arguments);
  };
  Particle.prototype.pipe = function pipe() {
    _createEventOutput.call(this);
    return this.pipe.apply(this, arguments);
  };
  Particle.prototype.unpipe = function unpipe() {
    _createEventOutput.call(this);
    return this.unpipe.apply(this, arguments);
  };
  module.exports = Particle;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/physics/forces/Drag", ["npm:famous@0.3.5/physics/forces/Force"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Force = require("npm:famous@0.3.5/physics/forces/Force");
  function Drag(options) {
    this.options = Object.create(this.constructor.DEFAULT_OPTIONS);
    if (options)
      this.setOptions(options);
    Force.call(this);
  }
  Drag.prototype = Object.create(Force.prototype);
  Drag.prototype.constructor = Drag;
  Drag.FORCE_FUNCTIONS = {
    LINEAR: function(velocity) {
      return velocity;
    },
    QUADRATIC: function(velocity) {
      return velocity.mult(velocity.norm());
    }
  };
  Drag.DEFAULT_OPTIONS = {
    strength: 0.01,
    forceFunction: Drag.FORCE_FUNCTIONS.LINEAR
  };
  Drag.prototype.applyForce = function applyForce(targets) {
    var strength = this.options.strength;
    var forceFunction = this.options.forceFunction;
    var force = this.force;
    var index;
    var particle;
    for (index = 0; index < targets.length; index++) {
      particle = targets[index];
      forceFunction(particle.velocity).mult(-strength).put(force);
      particle.applyForce(force);
    }
  };
  Drag.prototype.setOptions = function setOptions(options) {
    for (var key in options)
      this.options[key] = options[key];
  };
  module.exports = Drag;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/views/Scroller", ["npm:famous@0.3.5/core/Entity", "npm:famous@0.3.5/core/Group", "npm:famous@0.3.5/core/OptionsManager", "npm:famous@0.3.5/core/Transform", "npm:famous@0.3.5/utilities/Utility", "npm:famous@0.3.5/core/ViewSequence", "npm:famous@0.3.5/core/EventHandler"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Entity = require("npm:famous@0.3.5/core/Entity");
  var Group = require("npm:famous@0.3.5/core/Group");
  var OptionsManager = require("npm:famous@0.3.5/core/OptionsManager");
  var Transform = require("npm:famous@0.3.5/core/Transform");
  var Utility = require("npm:famous@0.3.5/utilities/Utility");
  var ViewSequence = require("npm:famous@0.3.5/core/ViewSequence");
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  function Scroller(options) {
    this.options = Object.create(this.constructor.DEFAULT_OPTIONS);
    this._optionsManager = new OptionsManager(this.options);
    if (options)
      this._optionsManager.setOptions(options);
    this._node = null;
    this._position = 0;
    this._positionOffset = 0;
    this._positionGetter = null;
    this._outputFunction = null;
    this._masterOutputFunction = null;
    this.outputFrom();
    this._onEdge = 0;
    this.group = new Group();
    this.group.add({render: _innerRender.bind(this)});
    this._entityId = Entity.register(this);
    this._size = [undefined, undefined];
    this._contextSize = [undefined, undefined];
    this._eventInput = new EventHandler();
    this._eventOutput = new EventHandler();
    EventHandler.setInputHandler(this, this._eventInput);
    EventHandler.setOutputHandler(this, this._eventOutput);
  }
  Scroller.DEFAULT_OPTIONS = {
    direction: Utility.Direction.Y,
    margin: 0,
    clipSize: undefined,
    groupScroll: false
  };
  var EDGE_TOLERANCE = 0;
  function _sizeForDir(size) {
    if (!size)
      size = this._contextSize;
    var dimension = this.options.direction;
    return size[dimension] === undefined ? this._contextSize[dimension] : size[dimension];
  }
  function _output(node, offset, target) {
    var size = node.getSize ? node.getSize() : this._contextSize;
    var transform = this._outputFunction(offset);
    target.push({
      transform: transform,
      target: node.render()
    });
    return _sizeForDir.call(this, size);
  }
  function _getClipSize() {
    if (this.options.clipSize !== undefined)
      return this.options.clipSize;
    if (this._contextSize[this.options.direction] > this.getCumulativeSize()[this.options.direction]) {
      return _sizeForDir.call(this, this.getCumulativeSize());
    } else {
      return _sizeForDir.call(this, this._contextSize);
    }
  }
  Scroller.prototype.getCumulativeSize = function(index) {
    if (index === undefined)
      index = this._node._.cumulativeSizes.length - 1;
    return this._node._.getSize(index);
  };
  Scroller.prototype.setOptions = function setOptions(options) {
    if (options.groupScroll !== this.options.groupScroll) {
      if (options.groupScroll)
        this.group.pipe(this._eventOutput);
      else
        this.group.unpipe(this._eventOutput);
    }
    this._optionsManager.setOptions(options);
  };
  Scroller.prototype.onEdge = function onEdge() {
    return this._onEdge;
  };
  Scroller.prototype.outputFrom = function outputFrom(fn, masterFn) {
    if (!fn) {
      fn = function(offset) {
        return this.options.direction === Utility.Direction.X ? Transform.translate(offset, 0) : Transform.translate(0, offset);
      }.bind(this);
      if (!masterFn)
        masterFn = fn;
    }
    this._outputFunction = fn;
    this._masterOutputFunction = masterFn ? masterFn : function(offset) {
      return Transform.inverse(fn(-offset));
    };
  };
  Scroller.prototype.positionFrom = function positionFrom(position) {
    if (position instanceof Function)
      this._positionGetter = position;
    else if (position && position.get)
      this._positionGetter = position.get.bind(position);
    else {
      this._positionGetter = null;
      this._position = position;
    }
    if (this._positionGetter)
      this._position = this._positionGetter.call(this);
  };
  Scroller.prototype.sequenceFrom = function sequenceFrom(node) {
    if (node instanceof Array)
      node = new ViewSequence({array: node});
    this._node = node;
    this._positionOffset = 0;
  };
  Scroller.prototype.getSize = function getSize(actual) {
    return actual ? this._contextSize : this._size;
  };
  Scroller.prototype.render = function render() {
    if (!this._node)
      return null;
    if (this._positionGetter)
      this._position = this._positionGetter.call(this);
    return this._entityId;
  };
  Scroller.prototype.commit = function commit(context) {
    var transform = context.transform;
    var opacity = context.opacity;
    var origin = context.origin;
    var size = context.size;
    if (!this.options.clipSize && (size[0] !== this._contextSize[0] || size[1] !== this._contextSize[1])) {
      this._onEdge = 0;
      this._contextSize[0] = size[0];
      this._contextSize[1] = size[1];
      if (this.options.direction === Utility.Direction.X) {
        this._size[0] = _getClipSize.call(this);
        this._size[1] = undefined;
      } else {
        this._size[0] = undefined;
        this._size[1] = _getClipSize.call(this);
      }
    }
    var scrollTransform = this._masterOutputFunction(-this._position);
    return {
      transform: Transform.multiply(transform, scrollTransform),
      size: size,
      opacity: opacity,
      origin: origin,
      target: this.group.render()
    };
  };
  function _innerRender() {
    var size = null;
    var position = this._position;
    var result = [];
    var offset = -this._positionOffset;
    var clipSize = _getClipSize.call(this);
    var currNode = this._node;
    while (currNode && offset - position < clipSize + this.options.margin) {
      offset += _output.call(this, currNode, offset, result);
      currNode = currNode.getNext ? currNode.getNext() : null;
    }
    var sizeNode = this._node;
    var nodesSize = _sizeForDir.call(this, sizeNode.getSize());
    if (offset < clipSize) {
      while (sizeNode && nodesSize < clipSize) {
        sizeNode = sizeNode.getPrevious();
        if (sizeNode)
          nodesSize += _sizeForDir.call(this, sizeNode.getSize());
      }
      sizeNode = this._node;
      while (sizeNode && nodesSize < clipSize) {
        sizeNode = sizeNode.getNext();
        if (sizeNode)
          nodesSize += _sizeForDir.call(this, sizeNode.getSize());
      }
    }
    if (!currNode && offset - position < clipSize - EDGE_TOLERANCE) {
      if (this._onEdge !== 1) {
        this._onEdge = 1;
        this._eventOutput.emit('onEdge', {position: offset - clipSize});
      }
    } else if (!this._node.getPrevious() && position < -EDGE_TOLERANCE) {
      if (this._onEdge !== -1) {
        this._onEdge = -1;
        this._eventOutput.emit('onEdge', {position: 0});
      }
    } else {
      if (this._onEdge !== 0) {
        this._onEdge = 0;
        this._eventOutput.emit('offEdge');
      }
    }
    currNode = this._node && this._node.getPrevious ? this._node.getPrevious() : null;
    offset = -this._positionOffset;
    if (currNode) {
      size = currNode.getSize ? currNode.getSize() : this._contextSize;
      offset -= _sizeForDir.call(this, size);
    }
    while (currNode && offset - position > -(clipSize + this.options.margin)) {
      _output.call(this, currNode, offset, result);
      currNode = currNode.getPrevious ? currNode.getPrevious() : null;
      if (currNode) {
        size = currNode.getSize ? currNode.getSize() : this._contextSize;
        offset -= _sizeForDir.call(this, size);
      }
    }
    return result;
  }
  module.exports = Scroller;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/inputs/TouchSync", ["npm:famous@0.3.5/inputs/TouchTracker", "npm:famous@0.3.5/core/EventHandler", "npm:famous@0.3.5/core/OptionsManager"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var TouchTracker = require("npm:famous@0.3.5/inputs/TouchTracker");
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  var OptionsManager = require("npm:famous@0.3.5/core/OptionsManager");
  function TouchSync(options) {
    this.options = Object.create(TouchSync.DEFAULT_OPTIONS);
    this._optionsManager = new OptionsManager(this.options);
    if (options)
      this.setOptions(options);
    this._eventOutput = new EventHandler();
    this._touchTracker = new TouchTracker({touchLimit: this.options.touchLimit});
    EventHandler.setOutputHandler(this, this._eventOutput);
    EventHandler.setInputHandler(this, this._touchTracker);
    this._touchTracker.on('trackstart', _handleStart.bind(this));
    this._touchTracker.on('trackmove', _handleMove.bind(this));
    this._touchTracker.on('trackend', _handleEnd.bind(this));
    this._payload = {
      delta: null,
      position: null,
      velocity: null,
      clientX: undefined,
      clientY: undefined,
      count: 0,
      touch: undefined
    };
    this._position = null;
  }
  TouchSync.DEFAULT_OPTIONS = {
    direction: undefined,
    rails: false,
    touchLimit: 1,
    velocitySampleLength: 10,
    scale: 1
  };
  TouchSync.DIRECTION_X = 0;
  TouchSync.DIRECTION_Y = 1;
  var MINIMUM_TICK_TIME = 8;
  function _handleStart(data) {
    var velocity;
    var delta;
    if (this.options.direction !== undefined) {
      this._position = 0;
      velocity = 0;
      delta = 0;
    } else {
      this._position = [0, 0];
      velocity = [0, 0];
      delta = [0, 0];
    }
    var payload = this._payload;
    payload.delta = delta;
    payload.position = this._position;
    payload.velocity = velocity;
    payload.clientX = data.x;
    payload.clientY = data.y;
    payload.count = data.count;
    payload.touch = data.identifier;
    this._eventOutput.emit('start', payload);
  }
  function _handleMove(data) {
    var history = data.history;
    var currHistory = history[history.length - 1];
    var prevHistory = history[history.length - 2];
    var distantHistory = history[history.length - this.options.velocitySampleLength] ? history[history.length - this.options.velocitySampleLength] : history[history.length - 2];
    var distantTime = distantHistory.timestamp;
    var currTime = currHistory.timestamp;
    var diffX = currHistory.x - prevHistory.x;
    var diffY = currHistory.y - prevHistory.y;
    var velDiffX = currHistory.x - distantHistory.x;
    var velDiffY = currHistory.y - distantHistory.y;
    if (this.options.rails) {
      if (Math.abs(diffX) > Math.abs(diffY))
        diffY = 0;
      else
        diffX = 0;
      if (Math.abs(velDiffX) > Math.abs(velDiffY))
        velDiffY = 0;
      else
        velDiffX = 0;
    }
    var diffTime = Math.max(currTime - distantTime, MINIMUM_TICK_TIME);
    var velX = velDiffX / diffTime;
    var velY = velDiffY / diffTime;
    var scale = this.options.scale;
    var nextVel;
    var nextDelta;
    if (this.options.direction === TouchSync.DIRECTION_X) {
      nextDelta = scale * diffX;
      nextVel = scale * velX;
      this._position += nextDelta;
    } else if (this.options.direction === TouchSync.DIRECTION_Y) {
      nextDelta = scale * diffY;
      nextVel = scale * velY;
      this._position += nextDelta;
    } else {
      nextDelta = [scale * diffX, scale * diffY];
      nextVel = [scale * velX, scale * velY];
      this._position[0] += nextDelta[0];
      this._position[1] += nextDelta[1];
    }
    var payload = this._payload;
    payload.delta = nextDelta;
    payload.velocity = nextVel;
    payload.position = this._position;
    payload.clientX = data.x;
    payload.clientY = data.y;
    payload.count = data.count;
    payload.touch = data.identifier;
    this._eventOutput.emit('update', payload);
  }
  function _handleEnd(data) {
    this._payload.count = data.count;
    this._eventOutput.emit('end', this._payload);
  }
  TouchSync.prototype.setOptions = function setOptions(options) {
    return this._optionsManager.setOptions(options);
  };
  TouchSync.prototype.getOptions = function getOptions() {
    return this.options;
  };
  module.exports = TouchSync;
  global.define = __define;
  return module.exports;
});



System.register("npm:lodash@3.7.0", ["npm:lodash@3.7.0/index"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = require("npm:lodash@3.7.0/index");
  global.define = __define;
  return module.exports;
});



System.register("src/components/demo/cubePlaneSurface", ["npm:lodash@3.7.0", "src/components/autoSurface", "npm:famous@0.3.5/core/Modifier", "npm:famous@0.3.5/core/Transform", "npm:famous@0.3.5/modifiers/ModifierChain"], function($__export) {
  "use strict";
  var __moduleName = "src/components/demo/cubePlaneSurface";
  var _,
      AutoSurface,
      Modifier,
      Transform,
      ModifierChain;
  return {
    setters: [function($__m) {
      _ = $__m.default;
    }, function($__m) {
      AutoSurface = $__m.default;
    }, function($__m) {
      Modifier = $__m.default;
    }, function($__m) {
      Transform = $__m.default;
    }, function($__m) {
      ModifierChain = $__m.default;
    }],
    execute: function() {
      $__export('default', (function($__super) {
        var CubePlaneSurface = function CubePlaneSurface(context, options, modifierOptions) {
          var $__1,
              $__2;
          var orientation = arguments[3] !== (void 0) ? arguments[3] : 'top';
          var rotation = [];
          var translation = [];
          var colour;
          var size = _.extend(this.defaultOptions, options).size;
          var halfSizeX = size[0] / 2;
          var halfSizeY = size[1] / 2;
          switch (orientation) {
            case 'top':
              rotation = [90 * (Math.PI / 180), 0, 0];
              translation = [0, -halfSizeY, 0];
              colour = 'red';
              break;
            case 'bottom':
              rotation = [90 * (Math.PI / 180), 0, 0];
              translation = [0, halfSizeY, 0];
              colour = 'green';
              break;
            case 'right':
              rotation = [0, 90 * (Math.PI / 180), 0];
              translation = [halfSizeX, 0, 0];
              colour = 'blue';
              break;
            case 'left':
              rotation = [0, 90 * (Math.PI / 180), 0];
              translation = [-halfSizeX, 0, 0];
              colour = 'yellow';
              break;
            case 'front':
              rotation = [0, 0, 0];
              translation = [0, 0, halfSizeX];
              colour = 'orange';
              break;
            case 'back':
              rotation = [0, 0, 0];
              translation = [0, 0, -halfSizeX];
              colour = 'cyan';
              break;
          }
          var ourSurfaceOptions = {properties: {backgroundColor: colour}};
          var ourModifierOptions = {};
          this._modifierChain = new ModifierChain();
          this._modifierChain.addModifier(new Modifier({transform: Transform.multiply4x4(($__1 = Transform).translate.apply($__1, $traceurRuntime.spread(translation)), ($__2 = Transform).rotate.apply($__2, $traceurRuntime.spread(rotation)))}));
          $traceurRuntime.superConstructor(CubePlaneSurface).call(this, context, _.merge(ourSurfaceOptions, options), _.extend(ourModifierOptions, modifierOptions));
        };
        return ($traceurRuntime.createClass)(CubePlaneSurface, {get defaultOptions() {
            return {
              size: [100, 100],
              properties: {'backface-visibility': 'visible'}
            };
          }}, {}, $__super);
      }(AutoSurface)));
    }
  };
});



System.register("github:jspm/nodelibs-process@0.1.1/index", ["npm:process@0.10.1"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = System._nodeRequire ? process : require("npm:process@0.10.1");
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/RenderNode", ["npm:famous@0.3.5/core/Entity", "npm:famous@0.3.5/core/SpecParser"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Entity = require("npm:famous@0.3.5/core/Entity");
  var SpecParser = require("npm:famous@0.3.5/core/SpecParser");
  function RenderNode(object) {
    this._object = null;
    this._child = null;
    this._hasMultipleChildren = false;
    this._isRenderable = false;
    this._isModifier = false;
    this._resultCache = {};
    this._prevResults = {};
    this._childResult = null;
    if (object)
      this.set(object);
  }
  RenderNode.prototype.add = function add(child) {
    var childNode = child instanceof RenderNode ? child : new RenderNode(child);
    if (this._child instanceof Array)
      this._child.push(childNode);
    else if (this._child) {
      this._child = [this._child, childNode];
      this._hasMultipleChildren = true;
      this._childResult = [];
    } else
      this._child = childNode;
    return childNode;
  };
  RenderNode.prototype.get = function get() {
    return this._object || (this._hasMultipleChildren ? null : this._child ? this._child.get() : null);
  };
  RenderNode.prototype.set = function set(child) {
    this._childResult = null;
    this._hasMultipleChildren = false;
    this._isRenderable = child.render ? true : false;
    this._isModifier = child.modify ? true : false;
    this._object = child;
    this._child = null;
    if (child instanceof RenderNode)
      return child;
    else
      return this;
  };
  RenderNode.prototype.getSize = function getSize() {
    var result = null;
    var target = this.get();
    if (target && target.getSize)
      result = target.getSize();
    if (!result && this._child && this._child.getSize)
      result = this._child.getSize();
    return result;
  };
  function _applyCommit(spec, context, cacheStorage) {
    var result = SpecParser.parse(spec, context);
    var keys = Object.keys(result);
    for (var i = 0; i < keys.length; i++) {
      var id = keys[i];
      var childNode = Entity.get(id);
      var commitParams = result[id];
      commitParams.allocator = context.allocator;
      var commitResult = childNode.commit(commitParams);
      if (commitResult)
        _applyCommit(commitResult, context, cacheStorage);
      else
        cacheStorage[id] = commitParams;
    }
  }
  RenderNode.prototype.commit = function commit(context) {
    var prevKeys = Object.keys(this._prevResults);
    for (var i = 0; i < prevKeys.length; i++) {
      var id = prevKeys[i];
      if (this._resultCache[id] === undefined) {
        var object = Entity.get(id);
        if (object.cleanup)
          object.cleanup(context.allocator);
      }
    }
    this._prevResults = this._resultCache;
    this._resultCache = {};
    _applyCommit(this.render(), context, this._resultCache);
  };
  RenderNode.prototype.render = function render() {
    if (this._isRenderable)
      return this._object.render();
    var result = null;
    if (this._hasMultipleChildren) {
      result = this._childResult;
      var children = this._child;
      for (var i = 0; i < children.length; i++) {
        result[i] = children[i].render();
      }
    } else if (this._child)
      result = this._child.render();
    return this._isModifier ? this._object.modify(result) : result;
  };
  module.exports = RenderNode;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/transitions/Transitionable", ["npm:famous@0.3.5/transitions/MultipleTransition", "npm:famous@0.3.5/transitions/TweenTransition"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var MultipleTransition = require("npm:famous@0.3.5/transitions/MultipleTransition");
  var TweenTransition = require("npm:famous@0.3.5/transitions/TweenTransition");
  function Transitionable(start) {
    this.currentAction = null;
    this.actionQueue = [];
    this.callbackQueue = [];
    this.state = 0;
    this.velocity = undefined;
    this._callback = undefined;
    this._engineInstance = null;
    this._currentMethod = null;
    this.set(start);
  }
  var transitionMethods = {};
  Transitionable.register = function register(methods) {
    var success = true;
    for (var method in methods) {
      if (!Transitionable.registerMethod(method, methods[method]))
        success = false;
    }
    return success;
  };
  Transitionable.registerMethod = function registerMethod(name, engineClass) {
    if (!(name in transitionMethods)) {
      transitionMethods[name] = engineClass;
      return true;
    } else
      return false;
  };
  Transitionable.unregisterMethod = function unregisterMethod(name) {
    if (name in transitionMethods) {
      delete transitionMethods[name];
      return true;
    } else
      return false;
  };
  function _loadNext() {
    if (this._callback) {
      var callback = this._callback;
      this._callback = undefined;
      callback();
    }
    if (this.actionQueue.length <= 0) {
      this.set(this.get());
      return ;
    }
    this.currentAction = this.actionQueue.shift();
    this._callback = this.callbackQueue.shift();
    var method = null;
    var endValue = this.currentAction[0];
    var transition = this.currentAction[1];
    if (transition instanceof Object && transition.method) {
      method = transition.method;
      if (typeof method === 'string')
        method = transitionMethods[method];
    } else {
      method = TweenTransition;
    }
    if (this._currentMethod !== method) {
      if (!(endValue instanceof Object) || method.SUPPORTS_MULTIPLE === true || endValue.length <= method.SUPPORTS_MULTIPLE) {
        this._engineInstance = new method();
      } else {
        this._engineInstance = new MultipleTransition(method);
      }
      this._currentMethod = method;
    }
    this._engineInstance.reset(this.state, this.velocity);
    if (this.velocity !== undefined)
      transition.velocity = this.velocity;
    this._engineInstance.set(endValue, transition, _loadNext.bind(this));
  }
  Transitionable.prototype.set = function set(endState, transition, callback) {
    if (!transition) {
      this.reset(endState);
      if (callback)
        callback();
      return this;
    }
    var action = [endState, transition];
    this.actionQueue.push(action);
    this.callbackQueue.push(callback);
    if (!this.currentAction)
      _loadNext.call(this);
    return this;
  };
  Transitionable.prototype.reset = function reset(startState, startVelocity) {
    this._currentMethod = null;
    this._engineInstance = null;
    this._callback = undefined;
    this.state = startState;
    this.velocity = startVelocity;
    this.currentAction = null;
    this.actionQueue = [];
    this.callbackQueue = [];
  };
  Transitionable.prototype.delay = function delay(duration, callback) {
    var endValue;
    if (this.actionQueue.length)
      endValue = this.actionQueue[this.actionQueue.length - 1][0];
    else if (this.currentAction)
      endValue = this.currentAction[0];
    else
      endValue = this.get();
    return this.set(endValue, {
      duration: duration,
      curve: function() {
        return 0;
      }
    }, callback);
  };
  Transitionable.prototype.get = function get(timestamp) {
    if (this._engineInstance) {
      if (this._engineInstance.getVelocity)
        this.velocity = this._engineInstance.getVelocity();
      this.state = this._engineInstance.get(timestamp);
    }
    return this.state;
  };
  Transitionable.prototype.isActive = function isActive() {
    return !!this.currentAction;
  };
  Transitionable.prototype.halt = function halt() {
    return this.set(this.get());
  };
  module.exports = Transitionable;
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/views/Scrollview", ["npm:famous@0.3.5/physics/PhysicsEngine", "npm:famous@0.3.5/physics/bodies/Particle", "npm:famous@0.3.5/physics/forces/Drag", "npm:famous@0.3.5/physics/forces/Spring", "npm:famous@0.3.5/core/EventHandler", "npm:famous@0.3.5/core/OptionsManager", "npm:famous@0.3.5/core/ViewSequence", "npm:famous@0.3.5/views/Scroller", "npm:famous@0.3.5/utilities/Utility", "npm:famous@0.3.5/inputs/GenericSync", "npm:famous@0.3.5/inputs/ScrollSync", "npm:famous@0.3.5/inputs/TouchSync"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var PhysicsEngine = require("npm:famous@0.3.5/physics/PhysicsEngine");
  var Particle = require("npm:famous@0.3.5/physics/bodies/Particle");
  var Drag = require("npm:famous@0.3.5/physics/forces/Drag");
  var Spring = require("npm:famous@0.3.5/physics/forces/Spring");
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  var OptionsManager = require("npm:famous@0.3.5/core/OptionsManager");
  var ViewSequence = require("npm:famous@0.3.5/core/ViewSequence");
  var Scroller = require("npm:famous@0.3.5/views/Scroller");
  var Utility = require("npm:famous@0.3.5/utilities/Utility");
  var GenericSync = require("npm:famous@0.3.5/inputs/GenericSync");
  var ScrollSync = require("npm:famous@0.3.5/inputs/ScrollSync");
  var TouchSync = require("npm:famous@0.3.5/inputs/TouchSync");
  GenericSync.register({
    scroll: ScrollSync,
    touch: TouchSync
  });
  var TOLERANCE = 0.5;
  var SpringStates = {
    NONE: 0,
    EDGE: 1,
    PAGE: 2
  };
  var EdgeStates = {
    TOP: -1,
    NONE: 0,
    BOTTOM: 1
  };
  function Scrollview(options) {
    this.options = Object.create(Scrollview.DEFAULT_OPTIONS);
    this._optionsManager = new OptionsManager(this.options);
    this._scroller = new Scroller(this.options);
    this.sync = new GenericSync(['scroll', 'touch'], {
      direction: this.options.direction,
      scale: this.options.syncScale,
      rails: this.options.rails,
      preventDefault: this.options.preventDefault !== undefined ? this.options.preventDefault : this.options.direction !== Utility.Direction.Y
    });
    this._physicsEngine = new PhysicsEngine();
    this._particle = new Particle();
    this._physicsEngine.addBody(this._particle);
    this.spring = new Spring({
      anchor: [0, 0, 0],
      period: this.options.edgePeriod,
      dampingRatio: this.options.edgeDamp
    });
    this.drag = new Drag({
      forceFunction: Drag.FORCE_FUNCTIONS.QUADRATIC,
      strength: this.options.drag
    });
    this.friction = new Drag({
      forceFunction: Drag.FORCE_FUNCTIONS.LINEAR,
      strength: this.options.friction
    });
    this._node = null;
    this._touchCount = 0;
    this._springState = SpringStates.NONE;
    this._onEdge = EdgeStates.NONE;
    this._pageSpringPosition = 0;
    this._edgeSpringPosition = 0;
    this._touchVelocity = 0;
    this._earlyEnd = false;
    this._needsPaginationCheck = false;
    this._displacement = 0;
    this._totalShift = 0;
    this._cachedIndex = 0;
    this._scroller.positionFrom(this.getPosition.bind(this));
    this._eventInput = new EventHandler();
    this._eventOutput = new EventHandler();
    this._eventInput.pipe(this.sync);
    this.sync.pipe(this._eventInput);
    EventHandler.setInputHandler(this, this._eventInput);
    EventHandler.setOutputHandler(this, this._eventOutput);
    _bindEvents.call(this);
    if (options)
      this.setOptions(options);
  }
  Scrollview.DEFAULT_OPTIONS = {
    direction: Utility.Direction.Y,
    rails: true,
    friction: 0.005,
    drag: 0.0001,
    edgeGrip: 0.2,
    edgePeriod: 300,
    edgeDamp: 1,
    margin: 1000,
    paginated: false,
    pagePeriod: 500,
    pageDamp: 0.8,
    pageStopSpeed: 10,
    pageSwitchSpeed: 0.5,
    speedLimit: 5,
    groupScroll: false,
    syncScale: 1
  };
  function _handleStart(event) {
    this._touchCount = event.count;
    if (event.count === undefined)
      this._touchCount = 1;
    _detachAgents.call(this);
    this.setVelocity(0);
    this._touchVelocity = 0;
    this._earlyEnd = false;
  }
  function _handleMove(event) {
    var velocity = -event.velocity;
    var delta = -event.delta;
    if (this._onEdge !== EdgeStates.NONE && event.slip) {
      if (velocity < 0 && this._onEdge === EdgeStates.TOP || velocity > 0 && this._onEdge === EdgeStates.BOTTOM) {
        if (!this._earlyEnd) {
          _handleEnd.call(this, event);
          this._earlyEnd = true;
        }
      } else if (this._earlyEnd && Math.abs(velocity) > Math.abs(this.getVelocity())) {
        _handleStart.call(this, event);
      }
    }
    if (this._earlyEnd)
      return ;
    this._touchVelocity = velocity;
    if (event.slip) {
      var speedLimit = this.options.speedLimit;
      if (velocity < -speedLimit)
        velocity = -speedLimit;
      else if (velocity > speedLimit)
        velocity = speedLimit;
      this.setVelocity(velocity);
      var deltaLimit = speedLimit * 16;
      if (delta > deltaLimit)
        delta = deltaLimit;
      else if (delta < -deltaLimit)
        delta = -deltaLimit;
    }
    this.setPosition(this.getPosition() + delta);
    this._displacement += delta;
    if (this._springState === SpringStates.NONE)
      _normalizeState.call(this);
  }
  function _handleEnd(event) {
    this._touchCount = event.count || 0;
    if (!this._touchCount) {
      _detachAgents.call(this);
      if (this._onEdge !== EdgeStates.NONE)
        _setSpring.call(this, this._edgeSpringPosition, SpringStates.EDGE);
      _attachAgents.call(this);
      var velocity = -event.velocity;
      var speedLimit = this.options.speedLimit;
      if (event.slip)
        speedLimit *= this.options.edgeGrip;
      if (velocity < -speedLimit)
        velocity = -speedLimit;
      else if (velocity > speedLimit)
        velocity = speedLimit;
      this.setVelocity(velocity);
      this._touchVelocity = 0;
      this._needsPaginationCheck = true;
    }
  }
  function _bindEvents() {
    this._eventInput.bindThis(this);
    this._eventInput.on('start', _handleStart);
    this._eventInput.on('update', _handleMove);
    this._eventInput.on('end', _handleEnd);
    this._eventInput.on('resize', function() {
      this._node._.calculateSize();
    }.bind(this));
    this._scroller.on('onEdge', function(data) {
      this._edgeSpringPosition = data.position;
      _handleEdge.call(this, this._scroller.onEdge());
      this._eventOutput.emit('onEdge');
    }.bind(this));
    this._scroller.on('offEdge', function() {
      this.sync.setOptions({scale: this.options.syncScale});
      this._onEdge = this._scroller.onEdge();
      this._eventOutput.emit('offEdge');
    }.bind(this));
    this._particle.on('update', function(particle) {
      if (this._springState === SpringStates.NONE)
        _normalizeState.call(this);
      this._displacement = particle.position.x - this._totalShift;
    }.bind(this));
    this._particle.on('end', function() {
      if (!this.options.paginated || this.options.paginated && this._springState !== SpringStates.NONE)
        this._eventOutput.emit('settle');
    }.bind(this));
  }
  function _attachAgents() {
    if (this._springState)
      this._physicsEngine.attach([this.spring], this._particle);
    else
      this._physicsEngine.attach([this.drag, this.friction], this._particle);
  }
  function _detachAgents() {
    this._springState = SpringStates.NONE;
    this._physicsEngine.detachAll();
  }
  function _nodeSizeForDirection(node) {
    var direction = this.options.direction;
    var nodeSize = node.getSize();
    return !nodeSize ? this._scroller.getSize()[direction] : nodeSize[direction];
  }
  function _handleEdge(edge) {
    this.sync.setOptions({scale: this.options.edgeGrip});
    this._onEdge = edge;
    if (!this._touchCount && this._springState !== SpringStates.EDGE) {
      _setSpring.call(this, this._edgeSpringPosition, SpringStates.EDGE);
    }
    if (this._springState && Math.abs(this.getVelocity()) < 0.001) {
      _detachAgents.call(this);
      _attachAgents.call(this);
    }
  }
  function _handlePagination() {
    if (this._touchCount)
      return ;
    if (this._springState === SpringStates.EDGE)
      return ;
    var velocity = this.getVelocity();
    if (Math.abs(velocity) >= this.options.pageStopSpeed)
      return ;
    var position = this.getPosition();
    var velocitySwitch = Math.abs(velocity) > this.options.pageSwitchSpeed;
    var nodeSize = _nodeSizeForDirection.call(this, this._node);
    var positionNext = position > 0.5 * nodeSize;
    var positionPrev = position < 0.5 * nodeSize;
    var velocityNext = velocity > 0;
    var velocityPrev = velocity < 0;
    this._needsPaginationCheck = false;
    if (positionNext && !velocitySwitch || velocitySwitch && velocityNext) {
      this.goToNextPage();
    } else if (velocitySwitch && velocityPrev) {
      this.goToPreviousPage();
    } else
      _setSpring.call(this, 0, SpringStates.PAGE);
  }
  function _setSpring(position, springState) {
    var springOptions;
    if (springState === SpringStates.EDGE) {
      this._edgeSpringPosition = position;
      springOptions = {
        anchor: [this._edgeSpringPosition, 0, 0],
        period: this.options.edgePeriod,
        dampingRatio: this.options.edgeDamp
      };
    } else if (springState === SpringStates.PAGE) {
      this._pageSpringPosition = position;
      springOptions = {
        anchor: [this._pageSpringPosition, 0, 0],
        period: this.options.pagePeriod,
        dampingRatio: this.options.pageDamp
      };
    }
    this.spring.setOptions(springOptions);
    if (springState && !this._springState) {
      _detachAgents.call(this);
      this._springState = springState;
      _attachAgents.call(this);
    }
    this._springState = springState;
  }
  function _normalizeState() {
    var offset = 0;
    var position = this.getPosition();
    position += (position < 0 ? -0.5 : 0.5) >> 0;
    var nodeSize = _nodeSizeForDirection.call(this, this._node);
    var nextNode = this._node.getNext();
    while (offset + position >= nodeSize && nextNode) {
      offset -= nodeSize;
      this._scroller.sequenceFrom(nextNode);
      this._node = nextNode;
      nextNode = this._node.getNext();
      nodeSize = _nodeSizeForDirection.call(this, this._node);
    }
    var previousNode = this._node.getPrevious();
    var previousNodeSize;
    while (offset + position <= 0 && previousNode) {
      previousNodeSize = _nodeSizeForDirection.call(this, previousNode);
      this._scroller.sequenceFrom(previousNode);
      this._node = previousNode;
      offset += previousNodeSize;
      previousNode = this._node.getPrevious();
    }
    if (offset)
      _shiftOrigin.call(this, offset);
    if (this._node) {
      if (this._node.index !== this._cachedIndex) {
        if (this.getPosition() < 0.5 * nodeSize) {
          this._cachedIndex = this._node.index;
          this._eventOutput.emit('pageChange', {
            direction: -1,
            index: this._cachedIndex
          });
        }
      } else {
        if (this.getPosition() > 0.5 * nodeSize) {
          this._cachedIndex = this._node.index + 1;
          this._eventOutput.emit('pageChange', {
            direction: 1,
            index: this._cachedIndex
          });
        }
      }
    }
  }
  function _shiftOrigin(amount) {
    this._edgeSpringPosition += amount;
    this._pageSpringPosition += amount;
    this.setPosition(this.getPosition() + amount);
    this._totalShift += amount;
    if (this._springState === SpringStates.EDGE) {
      this.spring.setOptions({anchor: [this._edgeSpringPosition, 0, 0]});
    } else if (this._springState === SpringStates.PAGE) {
      this.spring.setOptions({anchor: [this._pageSpringPosition, 0, 0]});
    }
  }
  Scrollview.prototype.getCurrentIndex = function getCurrentIndex() {
    return this._node.index;
  };
  Scrollview.prototype.goToPreviousPage = function goToPreviousPage() {
    if (!this._node || this._onEdge === EdgeStates.TOP)
      return null;
    if (this.getPosition() > 1 && this._springState === SpringStates.NONE) {
      _setSpring.call(this, 0, SpringStates.PAGE);
      return this._node;
    }
    var previousNode = this._node.getPrevious();
    if (previousNode) {
      var previousNodeSize = _nodeSizeForDirection.call(this, previousNode);
      this._scroller.sequenceFrom(previousNode);
      this._node = previousNode;
      _shiftOrigin.call(this, previousNodeSize);
      _setSpring.call(this, 0, SpringStates.PAGE);
    }
    return previousNode;
  };
  Scrollview.prototype.goToNextPage = function goToNextPage() {
    if (!this._node || this._onEdge === EdgeStates.BOTTOM)
      return null;
    var nextNode = this._node.getNext();
    if (nextNode) {
      var currentNodeSize = _nodeSizeForDirection.call(this, this._node);
      this._scroller.sequenceFrom(nextNode);
      this._node = nextNode;
      _shiftOrigin.call(this, -currentNodeSize);
      _setSpring.call(this, 0, SpringStates.PAGE);
    }
    return nextNode;
  };
  Scrollview.prototype.goToPage = function goToPage(index) {
    var currentIndex = this.getCurrentIndex();
    var i;
    if (currentIndex > index) {
      for (i = 0; i < currentIndex - index; i++)
        this.goToPreviousPage();
    }
    if (currentIndex < index) {
      for (i = 0; i < index - currentIndex; i++)
        this.goToNextPage();
    }
  };
  Scrollview.prototype.outputFrom = function outputFrom() {
    return this._scroller.outputFrom.apply(this._scroller, arguments);
  };
  Scrollview.prototype.getPosition = function getPosition() {
    return this._particle.getPosition1D();
  };
  Scrollview.prototype.getAbsolutePosition = function getAbsolutePosition() {
    return this._scroller.getCumulativeSize(this.getCurrentIndex())[this.options.direction] + this.getPosition();
  };
  Scrollview.prototype.getOffset = Scrollview.prototype.getPosition;
  Scrollview.prototype.setPosition = function setPosition(x) {
    this._particle.setPosition1D(x);
  };
  Scrollview.prototype.setOffset = Scrollview.prototype.setPosition;
  Scrollview.prototype.getVelocity = function getVelocity() {
    return this._touchCount ? this._touchVelocity : this._particle.getVelocity1D();
  };
  Scrollview.prototype.setVelocity = function setVelocity(v) {
    this._particle.setVelocity1D(v);
  };
  Scrollview.prototype.setOptions = function setOptions(options) {
    if (options.direction !== undefined) {
      if (options.direction === 'x')
        options.direction = Utility.Direction.X;
      else if (options.direction === 'y')
        options.direction = Utility.Direction.Y;
    }
    if (options.groupScroll !== this.options.groupScroll) {
      if (options.groupScroll)
        this.subscribe(this._scroller);
      else
        this.unsubscribe(this._scroller);
    }
    this._optionsManager.setOptions(options);
    this._scroller.setOptions(options);
    if (options.drag !== undefined)
      this.drag.setOptions({strength: this.options.drag});
    if (options.friction !== undefined)
      this.friction.setOptions({strength: this.options.friction});
    if (options.edgePeriod !== undefined || options.edgeDamp !== undefined) {
      this.spring.setOptions({
        period: this.options.edgePeriod,
        dampingRatio: this.options.edgeDamp
      });
    }
    if (options.rails || options.direction !== undefined || options.syncScale !== undefined || options.preventDefault) {
      this.sync.setOptions({
        rails: this.options.rails,
        direction: this.options.direction === Utility.Direction.X ? GenericSync.DIRECTION_X : GenericSync.DIRECTION_Y,
        scale: this.options.syncScale,
        preventDefault: this.options.preventDefault
      });
    }
  };
  Scrollview.prototype.sequenceFrom = function sequenceFrom(node) {
    if (node instanceof Array)
      node = new ViewSequence({
        array: node,
        trackSize: true
      });
    this._node = node;
    return this._scroller.sequenceFrom(node);
  };
  Scrollview.prototype.getSize = function getSize() {
    return this._scroller.getSize.apply(this._scroller, arguments);
  };
  Scrollview.prototype.render = function render() {
    if (this.options.paginated && this._needsPaginationCheck)
      _handlePagination.call(this);
    return this._scroller.render();
  };
  module.exports = Scrollview;
  global.define = __define;
  return module.exports;
});



System.register("src/components/demo/CubeSurface", ["npm:lodash@3.7.0", "src/components/autoSurface", "npm:famous@0.3.5/core/Modifier", "npm:famous@0.3.5/core/Transform", "src/components/demo/cubePlaneSurface", "npm:famous@0.3.5/modifiers/ModifierChain", "npm:famous@0.3.5/transitions/TransitionableTransform", "src/utils/ObjectHelper"], function($__export) {
  "use strict";
  var __moduleName = "src/components/demo/CubeSurface";
  var _,
      AutoSurface,
      Modifier,
      Transform,
      CubePlaneSurface,
      ModifierChain,
      TransitionableTransform,
      ObjectHelper;
  return {
    setters: [function($__m) {
      _ = $__m.default;
    }, function($__m) {
      AutoSurface = $__m.default;
    }, function($__m) {
      Modifier = $__m.default;
    }, function($__m) {
      Transform = $__m.default;
    }, function($__m) {
      CubePlaneSurface = $__m.default;
    }, function($__m) {
      ModifierChain = $__m.default;
    }, function($__m) {
      TransitionableTransform = $__m.default;
    }, function($__m) {
      ObjectHelper = $__m.default;
    }],
    execute: function() {
      $__export('default', (function($__super) {
        var CubeSurface = function CubeSurface(context, options, modifierOptions) {
          var mod = new Modifier();
          this._transitionable = new TransitionableTransform();
          mod.transformFrom(this._transitionable.get.bind(this._transitionable));
          this._modifierChain = new ModifierChain();
          this._modifierChain.addModifier(mod);
          var ourModifierOptions = {
            origin: [0.5, 0.5],
            align: [0.5, 0.5]
          };
          $traceurRuntime.superConstructor(CubeSurface).call(this, context, options, _.extend(ourModifierOptions, modifierOptions));
          var size = this.getSize();
          var planes = [new CubePlaneSurface(this.context, {size: size}, null, 'top'), new CubePlaneSurface(this.context, {size: size}, null, 'bottom'), new CubePlaneSurface(this.context, {size: size}, null, 'right'), new CubePlaneSurface(this.context, {size: size}, null, 'left'), new CubePlaneSurface(this.context, {size: size}, null, 'front'), new CubePlaneSurface(this.context, {size: size}, null, 'back')];
          ObjectHelper.bindAllMethods(this, this);
          this.animate();
        };
        return ($traceurRuntime.createClass)(CubeSurface, {
          get defaultOptions() {
            return {
              size: [100, 100],
              properties: {
                backgroundColor: 'black',
                opacity: 0.20
              }
            };
          },
          animate: function() {
            if (!this._cycle) {
              this._cycle = 1;
            }
            var rotation = this._cycle++ * 2 * Math.PI;
            this._transitionable.setRotate([rotation, rotation * 1.3, rotation * 0.1], {duration: 10000}, this.animate);
          }
        }, {}, $__super);
      }(AutoSurface)));
    }
  };
});



System.register("github:jspm/nodelibs-process@0.1.1", ["github:jspm/nodelibs-process@0.1.1/index"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = require("github:jspm/nodelibs-process@0.1.1/index");
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/Context", ["npm:famous@0.3.5/core/RenderNode", "npm:famous@0.3.5/core/EventHandler", "npm:famous@0.3.5/core/ElementAllocator", "npm:famous@0.3.5/core/Transform", "npm:famous@0.3.5/transitions/Transitionable"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var RenderNode = require("npm:famous@0.3.5/core/RenderNode");
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  var ElementAllocator = require("npm:famous@0.3.5/core/ElementAllocator");
  var Transform = require("npm:famous@0.3.5/core/Transform");
  var Transitionable = require("npm:famous@0.3.5/transitions/Transitionable");
  var _zeroZero = [0, 0];
  var usePrefix = !('perspective' in document.documentElement.style);
  function _getElementSize() {
    var element = this.container;
    return [element.clientWidth, element.clientHeight];
  }
  var _setPerspective = usePrefix ? function(element, perspective) {
    element.style.webkitPerspective = perspective ? perspective.toFixed() + 'px' : '';
  } : function(element, perspective) {
    element.style.perspective = perspective ? perspective.toFixed() + 'px' : '';
  };
  function Context(container) {
    this.container = container;
    this._allocator = new ElementAllocator(container);
    this._node = new RenderNode();
    this._eventOutput = new EventHandler();
    this._size = _getElementSize.call(this);
    this._perspectiveState = new Transitionable(0);
    this._perspective = undefined;
    this._nodeContext = {
      allocator: this._allocator,
      transform: Transform.identity,
      opacity: 1,
      origin: _zeroZero,
      align: _zeroZero,
      size: this._size
    };
    this._eventOutput.on('resize', function() {
      this.setSize(_getElementSize.call(this));
    }.bind(this));
  }
  Context.prototype.getAllocator = function getAllocator() {
    return this._allocator;
  };
  Context.prototype.add = function add(obj) {
    return this._node.add(obj);
  };
  Context.prototype.migrate = function migrate(container) {
    if (container === this.container)
      return ;
    this.container = container;
    this._allocator.migrate(container);
  };
  Context.prototype.getSize = function getSize() {
    return this._size;
  };
  Context.prototype.setSize = function setSize(size) {
    if (!size)
      size = _getElementSize.call(this);
    this._size[0] = size[0];
    this._size[1] = size[1];
  };
  Context.prototype.update = function update(contextParameters) {
    if (contextParameters) {
      if (contextParameters.transform)
        this._nodeContext.transform = contextParameters.transform;
      if (contextParameters.opacity)
        this._nodeContext.opacity = contextParameters.opacity;
      if (contextParameters.origin)
        this._nodeContext.origin = contextParameters.origin;
      if (contextParameters.align)
        this._nodeContext.align = contextParameters.align;
      if (contextParameters.size)
        this._nodeContext.size = contextParameters.size;
    }
    var perspective = this._perspectiveState.get();
    if (perspective !== this._perspective) {
      _setPerspective(this.container, perspective);
      this._perspective = perspective;
    }
    this._node.commit(this._nodeContext);
  };
  Context.prototype.getPerspective = function getPerspective() {
    return this._perspectiveState.get();
  };
  Context.prototype.setPerspective = function setPerspective(perspective, transition, callback) {
    return this._perspectiveState.set(perspective, transition, callback);
  };
  Context.prototype.emit = function emit(type, event) {
    return this._eventOutput.emit(type, event);
  };
  Context.prototype.on = function on(type, handler) {
    return this._eventOutput.on(type, handler);
  };
  Context.prototype.removeListener = function removeListener(type, handler) {
    return this._eventOutput.removeListener(type, handler);
  };
  Context.prototype.pipe = function pipe(target) {
    return this._eventOutput.pipe(target);
  };
  Context.prototype.unpipe = function unpipe(target) {
    return this._eventOutput.unpipe(target);
  };
  module.exports = Context;
  global.define = __define;
  return module.exports;
});



System.register("npm:events-browserify@0.0.1/events", ["github:jspm/nodelibs-process@0.1.1"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  (function(process) {
    if (!process.EventEmitter)
      process.EventEmitter = function() {};
    var EventEmitter = exports.EventEmitter = process.EventEmitter;
    var isArray = typeof Array.isArray === 'function' ? Array.isArray : function(xs) {
      return Object.prototype.toString.call(xs) === '[object Array]';
    };
    ;
    var defaultMaxListeners = 10;
    EventEmitter.prototype.setMaxListeners = function(n) {
      if (!this._events)
        this._events = {};
      this._events.maxListeners = n;
    };
    EventEmitter.prototype.emit = function(type) {
      if (type === 'error') {
        if (!this._events || !this._events.error || (isArray(this._events.error) && !this._events.error.length)) {
          if (arguments[1] instanceof Error) {
            throw arguments[1];
          } else {
            throw new Error("Uncaught, unspecified 'error' event.");
          }
          return false;
        }
      }
      if (!this._events)
        return false;
      var handler = this._events[type];
      if (!handler)
        return false;
      if (typeof handler == 'function') {
        switch (arguments.length) {
          case 1:
            handler.call(this);
            break;
          case 2:
            handler.call(this, arguments[1]);
            break;
          case 3:
            handler.call(this, arguments[1], arguments[2]);
            break;
          default:
            var args = Array.prototype.slice.call(arguments, 1);
            handler.apply(this, args);
        }
        return true;
      } else if (isArray(handler)) {
        var args = Array.prototype.slice.call(arguments, 1);
        var listeners = handler.slice();
        for (var i = 0,
            l = listeners.length; i < l; i++) {
          listeners[i].apply(this, args);
        }
        return true;
      } else {
        return false;
      }
    };
    EventEmitter.prototype.addListener = function(type, listener) {
      if ('function' !== typeof listener) {
        throw new Error('addListener only takes instances of Function');
      }
      if (!this._events)
        this._events = {};
      this.emit('newListener', type, listener);
      if (!this._events[type]) {
        this._events[type] = listener;
      } else if (isArray(this._events[type])) {
        if (!this._events[type].warned) {
          var m;
          if (this._events.maxListeners !== undefined) {
            m = this._events.maxListeners;
          } else {
            m = defaultMaxListeners;
          }
          if (m && m > 0 && this._events[type].length > m) {
            this._events[type].warned = true;
            console.error('(node) warning: possible EventEmitter memory ' + 'leak detected. %d listeners added. ' + 'Use emitter.setMaxListeners() to increase limit.', this._events[type].length);
            console.trace();
          }
        }
        this._events[type].push(listener);
      } else {
        this._events[type] = [this._events[type], listener];
      }
      return this;
    };
    EventEmitter.prototype.on = EventEmitter.prototype.addListener;
    EventEmitter.prototype.once = function(type, listener) {
      var self = this;
      self.on(type, function g() {
        self.removeListener(type, g);
        listener.apply(this, arguments);
      });
      return this;
    };
    EventEmitter.prototype.removeListener = function(type, listener) {
      if ('function' !== typeof listener) {
        throw new Error('removeListener only takes instances of Function');
      }
      if (!this._events || !this._events[type])
        return this;
      var list = this._events[type];
      if (isArray(list)) {
        var i = list.indexOf(listener);
        if (i < 0)
          return this;
        list.splice(i, 1);
        if (list.length == 0)
          delete this._events[type];
      } else if (this._events[type] === listener) {
        delete this._events[type];
      }
      return this;
    };
    EventEmitter.prototype.removeAllListeners = function(type) {
      if (type && this._events && this._events[type])
        this._events[type] = null;
      return this;
    };
    EventEmitter.prototype.listeners = function(type) {
      if (!this._events)
        this._events = {};
      if (!this._events[type])
        this._events[type] = [];
      if (!isArray(this._events[type])) {
        this._events[type] = [this._events[type]];
      }
      return this._events[type];
    };
  })(require("github:jspm/nodelibs-process@0.1.1"));
  global.define = __define;
  return module.exports;
});



System.register("npm:famous@0.3.5/core/Engine", ["npm:famous@0.3.5/core/Context", "npm:famous@0.3.5/core/EventHandler", "npm:famous@0.3.5/core/OptionsManager"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var Context = require("npm:famous@0.3.5/core/Context");
  var EventHandler = require("npm:famous@0.3.5/core/EventHandler");
  var OptionsManager = require("npm:famous@0.3.5/core/OptionsManager");
  var Engine = {};
  var contexts = [];
  var nextTickQueue = [];
  var currentFrame = 0;
  var nextTickFrame = 0;
  var deferQueue = [];
  var lastTime = Date.now();
  var frameTime;
  var frameTimeLimit;
  var loopEnabled = true;
  var eventForwarders = {};
  var eventHandler = new EventHandler();
  var options = {
    containerType: 'div',
    containerClass: 'famous-container',
    fpsCap: undefined,
    runLoop: true,
    appMode: true
  };
  var optionsManager = new OptionsManager(options);
  var MAX_DEFER_FRAME_TIME = 10;
  Engine.step = function step() {
    currentFrame++;
    nextTickFrame = currentFrame;
    var currentTime = Date.now();
    if (frameTimeLimit && currentTime - lastTime < frameTimeLimit)
      return ;
    var i = 0;
    frameTime = currentTime - lastTime;
    lastTime = currentTime;
    eventHandler.emit('prerender');
    var numFunctions = nextTickQueue.length;
    while (numFunctions--)
      nextTickQueue.shift()(currentFrame);
    while (deferQueue.length && Date.now() - currentTime < MAX_DEFER_FRAME_TIME) {
      deferQueue.shift().call(this);
    }
    for (i = 0; i < contexts.length; i++)
      contexts[i].update();
    eventHandler.emit('postrender');
  };
  function loop() {
    if (options.runLoop) {
      Engine.step();
      window.requestAnimationFrame(loop);
    } else
      loopEnabled = false;
  }
  window.requestAnimationFrame(loop);
  function handleResize(event) {
    for (var i = 0; i < contexts.length; i++) {
      contexts[i].emit('resize');
    }
    eventHandler.emit('resize');
  }
  window.addEventListener('resize', handleResize, false);
  handleResize();
  function initialize() {
    window.addEventListener('touchmove', function(event) {
      event.preventDefault();
    }, true);
    addRootClasses();
  }
  var initialized = false;
  function addRootClasses() {
    if (!document.body) {
      Engine.nextTick(addRootClasses);
      return ;
    }
    document.body.classList.add('famous-root');
    document.documentElement.classList.add('famous-root');
  }
  Engine.pipe = function pipe(target) {
    if (target.subscribe instanceof Function)
      return target.subscribe(Engine);
    else
      return eventHandler.pipe(target);
  };
  Engine.unpipe = function unpipe(target) {
    if (target.unsubscribe instanceof Function)
      return target.unsubscribe(Engine);
    else
      return eventHandler.unpipe(target);
  };
  Engine.on = function on(type, handler) {
    if (!(type in eventForwarders)) {
      eventForwarders[type] = eventHandler.emit.bind(eventHandler, type);
      addEngineListener(type, eventForwarders[type]);
    }
    return eventHandler.on(type, handler);
  };
  function addEngineListener(type, forwarder) {
    if (!document.body) {
      Engine.nextTick(addEventListener.bind(this, type, forwarder));
      return ;
    }
    document.body.addEventListener(type, forwarder);
  }
  Engine.emit = function emit(type, event) {
    return eventHandler.emit(type, event);
  };
  Engine.removeListener = function removeListener(type, handler) {
    return eventHandler.removeListener(type, handler);
  };
  Engine.getFPS = function getFPS() {
    return 1000 / frameTime;
  };
  Engine.setFPSCap = function setFPSCap(fps) {
    frameTimeLimit = Math.floor(1000 / fps);
  };
  Engine.getOptions = function getOptions(key) {
    return optionsManager.getOptions(key);
  };
  Engine.setOptions = function setOptions(options) {
    return optionsManager.setOptions.apply(optionsManager, arguments);
  };
  Engine.createContext = function createContext(el) {
    if (!initialized && options.appMode)
      Engine.nextTick(initialize);
    var needMountContainer = false;
    if (!el) {
      el = document.createElement(options.containerType);
      el.classList.add(options.containerClass);
      needMountContainer = true;
    }
    var context = new Context(el);
    Engine.registerContext(context);
    if (needMountContainer)
      mount(context, el);
    return context;
  };
  function mount(context, el) {
    if (!document.body) {
      Engine.nextTick(mount.bind(this, context, el));
      return ;
    }
    document.body.appendChild(el);
    context.emit('resize');
  }
  Engine.registerContext = function registerContext(context) {
    contexts.push(context);
    return context;
  };
  Engine.getContexts = function getContexts() {
    return contexts;
  };
  Engine.deregisterContext = function deregisterContext(context) {
    var i = contexts.indexOf(context);
    if (i >= 0)
      contexts.splice(i, 1);
  };
  Engine.nextTick = function nextTick(fn) {
    nextTickQueue.push(fn);
  };
  Engine.defer = function defer(fn) {
    deferQueue.push(fn);
  };
  optionsManager.on('change', function(data) {
    if (data.id === 'fpsCap')
      Engine.setFPSCap(data.value);
    else if (data.id === 'runLoop') {
      if (!loopEnabled && data.value) {
        loopEnabled = true;
        window.requestAnimationFrame(loop);
      }
    }
  });
  module.exports = Engine;
  global.define = __define;
  return module.exports;
});



System.register("npm:events-browserify@0.0.1", ["npm:events-browserify@0.0.1/events"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = require("npm:events-browserify@0.0.1/events");
  global.define = __define;
  return module.exports;
});



System.register("github:jspm/nodelibs-events@0.1.0/index", ["npm:events-browserify@0.0.1"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = System._nodeRequire ? System._nodeRequire('events') : require("npm:events-browserify@0.0.1");
  global.define = __define;
  return module.exports;
});



System.register("github:jspm/nodelibs-events@0.1.0", ["github:jspm/nodelibs-events@0.1.0/index"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = require("github:jspm/nodelibs-events@0.1.0/index");
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/Myo", ["github:jspm/nodelibs-events@0.1.0"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var EventEmitter = require("github:jspm/nodelibs-events@0.1.0").EventEmitter;
  var Myo = module.exports = function(data, context) {
    this.VIBRATION_SHORT = 0;
    this.VIBRATION_MEDIUM = 1;
    this.VIBRATION_LONG = 2;
    this.context = context;
  };
  Myo.prototype.requestRssi = function() {
    this.context.send({"requestRssi": true});
  };
  Myo.prototype.vibrate = function(length) {
    switch (length) {
      case this.VIBRATION_SHORT:
        this.context.send({
          "command": "vibrate",
          "args": [this.VIBRATION_SHORT]
        });
        break;
      case this.VIBRATION_MEDIUM:
        this.context.send({
          "command": "vibrate",
          "args": [this.VIBRATION_MEDIUM]
        });
        break;
      case this.VIBRATION_LONG:
        this.context.send({
          "command": "vibrate",
          "args": [this.VIBRATION_LONG]
        });
        break;
      default:
        throw new Error("Valid values are: Myo.VIBRATION_SHORT, Myo.VIBRATION_MEDIUM, Myo.VIBRATION_LONG");
        break;
    }
  };
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/Frame", ["npm:myojs@0.8.18/src/Hub", "npm:myojs@0.8.18/src/Myo", "npm:myojs@0.8.18/src/Pose", "npm:myojs@0.8.18/src/Quaternion", "npm:myojs@0.8.18/src/Vector3", "npm:underscore@1.8.3", "github:jspm/nodelibs-process@0.1.1"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  (function(process) {
    var Hub = require("npm:myojs@0.8.18/src/Hub"),
        Myo = require("npm:myojs@0.8.18/src/Myo"),
        Pose = require("npm:myojs@0.8.18/src/Pose"),
        Quaternion = require("npm:myojs@0.8.18/src/Quaternion"),
        Vector3 = require("npm:myojs@0.8.18/src/Vector3"),
        _ = require("npm:underscore@1.8.3");
    var Frame = module.exports = function(data) {
      this.id = data.id;
      this.timestamp = data.timestamp;
      if (data["euler"]) {
        this.euler = data["euler"];
      }
      if (data["pose"]) {
        this.pose = new Pose(data["pose"]);
      } else {
        this.pose = Pose.invalid();
      }
      if (data["rotation"]) {
        this.rotation = new Quaternion(data["rotation"]);
      } else {
        this.rotation = Quaternion.invalid();
      }
      if (data["accel"]) {
        this.accel = new Vector3(data["accel"]);
      } else {
        this.accel = Vector3.invalid();
      }
      if (data["gyro"]) {
        this.gyro = new Vector3(data["gyro"]);
      } else {
        this.gyro = Vector3.invalid();
      }
      this.data = data;
      this.type = "frame";
    };
    Frame.prototype.toString = function() {
      return "[Frame id:" + this.id + " timestamp:" + this.timestamp + " accel:" + this.accel.toString() + "]";
    };
  })(require("github:jspm/nodelibs-process@0.1.1"));
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/connection/BaseConnection", ["npm:myojs@0.8.18/src/Frame", "github:jspm/nodelibs-events@0.1.0", "npm:underscore@1.8.3", "npm:ws@0.7.1", "github:jspm/nodelibs-process@0.1.1"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  (function(process) {
    var Frame = require("npm:myojs@0.8.18/src/Frame"),
        EventEmitter = require("github:jspm/nodelibs-events@0.1.0").EventEmitter,
        _ = require("npm:underscore@1.8.3");
    var BaseConnection = module.exports = function(options) {
      "use strict";
      this.options = _.defaults(options || {}, {
        host: '127.0.0.1',
        port: 6450
      });
      this.host = this.options.host;
      this.port = this.options.port;
      this.connect();
    };
    BaseConnection.prototype.getUrl = function() {
      "use strict";
      return "ws://" + this.host + ":" + this.port + "/";
    };
    BaseConnection.prototype.handleOpen = function() {
      "use strict";
      if (!this.connected) {
        this.send({"command": "requestDeviceInfo"});
      }
    };
    BaseConnection.prototype.handleClose = function(code, reason) {
      "use strict";
      console.log("handleClose: " + code + ", reason: " + reason);
      if (!this.connected)
        return ;
      this.disconnect();
      this.startReconnection();
    };
    BaseConnection.prototype.startReconnection = function() {
      "use strict";
      var connection = this;
      if (!this.reconnectionTimer) {
        (this.reconnectionTimer = setInterval(function() {
          connection.reconnect();
        }, 500));
      }
    };
    BaseConnection.prototype.stopReconnection = function() {
      "use strict";
      this.reconnectionTimer = clearInterval(this.reconnectionTimer);
    };
    BaseConnection.prototype.disconnect = function(allowReconnect) {
      "use strict";
      if (!allowReconnect)
        this.stopReconnection();
      if (!this.socket)
        return ;
      this.socket.close();
      delete this.socket;
      if (this.connected) {
        this.connected = false;
        this.emit('disconnect');
      }
      return true;
    };
    BaseConnection.prototype.reconnect = function() {
      "use strict";
      if (this.connected) {
        this.stopReconnection();
      } else {
        this.disconnect(true);
        this.connect();
      }
    };
    BaseConnection.prototype.handleData = function(data) {
      "use strict";
      var message,
          messageEvent,
          frame,
          deviceInfo;
      message = JSON.parse(data);
      if (!this.connected && message.hasOwnProperty("frame")) {
        frame = message["frame"];
        if (frame.hasOwnProperty("deviceInfo")) {
          deviceInfo = frame["deviceInfo"];
          this.emit('deviceInfo', deviceInfo);
          this.connected = true;
          this.emit('connect');
        }
      }
      if (!this.connected)
        return ;
      if (message.hasOwnProperty("frame")) {
        messageEvent = new Frame(message.frame);
        this.emit(messageEvent.type, messageEvent);
        if (messageEvent.pose) {
          this.emit("pose", messageEvent.pose);
        }
      }
    };
    BaseConnection.prototype.connect = function() {
      "use strict";
      if (this.socket)
        return ;
      this.emit('ready');
      var inNode = (typeof(process) !== 'undefined' && process.versions && process.versions.node),
          connection = this,
          connectionType;
      if (inNode) {
        connectionType = require("npm:ws@0.7.1");
        this.socket = new connectionType(this.getUrl());
      } else {
        this.socket = new WebSocket(this.getUrl());
      }
      this.socket.onopen = function() {
        connection.handleOpen();
      };
      this.socket.onclose = function(data) {
        connection.handleClose(data['code'], data['reason']);
      };
      this.socket.onmessage = function(message) {
        connection.handleData(message.data);
      };
      this.socket.onerror = function(data) {
        connection.handleClose('connectError', data['data']);
      };
      return true;
    };
    BaseConnection.prototype.send = function(data) {
      "use strict";
      this.socket.send(JSON.stringify(data));
    };
    _.extend(BaseConnection.prototype, EventEmitter.prototype);
  })(require("github:jspm/nodelibs-process@0.1.1"));
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/Hub", ["npm:myojs@0.8.18/src/connection/BaseConnection", "github:jspm/nodelibs-events@0.1.0", "npm:myojs@0.8.18/src/CircularBuffer", "npm:underscore@1.8.3", "npm:myojs@0.8.18/src/connection/BaseConnection", "npm:myojs@0.8.18/src/Myo", "npm:myojs@0.8.18/src/CircularBuffer"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  var BaseConnection = require("npm:myojs@0.8.18/src/connection/BaseConnection").BaseConnection,
      EventEmitter = require("github:jspm/nodelibs-events@0.1.0").EventEmitter,
      CircularBuffer = require("npm:myojs@0.8.18/src/CircularBuffer"),
      _ = require("npm:underscore@1.8.3");
  var Hub = module.exports = function(data, opt) {
    this.connectionType = require("npm:myojs@0.8.18/src/connection/BaseConnection");
    this.myoType = require("npm:myojs@0.8.18/src/Myo");
    this.connection = new this.connectionType(opt);
    this.historyType = require("npm:myojs@0.8.18/src/CircularBuffer");
    this.history = new this.historyType(200);
    this.myos = [];
    this.listeners = [];
    var hub = this;
    this.connection.on("deviceInfo", function(data) {
      hub.myo = new hub.myoType(data, hub.connection);
    });
    this.connection.on("frame", function(frame) {
      hub.history.push(frame);
      hub.emit("frame", frame);
    });
    this.connection.on("pose", function(pose) {
      hub.emit("pose", pose);
    });
    this.connection.on("ready", function() {
      hub.emit("ready");
    });
    this.connection.on("connect", function() {
      hub.emit("connect");
    });
    this.connection.on("disconnect", function() {
      hub.emit("disconnect");
    });
  };
  Hub.prototype.frame = function(num) {
    return this.history.get(num) || null;
  };
  Hub.prototype.waitForMyo = function(timeoutMilliseconds) {
    var myo = this.connection.send({"waitForMyo": timeoutMilliseconds});
    if (myo) {
      myo.context = this.connection;
      this.myos.push(myo);
      return myo;
    }
    return null;
  };
  Hub.prototype.addListener = function(listener) {
    this.listeners.push(listener);
    this.connection.send({"addListener": listener});
  };
  Hub.prototype.removeListener = function(listener) {
    var i = 0;
    for (i; i < this.listeners.length; i++) {
      if (this.listeners[i] == listener) {
        this.listeners.splice(i, 1);
        break;
      }
    }
  };
  Hub.prototype.run = function(durationMilliseconds) {
    this.connection.send({"run": durationMilliseconds});
  };
  Hub.prototype.runOnce = function(durationMilliseconds) {
    this.connection.send({"runOnce": durationMilliseconds});
  };
  _.extend(Hub.prototype, EventEmitter.prototype);
  global.define = __define;
  return module.exports;
});



System.register("npm:myojs@0.8.18/src/Index", ["npm:myojs@0.8.18/src/Hub", "npm:myojs@0.8.18/src/Myo", "npm:myojs@0.8.18/src/CircularBuffer", "npm:myojs@0.8.18/src/Pose", "npm:myojs@0.8.18/src/Quaternion", "npm:myojs@0.8.18/src/Vector3", "npm:myojs@0.8.18/src/Frame", "npm:myojs@0.8.18/src/Version"], true, function(require, exports, module) {
  var global = System.global,
      __define = global.define;
  global.define = undefined;
  module.exports = {
    Hub: require("npm:myojs@0.8.18/src/Hub"),
    Myo: require("npm:myojs@0.8.18/src/Myo"),
    CircularBuffer: require("npm:myojs@0.8.18/src/CircularBuffer"),
    Pose: require("npm:myojs@0.8.18/src/Pose"),
    Quaternion: require("npm:myojs@0.8.18/src/Quaternion"),
    Vector3: require("npm:myojs@0.8.18/src/Vector3"),
    Frame: require("npm:myojs@0.8.18/src/Frame"),
    Version: require("npm:myojs@0.8.18/src/Version")
  };
  global.define = __define;
  return module.exports;
});



System.register("src/test", ["npm:myojs@0.8.18/src/Index", "npm:famous@0.3.5/core/Engine", "npm:famous@0.3.5/core/Surface", "npm:famous@0.3.5/core/Modifier", "npm:famous@0.3.5/surfaces/ContainerSurface", "npm:famous@0.3.5/views/Scrollview", "src/components/demo/CubeSurface"], function($__export) {
  "use strict";
  var __moduleName = "src/test";
  var Myo,
      Engine,
      Surface,
      Modifier,
      ContainerSurface,
      Scrollview,
      CubeSurface;
  return {
    setters: [function($__m) {
      Myo = $__m.default;
    }, function($__m) {
      Engine = $__m.default;
    }, function($__m) {
      Surface = $__m.default;
    }, function($__m) {
      Modifier = $__m.default;
    }, function($__m) {
      ContainerSurface = $__m.default;
    }, function($__m) {
      Scrollview = $__m.default;
    }, function($__m) {
      CubeSurface = $__m.default;
    }],
    execute: function() {
      $__export('default', (function() {
        var Test = function Test() {
          console.log('test3');
          console.log('connectMyo2');
          var hub = new Myo.Hub();
          var context = Engine.createContext();
          var cube = new CubeSurface(context, {size: [150, 150]});
        };
        return ($traceurRuntime.createClass)(Test, {}, {});
      }()));
    }
  };
});



System.register("src/main", ["src/test"], function($__export) {
  "use strict";
  var __moduleName = "src/main";
  var Test;
  return {
    setters: [function($__m) {
      Test = $__m.default;
    }],
    execute: function() {
      new Test();
    }
  };
});



});
//# sourceMappingURL=bundle.js.map